//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package ar.com.project.web.security;

import ar.com.project.core.domain.Role;
import ar.com.project.core.domain.User;
import ar.com.project.core.mail.SimpleEmailManager;
import ar.com.project.core.security.PasswordManager;
import ar.com.project.core.service.UserService;
import ar.com.project.core.service.WebService;
import ar.com.project.core.worker.Constants;
import com.curcico.jproject.core.daos.ConditionEntry;
import com.curcico.jproject.core.daos.ConditionSimple;
import com.curcico.jproject.core.daos.SearchOption;
import com.curcico.jproject.core.utils.ConditionsUtils;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.transaction.Transactional;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;

@Component
@Qualifier ( "customAuthenticationProvider")
public
class CustomAuthenticationProvider implements AuthenticationProvider {

  Logger logger = Logger.getLogger ( this.getClass ( ) );
  @Autowired
  UserService        userService;
  @Autowired
  SimpleEmailManager emailManager;
  @Autowired
  WebService         webService;

  public
  CustomAuthenticationProvider ( ) {
  }

  public
  Authentication authenticate ( Authentication authentication ) throws AuthenticationException {
    String msg = "";

    try {
      if ( ! UsernamePasswordAuthenticationToken.class.isInstance ( authentication ) ) {
        msg = "Error interno";
        throw new BadCredentialsException ( msg );
      }

      List < ConditionEntry > filters = new ArrayList ( );
      filters.add (
          new ConditionSimple ( "login" , SearchOption.EQUAL , authentication.getPrincipal ( ) ) );
      User usuario = ( User ) this.userService.findEntityByFilters (
          filters ,
          ConditionsUtils.createdFetchs ( new String[] { "roles" } )
                                                                   );
      if ( usuario == null ) {
        msg = "Credenciales invalidas ";
        throw new BadCredentialsException ( msg );
      }

      if ( usuario.isLocked ( ) ) {
        msg = "Su cuenta se encuentra bloqueda. Por favor, intente re-activar su cuenta para "
              + "poder continuar operando en nuestra plataforma.";
        throw new BadCredentialsException ( msg );
      }

      Collection < ? extends GrantedAuthority > authorities = this.userService.getAuthorities (
          usuario.getLogin ( ) );
      org.springframework.security.core.userdetails.User u =
          new org.springframework.security.core.userdetails.User (
              usuario.getUsername ( ) , usuario.getPassword ( ) , authorities );
      if ( usuario != null && ! usuario.isLocked ( ) && PasswordManager.getMD5 (
          authentication.getCredentials ( ).toString ( ) ).equals ( usuario.getPassword ( ) ) ) {
        usuario.setFailedAttempts ( 0 );
        usuario.setLastLogin ( new Timestamp ( Calendar.getInstance ( ).getTimeInMillis ( ) ) );
        WebAuthenticationDetails wd = ( WebAuthenticationDetails ) authentication.getDetails ( );
        usuario.setRemoteAddress ( wd.getRemoteAddress ( ) );
        this.userService.createOrUpdate ( usuario , usuario.getLogin ( ) );
        List < SimpleGrantedAuthority > userAuthorities = new ArrayList ( );
        Iterator                        var10           = authorities.iterator ( );

        while ( var10.hasNext ( ) ) {
          GrantedAuthority auth = ( GrantedAuthority ) var10.next ( );
          userAuthorities.add ( new SimpleGrantedAuthority ( ( ( Role ) auth ).toString ( ) ) );
        }

        return new UsernamePasswordAuthenticationToken (
            u , usuario.getPassword ( ) , userAuthorities );
      }

      if ( usuario.getFailedAttempts ( ) >= Constants.FAILED_LOGIN_ATTEMPTS
           && ! usuario.isLocked ( ) ) {
        this.lockUserForFailedAttempts ( usuario );
        msg = "Usuario bloqueado por intentos fallidos";
        throw new BadCredentialsException ( msg );
      }

      if ( ! usuario.isLocked ( ) ) {
        usuario.setFailedAttempts ( usuario.getFailedAttempts ( ) + 1 );
        this.userService.createOrUpdate ( usuario , "system" );
        msg = "Credenciales invalidas.";
        throw new BadCredentialsException ( msg );
      }
    }
    catch ( Exception var11 ) {
      msg = var11.getMessage ( );
      throw new BadCredentialsException ( msg );
    }

    throw new BadCredentialsException ( msg );
  }

  public
  boolean supports ( Class < ? extends Object > authentication ) {
    return Authentication.class.isAssignableFrom ( authentication );
  }

  @Transactional (
      rollbackOn = { Exception.class }
  )
  private
  boolean lockUserForFailedAttempts ( User u ) throws Exception {
    try {
      u.setLocked ( true );
      this.userService.update ( u , "system" );
      boolean sended = this.userService.resetPassword ( u , false );
      if ( sended ) {
        String receiver = u.getEmailAddress ( );
        if ( receiver == null ) {
          receiver = u.getManagementEmailAddress ( );
        }

        if ( receiver == null ) {
          receiver = u.getManagementEmailAddress ( );
        }

        if ( receiver == null ) {
          return false;
        }
        else {
          this.webService.sendEmail ( receiver , "" ,
                                      "Puerto Digital - Usuario bloqueado por intentos fallidos" ,
                                      "Estimado " + u.getFullName ( )
                                      + " su cuenta ha sido bloqueada ya que hemos detectado "
                                      + "intentos fallidos de inicio de sesión.\n "
                                      + "Por favor, intente re-activar su cuenta para poder "
                                      + "continuar operando en nuestra plataforma.\n\nReactivar "
                                      + "cuenta: https://apps.apmterminals.com"
                                      + ".ar/puertodigital/reactivateAccount\nGracias.\n\nAPM "
                                      + "Terminals Buenos Aires"
                                    );
          return true;
        }
      }
      else {
        throw new Exception ( "No se pudo enviar el email" );
      }
    }
    catch ( Exception var4 ) {
      throw new Exception ( var4.getMessage ( ) );
    }
  }
}
