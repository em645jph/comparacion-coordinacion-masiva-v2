//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.restservice.controllers;

import ar.com.project.core.domain.User;
import ar.com.project.core.service.AgpCoordinationAuditService;
import ar.com.project.core.service.CoordinationService;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping ( { "/AgpAudit" })
@SessionAttributes ( {
    "userid" , "loginName" , "username" , "parameters" , "fullname" , "firstNameLetter"
}
)
public
class AgpAuditController extends CommonController {

  private static final Logger logger = Logger.getLogger ( AgpAuditController.class );
  boolean canAddItem    = true;
  boolean canDeleteItem = true;
  User    user;
  @Autowired
  AgpCoordinationAuditService agpAuditService;
  @Autowired
  CoordinationService         coordinationService;
  private List privilegeInPageList = new ArrayList ( );

  public
  AgpAuditController ( ) {
  }

  @RequestMapping (
      value = { "/agpAudit" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  public
  ModelAndView agpAudit (
      HttpServletRequest request , HttpServletResponse response ,
      Object handler
                        ) {
    this.canAddItem    = true;
    this.canDeleteItem = true;
    String       dataTableHtml = this.getDataTableHtml ( );
    ModelAndView model         = new ModelAndView ( "agpAudit" );
    model.addObject ( "agpAuditDataTable" , dataTableHtml );
    return model;
  }

  @RequestMapping (
      value = { "/loadInitCombos" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > loadInitCombos ( ) {
    List categoryList = this.agpAuditService.getAgpAuditCategoryList ( );
    Map  resultMap    = new HashMap ( );
    resultMap.put ( "categoryList" , categoryList );
    ResponseEntity response = new ResponseEntity ( resultMap , HttpStatus.OK );
    return response;
  }

  @RequestMapping (
      value = { "/findAgpAuditByParam" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < List > findAgpAuditByParam (
      @RequestParam String prUnitId ,
      @RequestParam String prUnitCategory , @RequestParam String prVisitId ,
      @RequestParam String prCreator , @RequestParam String prFromDateStr ,
      @RequestParam String prToDateStr
                                              ) throws SQLException {
    List agpAuditList = this.agpAuditService.findAgpCoordAuditByParams ( prUnitId , prUnitCategory ,
                                                                         ( String ) null ,
                                                                         prVisitId ,
                                                                         ( String ) null ,
                                                                         ( String ) null ,
                                                                         prFromDateStr ,
                                                                         prToDateStr ,
                                                                         prCreator
                                                                       );
    ResponseEntity response = new ResponseEntity ( agpAuditList , HttpStatus.OK );
    return response;
  }

  private
  String getUserLogin ( ) {
    org.springframework.security.core.userdetails.User loggedUser =
        ( org.springframework.security.core.userdetails.User ) SecurityContextHolder
            .getContext ( )
            .getAuthentication ( ).getPrincipal ( );
    String login = loggedUser.getUsername ( );
    return login;
  }

  private
  String getDataTableHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    sb.append ( "<thead>\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <th>Contenedor</th>\n" );
    sb.append ( "    <th>Categoría</th>\n" );
    sb.append ( "    <th>Documento</th>\n" );
    sb.append ( "    <th>Id Visita</th>\n" );
    sb.append ( "    <th>Fecha</th>\n" );
    sb.append ( "    <th>Turno seleccionado</th>\n" );
    sb.append ( "    <th>Notificado</th>\n" );
    sb.append ( "    <th>Turnos</th>\n" );
    sb.append ( "    <th>Servicios</th>\n" );
    sb.append ( "    <th>Cliente</th>\n" );
    sb.append ( "    <th>Fecha creación</th>\n" );
    sb.append ( "    <th>Calendario</th>\n" );
    sb.append ( "</tr>\n" );
    sb.append ( "</thead>\n" );
    sb.append ( "<tbody data-bind=\"foreach: agpAuditViewModel.agpAuditTable\">\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitId\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitCategory\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: documentNbr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: visitId\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: limitDateStr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: selectedDateTimeStr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: emailSentStr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: openingCount\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: serviceOrderCount\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: creator\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: createdStr\"></label></td>\n" );
    sb.append (
        "    <td><button class=\"btn btn-icon btn-primary btn-sm\" data-bind=\"click: $parent"
        + ".viewUnitCalendar\"> <i class=\"ti-calendar\"></i> </button></td>\n" );
    sb.append ( "</tr>\n" );
    return sb.toString ( );
  }

  private
  Map buildSuccessResponse ( String msg ) {
    return this.buildResponse ( "OK" , msg );
  }

  private
  Map buildErrorResponse ( String msg ) {
    return this.buildResponse ( "ERROR" , msg );
  }

  private
  Map buildQuestionResponse ( String msg ) {
    return this.buildResponse ( "QUESTION" , msg );
  }

  private
  Map buildWarningResponse ( String msg ) {
    return this.buildResponse ( "WARNING" , msg );
  }

  private
  Map buildResponse ( String status , String msg ) {
    Map map = new HashMap ( );
    map.put ( "status" , status );
    map.put ( "msg" , msg );
    return map;
  }
}
