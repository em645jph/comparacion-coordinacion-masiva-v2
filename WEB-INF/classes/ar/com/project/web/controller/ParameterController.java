//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.restservice.controllers;

import ar.com.project.core.domain.Parameter;
import ar.com.project.core.service.ParameterService;
import com.curcico.jproject.core.daos.ConditionSimple;
import com.curcico.jproject.core.daos.SearchOption;
import com.curcico.jproject.core.exception.BaseException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.codehaus.plexus.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@SessionAttributes ( { "userid" , "parameters" , "costCenterSwitchery" })
@RequestMapping ( { "/Parameter" })
public
class ParameterController extends CommonController {

  private static final Logger logger = Logger.getLogger ( ParameterController.class );
  @Autowired
  ParameterService parameterService;

  public
  ParameterController ( ) {
  }

  @RequestMapping (
      value = { "/systemParameters" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  public
  ModelAndView systemParameters ( @ModelAttribute ( "userid") String userid , ModelMap modelMap )
  throws BaseException {
    ModelAndView             model             = new ModelAndView ( "systemParameters" );
    List < ConditionSimple > parametersFilters = new ArrayList ( );
    parametersFilters.add ( new ConditionSimple ( "id" , SearchOption.EQUAL , "COST_CENTER" ) );
    Parameter param = ( Parameter ) this.parameterService.findEntityByFilters ( parametersFilters );
    if ( param == null ) {
      return new ModelAndView ( "500" );
    }
    else {
      String checked = "checked";
      if ( param.getValue ( ).equalsIgnoreCase ( "12" ) ) {
        checked = "";
      }

      String ccSwitchery =
          "<input type='checkbox' data-plugin='switchery'  id='costCenterSwitch' " + checked + " \n"
          + " data-secondary-color='#3cb6ce' data-color='#ff6319' data-size='large' "
          + "data-switchery='true' style='display: none;'>";
      model.addObject ( "costCenterSwitchery" , ccSwitchery );
      return model;
    }
  }

  @RequestMapping (
      value = { "/changeCostCenter/{value}" },
      method = { RequestMethod.POST }
  )
  @PreAuthorize ( "isAuthenticated()")
  public
  ResponseEntity < Object > changeCostCenter ( @PathVariable String value ) throws BaseException {
    List < ConditionSimple > parametersFilters = new ArrayList ( );
    parametersFilters.add ( new ConditionSimple ( "id" , SearchOption.EQUAL , "COST_CENTER" ) );
    Parameter param = ( Parameter ) this.parameterService.findEntityByFilters ( parametersFilters );
    if ( param == null ) {
      return new ResponseEntity (
          "Error al momento de búsqueda. Reintente nuevamente." ,
          HttpStatus.BAD_REQUEST
      );
    }
    else {
      String costCenter;
      if ( StringUtils.isNumeric ( value ) && Integer.parseInt ( value ) == 1 ) {
        costCenter = "14";
      }
      else {
        if ( ! StringUtils.isNumeric ( value ) || Integer.parseInt ( value ) != 2 ) {
          return new ResponseEntity (
              "No se puede aplicar la actualización. Valor de Centro de Costo desconocido." ,
              HttpStatus.BAD_REQUEST
          );
        }

        costCenter = "12";
      }

      param.setValue ( costCenter );
      param = ( Parameter ) this.parameterService.createOrUpdate (
          param ,
          this.getUserContext ( ).getUsername ( )
                                                                 );
      return param == null ? new ResponseEntity (
          "Error al momento de actualización. Reintente nuevamente." , HttpStatus.BAD_REQUEST )
                           : new ResponseEntity (
                               "Centro de Costo actualizado correctamente." , HttpStatus.OK );
    }
  }
}
