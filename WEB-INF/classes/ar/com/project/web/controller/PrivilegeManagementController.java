//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.restservice.controllers;

import ar.com.project.core.domain.Privilege;
import ar.com.project.core.domain.User;
import ar.com.project.core.service.MenuService;
import ar.com.project.core.service.PrivilegeService;
import com.curcico.jproject.core.exception.BaseException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping ( { "/PrivilegeManagement" })
@SessionAttributes ( {
    "userid" , "loginName" , "username" , "parameters" , "fullname" , "firstNameLetter"
}
)
public
class PrivilegeManagementController extends CommonController {

  private static final Logger logger = Logger.getLogger ( PrivilegeManagementController.class );
  User user;
  @Autowired
  MenuService      menuService;
  @Autowired
  PrivilegeService privilegeService;

  public
  PrivilegeManagementController ( ) {
  }

  @RequestMapping (
      value = { "/privilegeManagement" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  public
  ModelAndView privilegeManagement (
      HttpServletRequest request , HttpServletResponse response ,
      Object handler
                                   ) {
    ModelAndView model       = new ModelAndView ( "privilegeManagement" );
    String       servletPath = request.getServletPath ( );
    org.springframework.security.core.userdetails.User loggedUser =
        ( org.springframework.security.core.userdetails.User ) SecurityContextHolder
            .getContext ( )
            .getAuthentication ( ).getPrincipal ( );
    String  login          = loggedUser.getUsername ( );
    List    privilegeList  = this.privilegeService.getMenuPagePrivileges ( login , servletPath );
    boolean canAddItem     = this.privilegeService.userCanAddItem ( privilegeList );
    boolean canEditItem    = this.privilegeService.userCanEditItem ( privilegeList );
    boolean canDeleteItem  = this.privilegeService.userCanDeleteItem ( privilegeList );
    boolean canRecoverItem = this.privilegeService.userCanRecoverItem ( privilegeList );
    String actionButtonsHtml = this.getActionButtonsHtml (
        canAddItem , canDeleteItem , canRecoverItem );
    String dataTableHtml = this.getDataTableHtml (
        canEditItem , canDeleteItem , canRecoverItem );
    model.addObject ( "actionButtons" , actionButtonsHtml );
    model.addObject ( "dataTable" , dataTableHtml );
    model.addObject ( "canAddItem" , canAddItem );
    model.addObject ( "canEditItem" , canEditItem );
    model.addObject ( "canDeleteItem" , canDeleteItem );
    model.addObject ( "canRecoverItem" , canRecoverItem );
    model.addObject ( "canRecoverItem" , canRecoverItem );
    return model;
  }

  @RequestMapping (
      value = { "/getMenuList" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < List > getMenuList ( ) {
    ResponseEntity response;
    try {
      List menuList = this.menuService.getActiveMenuList ( );
      response = new ResponseEntity ( menuList , HttpStatus.OK );
    }
    catch ( BaseException var3 ) {
      var3.printStackTrace ( );
      response = new ResponseEntity ( ( MultiValueMap ) null , HttpStatus.BAD_REQUEST );
    }

    return response;
  }

  @RequestMapping (
      value = { "/getAccessTypeList" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < List > getAccessTypeList ( ) throws SQLException {
    List accessTypeList = this.privilegeService.getAccessTypeList ( );
    return new ResponseEntity ( accessTypeList , HttpStatus.OK );
  }

  @RequestMapping (
      value = { "/findPrivilegeByParam" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < List > findPrivilegeByParam (
      @RequestParam String prLabel ,
      @RequestParam String prMenuGkeyStr , @RequestParam String prAccessType ,
      @RequestParam String prFromDateStr , @RequestParam String prToDateStr ,
      @RequestParam String prLifeCycleState
                                               ) {
    List menuList = this.privilegeService.findPrivilegeByParams ( prLabel , prMenuGkeyStr ,
                                                                  prAccessType , prFromDateStr ,
                                                                  prToDateStr , prLifeCycleState
                                                                );
    ResponseEntity response = new ResponseEntity ( menuList , HttpStatus.OK );
    return response;
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/addOrUpdatePrivilege" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > addOrUpdatePrivilege ( @RequestBody Privilege updatedPrivilege )
  throws BaseException, ParseException {
    String userLogin = this.getUserLogin ( );
    String result =
        updatedPrivilege.getGkey ( ) != null ? this.privilegeService.updatePrivilege (
            updatedPrivilege ,
            userLogin
                                                                                     )
                                             : this.privilegeService.addPrivilege (
                                                 updatedPrivilege , userLogin );
    return ! result.isEmpty ( ) ? new ResponseEntity (
        this.buildErrorResponse ( result ) , HttpStatus.OK )
                                : new ResponseEntity (
                                    this.buildSuccessResponse ( "Privilegio guardado con éxito" ) ,
                                    HttpStatus.OK
                                );
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/getPrivilege" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getPrivilege ( @RequestParam Integer prPrivilegeGkey )
  throws BaseException, ParseException {
    Privilege privilege = this.privilegeService.getPrivilegeByGkey ( prPrivilegeGkey );
    return privilege != null ? new ResponseEntity ( privilege , HttpStatus.OK )
                             : new ResponseEntity (
                                 "No se pudo cargar la información del privilegio. Por favor "
                                 + "refresque la página" ,
                                 HttpStatus.BAD_REQUEST
                             );
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/deletePrivilege" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < String > deletePrivilege ( @RequestBody Privilege privilege )
  throws BaseException, ParseException {
    this.privilegeService.deletePrivilege ( privilege , this.getUserLogin ( ) );
    return new ResponseEntity ( "Privilegio eliminado con éxito" , HttpStatus.OK );
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/recoverPrivilege" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < String > recoverPrivilege ( @RequestBody Privilege privilege )
  throws BaseException, ParseException {
    this.privilegeService.recoverPrivilege ( privilege , this.getUserLogin ( ) );
    return new ResponseEntity ( "Privilegio activado con éxito" , HttpStatus.OK );
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/deleteSelectedPrivilege" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < String > deleteSelectedPrivilege ( @RequestBody Long[] prPrivilegeGkeyArray )
  throws BaseException, ParseException {
    logger.info ( "step 1" );
    this.privilegeService.deletePrivilegeByGkey ( prPrivilegeGkeyArray , this.getUserLogin ( ) );
    return new ResponseEntity ( "Privilegios eliminados con éxito" , HttpStatus.OK );
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/recoverSelectedPrivilege" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < String > recoverSelectedPrivilege ( @RequestBody Long[] prPrivilegeGkeyArray )
  throws BaseException, ParseException {
    logger.info ( "step 1" );
    this.privilegeService.recoverPrivilegeByGkey ( prPrivilegeGkeyArray , this.getUserLogin ( ) );
    return new ResponseEntity ( "Privilegios activados con éxito" , HttpStatus.OK );
  }

  private
  String getUserLogin ( ) {
    org.springframework.security.core.userdetails.User loggedUser =
        ( org.springframework.security.core.userdetails.User ) SecurityContextHolder
            .getContext ( )
            .getAuthentication ( ).getPrincipal ( );
    String login = loggedUser.getUsername ( );
    return login;
  }

  private
  String getActionButtonsHtml (
      boolean canAddItem , boolean canDeleteItem ,
      boolean canRecoverItem
                              ) {
    StringBuilder sb = new StringBuilder ( );
    if ( canAddItem ) {
      sb.append (
          "<button type=\"button\" class=\"btn btn-primary\" "
          + "id=\"btnAddPrivilege\">Agregar</button>&nbsp;&nbsp;" );
    }

    if ( canDeleteItem ) {
      if ( sb.length ( ) > 0 ) {
        sb.append ( "\n" );
      }

      sb.append (
          "<button type=\"button\" class=\"btn btn-danger\" "
          + "id=\"btnDeleteSelectedPrivilege\">Eliminar</button>&nbsp;&nbsp;" );
    }

    if ( canRecoverItem ) {
      if ( sb.length ( ) > 0 ) {
        sb.append ( "\n" );
      }

      sb.append (
          "<button type=\"button\" class=\"btn btn-warning\" "
          + "id=\"btnRecoverSelectedPrivilege\">Activar</button>" );
    }

    return sb.toString ( );
  }

  private
  String getDataTableHtml (
      boolean canEditItem , boolean canDeleteItem ,
      boolean canRecoverItem
                          ) {
    StringBuilder sb = new StringBuilder ( );
    sb.append ( "<thead>\n" );
    sb.append ( "<tr>\n" );
    if ( canDeleteItem || canRecoverItem ) {
      sb.append ( "    <th> <input type=\"checkbox\" id=\"ckCheckAll\"></th>\n" );
    }

    sb.append ( "    <th>Id</th>\n" );
    sb.append ( "    <th>Nombre</th>\n" );
    sb.append ( "    <th>Descripción</th>\n" );
    sb.append ( "    <th>Acceso</th>\n" );
    sb.append ( "    <th>Menú</th>\n" );
    sb.append ( "    <th>Creado por</th>\n" );
    sb.append ( "    <th>Fecha Creación</th>\n" );
    sb.append ( "    <th>Act. por</th>\n" );
    sb.append ( "    <th>Fecha Act.</th>\n" );
    sb.append ( "    <th>Estado</th>\t\n" );
    if ( canEditItem ) {
      sb.append ( "    <th></th>\n" );
    }

    if ( canDeleteItem ) {
      sb.append ( "    <th></th>\n" );
    }

    if ( canRecoverItem ) {
      sb.append ( "    <th></th>\n" );
    }

    sb.append ( "</tr>\n" );
    sb.append ( "</thead>\n" );
    sb.append ( "<tbody data-bind=\"foreach: privilegeViewModel.createdPrivilege\">\n" );
    sb.append ( "<tr>\n" );
    if ( canDeleteItem || canRecoverItem ) {
      sb.append (
          "    <td><input id=\"ckCheckOne\" class=\"selectable\" type=\"checkbox\" "
          + "data-bind=\"attr: {gkey:gkey}\"></td>\n" );
    }

    sb.append ( "    <td><label data-bind=\"text: id\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: label\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: description\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: accessTypeDesc\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: menuLabel\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: creator\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: createdStr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: changer\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: changedStr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: lifeCycleState\"></label></td>\n" );
    if ( canEditItem ) {
      sb.append (
          "    <td><button class=\"btn btn-icon btn-info btn-sm\" data-bind=\"click: $parent"
          + ".editPrivilege, enable: isActive\"> <i class=\"ti-pencil-alt\"></i> "
          + "</button></td>\n" );
    }

    if ( canDeleteItem ) {
      sb.append (
          "    <td><button class=\"btn btn-icon btn-danger btn-sm\" data-bind=\"click: $parent"
          + ".deletePrivilege, enable: isActive\"> <i class=\"ti-close\"></i> </button></td>\n" );
    }

    if ( canRecoverItem ) {
      sb.append (
          "    <td><button class=\"btn btn-icon btn-warning btn-sm\" data-bind=\"click: $parent"
          + ".recoverPrivilege, enable: !isActive\"> <i class=\"ti-reload\"></i> "
          + "</button></td>\n" );
    }

    sb.append ( "</tr>\n" );
    return sb.toString ( );
  }

  private
  Map buildSuccessResponse ( String msg ) {
    return this.buildResponse ( "OK" , msg );
  }

  private
  Map buildErrorResponse ( String msg ) {
    return this.buildResponse ( "ERROR" , msg );
  }

  private
  Map buildResponse ( String status , String msg ) {
    Map map = new HashMap ( );
    map.put ( "status" , status );
    map.put ( "msg" , msg );
    return map;
  }
}
