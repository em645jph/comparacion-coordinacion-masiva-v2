//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.restservice.controllers;

import ar.com.project.core.domain.User;
import ar.com.project.core.service.CustomsEarlyLockService;
import ar.com.project.core.service.PrivilegeService;
import ar.com.project.core.utils.DateUtils;
import ar.com.project.core.utils.StringUtils;
import com.curcico.jproject.core.exception.BaseException;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping ( { "/CustomsEarlyLock" })
@SessionAttributes ( {
    "userid" , "loginName" , "username" , "parameters" , "fullname" , "firstNameLetter"
}
)
public
class CustomsEarlyLockController extends CommonController {

  private static final Logger logger = Logger.getLogger ( CustomsEarlyLockController.class );
  boolean canAddItem     = false;
  boolean canEditItem    = false;
  boolean canDeleteItem  = false;
  boolean canRecoverItem = false;
  User    user;
  @Autowired
  PrivilegeService        privilegeService;
  @Autowired
  CustomsEarlyLockService earlyLockService;

  public
  CustomsEarlyLockController ( ) {
  }

  @RequestMapping (
      value = { "/earlyLock" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  public
  ModelAndView earlyLock (
      HttpServletRequest request , HttpServletResponse response ,
      Object handler
                         ) {
    ModelAndView model = new ModelAndView ( "earlyLock" );
    return model;
  }

  @RequestMapping (
      value = { "/getActiveEarlyLock" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < List > getActiveEarlyLock ( ) throws SQLException {
    return new ResponseEntity (
        this.earlyLockService.getActiveCustomsEarlyLock ( ) , HttpStatus.OK );
  }

  @RequestMapping (
      value = { "/findEarlyLock" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > findEarlyLock (
      @RequestParam String prReferenceId ,
      @RequestParam String prReportedFrom , @RequestParam String prReportedTo
                                          ) throws SQLException {
    String validateFromDate = this.validateDateFormat ( prReportedFrom );
    if ( ! StringUtils.isNullOrEmpty ( validateFromDate ) ) {
      return new ResponseEntity (
          this.buildErrorResponse ( validateFromDate + " para el campo Reportado desde" ) ,
          HttpStatus.OK
      );
    }
    else {
      String validateToDate = this.validateDateFormat ( prReportedTo );
      return ! StringUtils.isNullOrEmpty ( validateToDate ) ? new ResponseEntity (
          this.buildErrorResponse ( validateToDate ) + " para el campo Reportado hasta" ,
          HttpStatus.OK
      )
                                                            : new ResponseEntity (
                                                                this.earlyLockService.findCustomsEarlyLockByParam (
                                                                    prReferenceId , prReportedFrom ,
                                                                    prReportedTo
                                                                                                                  ) ,
                                                                HttpStatus.OK
                                                            );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/addEarlyLock" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > addEarlyLock (
      HttpServletRequest request ,
      @RequestBody Map earlyLockMap
                                         ) throws BaseException, ParseException, SQLException {
    String validateActionMsg = this.validateUserCanAdd ( request );
    if ( ! StringUtils.isNullOrEmpty ( validateActionMsg ) ) {
      return new ResponseEntity ( this.buildErrorResponse ( validateActionMsg ) , HttpStatus.OK );
    }
    else {
      String referenceId = ( String ) earlyLockMap.get ( "referenceId" );
      String notes       = ( String ) earlyLockMap.get ( "notes" );
      String reportedStr = ( String ) earlyLockMap.get ( "reportedStr" );
      referenceId = StringUtils.trimText ( referenceId );
      if ( referenceId != null && ! referenceId.isEmpty ( ) ) {
        if ( referenceId.length ( ) != 16 ) {
          return new ResponseEntity (
              this.buildErrorResponse ( "Ingresá un permiso de embarque válido" ) , HttpStatus.OK );
        }
        else {
          String validateReportedDate = this.validateDateFormat ( reportedStr );
          if ( ! StringUtils.isNullOrEmpty ( validateReportedDate ) ) {
            return new ResponseEntity (
                this.buildErrorResponse ( validateReportedDate )
                + " para el campo Fecha de Reporte" ,
                HttpStatus.OK
            );
          }
          else {
            String userLogin = this.getUserLogin ( );
            Date   reported  = DateUtils.parseDateNoTime ( reportedStr );
            String result = this.earlyLockService.addCustomsEarlyLock ( referenceId , notes ,
                                                                        reported ,
                                                                        userLogin
                                                                      );
            if ( ! result.isEmpty ( ) ) {
              return new ResponseEntity ( this.buildErrorResponse ( result ) , HttpStatus.OK );
            }
            else {
              List   unitIdList = this.earlyLockService.findUnitIdByCustomsPrm ( referenceId );
              String successMsg = "Bloqueo anticipado guardado con éxito";
              if ( unitIdList != null && ! unitIdList.isEmpty ( ) ) {
                successMsg = successMsg + "<br><br>El permiso tiene " + unitIdList.size ( )
                             + " contenedores ingresados en la terminal. <br><br> Revisá los "
                             + "registros "
                             + unitIdList;
              }

              return new ResponseEntity (
                  this.buildSuccessResponse ( successMsg ) , HttpStatus.OK );
            }
          }
        }
      }
      else {
        return new ResponseEntity (
            this.buildErrorResponse ( "Ingresá el permiso de embarque" ) ,
            HttpStatus.OK
        );
      }
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/deleteEarlyLock" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > deleteEarlyLock (
      HttpServletRequest request ,
      @RequestBody Map earlyLockMap
                                            ) throws BaseException, ParseException, SQLException {
    String validateActionMsg = this.validateUserCanDelete ( request );
    if ( ! StringUtils.isNullOrEmpty ( validateActionMsg ) ) {
      return new ResponseEntity ( this.buildErrorResponse ( validateActionMsg ) , HttpStatus.OK );
    }
    else {
      Integer gkey = ( Integer ) earlyLockMap.get ( "gkey" );
      String result = this.earlyLockService.deleteCustomsEarlyLock (
          gkey , this.getUserLogin ( ) );
      if ( ! result.isEmpty ( ) ) {
        return new ResponseEntity ( this.buildErrorResponse ( result ) , HttpStatus.OK );
      }
      else {
        String referenceId = ( String ) earlyLockMap.get ( "reference_id" );
        List   unitIdList  = this.earlyLockService.findLockedUnitIdByCustomsPrm ( referenceId );
        String successMsg  = "Bloqueo anticipado eliminado con éxito";
        if ( unitIdList != null && ! unitIdList.isEmpty ( ) ) {
          successMsg = successMsg + "<br><br>El permiso tiene " + unitIdList.size ( )
                       + " contenedores con bloqueo anticipado. <br><br>Revisá los registros "
                       + unitIdList;
        }

        return new ResponseEntity ( this.buildSuccessResponse ( successMsg ) , HttpStatus.OK );
      }
    }
  }

  private
  String getUserLogin ( ) {
    org.springframework.security.core.userdetails.User loggedUser =
        ( org.springframework.security.core.userdetails.User ) SecurityContextHolder
            .getContext ( )
            .getAuthentication ( ).getPrincipal ( );
    String login = loggedUser.getUsername ( );
    return login;
  }

  private
  String getServletPath ( HttpServletRequest request ) {
    return request.getServletPath ( );
  }

  private
  String validateUserCanAdd ( HttpServletRequest request ) {
    String login       = this.getUserLogin ( );
    String servletPath = this.getServletPath ( request );
    List privilegeInPageList = this.privilegeService.getMenuPagePrivileges (
        login , servletPath );
    return this.privilegeService.userCanAddItem ( privilegeInPageList ) ? ""
                                                                        : "No tenés permisos para"
                                                                          + " agregar bloqueos "
                                                                          + "anticipados";
  }

  private
  String validateUserCanDelete ( HttpServletRequest request ) {
    String login       = this.getUserLogin ( );
    String servletPath = this.getServletPath ( request );
    List privilegeInPageList = this.privilegeService.getMenuPagePrivileges (
        login , servletPath );
    return this.privilegeService.userCanDeleteItem ( privilegeInPageList ) ? ""
                                                                           : "No tenés permisos "
                                                                             + "para eliminar "
                                                                             + "bloqueos "
                                                                             + "anticipados";
  }

  private
  String validateDateFormat ( String dateStr ) {
    return ! StringUtils.isNullOrEmpty ( dateStr ) && DateUtils.parseDateNoTime ( dateStr ) == null
           ? "Ingresá una fecha válida en formato YYYY-MM-DD " : "";
  }

  private
  Map buildSuccessResponse ( String msg ) {
    return this.buildResponse ( "OK" , msg );
  }

  private
  Map buildErrorResponse ( String msg ) {
    return this.buildResponse ( "ERROR" , msg );
  }

  private
  Map buildResponse ( String status , String msg ) {
    Map map = new HashMap ( );
    map.put ( "status" , status );
    map.put ( "msg" , msg );
    return map;
  }

  private
  boolean isNullOrEmpty ( String str ) {
    return str == null || str.isEmpty ( );
  }
}
