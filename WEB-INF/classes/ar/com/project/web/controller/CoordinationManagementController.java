﻿//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.restservice.controllers;

import ar.com.project.core.domain.User;
import ar.com.project.core.dto.AppointmentOpeningDto;
import ar.com.project.core.dto.BookingItemDTO;
import ar.com.project.core.dto.DocumentApptDTO;
import ar.com.project.core.service.AgpCoordinationAuditService;
import ar.com.project.core.service.AppointmentService;
import ar.com.project.core.service.AuditService;
import ar.com.project.core.service.CoordinationService;
import ar.com.project.core.service.LineOpService;
import ar.com.project.core.service.PrivilegeService;
import ar.com.project.core.service.ScheduleRuleService;
import ar.com.project.core.service.SrvOrderService;
import ar.com.project.core.service.UnitService;
import ar.com.project.core.service.VipApptService;
import ar.com.project.core.utils.DateUtils;
import ar.com.project.core.utils.EnumUtil;
import ar.com.project.core.utils.StringUtils;
import ar.com.project.core.worker.ApptCustomsChannelEnum;
import ar.com.project.core.worker.ApptTypeEnum;
import ar.com.project.core.worker.UnitCategoryEnum;
import com.curcico.jproject.core.exception.BaseException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.NumberUtils;
import org.apache.log4j.Logger;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping ( { "/CoordinationManagement" })
@SessionAttributes ( {
    "userid" , "loginName" , "username" , "parameters" , "fullname" , "firstNameLetter"
}
)
public
class CoordinationManagementController extends CommonController {

  private static final Logger logger = Logger.getLogger (
      CoordinationManagementController.class );
  boolean canAddItem     = true;
  boolean canEditItem    = true;
  boolean canDeleteItem  = true;
  boolean canRecoverItem = false;
  User    user;
  @Autowired
  AppointmentService          apptService;
  @Autowired
  LineOpService               lineOpService;
  @Autowired
  ScheduleRuleService         ruleService;
  @Autowired
  SrvOrderService             soService;
  @Autowired
  PrivilegeService            privilegeService;
  @Autowired
  CoordinationService         coordinationService;
  @Autowired
  AuditService                auditService;
  @Autowired
  AgpCoordinationAuditService agpCoordAuditService;
  @Autowired
  UnitService                 unitService;
  @Autowired
  VipApptService              vipApptService;
  private List privilegeInPageList = new ArrayList ( );

  public
  CoordinationManagementController ( ) {
  }

  @RequestMapping (
      value = { "/coordinationManagement" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  public
  ModelAndView coordinationManagement (
      HttpServletRequest request ,
      HttpServletResponse response , Object handler
                                      ) {
    ModelAndView model       = new ModelAndView ( "coordinationManagement" );
    String       servletPath = request.getServletPath ( );
    org.springframework.security.core.userdetails.User loggedUser =
        ( org.springframework.security.core.userdetails.User ) SecurityContextHolder
            .getContext ( )
            .getAuthentication ( ).getPrincipal ( );
    String login                     = loggedUser.getUsername ( );
    String dataTableHtml             = this.getDataTableHtml ( );
    String serviceOrderDataTableHtml = this.getServiceOrderDataTableHtml ( );
    model.addObject ( "dataTable" , dataTableHtml );
    model.addObject ( "serviceOrerDataTable" , serviceOrderDataTableHtml );
    return model;
  }

  @RequestMapping (
      value = { "/loadInitCombos" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < List > loadInitCombos ( ) {
    ResponseEntity response;
    try {
      List categoryList = this.coordinationService.getCoordinationCategoryList ( );
      List lineOpList   = this.lineOpService.getAllLineOp ( );
      Map  resultMap    = new HashMap ( );
      resultMap.put ( "categoryList" , categoryList );
      resultMap.put ( "lineOpList" , lineOpList );
      response = new ResponseEntity ( resultMap , HttpStatus.OK );
    }
    catch ( SQLException var5 ) {
      response = new ResponseEntity (
          "No se pudo cargar correctamente la información. Por favor refresque la página" ,
          HttpStatus.BAD_REQUEST
      );
      var5.printStackTrace ( );
    }

    return response;
  }

  @RequestMapping (
      value = { "/findUnit" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > findUnit (
      @RequestParam String prUnitCategory ,
      @RequestParam String prKeyParameter , @RequestParam String prLineOpId
                                     )
  throws SQLException, ServiceException, IOException, InterruptedException {
    prKeyParameter = prKeyParameter != null ? prKeyParameter.toUpperCase ( ) : prKeyParameter;
    if ( prUnitCategory != null && ! prUnitCategory.isEmpty ( ) ) {
      if ( UnitCategoryEnum.STRGE.getKey ( ).equals ( prUnitCategory ) ) {
        Map    resultMap = this.coordinationService.findStorageUnit ( prKeyParameter );
        String errorMsg  = ( String ) resultMap.get ( "errorMsg" );
        if ( errorMsg != null && ! errorMsg.isEmpty ( ) ) {
          return new ResponseEntity ( this.buildErrorResponse ( errorMsg ) , HttpStatus.OK );
        }
        else {
          List storageUnitList = ( List ) resultMap.get ( "unitList" );
          if ( storageUnitList != null ) {
            return new ResponseEntity ( storageUnitList , HttpStatus.OK );
          }
          else {
            return ! this.isValidUnitIdFormat ( prKeyParameter ) ? new ResponseEntity (
                this.buildErrorResponse (
                    "El número de contenedor debe estar compuesto por 4 letras y 7 números" ) ,
                HttpStatus.OK
            ) : new ResponseEntity ( resultMap , HttpStatus.OK );
          }
        }
      }
      else {
        List documentApptList = this.coordinationService.findDocument ( prUnitCategory ,
                                                                        prKeyParameter , prLineOpId
                                                                      );
        boolean isExport    = UnitCategoryEnum.EXPRT.getKey ( ).equals ( prUnitCategory );
        boolean isOrderAppt = "APPT".equals ( prUnitCategory );
        if ( documentApptList != null && ! documentApptList.isEmpty ( ) ) {
          if ( ( isExport || isOrderAppt ) && documentApptList.size ( ) > 1 && (
              prLineOpId == null
              || prLineOpId.isEmpty ( )
          ) ) {
            return new ResponseEntity (
                this.buildErrorResponse (
                    "Se encontró más de un resultado para el booking ingresado. Por favor indique"
                    + " la Línea Operadora" ) ,
                HttpStatus.OK
            );
          }
          else {
            DocumentApptDTO documentApptDto = ( DocumentApptDTO ) documentApptList.get ( 0 );
            if ( isOrderAppt && this.coordinationService.orderHasReservedEquipment (
                documentApptDto.getGkey ( ) , documentApptDto.getNbr ( ) ) ) {
              return new ResponseEntity (
                  this.buildErrorResponse (
                      "La reserva que estás intentando buscar posee contenedores digitados por lo"
                      + " que la coordinación deberá ser realizada por nuestro Departamento de "
                      + "Atención al Cliente.<br><br>Generá una <a style=\"color:#fd7e14;\" "
                      + "href=\"https://apps.apmterminals.com.ar/solicitudesonline\" "
                      + "target=\"_blank\">Solicitud Online</a> para consultar por esta "
                      + "coordinación." ) ,
                  HttpStatus.OK
              );
            }
            else {
              List unitList = this.coordinationService.findUnit ( ( Long ) null , prUnitCategory ,
                                                                  documentApptDto
                                                                );
              Long documentGkey = documentApptDto.getGkey ( );
              if ( isOrderAppt ) {
                Map result = new HashMap ( );
                result.put ( "unitList" , unitList );
                result.put ( "documentGkey" , documentGkey );
                return new ResponseEntity ( result , HttpStatus.OK );
              }
              else {
                return new ResponseEntity ( unitList , HttpStatus.OK );
              }
            }
          }
        }
        else {
          return new ResponseEntity ( this.buildErrorResponse (
              "No se encontró el " + ( ! isExport && ! isOrderAppt ? "contenedor" : "booking" )
              + " ingresado" ) , HttpStatus.OK );
        }
      }
    }
    else {
      return new ResponseEntity (
          this.buildErrorResponse ( "Por favor seleccione una categoría de contenedor" ) ,
          HttpStatus.OK
      );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/viewUnitApptSeals" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > viewUnitApptSeals ( @RequestBody Object unitDtoObject )
  throws SQLException, IOException {
    Map     unitMap      = ( Map ) unitDtoObject;
    String  unitCategory = ( String ) unitMap.get ( "unitCategory" );
    Integer unitGkey     = ( Integer ) unitMap.get ( "unitGkey" );
    boolean addEvent = unitMap.get ( "addEvent" ) != null ? ( Boolean ) unitMap.get (
        "addEvent" ) : false;
    if ( "IMPRT".equals ( unitCategory ) && ! this.apptService.unitHasSealChargeEvent (
        unitGkey.longValue ( ) ) ) {
      if ( ! addEvent ) {
        return new ResponseEntity (
            this.buildQuestionResponse (
                "Esta acción genera cargos adicionales" , ( String ) null ) ,
            HttpStatus.OK
        );
      }

      String unitId       = ( String ) unitMap.get ( "unitId" );
      String unitLineOpId = ( String ) unitMap.get ( "unitLineOpId" );
      String resultStr = this.apptService.recordSeccharge2ToUnit ( unitGkey.longValue ( ) , unitId ,
                                                                   unitCategory , unitLineOpId ,
                                                                   this.getUserLogin ( )
                                                                 );
      if ( resultStr != null && ! resultStr.isEmpty ( ) ) {
        return new ResponseEntity (
            this.buildErrorResponse (
                "Error al generar los cargos. Por favor intente nuevamente" ) ,
            HttpStatus.OK
        );
      }
    }

    Map result = this.apptService.getUnitApptSeals ( unitGkey.longValue ( ) );
    if ( result != null ) {
      List < String > data = new ArrayList ( );
      data.add ( "getUnitApptSeals: invoked" );
      data.add ( "unitGkey:" + unitGkey );
      this.auditService.saveAuditTransaction ( "viewUnitApptSeals" , data.toString ( ) ,
                                               this.getUserContext ( ).getUsername ( )
                                             );
      return new ResponseEntity ( result , HttpStatus.OK );
    }
    else {
      return new ResponseEntity (
          this.buildErrorResponse (
              "No se pudo obtener la información de precintos. Por favor intente nuevamente" ) ,
          HttpStatus.OK
      );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/viewUnitServices" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > viewUnitServices ( @RequestBody Object unitDtoObject )
  throws SQLException, IOException {
    return this.viewUnitServicesIntern ( unitDtoObject , false );
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/viewUnitServicesCus" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > viewUnitServicesCus ( @RequestBody Object unitDtoObject )
  throws SQLException, IOException {
    return this.viewUnitServicesIntern ( unitDtoObject , true );
  }

  private
  ResponseEntity < Object > viewUnitServicesIntern ( Object unitDtoObject , boolean includeAllSo )
  throws SQLException, IOException {
    Map     unitMap      = ( Map ) unitDtoObject;
    String  unitCategory = ( String ) unitMap.get ( "unitCategory" );
    Integer unitGkey     = ( Integer ) unitMap.get ( "unitGkey" );
    List soList = this.soService.findUnitSrvOrderList ( unitGkey.longValue ( ) , unitCategory ,
                                                        includeAllSo
                                                      );
    return new ResponseEntity ( soList , HttpStatus.OK );
  }

  @RequestMapping (
      value = { "/getCoordinationTypes" },
      method = { RequestMethod.POST },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < List > getCoordinationTypes ( @RequestBody Object prUnitDto ) {
    Map    unitMap              = ( Map ) prUnitDto;
    String unitCategory         = ( String ) unitMap.get ( "unitCategory" );
    List   coordinationTypeList = this.coordinationService.getCoordinationTypeList ( unitCategory );
    return new ResponseEntity ( coordinationTypeList , HttpStatus.OK );
  }

  @RequestMapping (
      value = { "/getUnitCalendar" },
      method = { RequestMethod.POST },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getUnitCalendar ( @RequestBody Object prUnitDto )
  throws SQLException, ServiceException, IOException {
    return this.getUnitCalendarIntern ( prUnitDto , true );
  }

  @RequestMapping (
      value = { "/getUnitCalendarCus" },
      method = { RequestMethod.POST },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getUnitCalendarCus ( @RequestBody Object prUnitDto )
  throws SQLException, ServiceException, IOException {
    return this.getUnitCalendarIntern ( prUnitDto , false );
  }

  private
  ResponseEntity < Object > getUnitCalendarIntern ( Object prUnitDto , boolean filterOpening )
  throws SQLException, ServiceException, IOException {
    Map unitMap = ( Map ) prUnitDto;
    unitMap.put ( "userId" , this.getUserLogin ( ) );
    String  unitCategory     = ( String ) unitMap.get ( "unitCategory" );
    Integer unitGkey         = ( Integer ) unitMap.get ( "unitGkey" );
    String  coordinationType = ( String ) unitMap.get ( "coordinationType" );
    unitMap.put ( "apptType" , coordinationType );
    boolean isStorage        = UnitCategoryEnum.STRGE.getKey ( ).equals ( unitCategory );
    boolean isOrderAppt      = "APPT".equals ( unitCategory );
    Long    unitFreeDebt     = ( Long ) unitMap.get ( "unitFreeDebtExpireDate" );
    Date    unitFreeDebtDate = unitFreeDebt != null ? new Date ( unitFreeDebt ) : null;
    if ( coordinationType != null && ! coordinationType.isEmpty ( )
         && this.coordinationService.isService ( coordinationType )
         && ! this.soService.isServiceableEvent ( coordinationType ) ) {
      return new ResponseEntity (
          this.buildQuestionResponse ( "El servicio no requiere coordinación" , ( String ) null ) ,
          HttpStatus.OK
      );
    }
    else {
      Map calendarDateMap =
          isStorage ? this.coordinationService.getCalendarMinMaxDates ( unitFreeDebtDate )
                    : (
              isOrderAppt ? this.coordinationService.getCalendarMinMaxDefaultDates ( )
                          : this.coordinationService.getCalendarMinMaxDates (
                              unitGkey.longValue ( ) ,
                              unitCategory , coordinationType
                                                                            )
          );
      String warningMsg = ( String ) calendarDateMap.get ( "warningMsg" );
      if ( warningMsg != null && ! warningMsg.isEmpty ( ) ) {
        return new ResponseEntity ( this.buildWarningResponse ( warningMsg ) , HttpStatus.OK );
      }
      else {
        Date minDate    = ( Date ) calendarDateMap.get ( "minDate" );
        Date maxDate    = ( Date ) calendarDateMap.get ( "maxDate" );
        Date next30Days = DateUtils.addDays ( new Date ( ) , 30 );
        if ( maxDate.after ( next30Days ) ) {
          maxDate = next30Days;
        }

        return this.getOpenings (
            unitMap , minDate , maxDate , coordinationType , true , filterOpening );
      }
    }
  }

  @RequestMapping (
      value = { "/resolveCalendarEvent" },
      method = { RequestMethod.POST },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > resolveCalendarEvent ( @RequestBody Object prObject )
  throws SQLException, ServiceException, IOException {
    return this.resolveCalendarEventIntern ( prObject , true );
  }

  @RequestMapping (
      value = { "/resolveCalendarEventCus" },
      method = { RequestMethod.POST },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > resolveCalendarEventCus ( @RequestBody Object prObject )
  throws SQLException, ServiceException, IOException {
    return this.resolveCalendarEventIntern ( prObject , false );
  }

  private
  ResponseEntity < Object > resolveCalendarEventIntern ( Object prObject , boolean filterOpening )
  throws SQLException, ServiceException, IOException {
    Map    objectMap        = ( Map ) prObject;
    Map    unitMap          = ( Map ) objectMap.get ( "unitDto" );
    Map    eventMap         = ( Map ) objectMap.get ( "event" );
    String coordinationType = ( String ) objectMap.get ( "coordinationType" );
    unitMap.put ( "apptType" , coordinationType );
    String eventTitle = ( String ) eventMap.get ( "title" );
    String eventId    = ( String ) eventMap.get ( "id" );
    if ( NumberUtils.isNumber ( eventId ) ) {
      return new ResponseEntity ( ( MultiValueMap ) null , HttpStatus.OK );
    }
    else if ( DateUtils.isValidDate ( eventId , "yyyy-MM-dd" ) ) {
      String  fromDateStr      = StringUtils.addDefaultTimeToDateStr ( eventId );
      String  toDateStr        = StringUtils.getNextDateStr ( fromDateStr );
      Integer unitGkey         = ( Integer ) unitMap.get ( "unitGkey" );
      String  unitCategory     = ( String ) unitMap.get ( "unitCategory" );
      boolean isStorage        = UnitCategoryEnum.STRGE.getKey ( ).equals ( unitCategory );
      boolean isOrderAppt      = "APPT".equals ( unitCategory );
      Long    unitFreeDebt     = ( Long ) unitMap.get ( "unitFreeDebtExpireDate" );
      Date    unitFreeDebtDate = unitFreeDebt != null ? new Date ( unitFreeDebt ) : null;
      Map calendarDateMap =
          isStorage ? this.coordinationService.getCalendarMinMaxDates ( unitFreeDebtDate )
                    : (
              isOrderAppt ? this.coordinationService.getCalendarMinMaxDefaultDates ( )
                          : this.coordinationService.getCalendarMinMaxDates (
                              unitGkey.longValue ( ) ,
                              unitCategory , coordinationType
                                                                            )
          );
      Date fromDate = DateUtils.parseDate ( fromDateStr );
      Date maxDate  = ( Date ) calendarDateMap.get ( "maxDate" );
      Date toDate   = DateUtils.parseDate ( toDateStr );
      if ( toDate.after ( maxDate ) ) {
        toDate = maxDate;
      }

      return this.getOpenings (
          unitMap , fromDate , toDate , coordinationType , false , filterOpening );
    }
    else {
      return new ResponseEntity ( ( MultiValueMap ) null , HttpStatus.OK );
    }
  }

  private
  ResponseEntity < Object > getOpenings (
      Map unitMap , Date minDate , Date maxDate ,
      String coordinationType , boolean totalsOnly , boolean filterOpening
                                        )
  throws SQLException, ServiceException, IOException {
    unitMap.put ( "userId" , this.getUserLogin ( ) );
    String  unitCategory       = ( String ) unitMap.get ( "unitCategory" );
    Integer unitGkey           = ( Integer ) unitMap.get ( "unitGkey" );
    boolean isStorage          = UnitCategoryEnum.STRGE.getKey ( ).equals ( unitCategory );
    boolean isOrderAppt        = "APPT".equals ( unitCategory );
    Long    unitFreeDebtExpire = ( Long ) unitMap.get ( "unitFreeDebtExpireDate" );
    Date unitFreeDebtExpireDate = unitFreeDebtExpire != null ? new Date ( unitFreeDebtExpire )
                                                             : null;
    Object openingResult = coordinationType != null && ! coordinationType.isEmpty ( )
                           ? this.coordinationService.getOpeningList ( unitMap , minDate , maxDate ,
                                                                       coordinationType ,
                                                                       totalsOnly , filterOpening
                                                                     ) : new ArrayList ( );
    if ( openingResult instanceof String ) {
      return new ResponseEntity (
          this.buildErrorResponse ( ( String ) openingResult ) , HttpStatus.OK );
    }
    else {
      Map resultMap = new HashMap ( );
      if ( totalsOnly ) {
        List calendarEventList = new ArrayList ( );
        List unitCoordinatedEventList =
            unitGkey != null ? this.coordinationService.getUnitCoordinatedEventList (
                unitGkey.longValue ( ) , unitCategory )
                             : (
                isStorage ? this.coordinationService.getUnitCoordinatedEventList (
                    unitFreeDebtExpireDate , ( List ) null ) : new ArrayList ( )
            );
        calendarEventList.addAll ( ( Collection ) unitCoordinatedEventList );
        calendarEventList.addAll ( ( List ) openingResult );
        resultMap.put ( "calendarEventList" , calendarEventList );
        resultMap.put ( "minDate" , minDate );
        resultMap.put ( "maxDate" , maxDate );
      }
      else {
        String fieldToShow = "";
        if ( this.coordinationService.isAppointment ( coordinationType )
             && UnitCategoryEnum.IMPRT.getKey ( ).equals ( unitCategory ) ) {
          fieldToShow = "#appointmentFieldId1";
          resultMap.put ( "customsChannelList" , EnumUtil.getApptCustomsChannelList ( ) );
        }
        else if ( this.coordinationService.isService ( coordinationType ) ) {
          fieldToShow = "#serviceFieldId2";
          boolean isVerificationService = this.soService.isVerificationService ( coordinationType );
          if ( isVerificationService ) {
            fieldToShow = "#serviceFieldId1";
            resultMap.put ( "bunchList" , EnumUtil.getSrvOrderBunchList ( ) );
          }
        }

        resultMap.put ( "openingList" , ( List ) openingResult );
        if ( ! fieldToShow.isEmpty ( ) ) {
          resultMap.put ( "fieldToShow" , fieldToShow );
        }
      }

      return new ResponseEntity ( resultMap , HttpStatus.OK );
    }
  }

  @RequestMapping (
      value = { "/saveCoordination" },
      method = { RequestMethod.POST },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > saveCoordination ( @RequestBody Object prObject )
  throws SQLException, ServiceException, IOException, BaseException, InterruptedException {
    return this.saveCoordinationIntern ( prObject , true , true , false );
  }

  @RequestMapping (
      value = { "/saveCoordinationCus" },
      method = { RequestMethod.POST },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > saveCoordinationCus ( @RequestBody Object prObject )
  throws SQLException, ServiceException, IOException, BaseException, InterruptedException {
    return this.saveCoordinationIntern ( prObject , false , false , true );
  }

  private
  ResponseEntity < Object > saveCoordinationIntern (
      Object prObject , boolean filterOpening ,
      boolean validateCoordRules , boolean cusCoord
                                                   )
  throws SQLException, ServiceException, IOException, BaseException, InterruptedException {
    Map objectMap = ( Map ) prObject;
    Map unitMap   = ( Map ) objectMap.get ( "unitDto" );
    this.setUnitLogPrefix ( unitMap );
    String  coordinationType   = ( String ) objectMap.get ( "coordinationType" );
    String  selectedDateTime   = ( String ) objectMap.get ( "selectedDateTime" );
    String  customsChannel     = ( String ) objectMap.get ( "customsChannel" );
    String  bunch              = ( String ) objectMap.get ( "bunch" );
    String  email              = ( String ) objectMap.get ( "email" );
    boolean isAppt             = this.coordinationService.isAppointment ( coordinationType );
    boolean isServiceableEvent = this.soService.isServiceableEvent ( coordinationType );
    this.logUser (
        "Start saveCoordination. isAppt?" + isAppt + ", isServiceableEvent?" + isServiceableEvent );
    if ( isAppt || isServiceableEvent ) {
      Integer unitGkey     = ( Integer ) unitMap.get ( "unitGkey" );
      String  unitCategory = ( String ) unitMap.get ( "unitCategory" );
      String  errorMsg     = "";
      if ( selectedDateTime != null && ! selectedDateTime.isEmpty ( ) ) {
        if ( coordinationType != null && ! coordinationType.isEmpty ( ) ) {
          if ( isAppt && unitGkey != null && this.coordinationService.unitAlreadyCoordinated (
              unitGkey.longValue ( ) , unitCategory , isAppt ) ) {
            ApptTypeEnum apptTypeEnum = EnumUtil.resolveApptType ( coordinationType );
            errorMsg = "El contenedor ya cuenta con un turno " + (
                apptTypeEnum != null ? "de "
                                       + apptTypeEnum.getDescription ( ) + " " : ""
            )
                       + "coordinado, debés cancelar el turno vigente para poder volver a "
                       + "coordinar.";
          }
        }
        else {
          errorMsg = "Seleccionó un tipo de coordinación inválido. Por favor vuelva a intentar";
        }
      }
      else {
        errorMsg = "La fecha/hora seleccionada no es válida. Por favor vuelva a intentar";
      }

      if ( ! errorMsg.isEmpty ( ) ) {
        return new ResponseEntity ( this.buildErrorResponse ( errorMsg ) , HttpStatus.OK );
      }

      String ruleValidation =
          validateCoordRules ? this.ruleService.validateSelectedDateTime ( coordinationType ,
                                                                           false ,
                                                                           selectedDateTime , isAppt
                                                                         ) : null;
      if ( ruleValidation != null && ! ruleValidation.isEmpty ( ) ) {
        return new ResponseEntity ( this.buildErrorResponse ( ruleValidation ) , HttpStatus.OK );
      }

      unitMap.put ( "userId" , this.getUserLogin ( ) );
      unitMap.put ( "apptType" , coordinationType );
      String timeValidation = this.coordinationService.validateSelectedDateTime ( unitMap ,
                                                                                  coordinationType ,
                                                                                  selectedDateTime ,
                                                                                  filterOpening
                                                                                );
      if ( timeValidation != null && ! timeValidation.isEmpty ( ) ) {
        return new ResponseEntity ( this.buildErrorResponse ( timeValidation ) , HttpStatus.OK );
      }

      boolean isStorage   = UnitCategoryEnum.STRGE.getKey ( ).equalsIgnoreCase ( unitCategory );
      boolean isOrderAppt = "APPT".equals ( unitCategory );
      if ( isStorage ) {
        String storageIsoTypeId = ( String ) unitMap.get ( "storageIsoTypeId" );
        String storageLineOpId  = ( String ) unitMap.get ( "storageLineOpId" );
        String unitIsoTypeId    = ( String ) unitMap.get ( "unitIsoTypeId" );
        String unitLineOpId     = ( String ) unitMap.get ( "unitLineOpId" );
        unitIsoTypeId =
            unitIsoTypeId != null && ! unitIsoTypeId.isEmpty ( ) ? unitIsoTypeId : storageIsoTypeId;
        unitLineOpId  =
            unitLineOpId != null && ! unitLineOpId.isEmpty ( ) ? unitLineOpId : storageLineOpId;
        if ( unitIsoTypeId == null || unitIsoTypeId.isEmpty ( ) ) {
          return new ResponseEntity (
              this.buildErrorResponse (
                  "El tipo de contenedor no es correcto. Por favor refresque la información de la"
                  + " página" ) ,
              HttpStatus.OK
          );
        }

        if ( unitLineOpId == null || unitLineOpId.isEmpty ( ) ) {
          return new ResponseEntity (
              this.buildErrorResponse (
                  "La línea operadora del contenedor no es correcta. Por favor refresque la "
                  + "información de la página" ) ,
              HttpStatus.OK
          );
        }

        unitMap.put ( "unitIsoTypeId" , unitIsoTypeId );
        unitMap.put ( "unitLineOpId" , unitLineOpId );
      }

      boolean importUnitFound =
          unitMap.get ( "importUnitFound" ) != null ? ( Boolean ) unitMap.get ( "importUnitFound" )
                                                    : false;
      boolean acceptStorageCharges =
          unitMap.get ( "acceptStorageCharges" ) != null ? ( Boolean ) unitMap.get (
              "acceptStorageCharges" ) : false;
      if ( isStorage && ! importUnitFound && ! acceptStorageCharges ) {
        return new ResponseEntity ( this.buildQuestionResponse (
            "Esta operación generará cargos que deben ser cancelados antes del ingreso del "
            + "contenedor" ,
            "acceptStorageCharges"
                                                               ) , HttpStatus.OK );
      }

      if ( isOrderAppt ) {
        Integer documentGkey     = ( Integer ) unitMap.get ( "documentGkey" );
        Integer documentItemGkey = ( Integer ) unitMap.get ( "documentItemGkey" );
        Object validateResult = this.validateDMOrderIntern ( documentGkey.longValue ( ) ,
                                                             documentItemGkey.longValue ( ) , false
                                                           );
        if ( validateResult instanceof String ) {
          return new ResponseEntity (
              this.buildErrorResponse ( ( String ) validateResult ) ,
              HttpStatus.OK
          );
        }
      }
    }

    this.logUser ( "Finish saveCoordination" );
    return isAppt ? this.createUnitAppointment ( unitMap , coordinationType , selectedDateTime ,
                                                 customsChannel , cusCoord
                                               )
                  : this.createUnitService ( unitMap , coordinationType , selectedDateTime , email ,
                                             bunch ,
                                             cusCoord
                                           );
  }

  private
  ResponseEntity < Object > createUnitAppointment (
      Map unitMap , String coordinationType ,
      String selectedDateTimeStr , String customsChannel , boolean cusCoord
                                                  )
  throws IOException, SQLException, BaseException, InterruptedException {
    Integer unitGkey        = ( Integer ) unitMap.get ( "unitGkey" );
    String  unitId          = ( String ) unitMap.get ( "unitId" );
    String  unitDocumentNbr = ( String ) unitMap.get ( "unitDocumentNbr" );
    String  unitCategory    = ( String ) unitMap.get ( "unitCategory" );
    String  unitIsoTypeId   = ( String ) unitMap.get ( "unitIsoTypeId" );
    boolean isExport        = UnitCategoryEnum.EXPRT.getKey ( ).equalsIgnoreCase ( unitCategory );
    boolean isImport        = UnitCategoryEnum.IMPRT.getKey ( ).equalsIgnoreCase ( unitCategory );
    boolean isStorage       = UnitCategoryEnum.STRGE.getKey ( ).equalsIgnoreCase ( unitCategory );
    boolean isOrderAppt     = "APPT".equalsIgnoreCase ( unitCategory );
    boolean accepStorageCharges =
        unitMap.get ( "acceptStorageCharges" ) != null ? ( Boolean ) unitMap.get (
            "acceptStorageCharges" )
                                                       : false;
    String  unitLineOpId         = ( String ) unitMap.get ( "unitLineOpId" );
    String  unitOwnerId          = ( String ) unitMap.get ( "unitOwnerId" );
    String  documentNbr          = ( String ) unitMap.get ( "documentNbr" );
    String  documentType         = ( String ) unitMap.get ( "documentType" );
    String  documentLineId       = ( String ) unitMap.get ( "documentLineId" );
    String  documentItemIsoType  = ( String ) unitMap.get ( "documentItemIsoType" );
    Date    selectedDateTime     = DateUtils.parseDate ( selectedDateTimeStr );
    boolean auditAgpCoordination = false;
    List    unitSrvOrderList     = new ArrayList ( );
    List    availableOpeningList = new ArrayList ( );
    int     availableQuota       = 0;
    if ( isOrderAppt ) {
      this.setOrderLogPrefix ( unitMap );
    }
    else {
      this.setUnitLogPrefix ( unitMap );
    }

    this.logUser (
        "Start createUnitAppointment. DocumentNbr" + unitDocumentNbr + ", coordinationType"
        + coordinationType + ", selectedDateTimeStr:" + selectedDateTimeStr + ", customsChannel:"
        + customsChannel );
    if ( isImport ) {
      if ( customsChannel == null || customsChannel.isEmpty ( )
           || EnumUtil.resolveCustomsChannel ( customsChannel ) == null ) {
        return new ResponseEntity (
            this.buildErrorResponse ( "Por favor seleccione el Tipo de Operativa" ) ,
            HttpStatus.OK
        );
      }

      boolean coordinateAfterForcedDate =
          unitMap.get ( "coordinateAfterForcedDate" ) != null ? ( Boolean ) unitMap.get (
              "coordinateAfterForcedDate" ) : false;
      Date forcedDate =
          unitMap.get ( "forcedDate" ) != null ? new Date ( ( Long ) unitMap.get ( "forcedDate" ) )
                                               : null;
      if ( forcedDate == null ) {
        return new ResponseEntity (
            this.buildErrorResponse (
                "No se pudo validar la fecha del Forzoso. Por favor refresque la página" ) ,
            HttpStatus.OK
        );
      }

      Date currentDate        = new Date ( );
      Date dayAfterForcedDate = DateUtils.addDays ( DateUtils.resetDate ( forcedDate ) , 1 );
      auditAgpCoordination = cusCoord ? false
                                      : currentDate.after ( dayAfterForcedDate )
                                        || selectedDateTime.after ( dayAfterForcedDate );
      this.logUser ( "auditAgpCoordination?" + auditAgpCoordination + ", coordinateAfterForcedDate?"
                     + coordinateAfterForcedDate );
      if ( auditAgpCoordination ) {
        unitSrvOrderList = this.soService.getUnitServiceCanNotBeDoneSameDay (
            unitGkey.longValue ( ) ,
            unitCategory , dayAfterForcedDate
                                                                            );
        if ( selectedDateTime.after ( dayAfterForcedDate ) ) {
          Map minMaxDateMap = this.coordinationService.getCalendarMinMaxDates (
              unitGkey.longValue ( ) ,
              unitCategory , coordinationType
                                                                              );
          Date minDate = ( Date ) minMaxDateMap.get ( "minDate" );
          Object openingResult = this.coordinationService.getOpeningList ( unitMap , minDate ,
                                                                           dayAfterForcedDate ,
                                                                           coordinationType ,
                                                                           false , true
                                                                         );
          if ( openingResult instanceof ArrayList ) {
            availableOpeningList = ( List ) openingResult;
            availableQuota       = this.coordinationService.getAvailableQuotaFromList (
                ( List ) availableOpeningList );
          }
        }

        if ( ! coordinateAfterForcedDate ) {
          String msg = "Usted está seleccionando turno fuera del forzoso" + (
              availableQuota > 0 ?
              " teniendo " + availableQuota + " turnos disponibles." : "."
          );
          return new ResponseEntity (
              this.buildQuestionResponse ( msg , "coordinateAfterForcedDate" ) ,
              HttpStatus.OK
          );
        }
      }
    }

    if ( isStorage && unitGkey == null ) {
      unitOwnerId = unitOwnerId != null && ! unitOwnerId.isEmpty ( ) ? unitOwnerId : unitLineOpId;
      String preadviseStorageResult = this.unitService.preadviseStorageUnit ( unitId ,
                                                                              unitLineOpId ,
                                                                              unitOwnerId ,
                                                                              unitIsoTypeId
                                                                            );
      if ( preadviseStorageResult != null && ! preadviseStorageResult.isEmpty ( ) ) {
        new ResponseEntity (
            this.buildErrorResponse (
                "Se produjo un error en la creación del contenedor. Por favor contáctese con "
                + "Atención al Cliente" ) ,
            HttpStatus.OK
        );
      }
    }

    Object result =
        isOrderAppt ? this.apptService.createOrderAppt ( coordinationType , selectedDateTimeStr ,
                                                         documentNbr , documentLineId ,
                                                         documentType , documentItemIsoType
                                                       )
                    : this.apptService.createUnitAppointment ( coordinationType , unitId ,
                                                               unitDocumentNbr ,
                                                               selectedDateTimeStr , unitIsoTypeId ,
                                                               unitLineOpId
                                                             );
    this.logUser ( "Create unit appointment result:" + result + ", class:" + result.getClass ( ) );
    if ( result instanceof String ) {
      return new ResponseEntity ( this.buildErrorResponse ( ( String ) result ) , HttpStatus.OK );
    }
    else {
      Long   coordinationNbr = ( Long ) result;
      String userLogin       = this.getUserLogin ( );
      this.logUser ( "Appointment " + coordinationNbr + " created by " + userLogin );
      List < String > data = new ArrayList ( );
      data.add ( "appotinmentNbr:" + coordinationNbr );
      data.add ( "prApptSubType:" + coordinationType );
      if ( isOrderAppt ) {
        data.add ( "prOrderNbr:" + documentNbr );
        data.add ( "prOrderType:" + documentType );
        data.add ( "prOrderLineId:" + documentLineId );
        data.add ( "prOrderItemIso:" + documentItemIsoType );
      }
      else {
        data.add ( "prUnitId:" + unitId );
        data.add ( "prUnitDocumentNbr:" + unitDocumentNbr );
        if ( isExport ) {
          Long vipApptGkey = this.vipApptService.getVipApptGkeyFromApptNbr (
              coordinationNbr ,
              userLogin
                                                                           );
          if ( vipApptGkey != null ) {
            data.add ( "isVipAppt?true" );
            data.add ( "vipApptGkey" + vipApptGkey );
          }
        }
      }

      data.add ( "prSelectedDateTime:" + selectedDateTimeStr );
      if ( isImport ) {
        data.add ( "customsChannel:" + customsChannel );
      }

      if ( isStorage ) {
        data.add ( "isoTypeId:" + unitIsoTypeId );
        data.add ( "accepStorageCharges:" + accepStorageCharges );
      }

      if ( cusCoord ) {
        data.add ( "coordinatedByCus:" + cusCoord );
      }

      this.logUser ( "Saving createUnitAppointment audit transaction.." );
      this.auditService.saveAuditTransaction (
          isOrderAppt ? "createOrderAppointment" : "createUnitAppointment" , data.toString ( ) ,
          this.getUserContext ( ).getUsername ( )
                                             );
      ApptCustomsChannelEnum customsChannelEnum = EnumUtil.resolveCustomsChannel ( customsChannel );
      String                 successMsg;
      if ( isImport && customsChannelEnum != null ) {
        successMsg = customsChannelEnum.getDescription ( );
        this.logUser ( "Updating appointment customs channel to " + successMsg );
        this.coordinationService.updateApptCustomsChannel ( coordinationNbr , successMsg );
      }

      String goToPayMsg;
      if ( auditAgpCoordination ) {
        boolean emailSent = this.coordinationService.sendApptAfterForcedDateEmail ( unitId ,
                                                                                    availableQuota ,
                                                                                    selectedDateTime ,
                                                                                    new Date ( ) ,
                                                                                    userLogin
                                                                                  );
        goToPayMsg = ( String ) unitMap.get ( "unitVisitId" );
        Date forcedDate = new Date ( ( Long ) unitMap.get ( "forcedDate" ) );
        this.logUser ( "Saving agp coordination audit. unitGkey:" + unitGkey + ", unitId:" + unitId
                       + ", coordinationNbr:" + coordinationNbr + ", emailSent?" + emailSent );
        this.agpCoordAuditService.saveAgpCoordinationAudit ( unitGkey.longValue ( ) , unitId ,
                                                             unitCategory , unitDocumentNbr ,
                                                             coordinationType , coordinationNbr ,
                                                             goToPayMsg ,
                                                             forcedDate , selectedDateTime ,
                                                             this.getUserContext ( ) ,
                                                             ( List ) availableOpeningList ,
                                                             ( List ) unitSrvOrderList , emailSent
                                                           );
      }

      this.logUser ( "Updating unit web email..." );
      this.coordinationService.updateUnitWebMail (
          unitGkey != null ? unitGkey.longValue ( ) : null ,
          unitId , coordinationNbr , userLogin
                                                 );
      successMsg = "¡Contenedor coordinado correctamente!";
      goToPayMsg = "<br><br>Recordá pagar los gastos generados para obtener el GatePass desde el "
                   + "módulo \"Documentación\"";
      String goToGatePassMsg = "<br><br>Podés descargar el GatePass desde desde el módulo "
                               + "\"Documentación\"";
      boolean updateUnitLineOp;
      if ( isStorage ) {
        if ( accepStorageCharges ) {
          this.coordinationService.recordApptStorageCharges ( coordinationNbr , unitId );
          successMsg = successMsg + goToPayMsg;
        }
        else {
          successMsg = successMsg + goToGatePassMsg;
        }

        updateUnitLineOp = ! unitLineOpId.equals ( unitOwnerId );
        String unitFreeDebtExpiredDebtStr = ( String ) unitMap.get ( "unitLimitDateStr" );
        if ( updateUnitLineOp
             || unitFreeDebtExpiredDebtStr != null && ! unitFreeDebtExpiredDebtStr.isEmpty ( ) ) {
          this.coordinationService.updateApptStorageUnit ( coordinationNbr , unitId , unitLineOpId ,
                                                           unitFreeDebtExpiredDebtStr
                                                         );
        }
      }
      else if ( isImport ) {
        successMsg = successMsg
                     + "<br><br>Recuerde que si su depósito asignado de devolución del contenedor"
                     + " vacío es Terminal 4/Terbasa debe coordinar el turno mediante Puerto "
                     + "Digital con anticipación.";
      }
      else if ( isOrderAppt ) {
        updateUnitLineOp = this.apptService.apptAppliedChargesOnCreate ( coordinationNbr );
        successMsg       = successMsg + ( updateUnitLineOp ? goToPayMsg : goToGatePassMsg );
      }

      this.logUser ( "Finish createUnitAppointment" );
      return new ResponseEntity ( this.buildSuccessResponse ( successMsg ) , HttpStatus.OK );
    }
  }

  private
  ResponseEntity < Object > createUnitService (
      Map unitMap , String coordinationType ,
      String selectedDateTimeStr , String email , String bunch , boolean cusCoord
                                              )
  throws IOException, SQLException {
    this.setUnitLogPrefix ( unitMap );
    Integer unitGkey = ( Integer ) unitMap.get ( "unitGkey" );
    String  unitId   = ( String ) unitMap.get ( "unitId" );
    this.soService.isScannerService ( coordinationType );
    this.logUser (
        "Start createUnitService. CoordinationType:" + coordinationType + ", selectedDateTimeStr:"
        + selectedDateTimeStr + ", email:" + email + ", bunch:" + bunch );
    String result = ( String ) this.soService.createUnitService ( unitGkey.toString ( ) ,
                                                                  coordinationType ,
                                                                  selectedDateTimeStr , email ,
                                                                  bunch
                                                                );
    this.logUser ( "Create unit service result:" + result + ", class:" + result.getClass ( ) );
    if ( ! result.isEmpty ( ) ) {
      return new ResponseEntity ( this.buildErrorResponse ( result ) , HttpStatus.OK );
    }
    else {
      List < String > data = new ArrayList ( );
      data.add ( "prUnitGkeyStr:" + unitGkey );
      data.add ( "prUnitId:" + unitId );
      data.add ( "prServiceTypeKey:" + coordinationType );
      data.add ( "prSelectedDateTime:" + selectedDateTimeStr );
      data.add ( "prEmail:" + email );
      data.add ( "prBunch:" + bunch );
      if ( cusCoord ) {
        data.add ( "coordinatedByCus:" + cusCoord );
      }

      this.logUser ( "Saving createUnitService audit transaction.." );
      this.auditService.saveAuditTransaction ( "createUnitService" , data.toString ( ) ,
                                               this.getUserContext ( ).getUsername ( )
                                             );
      String msg = "Coordinado correctamente";
      String scannerMsg = "Recuerde presentar la carpeta en Scanner 24 hs antes del turno"
                          + ".\nProcure coordinar el SCANNER previo a la verificación";
      msg = msg + (
          this.soService.isScannerService ( coordinationType ) ? ".\n" + scannerMsg : ""
      );
      this.logUser ( "Finish createUnitService" );
      return new ResponseEntity ( this.buildSuccessResponse ( msg ) , HttpStatus.OK );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/cancelUnitAppointment" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > cancelUnitAppointment ( @RequestBody Object unitApptDtoObject )
  throws IOException, SQLException {
    Map unitApptMap = ( Map ) unitApptDtoObject;
    if ( ! this.canDeleteItem ) {
      return new ResponseEntity (
          this.buildErrorResponse ( "No tiene permisos para realizar esta acción" ) ,
          HttpStatus.OK
      );
    }
    else {
      String apptSubType = ( String ) unitApptMap.get ( "requestApptSubType" );
      apptSubType = apptSubType != null && ! apptSubType.isEmpty ( ) ? apptSubType
                                                                     : ( String ) unitApptMap.get (
                                                                         "apptType" );
      String  apptDateTimeStr = ( String ) unitApptMap.get ( "apptRequestedDateStr" );
      Date    apptDateTime    = DateUtils.parseDate ( apptDateTimeStr );
      boolean expiredApptDate = ( new Date ( ) ).after ( apptDateTime );
      String ruleValidation =
          ! expiredApptDate ? this.ruleService.validateSelectedDateTime ( apptSubType , true ,
                                                                          apptDateTimeStr , true
                                                                        ) : "";
      boolean acceptCharges =
          unitApptMap.get ( "acceptCharges" ) != null ? ( Boolean ) unitApptMap.get (
              "acceptCharges" )
                                                      : false;
      if ( ( expiredApptDate || ruleValidation != null && ! ruleValidation.isEmpty ( ) )
           && ! acceptCharges ) {
        return new ResponseEntity ( this.buildQuestionResponse (
            "¿Está seguro que desa anular el turno? Esta operación generará cargos adicionales" ,
            "acceptCharges"
                                                               ) , HttpStatus.OK );
      }
      else {
        Integer unitGkey = ( Integer ) unitApptMap.get ( "unitGkey" );
        Integer apptNbr  = ( Integer ) unitApptMap.get ( "apptNbr" );
        boolean deleteDraft =
            unitApptMap.get ( "deleteDraft" ) != null ? ( Boolean ) unitApptMap.get (
                "deleteDraft" )
                                                      : false;
        boolean isOrderAppt = "APPT".equals ( ( String ) unitApptMap.get ( "unitCategory" ) );
        String  deleteDraftResult;
        if ( unitGkey != null && this.apptService.unitHasDraftInvoice ( unitGkey.longValue ( ) )
             || apptNbr != null && this.apptService.apptHasDraftInvoice (
            apptNbr.longValue ( ) ) ) {
          if ( ! deleteDraft ) {
            return new ResponseEntity ( this.buildQuestionResponse (
                "Esta acción eliminará los presupuestos asociados al " + (
                    isOrderAppt ? "turno"
                                : "contenedor"
                ) , "deleteDraft" ) , HttpStatus.OK );
          }

          deleteDraftResult =
              isOrderAppt ? this.apptService.deleteApptDraftInvoice ( apptNbr.longValue ( ) )
                          : this.apptService.deleteUnitApptDraftInvoice ( unitGkey.longValue ( ) );
          if ( deleteDraftResult != null && ! deleteDraftResult.isEmpty ( ) ) {
            return new ResponseEntity (
                this.buildErrorResponse ( deleteDraftResult ) , HttpStatus.OK );
          }
        }

        deleteDraftResult = this.apptService.cancelUnitAppointment ( apptNbr.longValue ( ) );
        if ( deleteDraftResult != null && deleteDraftResult.isEmpty ( ) ) {
          String unitId       = ( String ) unitApptMap.get ( "unitId" );
          String unitCategory = ( String ) unitApptMap.get ( "unitCategory" );
          this.apptService.cancelUnitOvertime ( unitGkey.longValue ( ) , unitId , unitCategory ,
                                                apptNbr.longValue ( )
                                              );
          if ( acceptCharges && ! isOrderAppt ) {
            String unitLineOpId = ( String ) unitApptMap.get ( "unitLineOpId" );
            this.apptService.recordRecoorToUnit ( unitGkey.longValue ( ) , unitId , unitCategory ,
                                                  unitLineOpId , this.getUserLogin ( )
                                                );
          }

          List < String > data = new ArrayList ( );
          data.add ( "apptNbr:" + apptNbr );
          data.add ( "acceptCharges:" + acceptCharges );
          this.auditService.saveAuditTransaction (
              isOrderAppt ? "cancelOrderAppointment" : "cancelUnitAppointment" , data.toString ( ) ,
              this.getUserContext ( ).getUsername ( )
                                                 );
          return new ResponseEntity (
              this.buildSuccessResponse ( "Se eliminó el appointment con éxito" ) , HttpStatus.OK );
        }
        else {
          return new ResponseEntity (
              this.buildErrorResponse ( deleteDraftResult ) , HttpStatus.OK );
        }
      }
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/cancelUnitService" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > cancelUnitService ( @RequestBody Object unitServiceDtoObject )
  throws IOException, SQLException {
    return this.cancelUnitServiceIntern ( unitServiceDtoObject , false );
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/cancelUnitServiceCus" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > cancelUnitServiceCus ( @RequestBody Object unitServiceDtoObject )
  throws IOException, SQLException {
    return this.cancelUnitServiceIntern ( unitServiceDtoObject , true );
  }

  private
  ResponseEntity < Object > cancelUnitServiceIntern (
      Object unitServiceDtoObject ,
      boolean includeAllSo
                                                    ) throws IOException, SQLException {
    if ( this.canDeleteItem ) {
      Map    unitServiceMap        = ( Map ) unitServiceDtoObject;
      String serviceTypeKey        = ( String ) unitServiceMap.get ( "srvOrderTypeKey" );
      String serviceOrderStartDate = ( String ) unitServiceMap.get ( "srvOrderStartDateStr" );
      String ruleValidation = this.ruleService.validateSelectedDateTime ( serviceTypeKey , true ,
                                                                          serviceOrderStartDate ,
                                                                          false
                                                                        );
      if ( ruleValidation != null && ! ruleValidation.isEmpty ( ) ) {
        return new ResponseEntity ( this.buildErrorResponse ( ruleValidation ) , HttpStatus.OK );
      }
      else {
        String serviceOrderNbr = ( String ) unitServiceMap.get ( "srvOrderNbr" );
        if ( this.soService.soHasDraftEvents ( serviceOrderNbr ) ) {
          return new ResponseEntity (
              this.buildErrorResponse (
                  "Tenés un presupuesto generado. Cancelá el presupuesto y continuá con la "
                  + "cancelación del servicio" ) ,
              HttpStatus.OK
          );
        }
        else {
          String result = this.soService.cancelUnitService ( unitServiceMap );
          if ( result != null && ! result.isEmpty ( ) ) {
            return new ResponseEntity ( this.buildErrorResponse ( result ) , HttpStatus.OK );
          }
          else {
            Integer         serviceOrderGkey = ( Integer ) unitServiceMap.get ( "srvOrderGkey" );
            List < String > data             = new ArrayList ( );
            data.add ( "serviceGkey:" + serviceOrderGkey );
            data.add ( "serviceOrderNbr:" + serviceOrderNbr );
            data.add ( "serviceTypeKey:" + serviceTypeKey );
            data.add ( "serviceOrderStartDate:" + serviceOrderStartDate );
            this.auditService.saveAuditTransaction ( "cancelUnitService" , data.toString ( ) ,
                                                     this.getUserContext ( ).getUsername ( )
                                                   );
            Map     resultMap    = this.buildSuccessResponse ( "Se canceló el servicio con éxito" );
            Integer unitGkey     = ( Integer ) unitServiceMap.get ( "unitGkey" );
            String  unitCategory = ( String ) unitServiceMap.get ( "unitCategory" );
            List unitSoList = this.soService.findUnitSrvOrderList ( unitGkey.longValue ( ) ,
                                                                    unitCategory , includeAllSo
                                                                  );
            resultMap.put ( "unitSoList" , unitSoList );
            return new ResponseEntity ( resultMap , HttpStatus.OK );
          }
        }
      }
    }
    else {
      return new ResponseEntity (
          this.buildErrorResponse ( "No tiene permisos para realizar esta acción" ) ,
          HttpStatus.OK
      );
    }
  }

  @RequestMapping (
      value = { "/validateDMOrder" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > validateDMOrder (
      @RequestParam String prDocumentGkeyStr ,
      @RequestParam String prDocumentItemGkeyStr , @RequestParam Boolean prSelectItem
                                            )
  throws SQLException, IOException {
    Long documentGkey = prDocumentGkeyStr != null && ! prDocumentGkeyStr.isEmpty ( )
                        ? Long.parseLong (
        prDocumentGkeyStr ) : null;
    Long documentItemGkey =
        prDocumentItemGkeyStr != null && ! prDocumentItemGkeyStr.isEmpty ( ) ? Long.parseLong (
            prDocumentItemGkeyStr ) : null;
    boolean isSelectItem = prSelectItem != null && prSelectItem;
    Object validateResult = this.validateDMOrderIntern ( documentGkey , documentItemGkey ,
                                                         isSelectItem
                                                       );
    return validateResult instanceof String ? new ResponseEntity (
        this.buildErrorResponse ( ( String ) validateResult ) , HttpStatus.OK )
                                            : new ResponseEntity ( validateResult , HttpStatus.OK );
  }

  private
  ResponseEntity < Object > getOpenings (
      String apptSubType , String fromDateStr ,
      String toDateStr , boolean totalsOnly , Map unitApptMap , boolean filterOpening
                                        )
  throws SQLException, ServiceException, IOException {
    unitApptMap.put ( "userId" , this.getUserLogin ( ) );
    Object result = this.apptService.getAppointmentOpenings ( apptSubType , fromDateStr ,
                                                              toDateStr ,
                                                              totalsOnly , unitApptMap ,
                                                              filterOpening
                                                            );
    if ( result instanceof String ) {
      return new ResponseEntity ( this.buildErrorResponse ( ( String ) result ) , HttpStatus.OK );
    }
    else {
      if ( unitApptMap != null && totalsOnly ) {
        List resultList = ( List ) result;
        this.addUnitDateToCalendar ( resultList , unitApptMap , fromDateStr , "unitStartDateName" ,
                                     "" ,
                                     ""
                                   );
        String unitCategory     = ( String ) unitApptMap.get ( "unitCategory" );
        String forcedDateStr    = ( String ) unitApptMap.get ( "forcedDateStr" );
        String unitLimitDateStr = ( String ) unitApptMap.get ( "unitLimitDateStr" );
        if ( "IMPRT".equals ( unitCategory ) && forcedDateStr != null
             && ! forcedDateStr.isEmpty ( ) ) {
          this.addUnitDateToCalendar ( resultList , unitApptMap , forcedDateStr , "" , "Forzoso" ,
                                       "#9EE6E5"
                                     );
        }
        else if ( "EXPRT".equals ( unitCategory ) && unitLimitDateStr != null
                  && ! unitLimitDateStr.isEmpty ( ) ) {
          this.addUnitDateToCalendar ( resultList , unitApptMap , unitLimitDateStr ,
                                       "unitEndDateName" ,
                                       "" , "#9EE6E5"
                                     );
        }
        else {
          this.addUnitDateToCalendar (
              resultList , unitApptMap , toDateStr , "unitEndDateName" , "" , "" );
        }
      }

      return new ResponseEntity ( ( List ) result , HttpStatus.OK );
    }
  }

  private
  Object validateDMOrderIntern (
      Long documentGkey , Long documentItemGkey ,
      boolean isSelectItem
                               ) throws SQLException, IOException {
    DocumentApptDTO documentApptDTO =
        documentGkey != null ? this.coordinationService.findEqBaseOrderByGkey ( documentGkey )
                             : null;
    if ( documentApptDTO == null ) {
      return "No se encontró información del Booking/EDO";
    }
    else {
      String documentNbr      = documentApptDTO.getNbr ( );
      String documentType     = documentApptDTO.getDocumentType ( );
      String documentTypeName = "BOOK".equals ( documentType ) ? "Booking" : "EDO";
      List   orderItemList    = this.coordinationService.findEqBaseOrderItemList ( documentGkey );
      if ( ! isSelectItem && (
          orderItemList.isEmpty ( ) || ! this.apptService.hasDMAvailability (
              documentNbr , documentGkey , ( Long ) null )
      ) ) {
        return "La reserva " + documentNbr
               + " ya ha sido coordinada o retirada en su totalidad.<br><br>"
               + "Si desea aumentar la cantidad de contenedores asignados, por favor comuníquese "
               + "con su Agencia Marítima";
      }
      else {
        if ( documentItemGkey == null ) {
          if ( isSelectItem ) {
            return "Por favor seleccione un item de la reserva " + documentNbr;
          }

          if ( orderItemList.size ( ) > 1 ) {
            return orderItemList;
          }

          documentItemGkey = this.coordinationService.getFirstEqoiGkey ( orderItemList );
        }

        if ( documentItemGkey == null ) {
          return "No se pudo cargar la información de los items de la reserva " + documentNbr
                 + ".<br>Por favor refresque la búsqueda";
        }
        else {
          BookingItemDTO itemDto = this.coordinationService.getEqoOrderItem (
              orderItemList ,
              documentItemGkey
                                                                            );
          if ( itemDto == null ) {
            return "El tipo de contenedor no coincide con la información de la reserva "
                   + documentNbr;
          }
          else if ( ! this.apptService.hasDMAvailability ( documentNbr , ( Long ) null ,
                                                           documentItemGkey
                                                         ) ) {
            return "Ya ha coordinado o retirado la totalidad de los contenedores del tipo "
                   + itemDto.getIsoType ( ) + " para la reserva " + documentNbr + ".<br><br>"
                   + "Si desea aumentar la cantidad de contenedores asignados, por favor "
                   + "comuníquese con su Agencia Marítima";
          }
          else {
            Map result = new HashMap ( );
            result.put ( "documentGkey" , documentGkey );
            result.put ( "documentItemGkey" , documentItemGkey );
            result.put ( "documentNbr" , documentNbr );
            result.put ( "documentType" , documentApptDTO.getDocumentType ( ) );
            result.put ( "unitCategory" , "APPT" );
            result.put ( "documentItemIsoType" , itemDto.getIsoType ( ) );
            result.put ( "documentLineId" , documentApptDTO.getLineOpId ( ) );
            result.put ( "unitLineOpId" , documentApptDTO.getLineOpId ( ) );
            return result;
          }
        }
      }
    }
  }

  private
  String getUserLogin ( ) {
    return this.getUserContext ( ).getUsername ( );
  }

  private
  String getDataTableHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    sb.append ( "<thead>\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <th>Contenedor</th>\n" );
    sb.append ( "    <th>Categoría</th>\n" );
    sb.append ( "    <th>Tipo</th>\n" );
    sb.append ( "    <th>Línea</th>\n" );
    sb.append ( "    <th>Estado</th>\n" );
    sb.append ( "    <th>Documento</th>\n" );
    sb.append ( "    <th>POL</th>\n" );
    sb.append ( "    <th>POD</th>\n" );
    sb.append ( "    <th>Nave</th>\n" );
    sb.append ( "    <th>Viaje</th>\n" );
    sb.append ( "    <th>Fecha</th>\n" );
    sb.append ( "    <th>Turno</th>\n" );
    sb.append ( "    <th>Precintos</th>\n" );
    sb.append ( "    <th>Servicios</th>\n" );
    sb.append ( "    <th>Coordinar</th>\n" );
    sb.append ( "    <th>Anular Turno</th>\n" );
    sb.append ( "</tr>\n" );
    sb.append ( "</thead>\n" );
    sb.append ( "<tbody data-bind=\"foreach: coordinationViewModel.unitTable\">\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitId\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitCategory\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitIsoTypeId\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitLineOpId\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: ufvTransitStateDesc\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitDocumentNbr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitPol\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitPod\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitVesselName\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitVoyage\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitLimitDateStr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: apptRequestedDateStr\"></label></td>\n" );
    sb.append (
        "    <td><button class=\"btn btn-icon btn-info btn-sm\" data-bind=\"click: $parent"
        + ".viewUnitSeals, enable: isImportInYardWithFFD\"> <i class=\"ti-view-list\"></i> "
        + "</button></td>\n" );
    sb.append (
        "    <td><button class=\"badge badge-white\" data-bind=\"text: unitServiceCount, click: "
        + "$parent.viewUnitServices, enable: hasService\"/> \n" );
    sb.append (
        "    <td><button class=\"btn btn-icon btn-primary btn-sm\" data-bind=\"click: $parent"
        + ".coordinateUnit, enable: allowToCreateAppt\"> <i class=\"ti-calendar\"></i> "
        + "</button></td>\n" );
    sb.append (
        "    <td><button class=\"btn btn-icon btn-danger btn-sm\" data-bind=\"click: $parent"
        + ".cancelAppt, enable: allowToCancelAppt\"> <i class=\"ti-close\"></i> </button></td>\n" );
    sb.append ( "</tr>\n" );
    return sb.toString ( );
  }

  private
  String getServiceOrderDataTableHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    sb.append ( "<thead>\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <th>Contenedor</th>\n" );
    sb.append ( "    <th>Servicio</th>\n" );
    sb.append ( "    <th>Fecha Servicio</th>\n" );
    sb.append ( "    <th>Estado Servicio</th>\n" );
    if ( this.canDeleteItem ) {
      sb.append ( "    <th>Anular</th>\n" );
    }

    sb.append ( "</tr>\n" );
    sb.append ( "</thead>\n" );
    sb.append ( "<tbody data-bind=\"foreach: coordinationViewModel.unitServiceTable\">\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitId\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: srvOrderTypeDescription\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: srvOrderStartDateStr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: srvOrderStatus\"></label></td>\n" );
    if ( this.canDeleteItem ) {
      sb.append (
          "    <td><button class=\"btn btn-icon btn-danger btn-sm\" data-bind=\"click: $parent"
          + ".cancelSo, enable: allowToCancelService\"> <i class=\"ti-close\"></i> "
          + "</button></td>\n" );
    }

    sb.append ( "</tr>\n" );
    return sb.toString ( );
  }

  private
  void addUnitDateToCalendar (
      List eventList , Map unitApptMap , String dateStr ,
      String dateNameField , String title , String color
                             ) {
    String   defaultColor  = "#AEFFA9";
    String[] dateStrArray  = dateStr.split ( " " );
    String   dateNoTimeStr = dateStrArray.length > 0 ? dateStrArray[ 0 ] : "";
    String   dateName      = ( String ) unitApptMap.get ( dateNameField );
    boolean addEvent = dateName != null && ! dateName.isEmpty ( )
                       || title != null && ! title.isEmpty ( );
    if ( addEvent && dateNoTimeStr != null && ! dateNoTimeStr.isEmpty ( ) ) {
      color = color != null && ! color.isEmpty ( ) ? color : defaultColor;
      title = title != null && ! title.isEmpty ( ) ? title : dateName;
      AppointmentOpeningDto dto = new AppointmentOpeningDto ( title , dateNoTimeStr , "" , "" , 0 ,
                                                              ( Long ) null , color , title
      );
      eventList.add ( dto );
    }

  }

  private
  boolean isValidUnitIdFormat ( String untiId ) {
    return untiId.matches ( "^[A-Z]{4}\\d{7}" );
  }

  private
  Map buildSuccessResponse ( String msg ) {
    return this.buildResponse ( "OK" , msg );
  }

  private
  Map buildErrorResponse ( String msg ) {
    return this.buildResponse ( "ERROR" , msg );
  }

  private
  Map buildQuestionResponse ( String msg , String questionType ) {
    Map map = this.buildResponse ( "QUESTION" , msg );
    if ( questionType != null && ! questionType.isEmpty ( ) ) {
      map.put ( "questionType" , questionType );
    }

    return map;
  }

  private
  Map buildWarningResponse ( String msg ) {
    return this.buildResponse ( "WARNING" , msg );
  }

  private
  Map buildResponse ( String status , String msg ) {
    this.logUser ( msg );
    Map map = new HashMap ( );
    map.put ( "status" , status );
    map.put ( "msg" , msg );
    this.clearLogPrefix ( );
    return map;
  }

  private
  void setUnitLogPrefix ( Map unitMap ) {
    Integer unitGkey     = ( Integer ) unitMap.get ( "unitGkey" );
    String  unitId       = ( String ) unitMap.get ( "unitId" );
    String  unitCategory = ( String ) unitMap.get ( "unitCategory" );
    this.setLogPrefix ( "Unit[" + unitGkey + "/" + unitId + "/" + unitCategory + "] " );
  }

  private
  void setOrderLogPrefix ( Map orderMap ) {
    Integer orderGkey        = ( Integer ) orderMap.get ( "documentGkey" );
    String  orderNbr         = ( String ) orderMap.get ( "documentNbr" );
    String  orderItemIsoType = ( String ) orderMap.get ( "documentItemIsoType" );
    this.setLogPrefix ( "Order[" + orderGkey + "/" + orderNbr + "/" + orderItemIsoType + "] " );
  }
}
