//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.restservice.controllers;

import ar.com.project.core.domain.AccountabilityLinkedUser;
import ar.com.project.core.domain.CustomerReport;
import ar.com.project.core.domain.CustomerReportDocument;
import ar.com.project.core.domain.CustomerReportFile;
import ar.com.project.core.domain.CustomerReportItem;
import ar.com.project.core.domain.User;
import ar.com.project.core.dto.BalanceDTO;
import ar.com.project.core.dto.CompositionBalanceDTO;
import ar.com.project.core.dto.CustomerInformDTO;
import ar.com.project.core.dto.DocumentAdditionalInformationDTO;
import ar.com.project.core.dto.FileDTO;
import ar.com.project.core.service.AccountabilityLinkedUserService;
import ar.com.project.core.service.AuditService;
import ar.com.project.core.service.BalanceService;
import ar.com.project.core.service.CustomerReportDocumentService;
import ar.com.project.core.service.CustomerReportFileService;
import ar.com.project.core.service.CustomerReportItemService;
import ar.com.project.core.service.CustomerReportService;
import ar.com.project.core.service.CustomerService;
import ar.com.project.core.service.LinkedUserService;
import ar.com.project.core.service.UserService;
import ar.com.project.core.utils.MessagesUtils;
import com.curcico.jproject.core.daos.ConditionSimple;
import com.curcico.jproject.core.daos.SearchOption;
import com.curcico.jproject.core.exception.BaseException;
import com.drew.lang.annotations.Nullable;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.File;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

@Controller
@Transactional
@SessionAttributes ( { "userid" , "parameters" , "firstname" , "lastname" })
@RequestMapping ( { "/balance" })
public
class BalanceController extends CommonController {

  private static final Logger logger          = Logger.getLogger ( BalanceController.class );
  private static final String APPLICATION_PDF = "application/x-download";
  @Autowired
  UserService                     userService;
  @Autowired
  CustomerService                 customerService;
  @Autowired
  BalanceService                  balanceService;
  @Autowired
  LinkedUserService               linkedUserService;
  @Autowired
  CustomerReportService           customerReportService;
  @Autowired
  CustomerReportDocumentService   customerReportDocumentService;
  @Autowired
  CustomerReportItemService       customerReportItemService;
  @Autowired
  CustomerReportFileService       customerReportFileService;
  @Autowired
  AuditService                    auditService;
  @Autowired
  AccountabilityLinkedUserService accountabilityLinkedUserService;

  public
  BalanceController ( ) {
  }

  private static
  String encodeFileToBase64 ( File file ) {
    try {
      byte[] fileContent = Files.readAllBytes ( file.toPath ( ) );
      return Base64.getEncoder ( ).encodeToString ( fileContent );
    }
    catch ( IOException var2 ) {
      throw new IllegalStateException ( "could not read file " + file , var2 );
    }
  }

  @RequestMapping (
      value = { "/balanceCompositionView" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "hasAnyRole('Usuario Puerto Digital','Usuario Cuenta Corriente')")
  public
  ModelAndView balanceCompositionView ( @ModelAttribute ( "userid") String userid )
  throws BaseException {
    logger.info ( "balanceCompositionView" );
    return new ModelAndView ( "balanceCompositionView" );
  }

  @RequestMapping (
      value = { "/customerReportView" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  public
  ModelAndView customerReportView ( @ModelAttribute ( "userid") String userid )
  throws BaseException {
    logger.info ( "customerReportView" );
    return new ModelAndView ( "customerReportView" );
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/getMyCompositionBalance" }
  )
  @PreAuthorize ( "hasAnyRole('Usuario Puerto Digital','Usuario Cuenta Corriente')")
  @ResponseBody
  public
  ResponseEntity < Object > getMyCompositionBalance ( @RequestBody String data ) {
    try {
      JSONParser jsonParser  = new JSONParser ( );
      JSONObject jsonObject  = ( JSONObject ) jsonParser.parse ( data );
      String     documentNbr = ( String ) jsonObject.get ( "documentNbr" );
      int        accountNbr  = this.customerService.getAccountNbrByCustomerId ( documentNbr );
      List < CompositionBalanceDTO > comps =
          this.balanceService.findCompositionBalanceByAccountNbr (
              accountNbr );
      return comps == null ? new ResponseEntity ( ( MultiValueMap ) null , HttpStatus.OK )
                           : new ResponseEntity ( comps , HttpStatus.OK );
    }
    catch ( Exception var7 ) {
      return new ResponseEntity ( "Error al cargar el listado" , HttpStatus.CONFLICT );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/getValuesAssociatedByDocumentNbr" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getValuesAssociatedByDocumentNbr ( @RequestBody String data ) {
    try {
      JSONParser jsonParser   = new JSONParser ( );
      JSONObject jsonObject   = ( JSONObject ) jsonParser.parse ( data );
      String     documentNbr  = ( String ) jsonObject.get ( "documentNbr" );
      String     documentDate = ( String ) jsonObject.get ( "documentDate" );
      DocumentAdditionalInformationDTO values =
          this.balanceService.getValuesAssociatedByDocumentNbr (
              documentNbr , documentDate );
      return values == null ? new ResponseEntity ( ( MultiValueMap ) null , HttpStatus.OK )
                            : new ResponseEntity ( values , HttpStatus.OK );
    }
    catch ( Exception var7 ) {
      return new ResponseEntity ( "Error al cargar el listado" , HttpStatus.CONFLICT );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/getCustomerInformationById" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getCustomerInformationById ( @RequestBody String data ) {
    try {
      ArrayList < String > linkedCuits = new ArrayList ( );
      JSONParser           jsonParser  = new JSONParser ( );
      JSONObject           jsonObject  = ( JSONObject ) jsonParser.parse ( data );
      String               documentNbr = ( String ) jsonObject.get ( "documentNbr" );
      linkedCuits.add ( documentNbr );
      List < BalanceDTO > balances = this.userService.getCustomerBalanceByTaxIds ( linkedCuits );
      Iterator            var8     = balances.iterator ( );

      while ( var8.hasNext ( ) ) {
        BalanceDTO balanceDTO = ( BalanceDTO ) var8.next ( );
        if ( balanceDTO.getCustomerCreditStatus ( ).equalsIgnoreCase ( "CASH" ) ) {
          balanceDTO.setCustomerCreditLimit ( 0.0 );
        }
      }

      return new ResponseEntity ( balances , HttpStatus.OK );
    }
    catch ( Exception var9 ) {
      return new ResponseEntity ( var9.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/getMyCustomers" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getMyCustomers ( ) {
    try {
      ArrayList < String > linkedCuits = new ArrayList ( );
      linkedCuits.add ( this.getUserContext ( ).getUsername ( ) );
      List < ConditionSimple > parametersFilters = new ArrayList ( );
      parametersFilters.add ( new ConditionSimple ( "linkedUserGkey" , SearchOption.EQUAL ,
                                                    this.userService.getUser (
                                                            this.getUserContext ( ).getUsername ( ) )
                                                                    .getGkey ( )
      ) );
      List < AccountabilityLinkedUser > results =
          ( List ) this.accountabilityLinkedUserService.findByFilters (
              parametersFilters );

      for ( int i = 0 ; i < results.size ( ) ; ++ i ) {
        parametersFilters.clear ( );
        parametersFilters.add ( new ConditionSimple ( "gkey" , SearchOption.EQUAL ,
                                                      (
                                                          ( AccountabilityLinkedUser ) results.get (
                                                              i )
                                                      ).getRequesterUserGkey ( )
        ) );
        linkedCuits.add (
            ( ( User ) this.userService.findEntityByFilters ( parametersFilters ) ).getLogin ( ) );
      }

      List < BalanceDTO > balances = this.userService.getCustomerBalanceByTaxIds ( linkedCuits );
      JSONArray           array    = new JSONArray ( );
      Iterator            var7     = balances.iterator ( );

      while ( var7.hasNext ( ) ) {
        BalanceDTO balanceDTO = ( BalanceDTO ) var7.next ( );
        JSONObject item       = new JSONObject ( );
        item.put ( "customerName" , balanceDTO.getCustomerName ( ) );
        item.put ( "customerTaxId" , balanceDTO.getCustomerTaxId ( ) );
        array.add ( item );
      }

      return new ResponseEntity ( array , HttpStatus.OK );
    }
    catch ( Exception var9 ) {
      return new ResponseEntity ( var9.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  private
  Map buildSuccessResponse ( String msg ) {
    return this.buildResponse ( "OK" , msg );
  }

  private
  Map buildErrorResponse ( String msg ) {
    return this.buildResponse ( "ERROR" , msg );
  }

  private
  Map buildQuestionResponse ( String msg , String questionType ) {
    Map map = this.buildResponse ( "QUESTION" , msg );
    if ( questionType != null && ! questionType.isEmpty ( ) ) {
      map.put ( "questionType" , questionType );
    }

    return map;
  }

  private
  Map buildWarningResponse ( String msg ) {
    return this.buildResponse ( "WARNING" , msg );
  }

  private
  Map buildResponse ( String status , String msg ) {
    this.logUser ( msg );
    Map map = new HashMap ( );
    map.put ( "status" , status );
    map.put ( "msg" , msg );
    this.clearLogPrefix ( );
    return map;
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/getCustomerReportsDefault" }
  )
  @PreAuthorize ( "hasAnyRole('COMPBAL_FIN_TEAM')")
  @ResponseBody
  public
  ResponseEntity < Object > getCustomerReportsDefault ( ) {
    try {
      List < CustomerInformDTO > informs = this.customerReportService.getCustomerReports (
          ( Integer ) null );
      return new ResponseEntity ( informs , HttpStatus.OK );
    }
    catch ( Exception var2 ) {
      return new ResponseEntity ( var2.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/getCustomerReports/{customerCuit}" }
  )
  @PreAuthorize ( "hasAnyRole('COMPBAL_FIN_TEAM')")
  @ResponseBody
  public
  ResponseEntity < Object > getCustomerReports ( @PathVariable String customerCuit ) {
    try {
      User customer = this.userService.getUser ( customerCuit );
      if ( customer == null ) {
        return new ResponseEntity (
            "No se pudo encontrar información sobre el cliente [" + customerCuit + "]" ,
            HttpStatus.BAD_REQUEST
        );
      }
      else {
        List < CustomerInformDTO > informs = this.customerReportService.getCustomerReports (
            customer.getGkey ( ) );
        return new ResponseEntity ( informs , HttpStatus.OK );
      }
    }
    catch ( Exception var4 ) {
      return new ResponseEntity ( var4.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/createPresentationByCustomer" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > createPresentationByCustomer (
      MultipartHttpServletRequest request ,
      @RequestParam ( "jsonObject") String data
                                                         ) {
    try {
      String         usrCreator     = this.getUserContext ( ).getUsername ( );
      JSONParser     jsonParser     = new JSONParser ( );
      JSONObject     jsonObject     = ( JSONObject ) jsonParser.parse ( data );
      String         customerNumber = ( String ) jsonObject.get ( "customer" );
      JSONArray      docs           = ( JSONArray ) jsonObject.get ( "docs" );
      JSONArray      items          = ( JSONArray ) jsonObject.get ( "items" );
      CustomerReport cusReport      = new CustomerReport ( );
      cusReport.setStatus ( "RECIBIDA" );
      cusReport.setCustomerKey ( this.userService.getUser ( customerNumber ).getGkey ( ) );
      cusReport.setCreator ( this.getUserContext ( ).getUsername ( ) );
      cusReport.setLifeCycleState ( "ACT" );
      cusReport.setReportNumber ( this.returnLastReportNumber ( ) );
      cusReport = ( CustomerReport ) this.customerReportService.createOrUpdate (
          cusReport , usrCreator );
      if ( cusReport.getReportNumber ( ) == null ) {
        return new ResponseEntity (
            "No se pudo realizar la presentación. Intente nuevamente más tarde." ,
            HttpStatus.BAD_REQUEST
        );
      }
      else {
        int        i;
        JSONObject item;
        for ( i = 0; i < docs.size ( ) ; ++ i ) {
          item = ( JSONObject ) docs.get ( i );
          CustomerReportDocument reportDocument = new CustomerReportDocument ( );
          reportDocument.setReportGkey ( cusReport.getGkey ( ) );
          reportDocument.setDocumentAmount (
              Float.valueOf ( ( ( String ) item.get ( "documentAmount" ) ).replaceAll (
                  "," , "" ) ) );
          reportDocument.setDocumentDate ( ( String ) item.get ( "documentDate" ) );
          reportDocument.setDocumentNumber ( item.get ( "documentNumber" ).toString ( ) );
          reportDocument.setCreator ( usrCreator );
          reportDocument.setLifeCycleState ( "ACT" );
          reportDocument =
              ( CustomerReportDocument ) this.customerReportDocumentService.createOrUpdate (
                  reportDocument , usrCreator );
        }

        for ( i = 0; i < items.size ( ) ; ++ i ) {
          item = ( JSONObject ) items.get ( i );
          CustomerReportItem reportItem = new CustomerReportItem ( );
          reportItem.setReportGkey ( cusReport.getGkey ( ) );
          reportItem.setItemType ( ( String ) item.get ( "type" ) );
          reportItem.setItemString01 ( ( String ) item.get ( "concept" ) );
          reportItem.setItemDate ( ( String ) item.get ( "date" ) );
          reportItem.setItemReference ( ( String ) item.get ( "reference" ) );
          if ( item.get ( "amount" ) instanceof Double ) {
            reportItem.setItemValue ( ( Float ) item.get ( "amount" ) );
          }
          else {
            reportItem.setItemValue (
                Float.valueOf ( ( ( String ) item.get ( "amount" ) ).replaceAll ( "," , "" ) ) );
          }

          reportItem.setCreator ( usrCreator );
          reportItem.setLifeCycleState ( "ACT" );
          reportItem = ( CustomerReportItem ) this.customerReportItemService.createOrUpdate (
              reportItem , usrCreator );
        }

        CustomerReportFile reportFile;
        String             sender;
        String             messageResult;
        for (
            Iterator < String > itr = request.getFileNames ( ) ; itr.hasNext ( ) ;
            reportFile = ( CustomerReportFile ) this.customerReportFileService.createOrUpdate (
                reportFile , usrCreator )
        ) {
          sender        = "C:\\Temp\\Presentaciones";
          messageResult = sender + "\\" + cusReport.getReportNumber ( ).toString ( );
          reportFile    = new CustomerReportFile ( );
          reportFile.setReportGkey ( cusReport.getGkey ( ) );
          File folder = new File ( messageResult );
          if ( ! folder.exists ( ) ) {
            folder.mkdir ( );
          }

          String        uploadedFile = ( String ) itr.next ( );
          MultipartFile file         = request.getFile ( uploadedFile );
          String        fileName     = file.getOriginalFilename ( );
          String        filePath     = messageResult + "\\" + fileName;
          long bytes = Files.copy ( file.getInputStream ( ) , Paths.get ( filePath ) ,
                                    new CopyOption[] { StandardCopyOption.REPLACE_EXISTING }
                                  );
          if ( bytes > 0L ) {
            reportFile.setFilePath ( filePath );
            reportFile.setCreator ( usrCreator );
            reportFile.setLifeCycleState ( "ACT" );
          }
        }

        if ( cusReport.getGkey ( ) != null ) {
          sender        = MessagesUtils.getBundle ( "email.sender.presentaciones.pago" );
          messageResult = this.balanceService.sendUserEmailUpdateStatus (
              this.userService.getUser ( cusReport.getCreator ( ) ).getEmailAddress ( ) ,
              cusReport.getStatus ( ) , sender
                                                                        );
          return new ResponseEntity ( messageResult , HttpStatus.OK );
        }
        else {
          return new ResponseEntity ( ( MultiValueMap ) null , HttpStatus.BAD_REQUEST );
        }
      }
    }
    catch ( Exception var21 ) {
      return new ResponseEntity ( var21.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  private
  Integer returnLastReportNumber ( ) {
    Integer reportNumber = this.balanceService.returnLastReportNumber ( );
    return reportNumber;
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/updateReportStatus/{reportNumber}/{stage}/{reason}" }
  )
  @PreAuthorize ( "hasAnyRole('PREST_PAGO_FIN_TEAM')")
  @ResponseBody
  public
  ResponseEntity < Object > updateReportStatus (
      @PathVariable Integer reportNumber ,
      @PathVariable String stage , @PathVariable @Nullable String reason
                                               ) {
    try {
      List < ConditionSimple > parametersFilters = new ArrayList ( );
      parametersFilters.add (
          new ConditionSimple ( "reportNumber" , SearchOption.EQUAL , reportNumber ) );
      CustomerReport report = ( CustomerReport ) this.customerReportService.findEntityByFilters (
          parametersFilters );
      String userChanger = this.getUserContext ( ).getUsername ( );
      String dataAudit =
          "Reporte Nro: " + reportNumber + " cambio su estado de " + report.getStatus ( ) + " -> "
          + stage + ". Usuario auditado: " + userChanger;
      boolean haveReason = false;
      report.setStatus ( this.getStage ( stage ) );
      report.setChanger ( userChanger );
      if ( stage.equalsIgnoreCase ( "-1" ) ) {
        haveReason = true;
        report.setNotes ( reason );
      }

      report.setChanged ( new Timestamp ( Calendar.getInstance ( ).getTimeInMillis ( ) ) );
      this.auditService.saveAuditTransaction ( "updateReportStatus" , dataAudit , userChanger );
      report = ( CustomerReport ) this.customerReportService.createOrUpdate (
          report , userChanger );
      String sender        = MessagesUtils.getBundle ( "email.sender.presentaciones.pago" );
      String messageResult = "";
      if ( ! haveReason ) {
        messageResult = this.balanceService.sendUserEmailUpdateStatus (
            this.userService.getUser ( report.getCreator ( ) ).getEmailAddress ( ) ,
            report.getStatus ( ) ,
            sender
                                                                      );
      }
      else {
        messageResult = this.balanceService.sendUserEmailUpdateStatus (
            this.userService.getUser ( report.getCreator ( ) ).getEmailAddress ( ) ,
            report.getStatus ( ) ,
            sender , report.getNotes ( )
                                                                      );
      }

      return new ResponseEntity ( messageResult , HttpStatus.OK );
    }
    catch ( Exception var11 ) {
      return new ResponseEntity ( var11.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/deleteMyReport/{reportNumber}" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > deleteMyReport ( @PathVariable Integer reportNumber ) {
    try {
      List < ConditionSimple > parametersFilters = new ArrayList ( );
      parametersFilters.add (
          new ConditionSimple ( "reportNumber" , SearchOption.EQUAL , reportNumber ) );
      CustomerReport report = ( CustomerReport ) this.customerReportService.findEntityByFilters (
          parametersFilters );
      String userChanger = this.getUserContext ( ).getUsername ( );
      if ( ! report.getCreator ( ).equalsIgnoreCase ( userChanger ) ) {
        return new ResponseEntity (
            "El reporte no puede ser eliminado, no se encuentra autorizado para realizar esta "
            + "acción." ,
            HttpStatus.UNAUTHORIZED
        );
      }
      else {
        String dataAudit =
            "Reporte Nro: " + reportNumber + " ha sido eliminado (OBS). Usuario auditado: "
            + userChanger;
        if ( report.getStatus ( ).equalsIgnoreCase ( "RECIBIDA" ) && report.getCreator ( )
                                                                           .equalsIgnoreCase (
                                                                               userChanger ) ) {
          report.setStatus ( "CANCELADO USUARIO" );
          report.setChanger ( userChanger );
          report.setChanged ( new Timestamp ( Calendar.getInstance ( ).getTimeInMillis ( ) ) );
          this.auditService.saveAuditTransaction ( "deleteMyReport" , dataAudit , userChanger );
          report = ( CustomerReport ) this.customerReportService.createOrUpdate (
              report , userChanger );
          String sender        = MessagesUtils.getBundle ( "email.sender.presentaciones.pago" );
          String messageResult = MessagesUtils.getBundle ( "stage.cancelado.usuario" );
          this.balanceService.sendUserEmailUpdateStatus (
              this.userService.getUser ( report.getCreator ( ) ).getEmailAddress ( ) ,
              report.getStatus ( ) ,
              sender
                                                        );
          return new ResponseEntity ( messageResult , HttpStatus.OK );
        }
        else {
          return new ResponseEntity (
              "El reporte no puede ser eliminado, ya que no se encuentra en estado [RECIBIDA]" ,
              HttpStatus.BAD_REQUEST
          );
        }
      }
    }
    catch ( Exception var8 ) {
      return new ResponseEntity ( var8.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  private
  String getStage ( String stage ) {
    switch ( stage ) {
      case "1":
        return "RECIBIDA";
      case "2":
        return "EN PROCESO";
      case "3":
        return "CONTABILIZADA";
      case "-1":
        return "RECHAZADA";
    }

    return null;
  }

  @JsonIgnore
  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/downloadFile/{fileGkey}" },
      produces = { "application/x-download" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > downloadFile (
      HttpServletRequest request ,
      HttpServletResponse response , @PathVariable Integer fileGkey
                                         ) {
    try {
      List < ConditionSimple > parametersFilters = new ArrayList ( );
      parametersFilters.add ( new ConditionSimple ( "gkey" , SearchOption.EQUAL , fileGkey ) );
      CustomerReportFile repfile =
          ( CustomerReportFile ) this.customerReportFileService.findEntityByFilters (
              parametersFilters );
      File    file     = new File ( repfile.getFilePath ( ) );
      FileDTO fDto     = new FileDTO ( );
      String  filename = file.getName ( );
      fDto.setFilename ( filename );
      String data = Base64.getEncoder ( )
                          .encodeToString ( com.google.common.io.Files.toByteArray ( file ) );
      fDto.setBase64data ( data );
      return new ResponseEntity ( fDto , HttpStatus.OK );
    }
    catch ( Exception var10 ) {
      String message = "No se puede descargar el archivo: " + var10.getMessage ( );
      logger.error ( message , var10 );
      return new ResponseEntity ( message , HttpStatus.UNSUPPORTED_MEDIA_TYPE );
    }
  }
}
