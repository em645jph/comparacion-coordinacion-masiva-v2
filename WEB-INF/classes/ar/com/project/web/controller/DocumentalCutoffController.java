//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.restservice.controllers;

import ar.com.project.core.domain.User;
import ar.com.project.core.dto.CustomsLoadVisitDTO;
import ar.com.project.core.service.DocumentalCutoffService;
import ar.com.project.core.service.PrivilegeService;
import ar.com.project.core.service.RoleService;
import ar.com.project.core.service.UserService;
import com.curcico.jproject.core.exception.BaseException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@SessionAttributes ( {
    "userid" , "loginName" , "username" , "parameters" , "fullname" , "firstNameLetter"
}
)
@RequestMapping ( { "/DocumentalCutoff" })
public
class DocumentalCutoffController extends CommonController {

  private static final Logger logger            = Logger.getLogger (
      DocumentalCutoffController.class );
  private final        String PRIVILEGE_ID_VIEW = "DOCUMENTAL_CUTOFF_VIEW";
  boolean canEditItem = false;
  User    user;
  @Autowired
  RoleService             roleService;
  @Autowired
  PrivilegeService        privilegeService;
  @Autowired
  UserService             userService;
  @Autowired
  DocumentalCutoffService documentalCutoffService;
  private List privilegeInPageList = new ArrayList ( );

  public
  DocumentalCutoffController ( ) {
  }

  @RequestMapping (
      value = { "/documentalCutoff" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  public
  ModelAndView documentalCutoff ( HttpServletRequest request ) throws BaseException {
    logger.info ( "documentalCutoff" );
    this.privilegeInPageList = this.getUserPrivilegeList ( request );
    if ( ! this.privilegeService.userCanViewPage (
        this.privilegeInPageList ,
        "DOCUMENTAL_CUTOFF_VIEW"
                                                 ) ) {
      return this.redirectTo404 ( );
    }
    else {
      ModelAndView m           = new ModelAndView ( "documentalCutoff" );
      String       servletPath = request.getServletPath ( );
      org.springframework.security.core.userdetails.User loggedUser =
          ( org.springframework.security.core.userdetails.User ) SecurityContextHolder
              .getContext ( )
              .getAuthentication ( ).getPrincipal ( );
      String login = loggedUser.getUsername ( );
      this.canEditItem = this.privilegeService.userCanEditItem ( this.privilegeInPageList );
      return m;
    }
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/findLoadVisitByParam" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > findLoadVisitByParam ( @RequestParam String param ) {
    try {
      List < CustomsLoadVisitDTO > loadVessel = this.documentalCutoffService.findLoadVisitByParam (
          param );
      Map resultMap = new HashMap ( );
      resultMap.put ( "hideEdit" , ! this.canEditItem );
      resultMap.put ( "vesselVisitList" , loadVessel );
      return new ResponseEntity ( resultMap , HttpStatus.OK );
    }
    catch ( Exception var4 ) {
      return new ResponseEntity ( "Error al cargar el listado" , HttpStatus.CONFLICT );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/addOrUpdateCutoff" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > addOrUpdateCutoff (
      @RequestParam String visitId ,
      @RequestParam String cutoffDate
                                              ) throws BaseException {
    if ( this.canEditItem ) {
      String userLogin      = this.getUserLogin ( );
      Date   cutoffEditDate = this.ParseDate ( cutoffDate );
      String result = this.documentalCutoffService.addOrUpdateCutoff ( visitId , cutoffEditDate ,
                                                                       userLogin
                                                                     );
      return ! result.isEmpty ( ) ? new ResponseEntity (
          this.buildErrorResponse ( result ) , HttpStatus.OK )
                                  : new ResponseEntity (
                                      this.buildSuccessResponse (
                                          "Cutoff Actualizado Correctamente" ) ,
                                      HttpStatus.OK
                                  );
    }
    else {
      return new ResponseEntity (
          this.buildErrorResponse ( "No tiene permiso para realizar esta acción" ) ,
          HttpStatus.OK
      );
    }
  }

  @RequestMapping (
      value = { "/getHistoryEvents" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getHistoryEvents ( @RequestParam String visitId ) {
    try {
      List eventList = this.documentalCutoffService.getHistoryEvents ( ( Long ) null , visitId );
      return new ResponseEntity ( eventList , HttpStatus.OK );
    }
    catch ( Exception var3 ) {
      return new ResponseEntity (
          "Error al cargar el listado." + var3.getMessage ( ) ,
          HttpStatus.BAD_REQUEST
      );
    }
  }

  private
  String getUserLogin ( ) {
    org.springframework.security.core.userdetails.User loggedUser =
        ( org.springframework.security.core.userdetails.User ) SecurityContextHolder
            .getContext ( )
            .getAuthentication ( ).getPrincipal ( );
    String login = loggedUser.getUsername ( );
    return login;
  }

  private
  Date ParseDate ( String date ) {
    Date cutoffDate = new Date ( );

    try {
      cutoffDate = ( new SimpleDateFormat ( "MM/dd/yyyy HH:mm:ss" ) ).parse ( date );
    }
    catch ( ParseException var4 ) {
      System.out.println ( var4 );
    }

    return cutoffDate;
  }

  private
  Map buildErrorResponse ( String msg ) {
    return this.buildResponse ( "ERROR" , msg );
  }

  private
  Map buildResponse ( String status , String msg ) {
    Map map = new HashMap ( );
    map.put ( "status" , status );
    map.put ( "msg" , msg );
    return map;
  }

  private
  Map buildSuccessResponse ( String msg ) {
    return this.buildResponse ( "OK" , msg );
  }

  private
  List getUserPrivilegeList ( HttpServletRequest request ) {
    String servletPath = request.getServletPath ( );
    String login       = this.getUserLogin ( );
    return this.privilegeService.getMenuPagePrivileges ( login , servletPath );
  }
}
