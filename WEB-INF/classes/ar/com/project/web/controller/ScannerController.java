//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//


package com.example.restservice.controllers;

import ar.com.project.core.dto.ShipmentCommnucationSrvDTO;
import ar.com.project.core.dto.UnitShipmenCommunicationParameterDTO;
import ar.com.project.core.service.CustomerScannerService;
import ar.com.project.core.service.CustomerShipmentCommunicationService;
import ar.com.project.core.service.PrivilegeService;
import ar.com.project.core.service.RoleService;
import ar.com.project.core.service.UserService;
import ar.com.project.core.utils.EnumUtil;
import com.curcico.jproject.core.exception.BaseException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@SessionAttributes ( {
    "userid" , "loginName" , "username" , "parameters" , "fullname" , "firstNameLetter"
}
)
@RequestMapping ( { "/ScannerManagement" })
public
class ScannerController extends CommonController {

  private static final Logger logger             = Logger.getLogger ( ScannerController.class );
  private final        String PRIVILEGE_ID_BLOCK = "SCANNER_BLOCK";
  private final        String PRIVILEGE_ID_VIEW  = "SCANNER_VIEW";
  @Autowired
  RoleService                          roleService;
  @Autowired
  UserService                          userService;
  @Autowired
  PrivilegeService                     privilegeService;
  @Autowired
  CustomerScannerService               customerScannerService;
  @Autowired
  CustomerShipmentCommunicationService customerShipmentCommunicationService;

  public
  ScannerController ( ) {
  }

  @RequestMapping (
      value = { "/scanner" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  public
  ModelAndView scanner ( HttpServletRequest request ) throws BaseException {
    logger.info ( "scanner" );
    List privilegeList = this.getUserPrivilegeList ( request );
    if ( ! this.privilegeService.userCanViewPage ( privilegeList , "SCANNER_VIEW" ) ) {
      return this.redirectTo404 ( );
    }
    else {
      ModelAndView m = new ModelAndView ( "scanner" );
      return m;
    }
  }

  @RequestMapping (
      value = { "/loadTypeBlock" },
      method = { RequestMethod.GET }
  )
  @ResponseBody
  @PreAuthorize ( "isAuthenticated()")
  public
  ResponseEntity < List > loadTypeBlock ( ) {
    ResponseEntity response = null;

    try {
      List lockTypesShipmentCommunicationList = EnumUtil.getShipmentCommunicationLockTypesList ( );
      Map  resultMap                          = new HashMap ( );
      resultMap.put ( "lockTypesShipmentCommunicationList" , lockTypesShipmentCommunicationList );
      response = new ResponseEntity ( resultMap , HttpStatus.OK );
    }
    catch ( Exception var4 ) {
      response = new ResponseEntity (
          "No se pudo cargar correctamente la informaciï¿½n. Por favor refresque la pï¿½gina" ,
          HttpStatus.BAD_REQUEST
      );
      var4.printStackTrace ( );
    }

    return response;
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/findUnitBlockScanner" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > findUnitScanner ( @RequestParam String unitId ) {
    ResponseEntity < Object > responseEntity = null;
    new ArrayList ( );

    try {
      List < ShipmentCommnucationSrvDTO > unitBlockScanner =
          this.customerScannerService.getContainerPendBlockScanner (
              ( String ) null , unitId , ( String ) null , ( String ) null ,
              EnumUtil.getUfvTransitStateFilter (
                  false , false , true , false , false , false , false , false )
                                                                   );
      Map resultMap = new HashMap ( );
      resultMap.put ( "unitBlockScanner" , unitBlockScanner );
      return new ResponseEntity ( resultMap , HttpStatus.OK );
    }
    catch ( Exception var5 ) {
      return new ResponseEntity ( "Error al cargar el listado" , HttpStatus.CONFLICT );
    }
  }

  @RequestMapping (
      value = { "/customsBlockScanner" },
      method = { RequestMethod.POST }
  )
  @ResponseBody
  @PreAuthorize ( "isAuthenticated()")
  public
  ResponseEntity < Object > customsBlockScanner (
      HttpServletRequest request ,
      @RequestBody UnitShipmenCommunicationParameterDTO unitShipmenCommunicationParameter
                                                ) {
    List   privilegeList = this.getUserPrivilegeList ( request );
    String userId        = this.getUserLogin ( );

    ResponseEntity responseEntity;
    try {
      String updateResult = this.customerScannerService.updateStatusUnitShipmentComunication (
          unitShipmenCommunicationParameter , privilegeList , userId , "SCANNER_BLOCK" );
      responseEntity = new ResponseEntity ( updateResult , HttpStatus.OK );
    }
    catch ( IOException var7 ) {
      var7.printStackTrace ( );
      responseEntity = new ResponseEntity ( var7.getMessage ( ) , HttpStatus.EXPECTATION_FAILED );
    }
    catch ( BaseException var8 ) {
      responseEntity = new ResponseEntity ( var8.getMessage ( ) , HttpStatus.EXPECTATION_FAILED );
      var8.printStackTrace ( );
    }
    catch ( SQLException var9 ) {
      var9.printStackTrace ( );
      responseEntity = new ResponseEntity ( var9.getMessage ( ) , HttpStatus.EXPECTATION_FAILED );
    }

    return responseEntity;
  }

  private
  String getUserLogin ( ) {
    return this.getUserContext ( ).getUsername ( );
  }

  private
  List getUserPrivilegeList ( HttpServletRequest request ) {
    String servletPath = request.getServletPath ( );
    String login       = this.getUserLogin ( );
    return this.privilegeService.getMenuPagePrivileges ( login , servletPath );
  }
}
