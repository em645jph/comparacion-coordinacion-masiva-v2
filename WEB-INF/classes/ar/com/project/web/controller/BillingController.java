//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.restservice.controllers;

import ar.com.project.core.dto.BillingUnitExportDTO;
import ar.com.project.core.dto.BillingUnitImportDTO;
import ar.com.project.core.dto.CueDTO;
import ar.com.project.core.dto.DocumentApptDTO;
import ar.com.project.core.service.AppointmentService;
import ar.com.project.core.service.CoordinationService;
import ar.com.project.core.service.InvoiceService;
import ar.com.project.core.service.LineOpService;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.NumberUtils;
import org.apache.log4j.Logger;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes ( { "userid" , "parameters" , "firstname" , "lastname" })
@RequestMapping ( { "/Billing" })
public
class BillingController extends CommonController {

  private static final Logger logger = Logger.getLogger ( BillingController.class );
  @Autowired
  InvoiceService      invoiceService;
  @Autowired
  CoordinationService coordinationService;
  @Autowired
  LineOpService       lineOpService;
  @Autowired
  AppointmentService  apptService;

  public
  BillingController ( ) {
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/findUnitsByBooking/{bkg}/{lineId}" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > findUnitsByBooking (
      @PathVariable String bkg ,
      @PathVariable String lineId
                                               ) {
    try {
      List < BillingUnitExportDTO > units = this.invoiceService.findUnitsByBookingAndLine (
          bkg , lineId );
      return units == null ? new ResponseEntity ( ( MultiValueMap ) null , HttpStatus.OK )
                           : new ResponseEntity ( units , HttpStatus.OK );
    }
    catch ( Exception var4 ) {
      return new ResponseEntity ( "Error al cargar el listado" , HttpStatus.CONFLICT );
    }
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/findUnitsByType/{type}/{value}" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > findUnitsByType (
      @PathVariable String type ,
      @PathVariable String value
                                            ) {
    try {
      List < BillingUnitImportDTO > units = this.invoiceService.findUnitsByType ( type , value );
      return units == null ? new ResponseEntity ( ( MultiValueMap ) null , HttpStatus.OK )
                           : new ResponseEntity ( units , HttpStatus.OK );
    }
    catch ( Exception var4 ) {
      return new ResponseEntity ( "Error al cargar el listado" , HttpStatus.CONFLICT );
    }
  }

  @RequestMapping (
      value = { "/findReceiveEmptyUnit" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > findReceiveEmptyUnit ( @RequestParam String prUnitId )
  throws SQLException, ServiceException, IOException {
    if ( prUnitId != null && ! prUnitId.isEmpty ( ) ) {
      prUnitId = prUnitId.replaceAll ( "\n" , "" ).replaceAll ( "\t" , "" ).replaceAll ( "\r" , "" )
                         .replaceAll ( " " , "" ).toUpperCase ( );
      List unitIdList = Arrays.asList ( prUnitId.split ( "," ) );
      return new ResponseEntity (
          this.coordinationService.findStorageUnit ( unitIdList ) ,
          HttpStatus.OK
      );
    }
    else {
      return new ResponseEntity (
          this.buildErrorResponse ( "Por favor ingrese algún número de contenedor" ) ,
          HttpStatus.OK
      );
    }
  }

  @RequestMapping (
      value = { "/findManifestList" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > findManifestList ( @RequestParam String prUnitId )
  throws SQLException, ServiceException, IOException {
    if ( prUnitId != null && ! prUnitId.isEmpty ( ) ) {
      prUnitId = prUnitId.replaceAll ( "\n" , "" ).replaceAll ( "\t" , "" ).replaceAll ( "\r" , "" )
                         .replaceAll ( " " , "" ).toUpperCase ( );
      List   manifestList    = new ArrayList ( Arrays.asList ( prUnitId.split ( "," ) ) );
      List   manifestCueList = this.invoiceService.findManifestCueList ( manifestList );
      String compareResult   = this.compareManifestCueList ( manifestList , manifestCueList );
      Map    resultMap       = new HashMap ( );
      if ( ! compareResult.isEmpty ( ) ) {
        resultMap.put ( "notFoundManifest" , compareResult );
      }

      resultMap.put ( "manifestCueList" , manifestCueList );
      return new ResponseEntity ( resultMap , HttpStatus.OK );
    }
    else {
      return new ResponseEntity (
          this.buildErrorResponse ( "Por favor ingrese algún número de manifiesto" ) ,
          HttpStatus.OK
      );
    }
  }

  @RequestMapping (
      value = { "/findAppointmentList" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > findAppointmentList (
      @RequestParam String prOrderNbr ,
      @RequestParam String prLineOpId , @RequestParam String prApptNbrStr
                                                )
  throws SQLException, ServiceException, IOException {
    if ( prOrderNbr != null && ! prOrderNbr.isEmpty ( )
         || prApptNbrStr != null && ! prApptNbrStr.isEmpty ( ) ) {
      prOrderNbr = prOrderNbr.replaceAll ( "\n" , "" ).replaceAll ( "\t" , "" ).replaceAll (
                                 "\r" , "" )
                             .replaceAll ( " " , "" ).toUpperCase ( );
      List searchApptNbrList = new ArrayList ( );
      if ( prApptNbrStr != null && ! prApptNbrStr.isEmpty ( ) ) {
        prApptNbrStr = prApptNbrStr.replaceAll ( "\n" , "" ).replaceAll ( "\t" , "" ).replaceAll (
                                       "\r" , "" )
                                   .replaceAll ( " " , "" ).toUpperCase ( );
        List tempList = new ArrayList ( Arrays.asList ( prApptNbrStr.split ( "," ) ) );

        for ( int i = 0 ; i < tempList.size ( ) ; ++ i ) {
          String apptNbrStr = ( String ) tempList.get ( i );
          if ( ! NumberUtils.isNumber ( apptNbrStr ) ) {
            return new ResponseEntity (
                this.buildErrorResponse ( "Número de turno " + apptNbrStr + " inválido" ) ,
                HttpStatus.OK
            );
          }

          Long apptNbr = Long.parseLong ( apptNbrStr );
          if ( ! searchApptNbrList.contains ( apptNbr ) ) {
            searchApptNbrList.add ( apptNbr );
          }
        }
      }

      DocumentApptDTO documentDto = null;
      if ( prOrderNbr != null && ! prOrderNbr.isEmpty ( ) ) {
        List documentApptList = this.coordinationService.findDocument ( "APPT" , prOrderNbr ,
                                                                        prLineOpId
                                                                      );
        if ( documentApptList.isEmpty ( ) ) {
          return new ResponseEntity (
              this.buildErrorResponse ( "No se encontró el número de Booking/EDO ingresado" ) ,
              HttpStatus.OK
          );
        }

        if ( documentApptList.size ( ) > 1 && ( prLineOpId == null || prLineOpId.isEmpty ( ) ) ) {
          return new ResponseEntity (
              this.buildErrorResponse (
                  "Se encontró más de un resultado para el Booking/EDO ingresado. Por favor "
                  + "seleccioná la Línea Operadora" ) ,
              HttpStatus.OK
          );
        }

        documentDto = ( DocumentApptDTO ) documentApptList.get ( 0 );
      }

      String orderNbr    = documentDto != null ? documentDto.getNbr ( ) : "";
      List   apptDtoList = this.apptService.findPumByOrderNbrAppt ( orderNbr , searchApptNbrList );
      List apptList = this.apptService.getApptWithCaeList (
          apptDtoList , searchApptNbrList , orderNbr );
      return new ResponseEntity ( apptList , HttpStatus.OK );
    }
    else {
      return new ResponseEntity ( this.buildErrorResponse (
          "Por favor ingresá el número de Booking/EDO o el número de turno" ) , HttpStatus.OK );
    }
  }

  @RequestMapping (
      value = { "/loadInitCombos" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < List > loadInitCombos ( ) {
    ResponseEntity response;
    try {
      List lineOpList = this.lineOpService.getAllLineOp ( );
      Map  resultMap  = new HashMap ( );
      resultMap.put ( "lineOpList" , lineOpList );
      response = new ResponseEntity ( resultMap , HttpStatus.OK );
    }
    catch ( SQLException var4 ) {
      response = new ResponseEntity (
          "No se pudo cargar correctamente la información. Por favor refresque la página" ,
          HttpStatus.BAD_REQUEST
      );
      var4.printStackTrace ( );
    }

    return response;
  }

  private
  String compareManifestCueList ( List manifestList , List manifestCueList ) {
    String compareResult       = "";
    List   foundManifestIdList = new ArrayList ( );

    int i;
    for ( i = 0; i < manifestCueList.size ( ) ; ++ i ) {
      CueDTO cueDto      = ( CueDTO ) manifestCueList.get ( i );
      String manifestNbr = cueDto.getManifestNbr ( );
      if ( ! foundManifestIdList.contains ( manifestNbr ) ) {
        foundManifestIdList.add ( manifestNbr );
      }
    }

    manifestList.removeAll ( foundManifestIdList );
    if ( ! manifestList.isEmpty ( ) ) {
      compareResult = "<p align=\"justify\">";
      compareResult = compareResult
                      + "   Los siguientes Manifiestos que está intentando presupuestar no han "
                      + "sido cerrados, no existen o no adeudan ningún concepto:<br>\n";

      for ( i = 0; i < manifestList.size ( ) ; ++ i ) {
        compareResult =
            compareResult + "      &nbsp;&nbsp;&nbsp;- " + ( String ) manifestList.get ( i )
            + "<br>"
            + "\n";
      }

      compareResult =
          compareResult + "   <br>Por favor verifique el número ingresado y vuelva a intentarlo.\n";
      compareResult = compareResult
                      + "   <br><br>Si el error persiste, contáctese mediante <a "
                      + "style=\"color:#fd7e14;\" href=\"https://apps.apmterminals.com"
                      + ".ar/solicitudesonline\" target=\"_blank\">Solicitudes Online</a><br>\n";
      compareResult = compareResult + "</p>";
    }

    return compareResult;
  }

  private
  Map buildSuccessResponse ( String msg ) {
    return this.buildResponse ( "OK" , msg );
  }

  private
  Map buildErrorResponse ( String msg ) {
    return this.buildResponse ( "ERROR" , msg );
  }

  private
  Map buildQuestionResponse ( String msg , String questionType ) {
    Map map = this.buildResponse ( "QUESTION" , msg );
    if ( questionType != null && ! questionType.isEmpty ( ) ) {
      map.put ( "questionType" , questionType );
    }

    return map;
  }

  private
  Map buildWarningResponse ( String msg ) {
    return this.buildResponse ( "WARNING" , msg );
  }

  private
  Map buildResponse ( String status , String msg ) {
    this.logUser ( msg );
    Map map = new HashMap ( );
    map.put ( "status" , status );
    map.put ( "msg" , msg );
    this.clearLogPrefix ( );
    return map;
  }
}
