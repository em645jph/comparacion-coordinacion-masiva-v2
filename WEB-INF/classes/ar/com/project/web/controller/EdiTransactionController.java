//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.restservice.controllers;

import ar.com.project.core.domain.User;
import ar.com.project.core.service.EdiTransactionService;
import ar.com.project.core.service.LineOpService;
import ar.com.project.core.utils.EnumUtil;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping ( { "/EdiTransaction" })
@SessionAttributes ( {
    "userid" , "loginName" , "username" , "parameters" , "fullname" , "firstNameLetter"
}
)
public
class EdiTransactionController extends CommonController {

  private static final Logger logger = Logger.getLogger (
      EdiTransactionController.class );
  boolean canAddItem    = false;
  boolean canDeleteItem = false;
  User    user;
  @Autowired
  EdiTransactionService ediTranService;
  @Autowired
  LineOpService         lineOpService;
  private List privilegeInPageList = new ArrayList ( );

  public
  EdiTransactionController ( ) {
  }

  @RequestMapping (
      value = { "/ediTransaction" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  public
  ModelAndView ediTransaction (
      HttpServletRequest request , HttpServletResponse response ,
      Object handler
                              ) {
    String       dataTableHtml = this.getDataTableHtml ( );
    ModelAndView model         = new ModelAndView ( "ediTransaction" );
    model.addObject ( "ediTransactionDataTable" , dataTableHtml );
    return model;
  }

  @RequestMapping (
      value = { "/loadInitCombos" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > loadInitCombos ( ) throws SQLException {
    List filterByList = EnumUtil.getEdiFilterList ( );
    List lineOpList   = this.lineOpService.getLineOpListByUser ( this.getUserLogin ( ) );
    Map  resultMap    = new HashMap ( );
    resultMap.put ( "filterByList" , filterByList );
    resultMap.put ( "lineOpList" , lineOpList );
    return new ResponseEntity ( resultMap , HttpStatus.OK );
  }

  @RequestMapping (
      value = { "/getEdiMsgTypeByFilter" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getEdiMsgTypeByFilter ( @RequestParam String prFilterBy )
  throws SQLException {
    List ediMsgTypeList =
        prFilterBy != null && ! prFilterBy.isEmpty ( ) ? EnumUtil.getEdiMsgTypeListByFilter (
            prFilterBy )
                                                       : new ArrayList ( );
    return new ResponseEntity ( ediMsgTypeList , HttpStatus.OK );
  }

  @RequestMapping (
      value = { "/findEdiTranByParam" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > findEdiTranByParam (
      @RequestParam String prFilterBy ,
      @RequestParam String prMessageType , @RequestParam String prPrimaryKeyword ,
      @RequestParam String prLineOp , @RequestParam String prVesselName ,
      @RequestParam String prVoyage , @RequestParam String prFromDateStr ,
      @RequestParam String prToDateStr
                                               ) throws SQLException {
    String errorMsg = "";
    if ( prFilterBy == null || prFilterBy.isEmpty ( ) ) {
      errorMsg = "Por favor seleccione un tipo de transacción";
    }

    List lineOpList = new ArrayList ( );
    if ( prLineOp != null && ! prLineOp.isEmpty ( ) ) {
      ( ( List ) lineOpList ).add ( prLineOp );
    }
    else {
      lineOpList = this.lineOpService.getLineOpListByUser ( this.getUserLogin ( ) );
    }

    if ( ( ( List ) lineOpList ).isEmpty ( ) ) {
      errorMsg = "Usted no tiene ninguna línea operadora asociada a su cuenta. Por favor "
                 + "comuníquese con Atención al cliente";
    }

    if ( this.isNullOrEmpty ( prMessageType ) && this.isNullOrEmpty ( prPrimaryKeyword )
         && this.isNullOrEmpty ( prLineOp ) && this.isNullOrEmpty ( prVesselName )
         && this.isNullOrEmpty (
        prVoyage ) && this.isNullOrEmpty ( prFromDateStr ) && this.isNullOrEmpty ( prToDateStr ) ) {
      errorMsg = "Por favor utilice alguno de los filtros para refinar la búsqueda. Tenga en "
                 + "cuenta que los resultados se limitarán a 200 registros como máximo";
    }

    if ( ! errorMsg.isEmpty ( ) ) {
      return new ResponseEntity ( this.buildErrorResponse ( errorMsg ) , HttpStatus.OK );
    }
    else {
      List ediTranList = this.ediTranService.findEdiTransactionByParams ( prFilterBy ,
                                                                          prMessageType ,
                                                                          prPrimaryKeyword ,
                                                                          ( List ) lineOpList ,
                                                                          prVesselName , prVoyage ,
                                                                          prFromDateStr ,
                                                                          prToDateStr
                                                                        );
      ResponseEntity response = new ResponseEntity ( ediTranList , HttpStatus.OK );
      return response;
    }
  }

  @RequestMapping (
      value = { "/getEdiFileSegment" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getEdiFileSegment ( @RequestParam String prBatchNbrStr )
  throws SQLException {
    String ediFileSegment = this.ediTranService.getEdiFileSegment (
        Long.parseLong ( prBatchNbrStr ) );
    return new ResponseEntity ( ediFileSegment , HttpStatus.OK );
  }

  private
  String getUserLogin ( ) {
    org.springframework.security.core.userdetails.User loggedUser =
        ( org.springframework.security.core.userdetails.User ) SecurityContextHolder
            .getContext ( )
            .getAuthentication ( ).getPrincipal ( );
    String login = loggedUser.getUsername ( );
    return login;
  }

  private
  String getDataTableHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    sb.append ( "<thead>\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <th>Nro</th>\n" );
    sb.append ( "    <th>Keyword</th>\n" );
    sb.append ( "    <th>Clase (EDI)</th>\n" );
    sb.append ( "    <th>Sesión</th>\n" );
    sb.append ( "    <th>Fecha creación</th>\n" );
    sb.append ( "    <th>Estado</th>\n" );
    sb.append ( "    <th>Error</th>\n" );
    sb.append ( "    <th>Nombre archivo</th>\n" );
    sb.append ( "</tr>\n" );
    sb.append ( "</thead>\n" );
    sb.append ( "<tbody data-bind=\"foreach: ediTransactionViewModel.ediTransactionTable\">\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <td><label data-bind=\"text: batchNbr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: primaryKeyword\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: sessionMsgClass\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: sessionName\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: createdStr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: status\"></label></td>\n" );
    sb.append (
        "    <td><a href=\"#\" class=\"datatlink\" data-bind=\"text: errorReadableMsg, click: "
        + "$parent.viewErrorDetail\"></a></td>\n" );
    sb.append (
        "    <td><a href=\"#\" class=\"datatlink\" data-bind=\"text: fileName, click: $parent"
        + ".viewEdiFile\"></a></td>\n" );
    sb.append ( "</tr>\n" );
    return sb.toString ( );
  }

  private
  Map buildSuccessResponse ( String msg ) {
    return this.buildResponse ( "OK" , msg );
  }

  private
  Map buildErrorResponse ( String msg ) {
    return this.buildResponse ( "ERROR" , msg );
  }

  private
  Map buildQuestionResponse ( String msg ) {
    return this.buildResponse ( "QUESTION" , msg );
  }

  private
  Map buildWarningResponse ( String msg ) {
    return this.buildResponse ( "WARNING" , msg );
  }

  private
  Map buildResponse ( String status , String msg ) {
    Map map = new HashMap ( );
    map.put ( "status" , status );
    map.put ( "msg" , msg );
    return map;
  }

  private
  boolean isNullOrEmpty ( String value ) {
    return value == null || value.isEmpty ( );
  }
}
