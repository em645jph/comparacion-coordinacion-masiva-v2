//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.restservice.controllers;

import ar.com.project.core.domain.Menu;
import ar.com.project.core.domain.User;
import ar.com.project.core.service.MenuService;
import ar.com.project.core.service.PrivilegeService;
import com.curcico.jproject.core.exception.BaseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping ( { "/MenuManagement" })
@SessionAttributes ( {
    "userid" , "loginName" , "username" , "parameters" , "fullname" , "firstNameLetter"
}
)
public
class MenuManagementController extends CommonController {

  private static final Logger logger = Logger.getLogger ( MenuManagementController.class );
  User user;
  @Autowired
  MenuService      menuService;
  @Autowired
  PrivilegeService privilegeService;

  public
  MenuManagementController ( ) {
  }

  @RequestMapping (
      value = { "/menuManagement" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  public
  ModelAndView menuManagement (
      HttpServletRequest request , HttpServletResponse response ,
      Object handler
                              ) {
    ModelAndView model       = new ModelAndView ( "menuManagement" );
    String       servletPath = request.getServletPath ( );
    org.springframework.security.core.userdetails.User loggedUser =
        ( org.springframework.security.core.userdetails.User ) SecurityContextHolder
            .getContext ( )
            .getAuthentication ( ).getPrincipal ( );
    String  login          = loggedUser.getUsername ( );
    List    privilegeList  = this.privilegeService.getMenuPagePrivileges ( login , servletPath );
    boolean canAddItem     = this.privilegeService.userCanAddItem ( privilegeList );
    boolean canEditItem    = this.privilegeService.userCanEditItem ( privilegeList );
    boolean canDeleteItem  = this.privilegeService.userCanDeleteItem ( privilegeList );
    boolean canRecoverItem = this.privilegeService.userCanRecoverItem ( privilegeList );
    String actionButtonsHtml = this.getActionButtonsHtml (
        canAddItem , canDeleteItem , canRecoverItem );
    String dataTableHtml = this.getDataTableHtml (
        canEditItem , canDeleteItem , canRecoverItem );
    model.addObject ( "actionButtons" , actionButtonsHtml );
    model.addObject ( "dataTable" , dataTableHtml );
    model.addObject ( "canAddItem" , canAddItem );
    model.addObject ( "canEditItem" , canEditItem );
    model.addObject ( "canDeleteItem" , canDeleteItem );
    model.addObject ( "canRecoverItem" , canRecoverItem );
    return model;
  }

  @RequestMapping (
      value = { "/getParentMenu" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < List > getParentMenu ( ) {
    ResponseEntity response;
    try {
      List parentMenuList = this.menuService.getParentMenuList ( );
      response = new ResponseEntity ( parentMenuList , HttpStatus.OK );
    }
    catch ( BaseException var3 ) {
      var3.printStackTrace ( );
      response = new ResponseEntity ( ( MultiValueMap ) null , HttpStatus.BAD_REQUEST );
    }

    return response;
  }

  @RequestMapping (
      value = { "/findMenuByParam" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < List > findMenuByParam (
      @RequestParam String prId ,
      @RequestParam String prLabel , @RequestParam String prParentGkeyStr ,
      @RequestParam String prFromDateStr , @RequestParam String prToDateStr ,
      @RequestParam String prLifeCycleState
                                          ) {
    List menuList = this.menuService.findMenuByParams ( prId , prLabel , prParentGkeyStr ,
                                                        prFromDateStr ,
                                                        prToDateStr , prLifeCycleState
                                                      );
    ResponseEntity response = new ResponseEntity ( menuList , HttpStatus.OK );
    return response;
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/addOrUpdateMenu" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > addOrUpdateMenu ( @RequestBody Menu updatedMenu )
  throws BaseException, ParseException {
    String  userLogin = this.getUserLogin ( );
    boolean isUpdate  = updatedMenu.getGkey ( ) != null;
    String result = isUpdate ? this.menuService.updateMenu ( updatedMenu , userLogin )
                             : this.menuService.addMenu ( updatedMenu , userLogin );
    if ( ! result.isEmpty ( ) ) {
      return new ResponseEntity ( this.buildErrorResponse ( result ) , HttpStatus.OK );
    }
    else {
      if ( ! isUpdate ) {
        this.privilegeService.addDefaultViewPrivilege ( updatedMenu , userLogin );
      }

      return new ResponseEntity (
          this.buildSuccessResponse ( "Menu guardado con éxito" ) ,
          HttpStatus.OK
      );
    }
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/getMenu" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getMenu ( @RequestParam Integer prMenuGkey )
  throws BaseException, ParseException {
    Menu menu = this.menuService.getMenuByGkey ( prMenuGkey );
    return menu != null ? new ResponseEntity ( menu , HttpStatus.OK ) : new ResponseEntity (
        "No se pudo cargar la información del menú. Por favor refresque la página" ,
        HttpStatus.BAD_REQUEST
    );
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/deleteMenu" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < String > deleteMenu ( @RequestBody Menu menu )
  throws BaseException, ParseException {
    this.menuService.deleteMenu ( menu , this.getUserLogin ( ) );
    return new ResponseEntity ( "Menu eliminado con éxito" , HttpStatus.OK );
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/recoverMenu" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < String > recoverMenu ( @RequestBody Menu menu )
  throws BaseException, ParseException {
    this.menuService.recoverMenu ( menu , this.getUserLogin ( ) );
    return new ResponseEntity ( "Menu activado con éxito" , HttpStatus.OK );
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/deleteSelectedMenu" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < String > deleteSelectedMenu ( @RequestBody Long[] prMenuGkeyArray )
  throws BaseException, ParseException {
    logger.info ( "step 1" );
    this.menuService.deleteMenuByGkey ( prMenuGkeyArray , this.getUserLogin ( ) );
    return new ResponseEntity ( "Menús eliminados con éxito" , HttpStatus.OK );
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/recoverSelectedMenu" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < String > recoverSelectedMenu ( @RequestBody Long[] prMenuGkeyArray )
  throws BaseException, ParseException {
    logger.info ( "step 1" );
    this.menuService.recoverMenuByGkey ( prMenuGkeyArray , this.getUserLogin ( ) );
    return new ResponseEntity ( "Menús activados con éxito" , HttpStatus.OK );
  }

  private
  String getUserLogin ( ) {
    org.springframework.security.core.userdetails.User loggedUser =
        ( org.springframework.security.core.userdetails.User ) SecurityContextHolder
            .getContext ( )
            .getAuthentication ( ).getPrincipal ( );
    String login = loggedUser.getUsername ( );
    return login;
  }

  private
  String getActionButtonsHtml (
      boolean canAddItem , boolean canDeleteItem ,
      boolean canRecoverItem
                              ) {
    StringBuilder sb = new StringBuilder ( );
    if ( canAddItem ) {
      sb.append (
          "<button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" "
          + "data-target=\"#addEditMenuModal\" id=\"btnAddMenu\">Agregar</button>&nbsp;&nbsp;" );
    }

    if ( canDeleteItem ) {
      if ( sb.length ( ) > 0 ) {
        sb.append ( "\n" );
      }

      sb.append (
          "<button type=\"button\" class=\"btn btn-danger\" "
          + "id=\"btnDeleteSelectedMenu\">Eliminar</button>&nbsp;&nbsp;" );
    }

    if ( canRecoverItem ) {
      if ( sb.length ( ) > 0 ) {
        sb.append ( "\n" );
      }

      sb.append (
          "<button type=\"button\" class=\"btn btn-warning\" "
          + "id=\"btnRecoverSelectedMenu\">Activar</button>" );
    }

    return sb.toString ( );
  }

  private
  String getDataTableHtml (
      boolean canEditItem , boolean canDeleteItem ,
      boolean canRecoverItem
                          ) {
    StringBuilder sb = new StringBuilder ( );
    sb.append ( "<thead>\n" );
    sb.append ( "<tr>\n" );
    if ( canDeleteItem || canRecoverItem ) {
      sb.append ( "    <th> <input type=\"checkbox\" id=\"ckCheckAll\"></th>\n" );
    }

    sb.append ( "    <th>Id</th>\n" );
    sb.append ( "    <th>Nombre</th>\n" );
    sb.append ( "    <th>Descripción</th>\n" );
    sb.append ( "    <th>Menu Padre</th>\n" );
    sb.append ( "    <th>Controlador</th>\n" );
    sb.append ( "    <th>Acción</th>\n" );
    sb.append ( "    <th>Ícono</th>\n" );
    sb.append ( "    <th>Creado por</th>\n" );
    sb.append ( "    <th>Fecha Creación</th>\n" );
    sb.append ( "    <th>Estado</th>\t\n" );
    if ( canEditItem ) {
      sb.append ( "    <th></th>\n" );
    }

    if ( canDeleteItem ) {
      sb.append ( "    <th></th>\n" );
    }

    if ( canRecoverItem ) {
      sb.append ( "    <th></th>\n" );
    }

    sb.append ( "</tr>\n" );
    sb.append ( "</thead>\n" );
    sb.append ( "<tbody data-bind=\"foreach: menuViewModel.createdMenu\">\n" );
    sb.append ( "<tr>\n" );
    if ( canDeleteItem || canRecoverItem ) {
      sb.append (
          "    <td><input id=\"ckCheckOne\" class=\"selectable\" type=\"checkbox\" "
          + "data-bind=\"attr: {gkey:gkey}\"></td>\n" );
    }

    sb.append ( "    <td><label data-bind=\"text: id\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: label\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: description\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: parentLabel\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: controller\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: action\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: icon\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: creator\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: createdStr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: lifeCycleState\"></label></td>\n" );
    if ( canEditItem ) {
      sb.append (
          "    <td><button class=\"btn btn-icon btn-info btn-sm\" data-bind=\"click: $parent"
          + ".editMenu, enable: isActive\"> <i class=\"ti-pencil-alt\"></i> </button></td>\n" );
    }

    if ( canDeleteItem ) {
      sb.append (
          "    <td><button class=\"btn btn-icon btn-danger btn-sm\" data-bind=\"click: $parent"
          + ".deleteMenu, enable: isActive\"> <i class=\"ti-close\"></i> </button></td>\n" );
    }

    if ( canRecoverItem ) {
      sb.append (
          "    <td><button class=\"btn btn-icon btn-warning btn-sm\" data-bind=\"click: $parent"
          + ".recoverMenu, enable: !isActive\"> <i class=\"ti-reload\"></i> </button></td>\n" );
    }

    sb.append ( "</tr>\n" );
    return sb.toString ( );
  }

  private
  Map buildSuccessResponse ( String msg ) {
    return this.buildResponse ( "OK" , msg );
  }

  private
  Map buildErrorResponse ( String msg ) {
    return this.buildResponse ( "ERROR" , msg );
  }

  private
  Map buildResponse ( String status , String msg ) {
    Map map = new HashMap ( );
    map.put ( "status" , status );
    map.put ( "msg" , msg );
    return map;
  }
}
