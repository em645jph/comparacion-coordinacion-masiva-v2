//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.restservice.controllers;

import ar.com.project.core.domain.User;
import ar.com.project.core.dto.DocumentApptDTO;
import ar.com.project.core.service.BookingService;
import ar.com.project.core.service.CoordinationService;
import ar.com.project.core.service.EdiTransactionService;
import ar.com.project.core.service.LineOpService;
import ar.com.project.core.utils.EnumUtil;
import ar.com.project.core.worker.UnitCategoryEnum;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping ( { "/BookingTracking" })
@SessionAttributes ( {
    "userid" , "loginName" , "username" , "parameters" , "fullname" , "firstNameLetter"
}
)
public
class BookingTrackingController extends CommonController {

  private static final Logger logger = Logger.getLogger (
      BookingTrackingController.class );
  boolean canAddItem    = false;
  boolean canDeleteItem = false;
  User    user;
  @Autowired
  EdiTransactionService ediTranService;
  @Autowired
  BookingService        bookingService;
  @Autowired
  CoordinationService   coordinationService;
  @Autowired
  LineOpService         lineOpService;
  private List privilegeInPageList = new ArrayList ( );

  public
  BookingTrackingController ( ) {
  }

  @RequestMapping (
      value = { "/bookingTracking" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  public
  ModelAndView bookingTracking (
      HttpServletRequest request , HttpServletResponse response ,
      Object handler
                               ) {
    String       dataTableHtml          = this.getDataTableHtml ( );
    String       bookingItemTableHtml   = this.getBookingItemDataTableHtml ( );
    String       bookingUnitTableHtml   = this.getBookingUnitDataTableHtml ( );
    String       bookingHazardTableHtml = this.getBookingHazardDataTableHtml ( );
    ModelAndView model                  = new ModelAndView ( "bookingTracking" );
    model.addObject ( "bookingTrackDataTable" , dataTableHtml );
    model.addObject ( "bookingItemDataTable" , bookingItemTableHtml );
    model.addObject ( "bookingUnitDataTable" , bookingUnitTableHtml );
    model.addObject ( "bookingHazardDataTable" , bookingHazardTableHtml );
    return model;
  }

  @RequestMapping (
      value = { "/loadInitCombos" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > loadInitCombos ( ) throws SQLException {
    List yesNoList  = EnumUtil.getSimpleYesNoEnumList ( );
    List lineOpList = this.lineOpService.getLineOpListByUser ( this.getUserLogin ( ) );
    Map  resultMap  = new HashMap ( );
    resultMap.put ( "yesNoList" , yesNoList );
    resultMap.put ( "lineOpList" , lineOpList );
    return new ResponseEntity ( resultMap , HttpStatus.OK );
  }

  @RequestMapping (
      value = { "/findBookingByParam" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > findBookingByParam (
      @RequestParam String prBookingNbr ,
      @RequestParam String prContainerNbr , @RequestParam String prLineOp ,
      @RequestParam String prVesselName , @RequestParam String prObVoyage ,
      @RequestParam String prPod , @RequestParam String prReefer ,
      @RequestParam String prHazardous ,
      @RequestParam String prOverrideCutoff
                                               ) throws SQLException {
    String errorMsg   = "";
    List   lineOpList = new ArrayList ( );
    if ( prLineOp != null && ! prLineOp.isEmpty ( ) ) {
      ( ( List ) lineOpList ).add ( prLineOp );
    }
    else {
      lineOpList = this.lineOpService.getLineOpIdListByUser ( this.getUserLogin ( ) );
    }

    if ( ( ( List ) lineOpList ).isEmpty ( ) ) {
      errorMsg = "Usted no tiene ninguna línea operadora asociada a su cuenta. Por favor "
                 + "comuníquese con Atención al cliente";
    }

    if ( this.isNullOrEmpty ( prBookingNbr ) && this.isNullOrEmpty ( prContainerNbr )
         && this.isNullOrEmpty ( prLineOp ) && this.isNullOrEmpty ( prVesselName )
         && this.isNullOrEmpty (
        prObVoyage ) && this.isNullOrEmpty ( prPod ) && this.isNullOrEmpty ( prReefer )
         && this.isNullOrEmpty ( prHazardous ) && this.isNullOrEmpty ( prOverrideCutoff ) ) {
      errorMsg = "Por favor utilice alguno de los filtros para refinar la búsqueda. Tenga en "
                 + "cuenta que los resultados se limitarán a 1000 registros como máximo";
    }

    if ( ! errorMsg.isEmpty ( ) ) {
      return new ResponseEntity ( this.buildErrorResponse ( errorMsg ) , HttpStatus.OK );
    }
    else {
      List bookingGkeyList = null;
      if ( prContainerNbr != null && ! prContainerNbr.isEmpty ( ) ) {
        bookingGkeyList = this.bookingService.getBookingGkeyListByUnitId ( prContainerNbr );
        if ( bookingGkeyList == null || bookingGkeyList.isEmpty ( ) ) {
          return new ResponseEntity ( new ArrayList ( ) , HttpStatus.OK );
        }
      }

      List bookingList = this.bookingService.findBookingByParam ( bookingGkeyList , prBookingNbr ,
                                                                  ( List ) lineOpList , prReefer ,
                                                                  prHazardous , prOverrideCutoff ,
                                                                  prVesselName , prObVoyage ,
                                                                  prPod , ( String ) null ,
                                                                  ( String ) null
                                                                );
      ResponseEntity response = new ResponseEntity ( bookingList , HttpStatus.OK );
      return response;
    }
  }

  @RequestMapping (
      value = { "/getBookingItem" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getBookingItem ( @RequestParam String prBookingGkeyStr )
  throws SQLException {
    List bkgItemList = this.bookingService.getBookingItemList (
        Long.parseLong ( prBookingGkeyStr ) ,
        ( String ) null , ( String ) null
                                                              );
    return new ResponseEntity ( bkgItemList , HttpStatus.OK );
  }

  @RequestMapping (
      value = { "/getBookingUnit" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getBookingUnit ( @RequestParam String prBookingGkeyStr )
  throws SQLException {
    DocumentApptDTO documentApptDto = new DocumentApptDTO ( Long.parseLong ( prBookingGkeyStr ) ,
                                                            ( String ) null , ( String ) null ,
                                                            ( String ) null
    );
    List bkgUnitList = this.coordinationService.findUnit ( ( Long ) null ,
                                                           UnitCategoryEnum.EXPRT.getKey ( ) ,
                                                           documentApptDto
                                                         );
    return new ResponseEntity ( bkgUnitList , HttpStatus.OK );
  }

  @RequestMapping (
      value = { "/getBookingHazard" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getBookingHazard ( @RequestParam String prBookingGkeyStr )
  throws SQLException {
    List bkgHazardList = this.bookingService.getBookingHazardList (
        Long.parseLong ( prBookingGkeyStr ) );
    return new ResponseEntity ( bkgHazardList , HttpStatus.OK );
  }

  private
  String getUserLogin ( ) {
    org.springframework.security.core.userdetails.User loggedUser =
        ( org.springframework.security.core.userdetails.User ) SecurityContextHolder
            .getContext ( )
            .getAuthentication ( ).getPrincipal ( );
    String login = loggedUser.getUsername ( );
    return login;
  }

  private
  String getDataTableHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    sb.append ( "<thead>\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <th>Booking</th>\n" );
    sb.append ( "    <th>Línea</th>\n" );
    sb.append ( "    <th>Nave</th>\n" );
    sb.append ( "    <th>Viaje</th>\n" );
    sb.append ( "    <th>Puerto Descarga</th>\n" );
    sb.append ( "    <th>Cantidad</th>\n" );
    sb.append ( "    <th>Preavisados</th>\n" );
    sb.append ( "    <th>Hazardous</th>\n" );
    sb.append ( "    <th>Reefer</th>\n" );
    sb.append ( "    <th>Override Cutoff</th>\n" );
    sb.append ( "    <th>Fecha creación</th>\n" );
    sb.append ( "</tr>\n" );
    sb.append ( "</thead>\n" );
    sb.append ( "<tbody data-bind=\"foreach: bookingTrackViewModel.bookingTable\">\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <td><label data-bind=\"text: nbr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: lineOpId\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: vesselName\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: obVoyage\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: pod\"></label></td>\n" );
    sb.append (
        "    <td><a class=\"datatlink\" href=\"#\" data-bind=\"text: qty, click: $parent"
        + ".viewBookingItem\"></a></td>\n" );
    sb.append (
        "    <td><a class=\"datatlink\" href=\"#\" data-bind=\"text: preadvisedUnitQty, click: "
        + "$parent.viewBookingUnit\"></a></td>\n" );
    sb.append (
        "    <td><a class=\"datatlink\" href=\"#\" data-bind=\"text: isHazardousStr, click: "
        + "$parent.viewBookingHazard\"></a></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: hasReefersStr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: overrideCutoffStr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: createdStr\"></label></td>\n" );
    sb.append ( "</tr>\n" );
    return sb.toString ( );
  }

  private
  String getBookingItemDataTableHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    sb.append ( "<thead>\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <th>Tipo</th>\n" );
    sb.append ( "    <th>Cantidad</th>\n" );
    sb.append ( "    <th>Mercadería</th>\n" );
    sb.append ( "    <th>Temperatura</th>\n" );
    sb.append ( "    <th>Ventilación</th>\n" );
    sb.append ( "    <th>Humedad</th>\n" );
    sb.append ( "    <th>CO2</th>\n" );
    sb.append ( "    <th>O2</th>\n" );
    sb.append ( "</tr>\n" );
    sb.append ( "</thead>\n" );
    sb.append ( "<tbody data-bind=\"foreach: bookingTrackViewModel.bookingItemTable\">\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <td><label data-bind=\"text: isoType\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: quantity\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: commodity\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: temperature\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: ventilation\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: humidity\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: co2\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: o2\"></label></td>\n" );
    sb.append ( "</tr>\n" );
    return sb.toString ( );
  }

  private
  String getBookingUnitDataTableHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    sb.append ( "<thead>\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <th>Contenedor</th>\n" );
    sb.append ( "    <th>Categoría</th>\n" );
    sb.append ( "    <th>Tipo</th>\n" );
    sb.append ( "    <th>Estado</th>\n" );
    sb.append ( "    <th>Turno</th>\n" );
    sb.append ( "    <th>Reefer</th>\n" );
    sb.append ( "    <th>Temp. (C)</th>\n" );
    sb.append ( "    <th>Hazardous</th>\n" );
    sb.append ( "    <th>OOG</th>\n" );
    sb.append ( "    <th>Peso VGM</th>\n" );
    sb.append ( "    <th>Entidad VGM</th>\n" );
    sb.append ( "</tr>\n" );
    sb.append ( "</thead>\n" );
    sb.append ( "<tbody data-bind=\"foreach: bookingTrackViewModel.bookingUnitTable\">\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitId\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitCategory\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitIsoTypeId\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: ufvTransitStateDesc\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: shortApptRequestedDateStr\"></label></td>\n" );
    sb.append (
        "    <td><label data-bind=\"text: unitRequiresPower != null && unitRequiresPower? 'Y' : "
        + "'N'\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitRequiredTempC\"></label></td>\n" );
    sb.append (
        "    <td><label data-bind=\"text: unitIsHazardous != null && unitIsHazardous? 'Y' : "
        + "'N'\"></label></td>\n" );
    sb.append (
        "    <td><label data-bind=\"text: unitIsOOG != null && unitIsOOG? 'Y' : "
        + "'N'\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitVgmWeight\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitVgmEntity\"></label></td>\n" );
    sb.append ( "</tr>\n" );
    return sb.toString ( );
  }

  private
  String getBookingHazardDataTableHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    sb.append ( "<thead>\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <th>Clase IMDG</th>\n" );
    sb.append ( "    <th>UN/NA</th>\n" );
    sb.append ( "    <th>Nombre</th>\n" );
    sb.append ( "    <th>Notas</th>\n" );
    sb.append ( "</tr>\n" );
    sb.append ( "</thead>\n" );
    sb.append ( "<tbody data-bind=\"foreach: bookingTrackViewModel.bookingHazardTable\">\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <td><label data-bind=\"text: imdgClass\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unNumber\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: description\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: notes\"></label></td>\n" );
    sb.append ( "</tr>\n" );
    return sb.toString ( );
  }

  private
  Map buildSuccessResponse ( String msg ) {
    return this.buildResponse ( "OK" , msg );
  }

  private
  Map buildErrorResponse ( String msg ) {
    return this.buildResponse ( "ERROR" , msg );
  }

  private
  Map buildQuestionResponse ( String msg ) {
    return this.buildResponse ( "QUESTION" , msg );
  }

  private
  Map buildWarningResponse ( String msg ) {
    return this.buildResponse ( "WARNING" , msg );
  }

  private
  Map buildResponse ( String status , String msg ) {
    Map map = new HashMap ( );
    map.put ( "status" , status );
    map.put ( "msg" , msg );
    return map;
  }

  private
  boolean isNullOrEmpty ( String value ) {
    return value == null || value.isEmpty ( );
  }
}
