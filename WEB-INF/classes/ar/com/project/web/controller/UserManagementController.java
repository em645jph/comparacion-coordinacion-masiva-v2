//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.restservice.controllers;

import ar.com.project.core.domain.User;
import ar.com.project.core.service.PrivilegeService;
import ar.com.project.core.service.UserService;
import com.curcico.jproject.core.exception.BaseException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.NumberUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping ( { "/UserManagement" })
@SessionAttributes ( {
    "userid" , "loginName" , "username" , "parameters" , "fullname" , "firstNameLetter"
}
)
public
class UserManagementController extends CommonController {

  private static final Logger logger = Logger.getLogger (
      UserManagementController.class );
  boolean canAddItem     = false;
  boolean canEditItem    = false;
  boolean canDeleteItem  = false;
  boolean canRecoverItem = false;
  User    user;
  @Autowired
  UserService      userService;
  @Autowired
  PrivilegeService privilegeService;
  private List privilegeInPageList = new ArrayList ( );

  public
  UserManagementController ( ) {
  }

  @RequestMapping (
      value = { "/userManagement" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  public
  ModelAndView userManagement (
      HttpServletRequest request , HttpServletResponse response ,
      Object handler
                              ) {
    ModelAndView model       = new ModelAndView ( "userManagement" );
    String       servletPath = request.getServletPath ( );
    org.springframework.security.core.userdetails.User loggedUser =
        ( org.springframework.security.core.userdetails.User ) SecurityContextHolder
            .getContext ( )
            .getAuthentication ( ).getPrincipal ( );
    String login = loggedUser.getUsername ( );
    this.privilegeInPageList = this.privilegeService.getMenuPagePrivileges ( login , servletPath );
    this.canAddItem          = this.privilegeService.userCanAddItem ( this.privilegeInPageList );
    this.canEditItem         = this.privilegeService.userCanEditItem ( this.privilegeInPageList );
    this.canDeleteItem       = this.privilegeService.userCanDeleteItem ( this.privilegeInPageList );
    this.canRecoverItem      = this.privilegeService.userCanRecoverItem (
        this.privilegeInPageList );
    String actionButtonsHtml = this.getActionButtonsHtml ( );
    String dataTableHtml     = this.getDataTableHtml ( );
    String modalButtons = this.getModalButtonsHtml (
        this.canAddItem , this.canDeleteItem );
    String credentialModalButtons = this.getUpdateCredentialModalButtonsHtml ( );
    model.addObject ( "actionButtons" , actionButtonsHtml );
    model.addObject ( "dataTable" , dataTableHtml );
    model.addObject ( "credentialModalButtons" , credentialModalButtons );
    return model;
  }

  @RequestMapping (
      value = { "/findUserByParam" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < List > findUserByParam (
      @RequestParam String prDocumentNbrStr ,
      @RequestParam String prName , @RequestParam String prRoleGkeyStr ,
      @RequestParam String prFromDateStr , @RequestParam String prToDateStr ,
      @RequestParam String prLifeCycleStateKey
                                          ) throws SQLException {
    String documentNbrStr = "";
    if ( prDocumentNbrStr != null && ! prDocumentNbrStr.isEmpty ( ) ) {
      String[] documentNbrArray = prDocumentNbrStr.split ( "," );

      for ( int i = 0 ; i < documentNbrArray.length ; ++ i ) {
        String nbrStr = documentNbrArray[ i ];
        nbrStr = nbrStr.replaceAll ( "\\r" , "" ).replaceAll ( "\\t" , "" ).trim ( );
        if ( nbrStr.contains ( "." ) || nbrStr.contains ( "-" ) || nbrStr.contains ( "+" )
             || ! NumberUtils.isNumber ( nbrStr ) ) {
          return new ResponseEntity ( this.buildErrorResponse (
              "El campo CUIT acepta sólo valores numéricos separados por comas.\nRevisar valor "
              + nbrStr ) , HttpStatus.OK );
        }

        documentNbrStr = documentNbrStr + ( documentNbrStr.isEmpty ( ) ? "" : "," );
        documentNbrStr = documentNbrStr + nbrStr;
      }
    }

    List filterDefList = this.userService.findUserByParams ( documentNbrStr , prName ,
                                                             prRoleGkeyStr ,
                                                             prFromDateStr , prToDateStr ,
                                                             prLifeCycleStateKey
                                                           );
    ResponseEntity response = new ResponseEntity ( filterDefList , HttpStatus.OK );
    return response;
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/updateSelectedUserCredential" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < List > updateSelectedUserCredential ( @RequestBody Object updateObject )
  throws SQLException, BaseException {
    Map     updateMap              = ( Map ) updateObject;
    List    gkeyList               = ( List ) updateMap.get ( "gkeyArray" );
    String  emailAddress           = ( String ) updateMap.get ( "emailAddress" );
    String  comexEmail             = ( String ) updateMap.get ( "comexEmail" );
    String  managementEmail        = ( String ) updateMap.get ( "managementEmail" );
    String  password               = ( String ) updateMap.get ( "password" );
    boolean updateEmail            = ( Boolean ) updateMap.get ( "updateCredentialEmail" );
    boolean updateComexEmail       = ( Boolean ) updateMap.get ( "updateComexEmail" );
    boolean updateManagementEmail  = ( Boolean ) updateMap.get ( "updateManagementEmail" );
    boolean updatePassword         = ( Boolean ) updateMap.get ( "updateCredentialPassword" );
    boolean requiresPasswordChange = ( Boolean ) updateMap.get ( "requiresEmailChange" );
    String  errorMsg               = "";
    if ( emailAddress != null && ! emailAddress.isEmpty ( )
         || password != null && ! password.isEmpty ( )
         || comexEmail != null && ! comexEmail.isEmpty ( ) ) {
      if ( updateEmail && ( emailAddress == null || emailAddress.isEmpty ( ) ) ) {
        errorMsg = "Ingresá un mail de notificación";
      }
      else if ( updateComexEmail && ( comexEmail == null || comexEmail.isEmpty ( ) ) ) {
        errorMsg = "Ingresá un mail para el representante de comercio exterior";
      }
      else if ( ! updateManagementEmail
                || managementEmail != null && ! managementEmail.isEmpty ( ) ) {
        if ( updatePassword && ( password == null || password.isEmpty ( ) ) ) {
          errorMsg = "Ingresá un password";
        }
      }
      else {
        errorMsg = "Ingresá un mail para el representante de comercio exterior";
      }
    }
    else {
      errorMsg = "Ingresá algún valor para actualizar";
    }

    if ( ! errorMsg.equals ( "" ) ) {
      return new ResponseEntity ( this.buildErrorResponse ( errorMsg ) , HttpStatus.OK );
    }
    else {
      this.userService.updateUserCredentialByGkey ( gkeyList , emailAddress , managementEmail ,
                                                    comexEmail , password , requiresPasswordChange ,
                                                    this.getUserLogin ( )
                                                  );
      return new ResponseEntity (
          this.buildSuccessResponse ( "Credenciales actualizadas con éxito" ) ,
          HttpStatus.OK
      );
    }
  }

  private
  String getUserLogin ( ) {
    org.springframework.security.core.userdetails.User loggedUser =
        ( org.springframework.security.core.userdetails.User ) SecurityContextHolder
            .getContext ( )
            .getAuthentication ( ).getPrincipal ( );
    String login = loggedUser.getUsername ( );
    return login;
  }

  private
  String getActionButtonsHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    if ( this.canAddItem ) {
      sb.append (
          "<button type=\"button\" class=\"btn btn-primary\" "
          + "id=\"btnAddUser\">Agregar</button>&nbsp;&nbsp;" );
    }

    if ( this.canDeleteItem ) {
      if ( sb.length ( ) > 0 ) {
        sb.append ( "\n" );
      }

      sb.append (
          "<button type=\"button\" class=\"btn btn-danger\" "
          + "id=\"btnDeleteSelectedUser\">Eliminar</button>&nbsp;&nbsp;" );
    }

    if ( this.canRecoverItem ) {
      if ( sb.length ( ) > 0 ) {
        sb.append ( "\n" );
      }

      sb.append (
          "<button type=\"button\" class=\"btn btn-warning\" "
          + "id=\"btnRecoverSelectedUser\">Activar</button>" );
    }

    if ( this.canEditItem ) {
      if ( sb.length ( ) > 0 ) {
        sb.append ( "\n" );
      }

      sb.append (
          "<button type=\"button\" class=\"btn btn-info\" "
          + "id=\"btnUpdateUserCredential\">Actualizar credenciales</button>" );
    }

    return sb.toString ( );
  }

  private
  String getModalButtonsHtml ( boolean canAddItem , boolean canDeleteItem ) {
    StringBuilder sb = new StringBuilder ( );
    if ( canAddItem || this.canEditItem ) {
      sb.append (
          "<button type=\"button\" class=\"btn btn-primary\" id=\"btnSaveRole\">Guardar</button>" );
    }

    if ( sb.length ( ) > 0 ) {
      sb.append ( "\n" );
    }

    sb.append (
        "<button type=\"button\" class=\"btn btn-dark\" id=\"btnCancelSaveRole\" "
        + "data-dismiss=\"modal\">Cancelar</button>" );
    return sb.toString ( );
  }

  private
  String getUpdateCredentialModalButtonsHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    if ( this.canEditItem ) {
      sb.append (
          "<button type=\"button\" class=\"btn btn-primary\" "
          + "id=\"btnSaveUserCredential\">Guardar</button>" );
    }

    if ( sb.length ( ) > 0 ) {
      sb.append ( "\n" );
    }

    sb.append (
        "<button type=\"button\" class=\"btn btn-dark\" id=\"btnCancelUserCredential\" "
        + "data-dismiss=\"modal\">Cancelar</button>" );
    return sb.toString ( );
  }

  private
  String getDataTableHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    sb.append ( "<thead>\n" );
    sb.append ( "<tr>\n" );
    if ( this.canEditItem || this.canDeleteItem || this.canRecoverItem ) {
      sb.append ( "    <th> <input type=\"checkbox\" id=\"ckCheckAll\"></th>\n" );
    }

    sb.append ( "    <th>CUIT</th>\n" );
    sb.append ( "    <th>Nombre</th>\n" );
    sb.append ( "    <th>Email notificación</th>\n" );
    sb.append ( "    <th>Email comex</th>\n" );
    sb.append ( "    <th>Email facturación</th>\n" );
    sb.append ( "    <th>Última fecha de ingreso</th>\n" );
    sb.append ( "    <th>Clave bloqueada</th>\n" );
    sb.append ( "    <th>Creado por</th>\n" );
    sb.append ( "    <th>Fecha Creación</th>\n" );
    sb.append ( "    <th>Act. por</th>\n" );
    sb.append ( "    <th>Fecha Act.</th>\n" );
    sb.append ( "    <th>Estado</th>\t\n" );
    sb.append ( "    <th></th>\n" );
    if ( this.canDeleteItem ) {
      sb.append ( "    <th></th>\n" );
    }

    if ( this.canRecoverItem ) {
      sb.append ( "    <th></th>\n" );
    }

    sb.append ( "</tr>\n" );
    sb.append ( "</thead>\n" );
    sb.append ( "<tbody data-bind=\"foreach: userViewModel.createdUser\">\n" );
    sb.append ( "<tr>\n" );
    if ( this.canEditItem || this.canDeleteItem || this.canRecoverItem ) {
      sb.append (
          "    <td><input id=\"ckCheckOne\" class=\"selectable\" type=\"checkbox\" "
          + "data-bind=\"attr: {gkey:gkey}\"></td>\n" );
    }

    sb.append ( "    <td><label data-bind=\"text: documentNbr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: fullName\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: contactEmail\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: comexEmail\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: managementEmail\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: lastLoginDateStr\"></label></td>\n" );
    sb.append ( "    <td><i data-bind=\"class: lockedIcon\"></i></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: creator\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: createdStr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: changer\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: changedStr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: lifeCycleState\"></label></td>\n" );
    if ( this.canEditItem ) {
      sb.append (
          "    <td><button class=\"btn btn-icon btn-info btn-sm\" data-bind=\"click: $parent"
          + ".editUser, enable: isActive\"> <i class=\"ti-pencil-alt\"></i> </button></td>\n" );
    }
    else {
      sb.append (
          "    <td><button class=\"btn btn-icon btn-default btn-sm\" data-bind=\"click: $parent"
          + ".editUser enable: isActive\"> <i class=\"ti-view-list\"></i> </button></td>\n" );
    }

    if ( this.canDeleteItem ) {
      sb.append (
          "    <td><button class=\"btn btn-icon btn-danger btn-sm\" data-bind=\"click: $parent"
          + ".deleteUser, enable: isActive\"> <i class=\"ti-close\"></i> </button></td>\n" );
    }

    if ( this.canRecoverItem ) {
      sb.append (
          "    <td><button class=\"btn btn-icon btn-warning btn-sm\" data-bind=\"click: $parent"
          + ".recoverUser, enable: !isActive\"> <i class=\"ti-reload\"></i> </button></td>\n" );
    }

    sb.append ( "</tr>\n" );
    return sb.toString ( );
  }

  private
  String getRoleDataTableHtml ( boolean viewOnly ) {
    StringBuilder sb = new StringBuilder ( );
    sb.append ( "<thead>\n" );
    sb.append ( "<tr>\n" );
    if ( ! viewOnly ) {
      sb.append ( "    <th> <input type=\"checkbox\" id=\"ckCheckAllPrivilege\"></th>\n" );
    }

    sb.append ( "    <th>Nombre</th>\n" );
    sb.append ( "</tr>\n" );
    sb.append ( "</thead>\n" );
    sb.append ( "<tbody data-bind=\"foreach: roleViewModel.rolePrivilege\">\n" );
    sb.append ( "<tr>\n" );
    if ( ! viewOnly ) {
      sb.append (
          "    <td><input id=\"ckCheckOnePrivilege\" class=\"selectablePrivilege\" "
          + "type=\"checkbox\" data-bind=\"attr: {gkey:gkey}, checked: "
          + "isIncludedOnRole\"></td>\n" );
    }

    sb.append ( "    <td><label data-bind=\"text: label\"></label></td>\n" );
    sb.append ( "</tr>\n" );
    return sb.toString ( );
  }

  private
  Map buildSuccessResponse ( String msg ) {
    return this.buildResponse ( "OK" , msg );
  }

  private
  Map buildErrorResponse ( String msg ) {
    return this.buildResponse ( "ERROR" , msg );
  }

  private
  Map buildResponse ( String status , String msg ) {
    Map map = new HashMap ( );
    map.put ( "status" , status );
    map.put ( "msg" , msg );
    return map;
  }
}
