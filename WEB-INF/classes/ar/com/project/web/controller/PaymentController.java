//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.restservice.controllers;

import ar.com.project.core.domain.Code;
import ar.com.project.core.domain.User;
import ar.com.project.core.dto.InvoiceDTO;
import ar.com.project.core.dto.UnitImportFreeDebtDTO;
import ar.com.project.core.mail.SimpleEmailManager;
import ar.com.project.core.service.AuditService;
import ar.com.project.core.service.CodeService;
import ar.com.project.core.service.DatabaseConnectorService;
import ar.com.project.core.service.InvoiceService;
import ar.com.project.core.service.PrivilegeService;
import ar.com.project.core.service.QueriesService;
import ar.com.project.core.service.RoleService;
import ar.com.project.core.service.UserService;
import ar.com.project.core.service.WebService;
import ar.com.project.core.utils.DateUtils;
import ar.com.project.core.utils.MessagesUtils;
import com.curcico.jproject.core.daos.ConditionSimple;
import com.curcico.jproject.core.daos.SearchOption;
import com.curcico.jproject.core.exception.BaseException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import javax.annotation.Nonnull;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@SessionAttributes ( { "userid" , "parameters" , "firstname" , "lastname" , "invoice" })
@RequestMapping ( { "/Payment" })
public
class PaymentController extends CommonController {

  private static final Logger logger = Logger.getLogger ( PaymentController.class );
  Timestamp now = new Timestamp ( Calendar.getInstance ( ).getTimeInMillis ( ) );
  @Autowired
  UserService              userService;
  @Autowired
  InvoiceService           invoiceService;
  @Autowired
  WebService               webService;
  @Autowired
  SimpleEmailManager       emailManager;
  @Autowired
  QueriesService           queriesService;
  @Autowired
  DatabaseConnectorService databaseConnectorService;
  @Autowired
  CodeService              codeService;
  @Autowired
  PrivilegeService         privilegeService;
  @Autowired
  AuditService             auditService;
  @Autowired
  RoleService              roleService;

  public
  PaymentController ( ) {
  }

  @RequestMapping (
      value = { "/paymentSelector/{draftnbr}" },
      method = { RequestMethod.GET }
  )
  @ResponseBody
  public
  ModelAndView paymentSelector ( @Nonnull @PathVariable String draftnbr )
  throws BaseException, ParseException, IOException, SQLException {
    boolean      havePendings = this.invoiceService.havePendingsTransactions ( draftnbr );
    ModelAndView m;
    if ( havePendings ) {
      m = new ModelAndView ( "drafts" );
      m.addObject ( "msg" , MessagesUtils.getBundle ( "DRAFT_HAVE_TRANSFERS_PENDING" ) );
      return m;
    }
    else {
      logger.info ( "paymentSelector" );
      m = new ModelAndView ( "paymentSelector" );
      InvoiceDTO inv = this.invoiceService.prevalidateDraft ( draftnbr );
      if ( inv == null ) {
        m = new ModelAndView ( "drafts" );
        m.addObject (
            "msg" ,
            "Usted está intentando abonar un presupuesto inexistente. Por favor, verifique."
                    );
        return m;
      }
      else if ( inv.getInvStatus ( ).equalsIgnoreCase ( "FINAL" ) ) {
        m = new ModelAndView ( "drafts" );
        m.addObject (
            "msg" ,
            "Usted está intentando abonar un presupuesto finalizado. Por favor, verifique."
                    );
        return m;
      }
      else {
        boolean showEPayment;
        if ( inv.getInvTypeKey ( ) == 5 ) {
          System.out.println ( "Soy un draft de Importacion" );
          showEPayment = this.validateImportUnits ( inv.getInvNbr ( ) );
          if ( showEPayment ) {
            System.out.println (
                "Encontre contenedores con error, asi que ahora me voy a la pagina "
                + "[unitsDebtFree]" );
            m = new ModelAndView ( "unitsDebtFree" );
            m.addObject ( "invNbr" , inv.getInvNbr ( ) );
            return m;
          }
        }

        m.addObject ( "invoice" , inv );
        showEPayment = true;
        m.addObject ( "showEPayment" , showEPayment );
        if ( inv.getCusAcctType ( ).equalsIgnoreCase ( "CASH" ) ) {
          if ( ! ( inv.getCusPositiveBalance ( ) > 0.0 ) ) {
            if ( inv.getCusPositiveBalance ( ) != 0.0 ) {
              m = new ModelAndView ( "drafts" );
              m.addObject ( "msg" , MessagesUtils.getBundle ( "CANNOT_PAY_CALL_OFFICE" ) );
              return m;
            }

            m.addObject (
                "interbankingIsAvailable" ,
                showEPayment && DateUtils.paymentIsAvailable ( "Interbanking" )
                        );
            return m;
          }

          if ( ! ( inv.getCusPositiveBalance ( ) >= inv.getInvOwed ( ) ) ) {
            m.addObject ( "msg" , MessagesUtils.getBundle ( "CUSTOMER_HAVE_SLDF" ) );
            m.addObject ( "sldf" , inv.getCusPositiveBalance ( ) );
            m.addObject (
                "interbankingIsAvailable" ,
                showEPayment && DateUtils.paymentIsAvailable ( "Interbanking" )
                        );
            return m;
          }

          JSONObject json = new JSONObject ( );
          json.put ( "type" , "SLDF" );
          json.put ( "amount" , inv.getInvOwed ( ).toString ( ) );
          this.addPayment ( json.toJSONString ( ) , inv );
          inv.setCusPositiveBalance ( inv.getCusPositiveBalance ( ) - inv.getInvOwed ( ) );
          m = new ModelAndView ( "drafts" );
          m.addObject ( "msg" , MessagesUtils.getBundle ( "SYSTEM_FINALIZE_INVOICE_AUTO" ) );
        }
        else if ( inv.getCusAcctType ( ).equalsIgnoreCase ( "OAC" ) ) {
          User myUser = this.userService.getUser ( this.getUserContext ( ).getUsername ( ) );
          if ( myUser.getRoles ( ).contains ( this.roleService.getRoleByGkey ( 12 ) ) ) {
            m = new ModelAndView ( "drafts" );
            m.addObject ( "msg" , "No tiene permisos para realizar esta acción" );
            return m;
          }

          if ( inv.getCreditLimit ( ) == 0.0
               || inv.getInvOwed ( ) > inv.getCusPositiveBalance ( ) ) {
            m = new ModelAndView ( "drafts" );
            m.addObject ( "msg" , MessagesUtils.getBundle ( "CUSTOMER_NOT_HAVE_CREDIT_LIMIT" ) );
            return m;
          }

          if ( ! ( inv.getCreditLimit ( ) > inv.getExpiredDebt ( ) ) ) {
            m = new ModelAndView ( "drafts" );
            m.addObject ( "msg" , MessagesUtils.getBundle ( "CANNOT_PAY_CALL_OFFICE" ) );
            return m;
          }

          double maxAmount = inv.getCreditLimit ( ) - inv.getExpiredDebt ( );
          if ( inv.getInvOwed ( ) > maxAmount ) {
            m.addObject (
                "interbankingIsAvailable" ,
                showEPayment && DateUtils.paymentIsAvailable ( "Interbanking" )
                        );
            return m;
          }

          JSONObject json = new JSONObject ( );
          json.put ( "type" , "SLDF" );
          json.put ( "cuitFrom" , inv.getCustomerTaxId ( ) );
          json.put ( "amount" , String.valueOf ( maxAmount ) );
          this.addPayment ( json.toJSONString ( ) , inv );
          inv.setCusPositiveBalance ( inv.getCusPositiveBalance ( ) - inv.getInvOwed ( ) );
          m = new ModelAndView ( "drafts" );
          m.addObject ( "msg" , MessagesUtils.getBundle ( "SYSTEM_FINALIZE_INVOICE_AUTO" ) );
        }

        return m;
      }
    }
  }

  @RequestMapping (
      value = { "/paymentMethod/{method}" },
      method = { RequestMethod.GET }
  )
  public
  ModelAndView paymentMethod ( @PathVariable int method ) throws BaseException {
    logger.info ( "paymentMethod selected is " + method );
    ModelAndView m;
    switch ( method ) {
      case 1:
        m = new ModelAndView ( "paymentMethodDebin" );
        break;
      case 2:
        m = new ModelAndView ( "404" );
        break;
      case 3:
        m = new ModelAndView ( "paymentMethodInterbkg" );
        m.addObject ( "interbankingIsAvailable" , DateUtils.paymentIsAvailable ( "Interbanking" ) );
        break;
      case 4:
        m = new ModelAndView ( "paymentMethodSldf" );
        break;
      default:
        m = new ModelAndView ( "404" );
    }

    return m;
  }

  @RequestMapping (
      value = { "/addPayment" },
      method = { RequestMethod.POST }
  )
  public
  ResponseEntity < Object > addPayment (
      @RequestBody String var1 ,
      @ModelAttribute ( "invoice") InvoiceDTO var2
                                       )
  throws BaseException, ParseException, IOException {
    // $FF: Couldn't be decompiled
  }

  @RequestMapping (
      value = { "/sendCode" },
      method = { RequestMethod.POST }
  )
  public
  ResponseEntity < Object > sendCode ( @ModelAttribute ( "invoice") InvoiceDTO invoice )
  throws BaseException, ParseException, IOException {
    try {
      if ( this.existsCode ( invoice.getInvNbr ( ) ) != null ) {
        return new ResponseEntity ( "EXISTS" , HttpStatus.OK );
      }
      else {
        String query = this.queriesService.findEmailCustomerByDraft (
            invoice.getInvNbr ( ) );
        Connection connection = this.databaseConnectorService.createN4BillingDBConnection ( );
        Statement  st         = connection.createStatement ( );
        ResultSet  rs         = st.executeQuery ( query );
        if ( rs == null ) {
          return new ResponseEntity (
              "No se pudo enviar el código. Intente nuevamente mas tarde." ,
              HttpStatus.BAD_REQUEST
          );
        }
        else {
          while ( rs.next ( ) ) {
            Code code = new Code ( );
            code.setCode ( String.valueOf ( ( new Random ( ) ).nextInt ( 32908 ) + 23546 ) );
            code.setDraftNbr ( invoice.getInvNbr ( ) );
            code.setCustomerEmail ( rs.getString ( "cusEmail" ) );
            code.setCustomerGkey ( rs.getInt ( "cusGkey" ) );
            code.setCreator ( this.getUserContext ( ).getUsername ( ) );
            code.setCreated ( this.now );
            code.setLifeCycleState ( "ACT" );
            code = ( Code ) this.codeService.createOrUpdate (
                code ,
                this.getUserContext ( ).getUsername ( )
                                                            );
            if ( code == null ) {
              return new ResponseEntity ( "Error al generar el codigo" , HttpStatus.BAD_REQUEST );
            }

            String body = "Codigo para Pago: " + code.getCode ( );
            this.emailManager.sendMail ( code.getCustomerEmail ( ) , "Solicitud de codigo" , body ,
                                         "puertodigital@apmterminals.com"
                                       );
          }

          return new ResponseEntity ( "SUCCESS" , HttpStatus.OK );
        }
      }
    }
    catch ( Exception var8 ) {
      return new ResponseEntity ( var8.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  @RequestMapping (
      value = { "/validateCode" },
      method = { RequestMethod.POST }
  )
  public
  ResponseEntity < Object > validateCode (
      @RequestBody String data ,
      @ModelAttribute ( "invoice") InvoiceDTO invoice
                                         )
  throws BaseException, ParseException, IOException {
    try {
      JSONParser jsonParser = new JSONParser ( );
      new JSONObject ( );
      JSONObject jsonObject  = ( JSONObject ) jsonParser.parse ( data );
      String     codeInputed = ( String ) jsonObject.get ( "codeInputed" );
      Code       c           = this.existsCode ( invoice.getInvNbr ( ) );
      if ( c == null ) {
        return new ResponseEntity ( "CODE_WRONG" , HttpStatus.OK );
      }
      else if ( ! c.getCode ( ).equalsIgnoreCase ( codeInputed ) ) {
        return new ResponseEntity ( "CODE_WRONG" , HttpStatus.OK );
      }
      else {
        c.setLifeCycleState ( "OBS" );
        c = ( Code ) this.codeService.createOrUpdate (
            c , this.getUserContext ( ).getUsername ( ) );
        return new ResponseEntity ( "SUCCESS" , HttpStatus.OK );
      }
    }
    catch ( Exception var7 ) {
      return new ResponseEntity ( var7.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  @RequestMapping (
      value = { "/resendCode" },
      method = { RequestMethod.POST }
  )
  public
  ResponseEntity < Object > resendCode ( @ModelAttribute ( "invoice") InvoiceDTO invoice )
  throws BaseException, ParseException, IOException {
    try {
      List < ConditionSimple > parametersFilters = new ArrayList ( );
      parametersFilters.add (
          new ConditionSimple ( "draftNbr" , SearchOption.EQUAL , invoice.getInvNbr ( ) ) );
      Code c = ( Code ) this.codeService.findEntityByFilters ( parametersFilters );
      if ( c == null ) {
        return new ResponseEntity (
            "No se pudo enviar el código. Intente nuevamente mas tarde." ,
            HttpStatus.BAD_REQUEST
        );
      }
      else {
        String body = "Codigo para Pago: " + c.getCode ( );
        this.emailManager.sendMail ( c.getCustomerEmail ( ) , "Reenvio de codigo" , body );
        return new ResponseEntity ( "SUCCESS" , HttpStatus.OK );
      }
    }
    catch ( Exception var5 ) {
      return new ResponseEntity ( var5.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  private
  Code existsCode ( Integer draft ) throws BaseException {
    List < ConditionSimple > parametersFilters = new ArrayList ( );
    parametersFilters.add ( new ConditionSimple ( "draftNbr" , SearchOption.EQUAL , draft ) );
    Code c = ( Code ) this.codeService.findEntityByFilters ( parametersFilters );
    return c == null ? null : c;
  }

  private
  boolean validateImportUnits ( Integer invNbr ) throws SQLException {
    boolean errors = false;
    String  query  = this.queriesService.findUnitsByDraft ( invNbr );
    System.out.println ( "(findUnitsByDraft):" + query );
    Connection connection =
        this.databaseConnectorService.createN4DBConnection ( );
    Statement                      st    = connection.createStatement ( );
    ResultSet                      rs    = st.executeQuery ( query );
    List < UnitImportFreeDebtDTO > units = new ArrayList ( );

    while ( rs.next ( ) ) {
      UnitImportFreeDebtDTO unit = new UnitImportFreeDebtDTO ( );
      unit.setUnitId ( rs.getString ( "unitId" ) );
      unit.setLdDate ( rs.getTimestamp ( "ldFecha" ) );
      unit.setLdStatus ( rs.getString ( "ldStatus" ) );
      unit.setApptTime ( rs.getTimestamp ( "apptTime" ) );
      units.add ( unit );
    }

    System.out.println ( "Comenzamos a validar los contenedores" );

    for ( int i = 0 ; i < units.size ( ) && ! errors ; ++ i ) {
      if ( ( ( UnitImportFreeDebtDTO ) units.get ( i ) ).getLdStatus ( ) != null
           && ( ( UnitImportFreeDebtDTO ) units.get ( i ) ).getLdStatus ( ).equalsIgnoreCase (
          "OK" ) ) {
        if ( ( ( UnitImportFreeDebtDTO ) units.get ( i ) ).getLdStatus ( ).equalsIgnoreCase (
            "OK" ) ) {
          if ( ( ( UnitImportFreeDebtDTO ) units.get ( i ) ).getLdDate ( ) != null && (
              ( ( UnitImportFreeDebtDTO ) units.get ( i ) ).getLdDate ( ).getTime ( )
              < ( new Date ( ) ).getTime ( )
              || ( ( UnitImportFreeDebtDTO ) units.get ( i ) ).getApptTime ( ) != null
                 && ( ( UnitImportFreeDebtDTO ) units.get ( i ) ).getLdDate ( ).getTime ( )
                    < ( ( UnitImportFreeDebtDTO ) units.get ( i ) ).getApptTime ( ).getTime ( )
          ) ) {
            errors = true;
            System.out.println (
                "encontré un error en el contenedor [" + (
                    ( UnitImportFreeDebtDTO ) units.get (
                        i )
                ).getUnitId ( ) + "]" );
          }
        }
        else {
          errors = true;
          System.out.println (
              "encontré un error en el contenedor [" + (
                  ( UnitImportFreeDebtDTO ) units.get (
                      i )
              ).getUnitId ( ) + "]" );
        }
      }
      else {
        errors = true;
        System.out.println (
            "encontré un error en el contenedor [" + (
                ( UnitImportFreeDebtDTO ) units.get (
                    i )
            ).getUnitId ( ) + "]" );
      }
    }

    return errors;
  }
}
