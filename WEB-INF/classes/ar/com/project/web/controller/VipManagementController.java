//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.restservice.controllers;

import ar.com.project.core.domain.User;
import ar.com.project.core.domain.VipAppointment;
import ar.com.project.core.domain.VipCustomer;
import ar.com.project.core.domain.VipEvent;
import ar.com.project.core.service.PrivilegeService;
import ar.com.project.core.service.ScheduleFilterDefService;
import ar.com.project.core.service.ScheduleFilterService;
import ar.com.project.core.service.ScheduleRuleService;
import ar.com.project.core.service.UserService;
import ar.com.project.core.service.VipApptService;
import ar.com.project.core.service.VipCustomerService;
import ar.com.project.core.service.VipEventService;
import ar.com.project.core.utils.DateUtils;
import ar.com.project.core.utils.EnumUtil;
import ar.com.project.core.utils.ListUtils;
import ar.com.project.core.worker.VipApptRuleTypeEnum;
import com.curcico.jproject.core.exception.BaseException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping ( { "/VipManagement" })
@SessionAttributes ( {
    "userid" , "loginName" , "username" , "parameters" , "fullname" , "firstNameLetter"
}
)
public
class VipManagementController extends CommonController {

  private static final Logger logger = Logger.getLogger (
      VipManagementController.class );
  boolean canAddItem     = false;
  boolean canEditItem    = false;
  boolean canDeleteItem  = false;
  boolean canRecoverItem = false;
  User    user;
  @Autowired
  ScheduleFilterDefService sfdService;
  @Autowired
  ScheduleFilterService    sfpService;
  @Autowired
  ScheduleRuleService      ruleService;
  @Autowired
  PrivilegeService         privilegeService;
  @Autowired
  VipCustomerService       vipCustomerService;
  @Autowired
  VipEventService          vipEventService;
  @Autowired
  VipApptService           vipApptService;
  @Autowired
  UserService              userService;
  private List privilegeInPageList = new ArrayList ( );

  public
  VipManagementController ( ) {
  }

  @RequestMapping (
      value = { "/vipManagement" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  public
  ModelAndView vipManagement (
      HttpServletRequest request , HttpServletResponse response ,
      Object handler
                             ) {
    ModelAndView model       = new ModelAndView ( "vipManagement" );
    String       servletPath = request.getServletPath ( );
    org.springframework.security.core.userdetails.User loggedUser =
        ( org.springframework.security.core.userdetails.User ) SecurityContextHolder
            .getContext ( )
            .getAuthentication ( ).getPrincipal ( );
    String login = loggedUser.getUsername ( );
    this.privilegeInPageList = this.privilegeService.getMenuPagePrivileges ( login , servletPath );
    this.canAddItem          = this.privilegeService.userCanAddItem ( this.privilegeInPageList );
    this.canEditItem         = this.privilegeService.userCanEditItem ( this.privilegeInPageList );
    this.canDeleteItem       = this.privilegeService.userCanDeleteItem ( this.privilegeInPageList );
    this.canRecoverItem      = this.privilegeService.userCanRecoverItem (
        this.privilegeInPageList );
    String apptActionButtonsHtml         = this.getApptActionButtonsHtml ( );
    String apptDataTableHtml             = this.getApptDataTableHtml ( );
    String customerActionButtonsHtml     = this.getCustomerActionButtonsHtml ( );
    String customerDataTableHtml         = this.getCustomerDataTableHtml ( );
    String customerModalButtonsHtml      = this.getVipCustomerModalButtonsHtml ( );
    String customerEventModalButtonsHtml = this.getVipCustomerEventModalButtonsHtml ( );
    String eventActionButtonsHtml        = this.getEventActionButtonsHtml ( );
    String eventDataTableHtml            = this.getEventDataTableHtml ( );
    String eventModalButtonsHtml         = this.getVipEventModalButtonsHtml ( );
    String quotaModalButtonsHtml         = this.getVipApptQuotaModalButtonsHtml ( );
    String apptModalButtonsHtml          = this.getVipApptModalButtonsHtml ( );
    String apptEventModalButtonsHtml     = this.getVipApptEventModalButtonsHtml ( );
    model.addObject ( "apptActionButtons" , apptActionButtonsHtml );
    model.addObject ( "apptDataTable" , apptDataTableHtml );
    model.addObject ( "customerActionButtons" , customerActionButtonsHtml );
    model.addObject ( "customerDataTable" , customerDataTableHtml );
    model.addObject ( "customerModalButtons" , customerModalButtonsHtml );
    model.addObject ( "customerEventModalButtons" , customerEventModalButtonsHtml );
    model.addObject ( "eventActionButtons" , eventActionButtonsHtml );
    model.addObject ( "eventDataTable" , eventDataTableHtml );
    model.addObject ( "eventModalButtons" , eventModalButtonsHtml );
    model.addObject ( "apptModalButtons" , apptModalButtonsHtml );
    model.addObject ( "quotaModalButtons" , quotaModalButtonsHtml );
    model.addObject ( "apptEventModalButtons" , apptEventModalButtonsHtml );
    return model;
  }

  @RequestMapping (
      value = { "/initLoad" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > initLoad ( ) throws SQLException {
    Map  map          = new HashMap ( );
    List categoryList = EnumUtil.getCoordinationCategoryList ( );
    List dayList      = EnumUtil.getSchFilterDayEnumList ( );
    List ruleTypeList = EnumUtil.getVipApptRuleTypeEnumList ( );
    map.put ( "categoryList" , categoryList );
    map.put ( "dayList" , dayList );
    map.put ( "ruleTypeList" , ruleTypeList );
    map.put ( "vipApptList" , this.vipApptService.getActiveVipAppt ( ) );
    map.put ( "vipCustomerList" , this.vipCustomerService.getAllVipCustomer ( ) );
    map.put ( "vipEventList" , this.vipEventService.getAllVipEvent ( ) );
    return new ResponseEntity ( map , HttpStatus.OK );
  }

  @RequestMapping (
      value = { "/loadVipCombos" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > loadVipCombos ( ) {
    Map  map          = new HashMap ( );
    List categoryList = EnumUtil.getCoordinationCategoryList ( );
    List dayList      = EnumUtil.getSchFilterDayEnumList ( );
    List ruleTypeList = EnumUtil.getVipApptRuleTypeEnumList ( );
    map.put ( "categoryList" , categoryList );
    map.put ( "dayList" , dayList );
    map.put ( "ruleTypeList" , ruleTypeList );
    return new ResponseEntity ( map , HttpStatus.OK );
  }

  @RequestMapping (
      value = { "/getActiveVipAppt" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < List > getActiveVipAppt ( ) throws SQLException {
    return new ResponseEntity ( this.vipApptService.getActiveVipAppt ( ) , HttpStatus.OK );
  }

  @RequestMapping (
      value = { "/getAllVipCustomer" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < List > getAllVipCustomer ( ) throws SQLException {
    return new ResponseEntity ( this.vipCustomerService.getAllVipCustomer ( ) , HttpStatus.OK );
  }

  @RequestMapping (
      value = { "/getActiveVipCustomer" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < List > getActiveVipCustomer ( ) throws SQLException {
    return new ResponseEntity ( this.vipCustomerService.getActiveVipCustomer ( ) , HttpStatus.OK );
  }

  @RequestMapping (
      value = { "/getAllVipEvent" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < List > getAllVipEvent ( ) throws SQLException {
    return new ResponseEntity ( this.vipEventService.getAllVipEvent ( ) , HttpStatus.OK );
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/addVipAppt" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > addVipAppt ( @RequestBody Map vipApptMap )
  throws BaseException, ParseException, SQLException {
    if ( ! this.canAddItem ) {
      return new ResponseEntity (
          this.buildErrorResponse ( "No tenés permisos para realizar esta acción" ) ,
          HttpStatus.OK
      );
    }
    else if ( vipApptMap != null && ! vipApptMap.isEmpty ( ) ) {
      String customerGkeyStr = ( String ) vipApptMap.get ( "vipCustomerGkey" );
      String category        = ( String ) vipApptMap.get ( "category" );
      String ruleType        = ( String ) vipApptMap.get ( "ruleType" );
      String dayStr          = ( String ) vipApptMap.get ( "dayStr" );
      String dateStr         = ( String ) vipApptMap.get ( "dateStr" );
      Boolean applyCustomerEvent =
          vipApptMap.get ( "applyCustomerEvent" ) != null ? ( Boolean ) vipApptMap.get (
              "applyCustomerEvent" ) : false;
      String error = "";
      if ( customerGkeyStr == null || customerGkeyStr.isEmpty ( ) ) {
        error = "Seleccioná un cliente vip";
      }

      if ( category == null || category.isEmpty ( ) ) {
        error = error + ( error.isEmpty ( ) ? "<br>" : "" ) + "Seleccioná una categoría";
      }

      if ( ruleType != null && ! ruleType.isEmpty ( ) ) {
        if ( VipApptRuleTypeEnum.DATE.getKey ( ).equals ( ruleType ) && (
            dateStr == null
            || dateStr.isEmpty ( )
        ) ) {
          error = ( error.isEmpty ( ) ? "<br>" : "" ) + "Seleccioná una fecha";
        }

        if ( VipApptRuleTypeEnum.WEEEKDAY.getKey ( ).equals ( ruleType ) && (
            dayStr == null
            || dayStr.isEmpty ( )
        ) ) {
          error = ( error.isEmpty ( ) ? "<br>" : "" ) + "Seleccioná un día de la lista";
        }
      }
      else {
        error = ( error.isEmpty ( ) ? "<br>" : "" ) + "Seleccioná una tipo de filtro";
      }

      if ( ! error.isEmpty ( ) ) {
        return new ResponseEntity ( this.buildErrorResponse ( error ) , HttpStatus.OK );
      }
      else {
        String userLogin = this.getUserLogin ( );
        Integer dayOfWeek = dayStr != null && ! dayStr.isEmpty ( ) ? Integer.valueOf ( dayStr )
                                                                   : null;
        Date date = DateUtils.parseDateNoTime ( dateStr );
        String result = this.vipApptService.addVipAppt ( Integer.valueOf ( customerGkeyStr ) ,
                                                         category ,
                                                         ruleType , dayOfWeek , date , userLogin
                                                       );
        if ( ! result.isEmpty ( ) ) {
          return new ResponseEntity ( this.buildErrorResponse ( result ) , HttpStatus.OK );
        }
        else {
          if ( applyCustomerEvent ) {
            List vipApptList = this.vipApptService.findVipApptByParams ( ( Long ) null ,
                                                                         Long.parseLong (
                                                                             customerGkeyStr ) ,
                                                                         ( String ) null ,
                                                                         category , ruleType ,
                                                                         dayStr , dateStr ,
                                                                         "ACT"
                                                                       );
            if ( vipApptList.isEmpty ( ) || vipApptList.size ( ) > 1 ) {
              return new ResponseEntity ( this.buildErrorResponse (
                  "No se pudo agregar eventos por defecto. Agregá manualmente" ) , HttpStatus.OK );
            }

            Map  row             = ( Map ) vipApptList.get ( 0 );
            Long vipApptGkey     = ( Long ) row.get ( "gkey" );
            Long vipCustomerGkey = ( Long ) row.get ( "vip_customer_gkey" );
            this.vipApptService.addVipCustomerEventByDefault ( vipApptGkey , vipCustomerGkey ,
                                                               userLogin
                                                             );
          }

          return new ResponseEntity (
              this.buildSuccessResponse ( "Turno vip guardado con éxito" ) ,
              HttpStatus.OK
          );
        }
      }
    }
    else {
      return new ResponseEntity (
          this.buildErrorResponse ( "Ingresá la información para crear el turno vip" ) ,
          HttpStatus.OK
      );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/addVipCustomer" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > addVipCustomer ( @RequestBody Map userMap )
  throws BaseException, ParseException, SQLException {
    if ( ! this.canAddItem ) {
      return new ResponseEntity (
          this.buildErrorResponse ( "No tenés permisos para realizar esta acción" ) ,
          HttpStatus.OK
      );
    }
    else if ( userMap != null && ! userMap.isEmpty ( ) ) {
      Integer userGkey = userMap == null ? null : ( Integer ) userMap.get ( "gkey" );
      List userList =
          userGkey == null ? null : this.userService.findUserByGkey ( userGkey.longValue ( ) );
      if ( userGkey != null && userList != null && ! userList.isEmpty ( ) ) {
        String userLogin = this.getUserLogin ( );
        String result    = this.vipCustomerService.addVipCustomer ( userGkey , userLogin );
        return ! result.isEmpty ( ) ? new ResponseEntity (
            this.buildErrorResponse ( result ) ,
            HttpStatus.OK
        )
                                    : new ResponseEntity (
                                        this.buildSuccessResponse (
                                            "Cliente vip guardado con éxito" ) ,
                                        HttpStatus.OK
                                    );
      }
      else {
        return new ResponseEntity ( this.buildErrorResponse (
            "No se pudo cargar la información del cliente. Refrescá la página" ) , HttpStatus.OK );
      }
    }
    else {
      return new ResponseEntity (
          this.buildErrorResponse ( "Ingresá la información de un cliente" ) ,
          HttpStatus.OK
      );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/addVipEvent" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > addVipEvent ( @RequestParam String prEventId )
  throws BaseException, ParseException, SQLException {
    if ( ! this.canAddItem ) {
      return new ResponseEntity (
          this.buildErrorResponse ( "No tenés permisos para realizar esta acción" ) ,
          HttpStatus.OK
      );
    }
    else {
      String error = "";
      prEventId = prEventId.trim ( ).toUpperCase ( );
      if ( prEventId == null || prEventId.isEmpty ( ) ) {
        error = "Ingresá un evento de N4";
      }

      if ( ! this.isNullOrEmpty ( error ) ) {
        return new ResponseEntity ( this.buildErrorResponse ( error ) , HttpStatus.OK );
      }
      else {
        String   userLogin = this.getUserLogin ( );
        VipEvent vipEvent  = new VipEvent ( );
        vipEvent.setEventId ( prEventId );
        String result = this.vipEventService.addVipEvent ( vipEvent , userLogin );
        return ! result.isEmpty ( ) ? new ResponseEntity (
            this.buildErrorResponse ( result ) ,
            HttpStatus.OK
        )
                                    : new ResponseEntity (
                                        this.buildSuccessResponse ( "Evento guardado con éxito" ) ,
                                        HttpStatus.OK
                                    );
      }
    }
  }

  @RequestMapping (
      value = { "/getVipApptEvents" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getVipApptEvents ( @RequestParam String prVipApptGkeyStr )
  throws SQLException {
    Long vipApptGkey =
        prVipApptGkeyStr != null && ! prVipApptGkeyStr.isEmpty ( ) ? Long.parseLong (
            prVipApptGkeyStr )
                                                                   : null;
    if ( vipApptGkey == null ) {
      return new ResponseEntity ( this.buildErrorResponse (
          "No se pudo obtener los eventos del turno vip. Intentá nuevamente" ) , HttpStatus.OK );
    }
    else {
      List apptEventList = this.vipApptService.getVipApptEventList ( vipApptGkey );
      return new ResponseEntity ( apptEventList , HttpStatus.OK );
    }
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/getVipApptQuota" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getVipApptQuota ( @RequestParam String prVipApptGkeyStr )
  throws SQLException, BaseException {
    Long vipApptGkey =
        prVipApptGkeyStr != null && ! prVipApptGkeyStr.isEmpty ( ) ? Long.parseLong (
            prVipApptGkeyStr )
                                                                   : null;
    VipAppointment vipAppt =
        vipApptGkey != null ? this.vipApptService.getVipApptByGkey ( vipApptGkey.intValue ( ) )
                            : null;
    if ( vipAppt == null ) {
      return new ResponseEntity ( this.buildErrorResponse (
          "No se pudo obtener las reservas del turno vip. Intentá nuevamente" ) , HttpStatus.OK );
    }
    else {
      List apptEventList = this.vipApptService.getVipApptQuotaList ( vipApptGkey ,
                                                                     vipAppt.getCategory ( ) ,
                                                                     vipAppt.getDate ( ) ,
                                                                     vipAppt.getDayOfWeek ( )
                                                                   );
      return new ResponseEntity ( apptEventList , HttpStatus.OK );
    }
  }

  @RequestMapping (
      value = { "/getVipCustomerEvents" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getVipCustomerEvents ( @RequestParam String prVipCustomerGkeyStr )
  throws SQLException {
    Long customerGkey =
        prVipCustomerGkeyStr != null && ! prVipCustomerGkeyStr.isEmpty ( ) ? Long.parseLong (
            prVipCustomerGkeyStr ) : null;
    if ( customerGkey == null ) {
      return new ResponseEntity ( this.buildErrorResponse (
          "No se pudo obtener los eventos del cliente vip. Intentá nuevamente" ) , HttpStatus.OK );
    }
    else {
      List customerEventList = this.vipCustomerService.getVipCustomerEventList ( customerGkey );
      return new ResponseEntity ( customerEventList , HttpStatus.OK );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/updateVipApptEvent" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > updateVipApptEvent ( @RequestBody Map vipApptEventMap )
  throws BaseException, ParseException, SQLException {
    if ( ! this.canAddItem && ! this.canEditItem ) {
      return new ResponseEntity (
          this.buildErrorResponse ( "No tenés permisos para realizar esta acción" ) ,
          HttpStatus.OK
      );
    }
    else {
      String apptGkeyStr = ( String ) vipApptEventMap.get ( "apptGkeyStr" );
      Integer apptGkey =
          apptGkeyStr != null && ! apptGkeyStr.isEmpty ( ) ? Integer.parseInt ( apptGkeyStr )
                                                           : null;
      if ( apptGkey == null ) {
        return new ResponseEntity (
            this.buildErrorResponse (
                "No se pudo obtener la información a actualizar. Intentá nuevamente" ) ,
            HttpStatus.OK
        );
      }
      else {
        List eventGkeyStrList = ( List ) vipApptEventMap.get ( "eventGkeyArray" );
        this.vipApptService.updateVipApptEventList ( apptGkey ,
                                                     ListUtils.gkeyStrToLongList (
                                                         eventGkeyStrList ) , this.getUserLogin ( )
                                                   );
        return new ResponseEntity (
            this.buildSuccessResponse ( "Eventos del turno vip actualizados con éxito" ) ,
            HttpStatus.OK
        );
      }
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/updateVipApptQuota" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > updateVipApptQuota ( @RequestBody Object quotaObject )
  throws BaseException, ParseException, SQLException {
    if ( ! this.canAddItem && ! this.canEditItem ) {
      return new ResponseEntity (
          this.buildErrorResponse ( "No tenés permisos para realizar esta acción" ) ,
          HttpStatus.OK
      );
    }
    else {
      List quotaList = new ArrayList ( );
      if ( quotaObject.getClass ( ).isArray ( ) ) {
        quotaList = Arrays.asList ( ( Object[] ) quotaObject );
      }
      else if ( quotaObject instanceof Collection ) {
        quotaList = new ArrayList ( ( Collection ) quotaObject );
      }

      if ( ( ( List ) quotaList ).isEmpty ( ) ) {
        return new ResponseEntity ( this.buildSuccessResponse ( "" ) , HttpStatus.OK );
      }
      else {
        String validationError = this.validateQtyFormat ( ( List ) quotaList );
        if ( ! validationError.isEmpty ( ) ) {
          return new ResponseEntity ( this.buildErrorResponse ( validationError ) , HttpStatus.OK );
        }
        else {
          String result = this.vipApptService.updateApptQuota (
              ( List ) quotaList ,
              this.getUserLogin ( )
                                                              );
          return result.isEmpty ( ) ? new ResponseEntity (
              this.buildSuccessResponse ( "Reservas del turno vip actualizadas con éxito" ) ,
              HttpStatus.OK
          ) : new ResponseEntity ( this.buildErrorResponse ( result ) , HttpStatus.OK );
        }
      }
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/updateVipCustomerEvent" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > updateVipCustomerEvent ( @RequestBody Map vipCustomerEventMap )
  throws BaseException, ParseException, SQLException {
    if ( ! this.canAddItem && ! this.canEditItem ) {
      return new ResponseEntity (
          this.buildErrorResponse ( "No tenés permisos para realizar esta acción" ) ,
          HttpStatus.OK
      );
    }
    else {
      String customerGkeyStr = ( String ) vipCustomerEventMap.get ( "customerGkeyStr" );
      Integer customerGkey =
          customerGkeyStr != null && ! customerGkeyStr.isEmpty ( ) ? Integer.parseInt (
              customerGkeyStr )
                                                                   : null;
      if ( customerGkey == null ) {
        return new ResponseEntity (
            this.buildErrorResponse (
                "No se pudo obtener la información a actualizar. Intentá nuevamente" ) ,
            HttpStatus.OK
        );
      }
      else {
        List eventGkeyStrList = ( List ) vipCustomerEventMap.get ( "eventGkeyArray" );
        this.vipCustomerService.updateVipCustomerEventList ( customerGkey ,
                                                             ListUtils.gkeyStrToLongList (
                                                                 eventGkeyStrList ) ,
                                                             this.getUserLogin ( )
                                                           );
        return new ResponseEntity (
            this.buildSuccessResponse ( "Eventos del cliente vip actualizados con éxito" ) ,
            HttpStatus.OK
        );
      }
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/deleteVipAppt" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > deleteVipAppt ( @RequestBody Map vipApptMap )
  throws BaseException, ParseException {
    if ( this.canDeleteItem ) {
      Integer        gkey    = ( Integer ) vipApptMap.get ( "gkey" );
      VipAppointment vipAppt = this.vipApptService.getVipApptByGkey ( gkey );
      if ( vipAppt == null ) {
        return new ResponseEntity (
            this.buildSuccessResponse ( "El turno vip no existe. Refrescá la página" ) ,
            HttpStatus.OK
        );
      }
      else {
        this.vipApptService.deleteVipAppt ( vipAppt , this.getUserLogin ( ) );
        return new ResponseEntity (
            this.buildSuccessResponse ( "Turno vip eliminado con éxito" ) ,
            HttpStatus.OK
        );
      }
    }
    else {
      return new ResponseEntity (
          this.buildErrorResponse ( "No tenés permisos para realizar esta acción" ) ,
          HttpStatus.OK
      );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/deleteVipCustomer" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > deleteVipCustomer ( @RequestBody Map vipCustomerMap )
  throws BaseException, ParseException {
    if ( this.canDeleteItem ) {
      Integer     gkey        = ( Integer ) vipCustomerMap.get ( "gkey" );
      VipCustomer vipCustomer = this.vipCustomerService.getVipCustomerByGkey ( gkey );
      if ( vipCustomer == null ) {
        return new ResponseEntity (
            this.buildSuccessResponse ( "El cliente vip no existe. Refrescá la página" ) ,
            HttpStatus.OK
        );
      }
      else {
        this.vipCustomerService.deleteVipCustomer ( vipCustomer , this.getUserLogin ( ) );
        return new ResponseEntity (
            this.buildSuccessResponse ( "Cliente vip eliminado con éxito" ) ,
            HttpStatus.OK
        );
      }
    }
    else {
      return new ResponseEntity (
          this.buildErrorResponse ( "No tenés permisos para realizar esta acción" ) ,
          HttpStatus.OK
      );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/deleteVipEvent" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > deleteVipEvent ( @RequestBody Map vipEventMap )
  throws BaseException, ParseException {
    if ( this.canDeleteItem ) {
      Integer  gkey     = ( Integer ) vipEventMap.get ( "gkey" );
      VipEvent vipEvent = this.vipEventService.getVipEventByGkey ( gkey );
      if ( vipEvent == null ) {
        return new ResponseEntity (
            this.buildSuccessResponse ( "El evento no existe. Refrescá la página" ) ,
            HttpStatus.OK
        );
      }
      else {
        this.vipEventService.deleteVipEvent ( vipEvent , this.getUserLogin ( ) );
        return new ResponseEntity (
            this.buildSuccessResponse ( "Evento eliminado con éxito" ) ,
            HttpStatus.OK
        );
      }
    }
    else {
      return new ResponseEntity (
          this.buildErrorResponse ( "No tenés permisos para realizar esta acción" ) ,
          HttpStatus.OK
      );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/recoverVipCustomer" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > recoverVipCustomer ( @RequestBody Map vipCustomerMap )
  throws BaseException, ParseException {
    if ( this.canRecoverItem ) {
      Integer     gkey        = ( Integer ) vipCustomerMap.get ( "gkey" );
      VipCustomer vipCustomer = this.vipCustomerService.getVipCustomerByGkey ( gkey );
      if ( vipCustomer == null ) {
        return new ResponseEntity (
            this.buildSuccessResponse ( "El cliente vip no existe. Refrescá la página" ) ,
            HttpStatus.OK
        );
      }
      else {
        this.vipCustomerService.recoverVipCustomer ( vipCustomer , this.getUserLogin ( ) );
        return new ResponseEntity (
            this.buildSuccessResponse ( "Cliente vip activado con éxito" ) ,
            HttpStatus.OK
        );
      }
    }
    else {
      return new ResponseEntity (
          this.buildErrorResponse ( "No tenés permisos para realizar esta acción" ) ,
          HttpStatus.OK
      );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/recoverVipEvent" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > recoverVipEvent ( @RequestBody Map vipEventMap )
  throws BaseException, ParseException {
    if ( this.canRecoverItem ) {
      Integer  gkey     = ( Integer ) vipEventMap.get ( "gkey" );
      VipEvent vipEvent = this.vipEventService.getVipEventByGkey ( gkey );
      if ( vipEvent == null ) {
        return new ResponseEntity (
            this.buildSuccessResponse ( "El evento no existe. Refrescá la página" ) ,
            HttpStatus.OK
        );
      }
      else {
        this.vipEventService.recoverVipEvent ( vipEvent , this.getUserLogin ( ) );
        return new ResponseEntity (
            this.buildSuccessResponse ( "Evento activado con éxito" ) ,
            HttpStatus.OK
        );
      }
    }
    else {
      return new ResponseEntity (
          this.buildErrorResponse ( "No tenés permisos para realizar esta acción" ) ,
          HttpStatus.OK
      );
    }
  }

  @RequestMapping (
      value = { "/findVipAppt" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < List > findVipAppt (
      @RequestParam String prCustomerWildCard ,
      String prCategory , String prDayStr
                                      ) throws SQLException {
    return new ResponseEntity (
        this.vipApptService.findVipApptByParams ( ( Long ) null , ( Long ) null ,
                                                  prCustomerWildCard ,
                                                  prCategory , ( String ) null , prDayStr ,
                                                  ( String ) null , "ACT"
                                                ) , HttpStatus.OK );
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/findUser" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > findUser ( @RequestParam String prWildcard )
  throws BaseException, ParseException, SQLException {
    prWildcard = prWildcard.trim ( ).replaceAll ( "\t" , "" ).replaceAll ( "\r" , "" )
                           .toUpperCase ( );
    if ( prWildcard != null && ! prWildcard.isEmpty ( ) ) {
      Object userResult = this.vipCustomerService.findUserByWildcard ( prWildcard );
      return userResult instanceof String ? new ResponseEntity (
          this.buildErrorResponse ( ( String ) userResult ) , HttpStatus.OK )
                                          : new ResponseEntity ( userResult , HttpStatus.OK );
    }
    else {
      return new ResponseEntity (
          this.buildErrorResponse ( "Ingresá el nombre o CUIT del cliente" ) ,
          HttpStatus.OK
      );
    }
  }

  private
  String getUserLogin ( ) {
    org.springframework.security.core.userdetails.User loggedUser =
        ( org.springframework.security.core.userdetails.User ) SecurityContextHolder
            .getContext ( )
            .getAuthentication ( ).getPrincipal ( );
    String login = loggedUser.getUsername ( );
    return login;
  }

  private
  String getApptActionButtonsHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    if ( this.canAddItem ) {
      sb.append (
          "<button type=\"button\" class=\"btn btn-primary\" id=\"btnAddVipAppt\" "
          + "data-toggle=\"modal\" data-target=\"#addVipApptModal\">Agregar</button>&nbsp;&nbsp;" );
    }

    return sb.toString ( );
  }

  private
  String getCustomerActionButtonsHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    if ( this.canAddItem ) {
      sb.append (
          "<button type=\"button\" class=\"btn btn-primary\" id=\"btnAddVipCustomer\" "
          + "data-toggle=\"modal\" data-target=\"#addVipCustomerModal\">Agregar</button>&nbsp;"
          + "&nbsp;" );
    }

    return sb.toString ( );
  }

  private
  String getEventActionButtonsHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    if ( this.canAddItem ) {
      sb.append (
          "<button type=\"button\" class=\"btn btn-primary\" id=\"btnAddVipEvent\" "
          + "data-toggle=\"modal\" data-target=\"#addVipEventModal\">Agregar</button>&nbsp;&nbsp;"
                );
    }

    return sb.toString ( );
  }

  private
  String getVipApptModalButtonsHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    if ( this.canAddItem || this.canEditItem ) {
      sb.append (
          "<button type=\"button\" class=\"btn btn-primary\" "
          + "id=\"btnSaveVipAppt\">Guardar</button>" );
    }

    return sb.toString ( );
  }

  private
  String getVipCustomerModalButtonsHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    if ( this.canAddItem ) {
      sb.append (
          "<button type=\"button\" class=\"btn btn-primary\" "
          + "id=\"btnSaveVipCustomer\">Guardar</button>&nbsp;&nbsp;" );
    }

    return sb.toString ( );
  }

  private
  String getVipEventModalButtonsHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    if ( this.canAddItem || this.canEditItem ) {
      sb.append (
          "<button type=\"button\" class=\"btn btn-primary\" "
          + "id=\"btnSaveVipEvent\">Guardar</button>&nbsp;&nbsp;" );
    }

    return sb.toString ( );
  }

  private
  String getVipApptEventModalButtonsHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    if ( this.canAddItem || this.canEditItem ) {
      sb.append (
          "<button type=\"button\" class=\"btn btn-primary\" "
          + "id=\"btnSaveVipApptEvent\">Guardar</button>&nbsp;&nbsp;" );
    }

    return sb.toString ( );
  }

  private
  String getVipApptQuotaModalButtonsHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    if ( this.canAddItem || this.canEditItem ) {
      sb.append (
          "<button type=\"button\" class=\"btn btn-primary\" "
          + "id=\"btnSaveApptQuota\">Guardar</button>" );
    }

    return sb.toString ( );
  }

  private
  String getVipCustomerEventModalButtonsHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    if ( this.canAddItem || this.canEditItem ) {
      sb.append (
          "<button type=\"button\" class=\"btn btn-primary\" "
          + "id=\"btnSaveVipCustomerEvent\">Guardar</button>&nbsp;&nbsp;" );
    }

    return sb.toString ( );
  }

  private
  String getApptDataTableHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    sb.append ( "<thead>\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <th>Cliente</th>\n" );
    sb.append ( "    <th># Documento</th>\n" );
    sb.append ( "    <th>Categoría</th>\n" );
    sb.append ( "    <th>Tipo Filtro</th>\n" );
    sb.append ( "    <th>Valor</th>\n" );
    sb.append ( "    <th>Turnos reservados</th>\n" );
    sb.append ( "    <th>Eventos</th>\n" );
    sb.append ( "    <th>Creado por</th>\n" );
    sb.append ( "    <th>Fecha creación</th>\n" );
    if ( this.canDeleteItem ) {
      sb.append ( "    <th></th>\n" );
    }

    sb.append ( "</tr>\n" );
    sb.append ( "</thead>\n" );
    sb.append ( "<tbody data-bind=\"foreach: vipViewModel.apptList\">\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <td><label data-bind=\"text: full_name\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: document_nbr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: category\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: rule_type_str\"></label></td>\n" );
    sb.append (
        "    <td><label data-bind=\"text: rule_type == 'WEEKDAY'? day_str : "
        + "date_str\"></label></td>\n" );
    sb.append (
        "\t   <td><a href=\"#\" style=\"color:#fd7e14;\" data-bind=\"text: quota_count, click: "
        + "$parent.viewApptQuota\"></a></td>\n" );
    sb.append (
        "\t   <td><a href=\"#\" style=\"color:#fd7e14;\" data-bind=\"text: event_count, click: "
        + "$parent.viewApptEvent\"></a></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: creator\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: created_str\"></label></td>\n" );
    if ( this.canDeleteItem ) {
      sb.append (
          "    <td><button class=\"btn btn-icon btn-danger btn-sm\" data-bind=\"click: $parent"
          + ".deleteAppt\"> <i class=\"ti-close\"></i> </button></td>\n" );
    }

    sb.append ( "</tr>\n" );
    return sb.toString ( );
  }

  private
  String getCustomerDataTableHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    sb.append ( "<thead>\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <th># Documento</th>\n" );
    sb.append ( "    <th>Nombre</th>\n" );
    sb.append ( "    <th>Eventos</th>\n" );
    sb.append ( "    <th>Creado por</th>\n" );
    sb.append ( "    <th>Fecha creación</th>\n" );
    sb.append ( "    <th>Act. por</th>\n" );
    sb.append ( "    <th>Fecha Act.</th>\n" );
    if ( this.canDeleteItem ) {
      sb.append ( "    <th></th>\n" );
    }

    if ( this.canRecoverItem ) {
      sb.append ( "    <th></th>\n" );
    }

    sb.append ( "</tr>\n" );
    sb.append ( "</thead>\n" );
    sb.append ( "<tbody data-bind=\"foreach: vipViewModel.customerList\">\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <td><label data-bind=\"text: document_nbr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: full_name\"></label></td>\n" );
    sb.append (
        "\t   <td><a href=\"#\" style=\"color:#fd7e14;\" data-bind=\"text: event_count, click: "
        + "$parent.viewCustomerEvent\"></a></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: creator\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: created_str\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: changer\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: changed_str\"></label></td>\n" );
    if ( this.canDeleteItem ) {
      sb.append (
          "    <td><button class=\"btn btn-icon btn-danger btn-sm\" data-bind=\"click: $parent"
          + ".deleteCustomer, enable: life_cycle_state == 'ACT'\"> <i class=\"ti-close\"></i> "
          + "</button></td>\n" );
    }

    if ( this.canRecoverItem ) {
      sb.append (
          "    <td><button class=\"btn btn-icon btn-warning btn-sm\" data-bind=\"click: $parent"
          + ".recoverCustomer, enable: life_cycle_state == 'OBS'\"> <i class=\"ti-reload\"></i> "
          + "</button></td>\n" );
    }

    sb.append ( "</tr>\n" );
    return sb.toString ( );
  }

  private
  String getEventDataTableHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    sb.append ( "<thead>\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <th>Id</th>\n" );
    sb.append ( "    <th>Entidad</th>\n" );
    sb.append ( "    <th>Creado por</th>\n" );
    sb.append ( "    <th>Fecha creación</th>\n" );
    sb.append ( "    <th>Act. por</th>\n" );
    sb.append ( "    <th>Fecha Act.</th>\n" );
    if ( this.canDeleteItem ) {
      sb.append ( "    <th></th>\n" );
    }

    if ( this.canRecoverItem ) {
      sb.append ( "    <th></th>\n" );
    }

    sb.append ( "</tr>\n" );
    sb.append ( "</thead>\n" );
    sb.append ( "<tbody data-bind=\"foreach: vipViewModel.eventList\">\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <td><label data-bind=\"text: event_type_id\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: applies_to\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: creator\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: created_str\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: changer\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: changed_str\"></label></td>\n" );
    if ( this.canDeleteItem ) {
      sb.append (
          "    <td><button class=\"btn btn-icon btn-danger btn-sm\" data-bind=\"click: $parent"
          + ".deleteEvent, enable: life_cycle_state == 'ACT'\"> <i class=\"ti-close\"></i> "
          + "</button></td>\n" );
    }

    if ( this.canRecoverItem ) {
      sb.append (
          "    <td><button class=\"btn btn-icon btn-warning btn-sm\" data-bind=\"click: $parent"
          + ".recoverEvent, enable: life_cycle_state == 'OBS'\"> <i class=\"ti-reload\"></i> "
          + "</button></td>\n" );
    }

    sb.append ( "</tr>\n" );
    return sb.toString ( );
  }

  private
  String validateQtyFormat ( List quotaList ) {
    for ( int i = 0 ; i < quotaList.size ( ) ; ++ i ) {
      Map    row   = ( Map ) quotaList.get ( i );
      String value = ( String ) row.get ( "value" );
      if ( value != null && ! value.isEmpty ( ) && ! value.matches ( "^[0-9]*$" ) ) {
        return "La cantidad \"" + value + "\" no es válida";
      }
    }

    return "";
  }

  private
  Map buildSuccessResponse ( String msg ) {
    return this.buildResponse ( "OK" , msg );
  }

  private
  Map buildErrorResponse ( String msg ) {
    return this.buildResponse ( "ERROR" , msg );
  }

  private
  Map buildResponse ( String status , String msg ) {
    Map map = new HashMap ( );
    map.put ( "status" , status );
    map.put ( "msg" , msg );
    return map;
  }

  private
  boolean isNullOrEmpty ( String str ) {
    return str == null || str.isEmpty ( );
  }
}
