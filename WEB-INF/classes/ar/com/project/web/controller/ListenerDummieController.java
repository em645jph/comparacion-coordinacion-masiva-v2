//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.restservice.controllers;

import com.curcico.jproject.core.exception.BaseException;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping ( { "/listenerDummie" })
public
class ListenerDummieController {

  private static final Logger logger = Logger.getLogger ( ListenerDummieController.class );

  public
  ListenerDummieController ( ) {
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/invoke" }
  )
  @ResponseBody
  public
  String invoke ( ) throws BaseException {
    logger.debug ( "/listenerDummie/delete" );
    return "OK";
  }
}
