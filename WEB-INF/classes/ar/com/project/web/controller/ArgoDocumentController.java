//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.restservice.controllers;

import com.curcico.jproject.core.exception.BaseException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
public
class ArgoDocumentController extends CommonController {

  private static final Logger logger = Logger.getLogger ( ArgoDocumentController.class );

  public
  ArgoDocumentController ( ) {
  }

  public
  ResponseEntity < String > uploadFileToServer ( MultipartHttpServletRequest file )
  throws UnsupportedEncodingException, BaseException {
    ResponseEntity < String > response = null;
    Iterator < String >       itr      = file.getFileNames ( );
    MultipartFile             mpf      = null;

    while ( itr.hasNext ( ) ) {
      mpf = file.getFile ( ( String ) itr.next ( ) );
      String encName = new String (
          mpf.getOriginalFilename ( ).getBytes ( "iso-8859-1" ) , "UTF-8" );
      logger.debug ( "filename: " + encName );
      FileOutputStream fos = null;

      ResponseEntity var12;
      try {
        byte[] byteArrayFile = mpf.getBytes ( );
        String ext           = encName.substring ( encName.lastIndexOf ( "." ) + 1 );
        if ( "pdf".equalsIgnoreCase ( ext ) ) {
          String fullPathToFile = "C:\\Temp\\";
          File   f              = new File ( fullPathToFile + File.separator + encName );
          fos = new FileOutputStream ( f );
          fos.write ( byteArrayFile );
          response = new ResponseEntity ( HttpStatus.OK );
          continue;
        }

        response = new ResponseEntity ( "invalid.file.error" , HttpStatus.BAD_REQUEST );
        var12    = response;
      }
      catch ( IOException var21 ) {
        logger.error ( "file.transfer.error" , var21 );
        response = new ResponseEntity ( "file.transfer.error" , HttpStatus.BAD_REQUEST );
        continue;
      }
      finally {
        try {
          if ( fos != null ) {
            fos.close ( );
          }
        }
        catch ( IOException var20 ) {
          logger.error ( "Error while closing stream: " + var20 );
        }

      }

      return var12;
    }

    return response;
  }
}
