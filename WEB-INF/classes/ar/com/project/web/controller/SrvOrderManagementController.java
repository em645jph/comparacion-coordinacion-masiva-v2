//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//


package com.example.restservice.controllers;

import ar.com.project.core.domain.User;
import ar.com.project.core.dto.DocumentApptDTO;
import ar.com.project.core.service.AppointmentService;
import ar.com.project.core.service.AuditService;
import ar.com.project.core.service.LineOpService;
import ar.com.project.core.service.ScheduleRuleService;
import ar.com.project.core.service.SrvOrderService;
import ar.com.project.core.utils.DateUtils;
import ar.com.project.core.utils.EnumUtil;
import ar.com.project.core.utils.StringUtils;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.NumberUtils;
import org.apache.log4j.Logger;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping ( { "/SrvOrderManagement" })
@SessionAttributes ( {
    "userid" , "loginName" , "username" , "parameters" , "fullname" , "firstNameLetter"
}
)
public
class SrvOrderManagementController extends CommonController {

  private static final Logger logger = Logger.getLogger (
      SrvOrderManagementController.class );
  boolean canAddItem    = true;
  boolean canDeleteItem = true;
  User    user;
  @Autowired
  SrvOrderService     soService;
  @Autowired
  AppointmentService  apptService;
  @Autowired
  LineOpService       lineOpService;
  @Autowired
  ScheduleRuleService ruleService;
  @Autowired
  AuditService        auditService;
  private List privilegeInPageList = new ArrayList ( );

  public
  SrvOrderManagementController ( ) {
  }

  @RequestMapping (
      value = { "/srvOrderManagement" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  public
  ModelAndView srvOrderManagement (
      HttpServletRequest request , HttpServletResponse response ,
      Object handler
                                  ) {
    ModelAndView model         = new ModelAndView ( "srvOrderManagement" );
    String       dataTableHtml = this.getDataTableHtml ( this.canAddItem , this.canDeleteItem );
    model.addObject ( "dataTable" , dataTableHtml );
    return model;
  }

  @RequestMapping (
      value = { "/loadCombos" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > loadCombos ( ) {
    ResponseEntity response;
    try {
      List lineOpList           = this.lineOpService.getAllLineOp ( );
      List srvOrderCategoryList = this.soService.getSrvOrderCategoryList ( );
      Map  map                  = new HashMap ( );
      map.put ( "lineOpList" , lineOpList );
      map.put ( "soCategoryList" , srvOrderCategoryList );
      response = new ResponseEntity ( map , HttpStatus.OK );
    }
    catch ( SQLException var5 ) {
      response = new ResponseEntity (
          "No se pudo cargar la página correctamente. Por favor refresque la página" ,
          HttpStatus.BAD_REQUEST
      );
      var5.printStackTrace ( );
    }

    return response;
  }

  @RequestMapping (
      value = { "/findUnitService" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > findUnitService (
      @RequestParam String prCategory ,
      @RequestParam String prKeyParameter , @RequestParam String prLineOpId
                                            )
  throws SQLException, ServiceException, IOException {
    List documentApptList = this.soService.findDocumentService ( prCategory , prKeyParameter ,
                                                                 prLineOpId
                                                               );
    if ( documentApptList != null && ! documentApptList.isEmpty ( ) ) {
      DocumentApptDTO documentDto     = ( DocumentApptDTO ) documentApptList.get ( 0 );
      List            unitServiceList = this.soService.findUnitService ( prCategory , documentDto );
      Map             map             = new HashMap ( );
      map.put ( "unitServiceList" , unitServiceList );
      map.put ( "serviceDocument" , documentDto );
      if ( unitServiceList.isEmpty ( ) ) {
        map.put ( "question" , "Desea coordinar un servicio?" );
      }

      return new ResponseEntity ( map , HttpStatus.OK );
    }
    else {
      return new ResponseEntity (
          this.buildErrorResponse ( "No se encontró el contenedor ingresado" ) ,
          HttpStatus.OK
      );
    }
  }

  @RequestMapping (
      value = { "/loadModalCombos" },
      method = { RequestMethod.POST },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > loadModalCombos ( @RequestBody Object prDocumentDto )
  throws SQLException, ServiceException, IOException {
    Integer documentGkey = ( Integer ) ( ( Map ) prDocumentDto ).get ( "gkey" );
    String  documentNbr  = ( String ) ( ( Map ) prDocumentDto ).get ( "nbr" );
    List activeUnitList = this.soService.findImportActiveUnitByDocument (
        documentGkey.longValue ( ) ,
        documentNbr
                                                                        );
    List serviceTypeList = this.soService.getSrvOrderSubTypeList ( );
    Map  map             = new HashMap ( );
    map.put ( "activeUnitList" , activeUnitList );
    map.put ( "serviceTypeList" , serviceTypeList );
    return new ResponseEntity ( map , HttpStatus.OK );
  }

  @RequestMapping (
      value = { "/getServiceDates" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getServiceDates (
      @RequestParam String prUnitGkeyStr ,
      @RequestParam String prServiceTypeKey
                                            ) throws SQLException, ServiceException, IOException {
    Long unitGkey =
        prUnitGkeyStr != null && ! prUnitGkeyStr.isEmpty ( ) ? Long.parseLong ( prUnitGkeyStr )
                                                             : null;
    Map    map        = this.soService.getServiceMinMaxDate ( unitGkey , prServiceTypeKey );
    String warningMsg = ( String ) map.get ( "warningMsg" );
    if ( warningMsg != null && ! warningMsg.isEmpty ( ) ) {
      return new ResponseEntity ( this.buildWarningResponse ( warningMsg ) , HttpStatus.OK );
    }
    else {
      return ! this.soService.isServiceableEvent ( prServiceTypeKey ) ? new ResponseEntity (
          this.buildQuestionResponse ( "El servicio no requiere coordinación" ) , HttpStatus.OK )
                                                                      : new ResponseEntity (
                                                                          map , HttpStatus.OK );
    }
  }

  @RequestMapping (
      value = { "/getServiceOpenings" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getServiceOpenings (
      @RequestParam String prUnitGkeyStr ,
      @RequestParam String prServiceTypeKey
                                               )
  throws SQLException, ServiceException, IOException {
    return this.getOpenings ( prUnitGkeyStr , prServiceTypeKey , ( String ) null , true );
  }

  @RequestMapping (
      value = { "/resolveCalendarEvent" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > resolveCalendarEvent (
      @RequestParam String prUnitGkeyStr ,
      @RequestParam String prServiceTypeKey , @RequestParam String prEventId ,
      @RequestParam String prEventTitle
                                                 )
  throws SQLException, ServiceException, IOException {
    return NumberUtils.isNumber ( prEventTitle ) && prEventId.contains ( "-" )
           ? this.getDateOpenings (
        prUnitGkeyStr , prServiceTypeKey , prEventId )
           : new ResponseEntity ( ( MultiValueMap ) null , HttpStatus.OK );
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/createUnitService" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > createUnitService (
      @RequestParam String prUnitGkeyStr ,
      @RequestParam String prServiceTypeKey , @RequestParam String prSelectedDateTime ,
      @RequestParam String prEmail , @RequestParam String prBunch
                                              ) throws IOException, SQLException {
    boolean isServiceableEvent = this.soService.isServiceableEvent ( prServiceTypeKey );
    String  errorMsg           = "";
    if ( ! isServiceableEvent || prSelectedDateTime != null && ! prSelectedDateTime.isEmpty ( ) ) {
      if ( prServiceTypeKey != null && ! prServiceTypeKey.isEmpty ( ) ) {
        if ( prUnitGkeyStr == null || prUnitGkeyStr.isEmpty ( ) ) {
          errorMsg = "El número de contenedor no es válido. Por favor vuelva a intentar";
        }
      }
      else {
        errorMsg = "Seleccionó un tipo de servicio inválido. Por favor vuelva a intentar";
      }
    }
    else {
      errorMsg = "La fecha/hora seleccionada no es válida. Por favor vuelva a intentar";
    }

    if ( ! errorMsg.isEmpty ( ) ) {
      return new ResponseEntity ( this.buildErrorResponse ( errorMsg ) , HttpStatus.OK );
    }
    else {
      String ruleValidation = this.ruleService.validateSelectedDateTime ( prServiceTypeKey , false ,
                                                                          prSelectedDateTime , false
                                                                        );
      if ( ruleValidation != null && ! ruleValidation.isEmpty ( ) ) {
        return new ResponseEntity ( this.buildErrorResponse ( ruleValidation ) , HttpStatus.OK );
      }
      else {
        String result = ( String ) this.soService.createUnitService ( prUnitGkeyStr ,
                                                                      prServiceTypeKey ,
                                                                      prSelectedDateTime , prEmail ,
                                                                      prBunch
                                                                    );
        if ( ! result.isEmpty ( ) ) {
          return new ResponseEntity ( this.buildErrorResponse ( result ) , HttpStatus.OK );
        }
        else {
          List < String > data = new ArrayList ( );
          data.add ( "prUnitGkeyStr:" + prUnitGkeyStr );
          data.add ( "prServiceTypeKey:" + prServiceTypeKey );
          data.add ( "prSelectedDateTime:" + prSelectedDateTime );
          data.add ( "prEmail:" + prEmail );
          data.add ( "prBunch:" + prBunch );
          this.auditService.saveAuditTransaction ( "createUnitService" , data.toString ( ) ,
                                                   this.getUserContext ( ).getUsername ( )
                                                 );
          String msg = "Coordinado correctamente";
          String scannerMsg = "Recuerde presentar la carpeta en Scanner 24 hs antes del turno"
                              + ".\nProcure coordinar el SCANNER previo a la verificación";
          msg = msg + (
              this.soService.isScannerService ( prServiceTypeKey ) ? ".\n" + scannerMsg : ""
          );
          return new ResponseEntity ( this.buildSuccessResponse ( msg ) , HttpStatus.OK );
        }
      }
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/cancelUnitService" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > cancelUnitService ( @RequestBody Object unitServiceDtoObject )
  throws IOException, SQLException {
    if ( this.canDeleteItem ) {
      Map    unitServiceMap        = ( Map ) unitServiceDtoObject;
      String serviceTypeKey        = ( String ) unitServiceMap.get ( "srvOrderTypeKey" );
      String serviceOrderStartDate = ( String ) unitServiceMap.get ( "srvOrderStartDateStr" );
      String ruleValidation = this.ruleService.validateSelectedDateTime ( serviceTypeKey , true ,
                                                                          serviceOrderStartDate ,
                                                                          false
                                                                        );
      if ( ruleValidation != null && ! ruleValidation.isEmpty ( ) ) {
        return new ResponseEntity ( this.buildErrorResponse ( ruleValidation ) , HttpStatus.OK );
      }
      else {
        String result = this.soService.cancelUnitService ( unitServiceMap );
        if ( result != null && ! result.isEmpty ( ) ) {
          return new ResponseEntity ( this.buildErrorResponse ( result ) , HttpStatus.OK );
        }
        else {
          List < String > data = new ArrayList ( );
          data.add ( "serviceTypeKey:" + serviceTypeKey );
          data.add ( "serviceOrderStartDate:" + serviceOrderStartDate );
          data.add ( "ruleValidation:" + ruleValidation );
          this.auditService.saveAuditTransaction ( "cancelUnitService" , data.toString ( ) ,
                                                   this.getUserContext ( ).getUsername ( )
                                                 );
          return new ResponseEntity (
              this.buildSuccessResponse ( "Se canceló el servicio con éxito" ) ,
              HttpStatus.OK
          );
        }
      }
    }
    else {
      return new ResponseEntity (
          this.buildErrorResponse ( "No tiene permisos para realizar esta acción" ) ,
          HttpStatus.OK
      );
    }
  }

  private
  ResponseEntity < Object > getDateOpenings (
      String unitGkeyStr , String serviceTypeKey ,
      String requestedDateStr
                                            ) throws SQLException, ServiceException, IOException {
    return this.getOpenings ( unitGkeyStr , serviceTypeKey , requestedDateStr , false );
  }

  private
  ResponseEntity < Object > getOpenings (
      String prUnitGkeyStr , String prServiceTypeKey ,
      String prRequestedDateStr , boolean totalsOnly
                                        )
  throws SQLException, ServiceException, IOException {
    Long unitGkey =
        prUnitGkeyStr != null && ! prUnitGkeyStr.isEmpty ( ) ? Long.parseLong ( prUnitGkeyStr )
                                                             : null;
    Date minDate;
    Date maxDate;
    if ( prRequestedDateStr != null && ! prRequestedDateStr.isEmpty ( ) ) {
      minDate = DateUtils.parseDate ( StringUtils.addDefaultTimeToDateStr ( prRequestedDateStr ) );
      maxDate = DateUtils.addDays ( minDate , 1 );
    }
    else {
      Map map = this.soService.getServiceMinMaxDate ( unitGkey , prServiceTypeKey );
      minDate = ( Date ) map.get ( "serviceMinDate" );
      maxDate = ( Date ) map.get ( "serviceMaxDate" );
    }

    List serviceOpeningList = this.soService.getServiceOpening ( new HashMap ( ) ,
                                                                 prServiceTypeKey ,
                                                                 minDate , maxDate , totalsOnly
                                                               );
    if ( totalsOnly ) {
      return new ResponseEntity ( serviceOpeningList , HttpStatus.OK );
    }
    else {
      Map     map                   = new HashMap ( );
      boolean isVerificationService = this.soService.isVerificationService ( prServiceTypeKey );
      String  serviceFieldToShow    = "#serviceFieldId2";
      if ( isVerificationService ) {
        serviceFieldToShow = "#serviceFieldId1";
        map.put ( "bunchList" , EnumUtil.getSrvOrderBunchList ( ) );
      }

      map.put ( "openingList" , serviceOpeningList );
      map.put ( "serviceField" , serviceFieldToShow );
      return new ResponseEntity ( map , HttpStatus.OK );
    }
  }

  private
  String getUserLogin ( ) {
    org.springframework.security.core.userdetails.User loggedUser =
        ( org.springframework.security.core.userdetails.User ) SecurityContextHolder
            .getContext ( )
            .getAuthentication ( ).getPrincipal ( );
    String login = loggedUser.getUsername ( );
    return login;
  }

  private
  String getDataTableHtml ( boolean canAddItem , boolean canDeleteItem ) {
    StringBuilder sb = new StringBuilder ( );
    sb.append ( "<thead>\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <th>Contenedor</th>\n" );
    sb.append ( "    <th>Categoría</th>\n" );
    sb.append ( "    <th>Tipo</th>\n" );
    sb.append ( "    <th>Línea</th>\n" );
    sb.append ( "    <th>Estado</th>\n" );
    sb.append ( "    <th>Documento</th>\n" );
    sb.append ( "    <th>Nave</th>\n" );
    sb.append ( "    <th>Viaje</th>\n" );
    sb.append ( "    <th>Fecha</th>\n" );
    sb.append ( "    <th>Turno</th>\n" );
    sb.append ( "    <th>Servicio</th>\n" );
    sb.append ( "    <th>Fecha Serv.</th>\n" );
    sb.append ( "    <th>Estado Serv.</th>\n" );
    if ( canDeleteItem ) {
      sb.append ( "    <th>Anular</th>\n" );
    }

    sb.append ( "</tr>\n" );
    sb.append ( "</thead>\n" );
    sb.append ( "<tbody data-bind=\"foreach: soViewModel.unitServiceTable\">\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitId\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitCategory\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitIsoTypeId\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitLineOpId\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: ufvTransitStateDesc\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitDocumentNbr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitVesselName\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitVoyage\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitLimitDateStr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: apptRequestedDateStr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: srvOrderTypeDescription\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: srvOrderStartDateStr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: srvOrderStatus\"></label></td>\n" );
    if ( canDeleteItem ) {
      sb.append (
          "    <td><button class=\"btn btn-icon btn-danger btn-sm\" data-bind=\"click: $parent"
          + ".cancelSo, enable: allowToCancelService\"> <i class=\"ti-close\"></i> "
          + "</button></td>\n" );
    }

    sb.append ( "</tr>\n" );
    return sb.toString ( );
  }

  private
  Map buildSuccessResponse ( String msg ) {
    return this.buildResponse ( "OK" , msg );
  }

  private
  Map buildErrorResponse ( String msg ) {
    return this.buildResponse ( "ERROR" , msg );
  }

  private
  Map buildQuestionResponse ( String msg ) {
    return this.buildResponse ( "QUESTION" , msg );
  }

  private
  Map buildWarningResponse ( String msg ) {
    return this.buildResponse ( "WARNING" , msg );
  }

  private
  Map buildResponse ( String status , String msg ) {
    Map map = new HashMap ( );
    map.put ( "status" , status );
    map.put ( "msg" , msg );
    return map;
  }
}
