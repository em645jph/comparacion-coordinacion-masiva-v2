//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.restservice.controllers;

import ar.com.project.core.domain.CustomerDraft;
import ar.com.project.core.domain.LinkedUser;
import ar.com.project.core.domain.User;
import ar.com.project.core.domain.UserSignInRequest;
import ar.com.project.core.dto.CustomerBalanceDTO;
import ar.com.project.core.dto.CustomerDTO;
import ar.com.project.core.dto.DraftDTO;
import ar.com.project.core.dto.WebServiceResponseDTO;
import ar.com.project.core.service.AuditService;
import ar.com.project.core.service.CustomerDraftService;
import ar.com.project.core.service.DatabaseConnectorService;
import ar.com.project.core.service.InvoiceService;
import ar.com.project.core.service.LinkedUserService;
import ar.com.project.core.service.QueriesService;
import ar.com.project.core.service.RoleService;
import ar.com.project.core.service.UserService;
import ar.com.project.core.service.UserSignInRequestService;
import ar.com.project.core.service.WebService;
import ar.com.project.core.utils.DateUtils;
import ar.com.project.core.utils.MessagesUtils;
import ar.com.project.core.worker.UnitCategoryEnum;
import com.curcico.jproject.core.daos.ConditionSimple;
import com.curcico.jproject.core.daos.SearchOption;
import com.curcico.jproject.core.exception.BaseException;
import com.curcico.jproject.core.exception.BusinessException;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.transaction.Transactional;
import org.apache.log4j.Logger;
import org.codehaus.plexus.util.StringUtils;
import org.hibernate.service.spi.ServiceException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@SessionAttributes ( {
    "userid" , "parameters" , "firstname" , "lastname" , "invoice" , "customer"
}
)
@RequestMapping ( { "/Invoice" })
public
class InvoiceController extends CommonController {

  private static final Logger logger = Logger.getLogger ( InvoiceController.class );
  Timestamp now = new Timestamp ( Calendar.getInstance ( ).getTimeInMillis ( ) );
  @Autowired
  UserService              userService;
  @Autowired
  UserSignInRequestService userSignInRequestService;
  @Autowired
  WebService               webService;
  @Autowired
  InvoiceService           invoiceService;
  @Autowired
  LinkedUserService        linkedUserService;
  @Autowired
  DatabaseConnectorService databaseConnectorService;
  @Autowired
  QueriesService           queriesService;
  @Autowired
  RoleService              roleService;
  @Autowired
  CustomerDraftService     customerDraftService;
  @Autowired
  AuditService             auditService;

  public
  InvoiceController ( ) {
  }

  @RequestMapping (
      value = { "/invoicing" },
      method = { RequestMethod.GET }
  )
  public
  ModelAndView invoicing ( ) throws BaseException {
    logger.info ( "invoicing" );
    return new ModelAndView ( "invoicing" );
  }

  @RequestMapping (
      value = { "/drafts" },
      method = { RequestMethod.GET }
  )
  public
  ModelAndView drafts ( ) throws BaseException {
    logger.info ( "drafts" );
    return new ModelAndView ( "drafts" );
  }

  @RequestMapping (
      value = { "/invoicingSelector" },
      method = { RequestMethod.GET }
  )
  public
  ModelAndView invoicingSelector (
      @ModelAttribute ( "customer") CustomerBalanceDTO customer ,
      Model model
                                 ) throws BaseException {
    try {
      new ModelAndView ( "invoicing" );
      ModelAndView m;
      if ( ! model.containsAttribute ( "customer" ) ) {
        m = new ModelAndView ( "invoicing" );
        m.addObject ( "customer" , customer );
      }
      else {
        m = new ModelAndView ( "invoicingSelector" );
        m.addObject ( "customer" , customer );
      }

      logger.info ( "invoicingSelector" );
      return m;
    }
    catch ( Exception var4 ) {
      return new ModelAndView ( "index" );
    }
  }

  @RequestMapping (
      value = { "/billingImport" },
      method = { RequestMethod.GET }
  )
  public
  ModelAndView billingImport ( ) throws BaseException {
    logger.info ( "billingImport" );
    return new ModelAndView ( "billingImport" );
  }

  @RequestMapping (
      value = { "/billingExport" },
      method = { RequestMethod.GET }
  )
  public
  ModelAndView billingExport ( ) throws BaseException {
    logger.info ( "billingExport" );
    return new ModelAndView ( "billingExport" );
  }

  @RequestMapping (
      value = { "/billingEmpty" },
      method = { RequestMethod.GET }
  )
  public
  ModelAndView billingEmpty ( ) throws BaseException {
    logger.info ( "billingEmpty" );
    return new ModelAndView ( "billingEmpty" );
  }

  @RequestMapping (
      value = { "/billingManifest" },
      method = { RequestMethod.GET }
  )
  public
  ModelAndView billingManifest ( ) throws BaseException {
    logger.info ( "billingManifest" );
    ModelAndView model         = new ModelAndView ( "billingManifest" );
    String       dataTableHtml = this.getManifestCueDataTableHtml ( );
    model.addObject ( "dataTable" , dataTableHtml );
    return model;
  }

  @RequestMapping (
      value = { "/billingReceiveEmpty" },
      method = { RequestMethod.GET }
  )
  public
  ModelAndView billingReceiveEmpty ( ) throws BaseException {
    logger.info ( "billingReceiveEmpty" );
    ModelAndView model         = new ModelAndView ( "billingReceiveEmpty" );
    String       dataTableHtml = this.getReceiveEmptyDataTableHtml ( );
    model.addObject ( "dataTable" , dataTableHtml );
    return model;
  }

  @RequestMapping (
      value = { "/billingDeliverEmpty" },
      method = { RequestMethod.GET }
  )
  public
  ModelAndView billingDeliverEmpty ( ) throws BaseException {
    logger.info ( "billingDeliverEmpty" );
    ModelAndView model         = new ModelAndView ( "billingDeliverEmpty" );
    String       dataTableHtml = this.getPumApptDataTableHtml ( );
    model.addObject ( "dataTable" , dataTableHtml );
    return model;
  }

  @JsonIgnore
  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/createInvoiceDraftByUnits" }
  )
  @ResponseBody
  @Transactional (
      rollbackOn = { BusinessException.class }
  )
  public
  ResponseEntity < Object > createInvoiceDraftByUnits (
      @RequestBody String datos ,
      @ModelAttribute ( "customer") CustomerBalanceDTO customer
                                                      )
  throws BaseException, ParseException, IOException {
    try {
      JSONParser jsonParser = new JSONParser ( );
      new JSONObject ( );
      JSONObject jsonObject = ( JSONObject ) jsonParser.parse ( datos );
      JSONArray  units      = ( JSONArray ) jsonObject.get ( "formUnits" );
      if ( units.isEmpty ( ) ) {
        return new ResponseEntity (
            "Error al obtener contenedores para facturar" ,
            HttpStatus.BAD_REQUEST
        );
      }
      else {
        String us = "";

        for ( int i = 0 ; i < units.size ( ) ; ++ i ) {
          us = us + units.get ( i ) + ",";
        }

        String           category         = ( String ) jsonObject.get ( "formCategory" );
        String           docNbr           = customer.getCustomerTaxId ( ).replaceAll ( "-" , "" );
        String           customerId       = customer.getCustomerId ( );
        String           pattern          = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat ( pattern );
        Date             paidThruDay      = new Date ( );
        JSONArray        apptTimeStrArray;
        Date             apptTime;
        if ( UnitCategoryEnum.IMPRT.getKey ( ).equals ( category ) ) {
          apptTimeStrArray = ( JSONArray ) jsonObject.get ( "formApptTime" );
          if ( apptTimeStrArray == null || apptTimeStrArray.isEmpty ( ) ) {
            return new ResponseEntity (
                "No se pudo obtener información del turno. Por favor limpiá los cookies de la "
                + "página (CTRL+F5) o comunicate con Atención al cliente para mayor asistencia" ,
                HttpStatus.BAD_REQUEST
            );
          }

          apptTime = DateUtils.parseDate ( ( String ) apptTimeStrArray.get ( 0 ) );

          for ( int i = 0 ; i < apptTimeStrArray.size ( ) ; ++ i ) {
            Date tempApptTime = DateUtils.parseDate ( ( String ) apptTimeStrArray.get ( i ) );
            if ( ! DateUtils.resetDate ( apptTime ).equals (
                DateUtils.resetDate ( tempApptTime ) ) ) {
              return new ResponseEntity (
                  "Seleccioná contenedores con turno para el mismo día" ,
                  HttpStatus.BAD_REQUEST
              );
            }

            apptTime    = apptTime.after ( tempApptTime ) ? apptTime : tempApptTime;
            paidThruDay = apptTime;
          }
        }
        else if ( UnitCategoryEnum.EXPRT.getKey ( ).equals ( category ) ) {
          apptTimeStrArray = ( JSONArray ) jsonObject.get ( "formCutoffTime" );
          if ( apptTimeStrArray == null || apptTimeStrArray.isEmpty ( ) ) {
            return new ResponseEntity (
                "No se pudo obtener información del cutoff del buque. Por favor limpiá los "
                + "cookies de la página (CTRL+F5) o comunicate con Atención al cliente para mayor"
                + " asistencia" ,
                HttpStatus.BAD_REQUEST
            );
          }

          apptTime = DateUtils.parseDate ( ( String ) apptTimeStrArray.get ( 0 ) );
          if ( apptTime == null ) {
            return new ResponseEntity (
                "No se encontró la información del cutoff del buque" ,
                HttpStatus.BAD_REQUEST
            );
          }

          paidThruDay = apptTime;
        }

        String paidThruDayStr = simpleDateFormat.format ( paidThruDay );
        WebServiceResponseDTO msg = this.webService.createInvoiceDraftByParam ( us , category , "" ,
                                                                                customerId ,
                                                                                paidThruDayStr ,
                                                                                ( String ) null ,
                                                                                ( String ) null ,
                                                                                ( String ) null
                                                                              );
        return this.processGenerateDraftResponse ( msg , customerId , docNbr );
      }
    }
    catch ( ServiceException var17 ) {
      return new ResponseEntity ( var17.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/generateReceiveEmptyDraft" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > generateReceiveEmptyDraft (
      @RequestBody Object draftParamObject ,
      @ModelAttribute ( "customer") CustomerBalanceDTO customer
                                                      )
  throws BaseException, ParseException {
    logger.info ( "step 1" );
    Map  paramMap   = ( Map ) draftParamObject;
    List unitIdList = ( List ) paramMap.get ( "unitId" );
    return this.generateDraftByParam ( unitIdList , UnitCategoryEnum.STRGE.getKey ( ) , "" , "" ,
                                       customer ,
                                       ( List ) null , ( String ) null , ( List ) null
                                     );
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/generateManifestDraft" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > generateManifestDraft (
      @RequestBody Object draftParamObject ,
      @ModelAttribute ( "customer") CustomerBalanceDTO customer
                                                  )
  throws BaseException, ParseException {
    logger.info ( "step 1" );
    Map  paramMap        = ( Map ) draftParamObject;
    List manifestNbrList = ( List ) paramMap.get ( "manifestNbr" );
    return this.generateDraftByParam ( ( List ) null , "MANIFEST" , "" , "" , customer ,
                                       manifestNbrList ,
                                       ( String ) null , ( List ) null
                                     );
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/generateDeliverEmptyDraft" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > generateDeliverEmptyDraft (
      @RequestBody Object draftParamObject ,
      @ModelAttribute ( "customer") CustomerBalanceDTO customer
                                                      )
  throws BaseException, ParseException {
    logger.info ( "step 1" );
    Map    paramMap    = ( Map ) draftParamObject;
    List   apptNbrList = ( List ) paramMap.get ( "apptNbr" );
    String orderNbr    = ( String ) paramMap.get ( "orderNbr" );
    return this.generateDraftByParam ( ( List ) null , "APPT" , "" , "" , customer , ( List ) null ,
                                       orderNbr ,
                                       apptNbrList
                                     );
  }

  @JsonIgnore
  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/deleteInvoiceByNbr/{draftNbr}" }
  )
  @ResponseBody
  @Transactional (
      rollbackOn = { BusinessException.class }
  )
  public
  ResponseEntity < Object > deleteInvoiceByNbr ( @PathVariable String draftNbr )
  throws BaseException, ParseException, IOException {
    try {
      if ( draftNbr == null ) {
        return new ResponseEntity (
            "No se puede eliminar el draft solicitado" ,
            HttpStatus.BAD_REQUEST
        );
      }
      else {
        WebServiceResponseDTO msg = this.webService.deleteInvoiceByNbr ( draftNbr );
        if ( msg.getStatus ( ).equalsIgnoreCase ( "SUCCESS" ) ) {
          List < String > data = new ArrayList ( );
          data.add ( "draftNbr:" + draftNbr );
          this.auditService.saveAuditTransaction ( "deleteInvoiceByNbr" , data.toString ( ) ,
                                                   this.getUserContext ( ).getUsername ( )
                                                 );
          return new ResponseEntity (
              MessagesUtils.getBundle ( msg.getStatus ( ) ) , HttpStatus.OK );
        }
        else {
          return new ResponseEntity ( msg.getMessage ( ) , HttpStatus.OK );
        }
      }
    }
    catch ( ServiceException var4 ) {
      return new ResponseEntity ( var4.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  @JsonIgnore
  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/finalizeInvoiceByNbr/{draftNbr}" }
  )
  @ResponseBody
  @Transactional (
      rollbackOn = { BusinessException.class }
  )
  public
  ResponseEntity < Object > finalizeInvoiceByNbr ( @PathVariable String draftNbr )
  throws BaseException, ParseException, IOException {
    try {
      if ( draftNbr == null ) {
        return new ResponseEntity (
            "No se puede finalizar el draft solicitado" ,
            HttpStatus.BAD_REQUEST
        );
      }
      else {
        WebServiceResponseDTO msg  = this.webService.finalizeInvoiceByNbr ( draftNbr );
        List < String >       data = new ArrayList ( );
        data.add ( "draftNbr:" + draftNbr );
        this.auditService.saveAuditTransaction ( "finalizeInvoiceByNbr" , data.toString ( ) ,
                                                 this.getUserContext ( ).getUsername ( )
                                               );
        return new ResponseEntity ( msg , HttpStatus.OK );
      }
    }
    catch ( ServiceException var4 ) {
      return new ResponseEntity ( var4.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  @JsonIgnore
  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/getInvoiceReportUrlByDraftNbr/{draftNbr}" }
  )
  @ResponseBody
  @Transactional (
      rollbackOn = { BusinessException.class }
  )
  public
  ResponseEntity < Object > getInvoiceReportUrlByDraftNbr ( @PathVariable String draftNbr )
  throws BaseException, ParseException, IOException {
    try {
      if ( draftNbr == null ) {
        return new ResponseEntity (
            "No se puede consultar el draft solicitado" ,
            HttpStatus.BAD_REQUEST
        );
      }
      else {
        WebServiceResponseDTO msg = this.webService.getInvoiceReportUrlByDraftNbr ( draftNbr );
        return new ResponseEntity ( msg , HttpStatus.OK );
      }
    }
    catch ( ServiceException var3 ) {
      return new ResponseEntity ( var3.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/findDraftsByUsers" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > findMyDrafts ( @RequestBody String datos )
  throws ParseException, BaseException, java.text.ParseException {
    try {
      JSONParser jsonParser = new JSONParser ( );
      new JSONObject ( );
      JSONObject jsonObject = ( JSONObject ) jsonParser.parse ( datos );
      String     startDate  = ( String ) jsonObject.get ( "formStartDate" );
      String     endDate    = ( String ) jsonObject.get ( "formEndDate" );
      if ( ! startDate.isEmpty ( ) && ! endDate.isEmpty ( ) ) {
        Date date1     = ( new SimpleDateFormat ( "yyyy-MM-dd" ) ).parse ( startDate );
        Date date2     = ( new SimpleDateFormat ( "yyyy-MM-dd" ) ).parse ( endDate );
        long startTime = date1.getTime ( );
        long endTime   = date2.getTime ( );
        long diffTime  = endTime - startTime;
        long diffDays  = diffTime / 86400000L;
        if ( diffDays > 30L ) {
          return new ResponseEntity (
              "El lapso de fechas de inicio y fin, supera el máximo permitido de 30 días. "
              + "Modifique el criterio de búsqueda." ,
              HttpStatus.BAD_REQUEST
          );
        }
      }
      else if ( ! startDate.isEmpty ( ) && endDate.isEmpty ( )
                || startDate.isEmpty ( ) && ! endDate.isEmpty ( ) ) {
        return new ResponseEntity (
            "Si utiliza el buscador por fechas, por favor, utilice una fecha de inicio y una "
            + "fecha de fin." ,
            HttpStatus.BAD_REQUEST
        );
      }

      ArrayList < String > linkedCuits = new ArrayList ( );
      linkedCuits.add ( this.getUserContext ( ).getUsername ( ) );
      List < ConditionSimple > parametersFilters = new ArrayList ( );
      parametersFilters.add ( new ConditionSimple ( "linkedUserGkey" , SearchOption.EQUAL ,
                                                    this.userService.getUser (
                                                            this.getUserContext ( ).getUsername ( ) )
                                                                    .getGkey ( )
      ) );
      List < LinkedUser > results = ( List ) this.linkedUserService.findByFilters (
          parametersFilters );

      for ( int i = 0 ; i < results.size ( ) ; ++ i ) {
        parametersFilters.clear ( );
        parametersFilters.add ( new ConditionSimple ( "gkey" , SearchOption.EQUAL ,
                                                      (
                                                          ( LinkedUser ) results.get ( i )
                                                      ).getRequesterUserGkey ( )
        ) );
        linkedCuits.add (
            ( ( User ) this.userService.findEntityByFilters ( parametersFilters ) ).getLogin ( ) );
      }

      List < DraftDTO > drafts = new ArrayList ( );
      if ( ! linkedCuits.isEmpty ( ) ) {
        drafts = this.invoiceService.findDraftsByUsers ( startDate , endDate , linkedCuits );
      }

      ( ( List ) drafts ).addAll ( this.invoiceService.findMyDrafts ( startDate , endDate ,
                                                                      this.getUserContext ( )
                                                                          .getUsername ( )
                                                                    ) );
      return new ResponseEntity ( drafts , HttpStatus.OK );
    }
    catch ( ServiceException var16 ) {
      return new ResponseEntity (
          "No se pudo consultar las facturas solicitadas." ,
          HttpStatus.BAD_REQUEST
      );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/getPdfByDraftId/{draftNbr}" }
  )
  @ResponseBody
  public
  ResponseEntity < Object > getPdfByDraftId ( @PathVariable String draftNbr )
  throws BaseException, ParseException, IOException {
    try {
      if ( draftNbr == null ) {
        return new ResponseEntity (
            "No se puede obtener el PDF solicitado" , HttpStatus.BAD_REQUEST );
      }
      else {
        WebServiceResponseDTO msg = this.webService.getPdfByDraftId ( draftNbr );
        return msg.getStatus ( ).equalsIgnoreCase ( "SUCCESS" ) ? new ResponseEntity (
            msg.getMessage ( ) ,
            HttpStatus.OK
        ) : new ResponseEntity ( msg.getMessage ( ) , HttpStatus.OK );
      }
    }
    catch ( ServiceException var3 ) {
      return new ResponseEntity ( var3.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/validateCuitToBilling/{userCuit}" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ModelAndView validateCuitToBilling ( @PathVariable String userCuit ) {
    ModelAndView m;
    try {
      if ( ! StringUtils.isNumeric ( userCuit ) ) {
        m = new ModelAndView ( "invoicing" );
        m.addObject ( "msg" , "Por favor, ingrese un CUIT valido." );
        return m;
      }
      else {
        new ArrayList ( );
        User user = this.userService.getUser ( userCuit );
        if ( user == null ) {
          m = new ModelAndView ( "formManagerUser" );
          m.addObject ( "userCuit" , userCuit );
          m.addObject ( "form" , CustomerController.getFormByCode ( 1 ) );
          m.addObject ( "userRequestedBilling" , "on" );
          m.addObject ( "action" , "UPDATE_CUSTOMER" );
          return m;
        }
        else {
          List < ConditionSimple > parametersFilters = new ArrayList ( );
          parametersFilters.add (
              new ConditionSimple ( "userGkey" , SearchOption.EQUAL , user.getGkey ( ) ) );
          parametersFilters.add (
              new ConditionSimple ( "status" , SearchOption.EQUAL , "PENDING" ) );
          List < UserSignInRequest > res = ( List ) this.userSignInRequestService.findByFilters (
              parametersFilters );
          if ( res.size ( ) > 0 ) {
            m = new ModelAndView ( "invoicing" );
            m.addObject ( "msg" , MessagesUtils.getBundle ( "CUSTOMER_HAVE_REQUEST_PENDING" ) );
            return m;
          }
          else if ( user.getComexEmailAddress ( ) != null && ! user.getComexEmailAddress ( )
                                                                   .isEmpty ( ) ) {
            Connection  conn     = this.databaseConnectorService.createN4BillingDBConnection ( );
            Statement   st       = conn.createStatement ( );
            String      query    = this.queriesService.getCustomerByCuit ( userCuit );
            ResultSet   rs       = st.executeQuery ( query );
            CustomerDTO customer = null;
            if ( rs.next ( ) ) {
              customer = new CustomerDTO ( );
              customer.setCustomerCreditStatus ( rs.getString ( "creditStatus" ) );
              customer.setCustomerCuit ( rs.getString ( "cuit" ) );
              customer.setCustomerId ( rs.getString ( "id" ) );
              customer.setCustomerName ( rs.getString ( "name" ) );
            }

            if ( customer == null ) {
              m = new ModelAndView ( "formManagerUser" );
              m.addObject ( "userCuit" , userCuit );
              m.addObject ( "form" , CustomerController.getFormByCode ( 1 ) );
              m.addObject ( "userRequestedBilling" , "on" );
              m.addObject ( "action" , "CREATE_CUSTOMER" );
              return m;
            }
            else {
              if ( customer.getCustomerCreditStatus ( ).equalsIgnoreCase ( "OAC" ) ) {
                User myUser = this.userService.getUser ( this.getUserContext ( ).getUsername ( ) );
                if ( ! this.userHaveParents ( user.getGkey ( ) , myUser.getGkey ( ) ) && ! myUser
                    .getLogin ( )
                    .equalsIgnoreCase ( userCuit ) && ! myUser.getRoles ( )
                                                              .contains (
                                                                  this.roleService.getRoleByGkey (
                                                                      12 ) ) ) {
                  m = new ModelAndView ( "invoicing" );
                  m.addObject (
                      "msg" , MessagesUtils.getBundle ( "USER_NOT_AUTHORIZE_TO_BIL_CUSTOMER" ) );
                  return m;
                }
              }

              m = new ModelAndView ( "myBalance" );
              m.addObject ( "customer" , this.userService.getBalanceAccountByTaxId ( userCuit ) );
              return m;
            }
          }
          else {
            m = new ModelAndView ( "formManagerUser" );
            m.addObject ( "userCuit" , userCuit );
            m.addObject ( "form" , CustomerController.getFormByCode ( 1 ) );
            m.addObject ( "userRequestedBilling" , "on" );
            m.addObject ( "action" , "UPDATE_CUSTOMER" );
            return m;
          }
        }
      }
    }
    catch ( Exception var11 ) {
      m = new ModelAndView ( "invoicing" );
      m.addObject ( "msg" , "No se pudo obtener el CUIT a Facturar" );
      m.addObject ( "error" , var11.getMessage ( ) );
      return m;
    }
  }

  private
  boolean userHaveParents ( Integer parentGkey , Integer childGkey ) {
    List < ConditionSimple > parametersFilters = new ArrayList ( );
    parametersFilters.add (
        new ConditionSimple ( "requesterUserGkey" , SearchOption.EQUAL , parentGkey ) );
    parametersFilters.add (
        new ConditionSimple ( "linkedUserGkey" , SearchOption.EQUAL , childGkey ) );
    parametersFilters.add ( new ConditionSimple ( "lifeCycleState" , SearchOption.EQUAL , "ACT" ) );

    try {
      List < LinkedUser > results = ( List ) this.linkedUserService.findByFilters (
          parametersFilters );
      return results.size ( ) > 0;
    }
    catch ( BaseException var6 ) {
      return false;
    }
  }

  private
  ResponseEntity < Object > generateDraftByParam (
      List unitIdList , String unitCategory ,
      String documentNbr , String paidThruDayStr , CustomerBalanceDTO customerDto ,
      List manifestList ,
      String orderNbr , List apptNbrList
                                                 ) throws BaseException, ParseException {
    String  unitId                = this.listToStr ( unitIdList );
    String  manifestNbr           = this.listToStr ( manifestList );
    String  apptNbrStr            = this.listToStr ( apptNbrList );
    boolean isManifestInvoice     = "MANIFEST".equals ( unitCategory );
    boolean isDeliverEmptyInvoice = "APPT".equals ( unitCategory );
    String  customerId;
    if ( ( isManifestInvoice || isDeliverEmptyInvoice || ! unitId.isEmpty ( ) ) && (
        ! isManifestInvoice
        || ! manifestNbr.isEmpty ( )
    ) && ( ! isDeliverEmptyInvoice || ! apptNbrStr.isEmpty ( ) ) ) {
      customerId = customerDto != null ? customerDto.getCustomerId ( ) : null;
      String customerTaxId = customerDto != null ? customerDto.getCustomerTaxId ( ) : null;
      if ( customerId != null && ! customerId.isEmpty ( ) ) {
        WebServiceResponseDTO msg;
        try {
          msg = this.webService.createInvoiceDraftByParam ( unitId , unitCategory , documentNbr ,
                                                            customerId , paidThruDayStr ,
                                                            manifestNbr , orderNbr , apptNbrStr
                                                          );
        }
        catch ( IOException var18 ) {
          return new ResponseEntity ( var18.getMessage ( ) , HttpStatus.BAD_REQUEST );
        }

        return this.processGenerateDraftResponse ( msg , customerId , customerTaxId );
      }
      else {
        return new ResponseEntity (
            this.buildErrorResponse (
                "No se pudo obtener la información del cliente a facturar. Por favor refresque la"
                + " página" ) ,
            HttpStatus.OK
        );
      }
    }
    else {
      customerId =
          isManifestInvoice ? "manifiestos" : ( isDeliverEmptyInvoice ? "turnos" : "contenedores" );
      return new ResponseEntity ( this.buildErrorResponse (
          "No se pudo obtener la información de los " + customerId
          + " a facturar. Por favor refresque la página" ) , HttpStatus.OK );
    }
  }

  private
  ResponseEntity < Object > processGenerateDraftResponse (
      WebServiceResponseDTO msg ,
      String customerId , String customerTaxId
                                                         ) throws BaseException {
    if ( msg.getStatus ( ).equalsIgnoreCase ( "SUCCESS" ) && ! customerId.equalsIgnoreCase (
        this.getUserContext ( ).getUsername ( ) ) ) {
      CustomerDraft cusDraft = new CustomerDraft ( );
      cusDraft.setCustomerCreator ( this.getUserContext ( ).getUsername ( ) );
      cusDraft.setCustomerBilled ( customerTaxId );
      cusDraft.setDraftNbr ( Integer.valueOf ( msg.getMessage ( ) ) );
      cusDraft.setCreated ( new Timestamp ( System.currentTimeMillis ( ) ) );
      cusDraft.setCreator ( this.getUserContext ( ).getUsername ( ) );
      cusDraft.setLifeCycleState ( "ACT" );
      cusDraft = ( CustomerDraft ) this.customerDraftService.createOrUpdate ( cusDraft , "system" );
      List < String > data = new ArrayList ( );
      data.add ( "CustomerCreator:" + cusDraft.getCustomerCreator ( ) );
      data.add ( "CustomerBilled:" + customerTaxId );
      data.add ( "DraftNbr:" + msg.getMessage ( ) );
      this.auditService.saveAuditTransaction ( "createInvoiceDraftByUnits" , data.toString ( ) ,
                                               this.getUserContext ( ).getUsername ( )
                                             );
      return new ResponseEntity (
          this.buildSuccessResponse ( msg.getMessage ( ) ) , HttpStatus.OK );
    }
    else {
      return new ResponseEntity ( msg.getMessage ( ) , HttpStatus.OK );
    }
  }

  private
  String getReceiveEmptyDataTableHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    sb.append ( "<thead>\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <th> <input type=\"checkbox\" id=\"ckCheckAll\"></th>\n" );
    sb.append ( "    <th>Contenedor</th>\n" );
    sb.append ( "    <th>Categoría</th>\n" );
    sb.append ( "    <th>Tipo</th>\n" );
    sb.append ( "    <th>Línea</th>\n" );
    sb.append ( "    <th>Estado</th>\n" );
    sb.append ( "    <th>Límite Devolución</th>\n" );
    sb.append ( "    <th>Turno</th>\n" );
    sb.append ( "</tr>\n" );
    sb.append ( "</thead>\n" );
    sb.append ( "<tbody data-bind=\"foreach: draftViewModel.receiveEmptyTable\">\n" );
    sb.append ( "<tr>\n" );
    sb.append (
        "    <td><input id=\"ckCheckOne\" class=\"selectable\" type=\"checkbox\" "
        + "data-bind=\"attr: {unitId:unitId}, enable: isReceiveEmptyBillable\"></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitId\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitCategory\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitIsoTypeId\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitLineOpId\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: ufvTransitStateDesc\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitLimitDateStr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: apptRequestedDateStr\"></label></td>\n" );
    sb.append ( "</tr>\n" );
    return sb.toString ( );
  }

  private
  String getManifestCueDataTableHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    sb.append ( "<thead>\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <th> <input type=\"checkbox\" id=\"ckCheckAll\"></th>\n" );
    sb.append ( "    <th>Manifiesto</th>\n" );
    sb.append ( "    <th>Nave</th>\n" );
    sb.append ( "    <th>Viaje</th>\n" );
    sb.append ( "    <th>Estado</th>\n" );
    sb.append ( "</tr>\n" );
    sb.append ( "</thead>\n" );
    sb.append ( "<tbody data-bind=\"foreach: draftViewModel.manifestCueTable\">\n" );
    sb.append ( "<tr>\n" );
    sb.append (
        "    <td><input id=\"ckCheckOne\" class=\"selectable\" type=\"checkbox\" "
        + "data-bind=\"attr: {manifestNbr:manifestNbr}, enable: isBillable\"></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: manifestNbr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: ibCarrierName\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: ibCarrierVygNbr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: statusDesc\"></label></td>\n" );
    sb.append ( "</tr>\n" );
    return sb.toString ( );
  }

  private
  String getPumApptDataTableHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    sb.append ( "<thead>\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <th> <input type=\"checkbox\" id=\"ckCheckAll\"></th>\n" );
    sb.append ( "    <th>Num. Turno</th>\n" );
    sb.append ( "    <th>Tipo</th>\n" );
    sb.append ( "    <th>Línea</th>\n" );
    sb.append ( "    <th>Documento</th>\n" );
    sb.append ( "    <th>Turno</th>\n" );
    sb.append ( "    <th>Estado</th>\n" );
    sb.append ( "</tr>\n" );
    sb.append ( "</thead>\n" );
    sb.append ( "<tbody data-bind=\"foreach: draftViewModel.pumApptTable\">\n" );
    sb.append ( "<tr>\n" );
    sb.append (
        "    <td><input id=\"ckCheckOne\" class=\"selectable\" type=\"checkbox\" "
        + "data-bind=\"attr: {nbr:nbr, orderNbr:orderNbr}, enable: isBillable\"></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: nbr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitIsoTypeId\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: lineOpId\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: orderNbr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: requestedDateStr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: billableStatusDesc\"></label></td>\n" );
    sb.append ( "</tr>\n" );
    return sb.toString ( );
  }

  private
  Map buildSuccessResponse ( String msg ) {
    return this.buildResponse ( "OK" , msg );
  }

  private
  Map buildErrorResponse ( String msg ) {
    return this.buildResponse ( "ERROR" , msg );
  }

  private
  Map buildQuestionResponse ( String msg , String questionType ) {
    Map map = this.buildResponse ( "QUESTION" , msg );
    if ( questionType != null && ! questionType.isEmpty ( ) ) {
      map.put ( "questionType" , questionType );
    }

    return map;
  }

  private
  Map buildWarningResponse ( String msg ) {
    return this.buildResponse ( "WARNING" , msg );
  }

  private
  Map buildResponse ( String status , String msg ) {
    this.logUser ( msg );
    Map map = new HashMap ( );
    map.put ( "status" , status );
    map.put ( "msg" , msg );
    this.clearLogPrefix ( );
    return map;
  }

  private
  void setUnitLogPrefix ( Map unitMap ) {
    Integer unitGkey     = ( Integer ) unitMap.get ( "unitGkey" );
    String  unitId       = ( String ) unitMap.get ( "unitId" );
    String  unitCategory = ( String ) unitMap.get ( "unitCategory" );
    this.setLogPrefix ( "Unit[" + unitGkey + "/" + unitId + "/" + unitCategory + "] " );
  }

  private
  String listToStr ( List list ) {
    if ( list == null ) {
      return "";
    }
    else {
      String str = "";

      for ( int i = 0 ; i < list.size ( ) ; ++ i ) {
        Object value = list.get ( i );
        if ( value != null ) {
          str = str + ( str.isEmpty ( ) ? "" : "," );
          str = str + value.toString ( );
        }
      }

      return str;
    }
  }
}
