//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package ar.com.project.web.controller;

import ar.com.project.core.domain.Audit;
import ar.com.project.core.service.AuditService;
import com.curcico.jproject.core.exception.BaseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping({
    "/logs"
})
public
class ActivityLogController extends CommonController {

  static final Logger logger = Logger.getLogger(ActivityLogController.class);
  @Autowired
  AuditService auditService;
  @Autowired
  SessionFactory sessionFactory;

  public
  ActivityLogController() {}

  @RequestMapping(
      value = {
          "/audit"
      },
      method = {
          RequestMethod.GET
      }
  )
  @PreAuthorize("hasAnyRole('USER_AUDITOR')")
  public
  ModelAndView audit() throws BaseException {
    return new ModelAndView("audit");
  }

  @RequestMapping(
      method = {
          RequestMethod.POST
      },
      value = {
          "/getAuditData"
      }
  )
  @PreAuthorize("isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getAuditData(@RequestBody String datos)
  throws ParseException, BaseException, java.text.ParseException {
    Session sess = this.sessionFactory.openSession();
    JSONParser jsonParser = new JSONParser();
    new JSONObject();
    JSONObject jsonObject = (JSONObject) jsonParser.parse(datos);
    String startDate = (String) jsonObject.get("formStartDate");
    String endDate = (String) jsonObject.get("formEndDate");
    if (!startDate.isEmpty() && !endDate.isEmpty()) {
      Date date1 = (new SimpleDateFormat("yyyy-MM-dd")).parse(startDate);
      Date date2 = (new SimpleDateFormat("yyyy-MM-dd")).parse(endDate);
      long startTime = date1.getTime();
      long endTime = date2.getTime();
      long diffTime = endTime - startTime;
      long diffDays = diffTime / 86400000 L;
      if (diffDays > 30 L) {
        return new ResponseEntity(
            "El lapso de fechas de inicio y fin, supera el máximo permitido de 30 días. Modifique" +
            " el criterio de búsqueda.",
            HttpStatus.BAD_REQUEST
        );
      } else {
        Criterion dateRestriction = Restrictions.between("timestamp", date1, date2);
        List < Audit > results = sess.createCriteria(Audit.class).add(dateRestriction)
                                     .list();
        sess.close();
        return new ResponseEntity(results, HttpStatus.OK);
      }
    } else if ((startDate.isEmpty() || !endDate.isEmpty()) && (
        !startDate.isEmpty() ||
        endDate.isEmpty()
    )) {
      List < Audit > results = this.auditService.findAll();
      return new ResponseEntity(results, HttpStatus.OK);
    } else {
      return new ResponseEntity(
          "Si utiliza el buscador por fechas, por favor, utilice una fecha de inicio y una fecha " +
          "de fin.",
          HttpStatus.BAD_REQUEST
      );
    }
  }
}