//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//


package com.example.restservice.controllers;

import ar.com.project.core.domain.User;
import ar.com.project.core.dto.AppointmentOpeningDto;
import ar.com.project.core.service.AgpCoordinationAuditService;
import ar.com.project.core.service.AppointmentService;
import ar.com.project.core.service.AuditService;
import ar.com.project.core.service.CoordinationService;
import ar.com.project.core.service.CustomerSupportService;
import ar.com.project.core.service.LineOpService;
import ar.com.project.core.service.PrivilegeService;
import ar.com.project.core.service.ScheduleRuleService;
import ar.com.project.core.service.SrvOrderService;
import ar.com.project.core.service.UnitService;
import ar.com.project.core.utils.EnumUtil;
import ar.com.project.core.utils.ListUtils;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping ( { "/SupportUnit" })
@SessionAttributes ( {
    "userid" , "loginName" , "username" , "parameters" , "fullname" , "firstNameLetter"
}
)
public
class SupportUnitController extends CommonController {

  private static final Logger logger = Logger.getLogger (
      SupportUnitController.class );
  boolean canAddItem     = false;
  boolean canEditItem    = false;
  boolean canDeleteItem  = false;
  boolean canRecoverItem = false;
  User    user;
  @Autowired
  AppointmentService          apptService;
  @Autowired
  LineOpService               lineOpService;
  @Autowired
  ScheduleRuleService         ruleService;
  @Autowired
  SrvOrderService             soService;
  @Autowired
  PrivilegeService            privilegeService;
  @Autowired
  CoordinationService         coordinationService;
  @Autowired
  AuditService                auditService;
  @Autowired
  AgpCoordinationAuditService agpCoordAuditService;
  @Autowired
  UnitService                 unitService;
  @Autowired
  CustomerSupportService      supportService;
  private List privilegeInPageList = new ArrayList ( );

  public
  SupportUnitController ( ) {
  }

  @RequestMapping (
      value = { "/supportUnit" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  public
  ModelAndView supportUnit (
      HttpServletRequest request , HttpServletResponse response ,
      Object handler
                           ) {
    ModelAndView model       = new ModelAndView ( "supportUnit" );
    String       servletPath = request.getServletPath ( );
    org.springframework.security.core.userdetails.User loggedUser =
        ( org.springframework.security.core.userdetails.User ) SecurityContextHolder
            .getContext ( )
            .getAuthentication ( ).getPrincipal ( );
    String login = loggedUser.getUsername ( );
    this.canAddItem    = true;
    this.canEditItem   = true;
    this.canDeleteItem = true;
    return model;
  }

  @RequestMapping (
      value = { "/loadInitCombos" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < List > loadInitCombos ( ) {
    ResponseEntity response;
    try {
      List categoryList     = EnumUtil.getUnitCategoryList ( );
      List lineOpList       = this.lineOpService.getAllLineOp ( );
      List transitStateList = EnumUtil.getUfvTransitStateList ( );
      Map  resultMap        = new HashMap ( );
      resultMap.put ( "categoryList" , categoryList );
      resultMap.put ( "lineOpList" , lineOpList );
      resultMap.put ( "transitStateList" , transitStateList );
      resultMap.put ( "yesNoList" , EnumUtil.getSimpleYesNoEnumList ( ) );
      response = new ResponseEntity ( resultMap , HttpStatus.OK );
    }
    catch ( SQLException var6 ) {
      response = new ResponseEntity (
          "No se pudo cargar correctamente la información. Por favor refresque la página" ,
          HttpStatus.BAD_REQUEST
      );
      var6.printStackTrace ( );
    }

    return response;
  }

  @RequestMapping (
      value = { "/findUnit" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < List > findUnit (
      @RequestParam String prUnitId ,
      @RequestParam String prCategory , @RequestParam String prTransitState ,
      @RequestParam String prLineOp , @RequestParam String prInboundVisit ,
      @RequestParam String prOutboundVisit , @RequestParam String prDocumentNbr ,
      @RequestParam String prApptNbrStr , @RequestParam String prReeferStr ,
      @RequestParam String prHazardousStr , @RequestParam String prEventType ,
      @RequestParam String prHoldId
                                   ) throws SQLException {
    List unitIdList          = ListUtils.strToList ( prUnitId );
    List unitCategoryList    = ListUtils.strToList ( prCategory );
    List ufvTransitStateList = ListUtils.strToList ( prTransitState );
    List lineIdList          = ListUtils.strToList ( prLineOp );
    List eventIdList         = ListUtils.strToList ( prEventType );
    List holdIdList          = ListUtils.strToList ( prHoldId );
    Long apptNbr =
        prApptNbrStr != null && ! prApptNbrStr.isEmpty ( ) ? Long.parseLong ( prApptNbrStr ) : null;
    Integer isReefer =
        prReeferStr != null && ! prReeferStr.isEmpty ( ) ? Integer.parseInt ( prReeferStr ) : null;
    Integer isHazardous =
        prHazardousStr != null && ! prHazardousStr.isEmpty ( ) ? Integer.parseInt ( prHazardousStr )
                                                               : null;
    List unitList = this.supportService.findUnitByParam ( unitIdList , unitCategoryList ,
                                                          ufvTransitStateList , prInboundVisit ,
                                                          prOutboundVisit , prDocumentNbr ,
                                                          lineIdList , apptNbr ,
                                                          isReefer , isHazardous , ( String ) null ,
                                                          eventIdList , holdIdList
                                                        );
    ResponseEntity response = new ResponseEntity ( unitList , HttpStatus.OK );
    return response;
  }

  private
  String getUserLogin ( ) {
    return this.getUserContext ( ).getUsername ( );
  }

  private
  String getServiceOrderDataTableHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    sb.append ( "<thead>\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <th>Contenedor</th>\n" );
    sb.append ( "    <th>Servicio</th>\n" );
    sb.append ( "    <th>Fecha Servicio</th>\n" );
    sb.append ( "    <th>Estado Servicio</th>\n" );
    if ( this.canDeleteItem ) {
      sb.append ( "    <th>Anular</th>\n" );
    }

    sb.append ( "</tr>\n" );
    sb.append ( "</thead>\n" );
    sb.append ( "<tbody data-bind=\"foreach: coordinationViewModel.unitServiceTable\">\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitId\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: srvOrderTypeDescription\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: srvOrderStartDateStr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: srvOrderStatus\"></label></td>\n" );
    if ( this.canDeleteItem ) {
      sb.append (
          "    <td><button class=\"btn btn-icon btn-danger btn-sm\" data-bind=\"click: $parent"
          + ".cancelSo, enable: allowToCancelService\"> <i class=\"ti-close\"></i> "
          + "</button></td>\n" );
    }

    sb.append ( "</tr>\n" );
    return sb.toString ( );
  }

  private
  void addUnitDateToCalendar (
      List eventList , Map unitApptMap , String dateStr ,
      String dateNameField , String title , String color
                             ) {
    String   defaultColor  = "#AEFFA9";
    String[] dateStrArray  = dateStr.split ( " " );
    String   dateNoTimeStr = dateStrArray.length > 0 ? dateStrArray[ 0 ] : "";
    String   dateName      = ( String ) unitApptMap.get ( dateNameField );
    boolean addEvent = dateName != null && ! dateName.isEmpty ( )
                       || title != null && ! title.isEmpty ( );
    if ( addEvent && dateNoTimeStr != null && ! dateNoTimeStr.isEmpty ( ) ) {
      color = color != null && ! color.isEmpty ( ) ? color : defaultColor;
      title = title != null && ! title.isEmpty ( ) ? title : dateName;
      AppointmentOpeningDto dto = new AppointmentOpeningDto ( title , dateNoTimeStr , "" , "" , 0 ,
                                                              ( Long ) null , color , title
      );
      eventList.add ( dto );
    }

  }

  private
  boolean isValidUnitIdFormat ( String untiId ) {
    return untiId.matches ( "^[A-Z]{4}\\d{7}" );
  }

  private
  Map buildSuccessResponse ( String msg ) {
    return this.buildResponse ( "OK" , msg );
  }

  private
  Map buildErrorResponse ( String msg ) {
    return this.buildResponse ( "ERROR" , msg );
  }

  private
  Map buildQuestionResponse ( String msg , String questionType ) {
    Map map = this.buildResponse ( "QUESTION" , msg );
    if ( questionType != null && ! questionType.isEmpty ( ) ) {
      map.put ( "questionType" , questionType );
    }

    return map;
  }

  private
  Map buildWarningResponse ( String msg ) {
    return this.buildResponse ( "WARNING" , msg );
  }

  private
  Map buildResponse ( String status , String msg ) {
    this.logUser ( msg );
    Map map = new HashMap ( );
    map.put ( "status" , status );
    map.put ( "msg" , msg );
    this.clearLogPrefix ( );
    return map;
  }

  private
  void setUnitLogPrefix ( Map unitMap ) {
    Integer unitGkey     = ( Integer ) unitMap.get ( "unitGkey" );
    String  unitId       = ( String ) unitMap.get ( "unitId" );
    String  unitCategory = ( String ) unitMap.get ( "unitCategory" );
    this.setLogPrefix ( "Unit[" + unitGkey + "/" + unitId + "/" + unitCategory + "] " );
  }

  private
  void setOrderLogPrefix ( Map orderMap ) {
    Integer orderGkey        = ( Integer ) orderMap.get ( "documentGkey" );
    String  orderNbr         = ( String ) orderMap.get ( "documentNbr" );
    String  orderItemIsoType = ( String ) orderMap.get ( "documentItemIsoType" );
    this.setLogPrefix ( "Order[" + orderGkey + "/" + orderNbr + "/" + orderItemIsoType + "] " );
  }
}
