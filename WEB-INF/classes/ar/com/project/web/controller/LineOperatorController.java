//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.restservice.controllers;

import ar.com.project.core.service.AppointmentService;
import ar.com.project.core.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes ( { "userid" , "parameters" , "firstname" , "lastname" })
@RequestMapping ( { "/Line" })
public
class LineOperatorController extends CommonController {

  private static final Logger logger = Logger.getLogger ( LineOperatorController.class );
  @Autowired
  UserService        userService;
  @Autowired
  AppointmentService appointmentService;

  public
  LineOperatorController ( ) {
  }
}
