//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.restservice.controllers;

import ar.com.project.core.domain.User;
import ar.com.project.core.dto.AppointmentOpeningDto;
import ar.com.project.core.dto.DocumentApptDTO;
import ar.com.project.core.service.AppointmentService;
import ar.com.project.core.service.AuditService;
import ar.com.project.core.service.BookingService;
import ar.com.project.core.service.LineOpService;
import ar.com.project.core.service.PrivilegeService;
import ar.com.project.core.service.ScheduleRuleService;
import ar.com.project.core.service.SrvOrderService;
import ar.com.project.core.utils.DateUtils;
import ar.com.project.core.utils.StringUtils;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping ( { "/AppointmentManagement" })
@SessionAttributes ( {
    "userid" , "loginName" , "username" , "parameters" , "fullname" , "firstNameLetter"
}
)
public
class AppointmentManagementController extends CommonController {

  private static final Logger logger = Logger.getLogger (
      AppointmentManagementController.class );
  boolean canAddItem     = false;
  boolean canEditItem    = false;
  boolean canDeleteItem  = false;
  boolean canRecoverItem = false;
  User    user;
  @Autowired
  AppointmentService  apptService;
  @Autowired
  LineOpService       lineOpService;
  @Autowired
  BookingService      bookingService;
  @Autowired
  ScheduleRuleService ruleService;
  @Autowired
  SrvOrderService     soService;
  @Autowired
  PrivilegeService    privilegeService;
  @Autowired
  AuditService        auditService;
  private List privilegeInPageList = new ArrayList ( );

  public
  AppointmentManagementController ( ) {
  }

  @RequestMapping (
      value = { "/appointmentManagement" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  public
  ModelAndView appointmentManagement (
      HttpServletRequest request ,
      HttpServletResponse response , Object handler
                                     ) {
    ModelAndView model       = new ModelAndView ( "appointmentManagement" );
    String       servletPath = request.getServletPath ( );
    org.springframework.security.core.userdetails.User loggedUser =
        ( org.springframework.security.core.userdetails.User ) SecurityContextHolder
            .getContext ( )
            .getAuthentication ( ).getPrincipal ( );
    String login = loggedUser.getUsername ( );
    this.privilegeInPageList = this.privilegeService.getMenuPagePrivileges ( login , servletPath );
    this.canAddItem          = true;
    this.canEditItem         = true;
    this.canDeleteItem       = true;
    String dataTableHtml = this.getDataTableHtml ( this.canAddItem , this.canDeleteItem );
    model.addObject ( "dataTable" , dataTableHtml );
    return model;
  }

  @RequestMapping (
      value = { "/getLineOpList" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < List > getLineOpList ( ) {
    ResponseEntity response;
    try {
      List lineOpList = this.lineOpService.getAllLineOp ( );
      response = new ResponseEntity ( lineOpList , HttpStatus.OK );
    }
    catch ( SQLException var4 ) {
      response = new ResponseEntity (
          "No se pudo cargar la lista de líneas operadoras. Por favor refresque la página" ,
          HttpStatus.BAD_REQUEST
      );
      var4.printStackTrace ( );
    }

    return response;
  }

  @RequestMapping (
      value = { "/getApptSubTypeList" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < List > getApptSubTypeList ( ) {
    List apptSubTypeList = this.apptService.getApptSubTypeList ( );
    return new ResponseEntity ( apptSubTypeList , HttpStatus.OK );
  }

  @RequestMapping (
      value = { "/findUnitAppointment" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > findUnitAppointment (
      @RequestParam String prApptSubType ,
      @RequestParam String prKeyParameter , @RequestParam String prLineOpId
                                                )
  throws SQLException, ServiceException, IOException {
    if ( this.apptService.isDropOffEmpty ( prApptSubType ) ) {
      return new ResponseEntity (
          this.buildErrorResponse (
              "Esta opción estará disponible a la brevedad. Por el momento, si devuelve en "
              + "TERBASA podrá hacerlo sin coordinación ni gatepass" ) ,
          HttpStatus.OK
      );
    }
    else {
      List documentApptList = this.apptService.findDocumentAppt ( prApptSubType , prKeyParameter ,
                                                                  prLineOpId
                                                                );
      if ( this.apptService.isDropOffExport ( prApptSubType ) ) {
        if ( documentApptList == null || documentApptList.isEmpty ( ) ) {
          return new ResponseEntity (
              this.buildErrorResponse ( "No se encontró el booking ingresado" ) ,
              HttpStatus.OK
          );
        }

        if ( documentApptList.size ( ) > 1 && ( prLineOpId == null || prLineOpId.isEmpty ( ) ) ) {
          return new ResponseEntity (
              this.buildErrorResponse (
                  "Se encontró más de un resultado para el booking ingresado. Por favor indique "
                  + "la Línea Operadora" ) ,
              HttpStatus.OK
          );
        }
      }
      else if ( documentApptList == null || documentApptList.isEmpty ( ) ) {
        return new ResponseEntity (
            this.buildErrorResponse ( "No se encontró el contenedor ingresado" ) ,
            HttpStatus.OK
        );
      }

      return new ResponseEntity (
          this.apptService.findUnitAppt (
              prApptSubType , ( DocumentApptDTO ) documentApptList.get ( 0 ) ) ,
          HttpStatus.OK
      );
    }
  }

  @RequestMapping (
      value = { "/getGateOpenings" },
      method = { RequestMethod.POST },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getGateOpenings ( @RequestBody Object prUnitApptDto )
  throws SQLException, ServiceException, IOException {
    Map     unitApptMap    = ( Map ) prUnitApptDto;
    String  apptSubType    = ( String ) unitApptMap.get ( "requestApptSubType" );
    String  fromDateStr    = ( String ) unitApptMap.get ( "unitStartDateStr" );
    Integer unitGkey       = ( Integer ) unitApptMap.get ( "unitGkey" );
    Date    maxServiceDate = this.soService.getUnitNotServiceableMaxDate ( unitGkey.longValue ( ) );
    if ( maxServiceDate != null && fromDateStr != null && ! fromDateStr.isEmpty ( ) ) {
      Date fromDate = DateUtils.parseDate ( fromDateStr );
      fromDateStr =
          maxServiceDate.after ( fromDate ) ? StringUtils.formatDate ( maxServiceDate )
                                            : fromDateStr;
    }

    String toDateStr = ( String ) unitApptMap.get ( "unitEndDateStr" );
    return this.getOpenings ( apptSubType , fromDateStr , toDateStr , true , unitApptMap );
  }

  @RequestMapping (
      value = { "/getDateOpenings" },
      method = { RequestMethod.POST },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getDateOpenings ( @RequestBody Object prUnitApptDto )
  throws SQLException, ServiceException, IOException {
    Map    unitApptMap      = ( Map ) prUnitApptDto;
    String requestedDateStr = ( String ) unitApptMap.get ( "requestedDateStr" );
    if ( requestedDateStr != null && ! requestedDateStr.isEmpty ( ) ) {
      if ( DateUtils.isValidDate ( requestedDateStr , "yyyy-MM-dd" ) ) {
        String fromDateStr     = StringUtils.addDefaultTimeToDateStr ( requestedDateStr );
        String toDateStr       = StringUtils.getNextDateStr ( fromDateStr );
        String unitEndDateName = ( String ) unitApptMap.get ( "unitEndDateName" );
        String apptSubType     = ( String ) unitApptMap.get ( "requestApptSubType" );
        if ( unitEndDateName != null && ! unitEndDateName.isEmpty ( ) ) {
          String unitEndDateStr = ( String ) unitApptMap.get ( "unitEndDateStr" );
          Date   toDate         = DateUtils.parseDate ( toDateStr );
          Date   unitEndDate    = DateUtils.parseDate ( unitEndDateStr );
          if ( toDate.after ( unitEndDate ) ) {
            toDateStr = StringUtils.formatDate ( unitEndDate );
          }
        }

        return this.getOpenings ( apptSubType , fromDateStr , toDateStr , false , unitApptMap );
      }
      else {
        return new ResponseEntity ( ( MultiValueMap ) null , HttpStatus.OK );
      }
    }
    else {
      return new ResponseEntity (
          this.buildErrorResponse ( "La fecha seleccionada no es una fecha válida" ) ,
          HttpStatus.OK
      );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/createUnitAppointment" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > createUnitAppointment (
      @RequestParam String prApptSubType ,
      @RequestParam String prUnitId , @RequestParam String prUnitDocumentNbr ,
      String prSelectedDateTime
                                                  ) throws IOException, SQLException {
    String errorMsg = "";
    if ( prSelectedDateTime != null && ! prSelectedDateTime.isEmpty ( ) ) {
      if ( prApptSubType != null && ! prApptSubType.isEmpty ( ) ) {
        if ( prUnitDocumentNbr == null || prUnitId.isEmpty ( ) ) {
          errorMsg = "El número de contenedor no es válido. Por favor vuelva a intentar";
        }
      }
      else {
        errorMsg = "Seleccionó un tipo de coordinación inválido. Por favor vuelva a intentar";
      }
    }
    else {
      errorMsg = "La fecha/hora seleccionada no es válida. Por favor vuelva a intentar";
    }

    if ( ! errorMsg.isEmpty ( ) ) {
      return new ResponseEntity ( this.buildErrorResponse ( errorMsg ) , HttpStatus.OK );
    }
    else {
      String ruleValidation = this.ruleService.validateSelectedDateTime ( prApptSubType , false ,
                                                                          prSelectedDateTime , true
                                                                        );
      if ( ruleValidation != null && ! ruleValidation.isEmpty ( ) ) {
        return new ResponseEntity ( this.buildErrorResponse ( ruleValidation ) , HttpStatus.OK );
      }
      else {
        Object result = this.apptService.createUnitAppointment ( prApptSubType , prUnitId ,
                                                                 prUnitDocumentNbr ,
                                                                 prSelectedDateTime ,
                                                                 ( String ) null , ( String ) null
                                                               );
        if ( result instanceof String ) {
          return new ResponseEntity (
              this.buildErrorResponse ( ( String ) result ) , HttpStatus.OK );
        }
        else {
          List < String > data = new ArrayList ( );
          data.add ( "prApptSubType:" + prApptSubType );
          data.add ( "prUnitId:" + prUnitId );
          data.add ( "prUnitDocumentNbr:" + prUnitDocumentNbr );
          data.add ( "prSelectedDateTime:" + prSelectedDateTime );
          this.auditService.saveAuditTransaction ( "createUnitAppointment" , data.toString ( ) ,
                                                   this.getUserContext ( ).getUsername ( )
                                                 );
          return new ResponseEntity (
              this.buildSuccessResponse ( "¡Contenedor coordinado correctamente!" ) ,
              HttpStatus.OK
          );
        }
      }
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/cancelUnitAppointment" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > cancelUnitAppointment ( @RequestBody Object unitApptDtoObject )
  throws IOException, SQLException {
    Map unitApptMap = ( Map ) unitApptDtoObject;
    if ( this.canDeleteItem ) {
      String  apptSubtType    = ( String ) unitApptMap.get ( "requestApptSubType" );
      String  apptDateTimeStr = ( String ) unitApptMap.get ( "apptRequestedDateStr" );
      Date    apptDateTime    = DateUtils.parseDate ( apptDateTimeStr );
      boolean expiredApptDate = ( new Date ( ) ).after ( apptDateTime );
      String ruleValidation =
          ! expiredApptDate ? this.ruleService.validateSelectedDateTime ( apptSubtType , true ,
                                                                          apptDateTimeStr , true
                                                                        ) : "";
      boolean acceptCharges =
          unitApptMap.get ( "acceptCharges" ) != null ? ( Boolean ) unitApptMap.get (
              "acceptCharges" )
                                                      : false;
      if ( ( expiredApptDate || ruleValidation != null && ! ruleValidation.isEmpty ( ) )
           && ! acceptCharges ) {
        Map resultMap = this.buildQuestionResponse (
            "¿Está seguro que desa anular el turno? Esta operación generará cargos adicionales" );
        resultMap.put ( "questionType" , "acceptCharges" );
        return new ResponseEntity ( resultMap , HttpStatus.OK );
      }
      else {
        Integer unitGkey = ( Integer ) unitApptMap.get ( "unitGkey" );
        Integer apptNbr  = ( Integer ) unitApptMap.get ( "apptNbr" );
        boolean deleteDraft =
            unitApptMap.get ( "deleteDraft" ) != null ? ( Boolean ) unitApptMap.get (
                "deleteDraft" )
                                                      : false;
        String deleteDraftResult;
        if ( this.apptService.unitHasDraftInvoice ( unitGkey.longValue ( ) ) ) {
          if ( ! deleteDraft ) {
            Map resultMap = this.buildQuestionResponse (
                "Esta acción eliminará los presupuestos asociados al contenedor" );
            resultMap.put ( "questionType" , "deleteDraft" );
            return new ResponseEntity ( resultMap , HttpStatus.OK );
          }

          deleteDraftResult = this.apptService.deleteUnitApptDraftInvoice (
              unitGkey.longValue ( ) );
          if ( deleteDraftResult != null && ! deleteDraftResult.isEmpty ( ) ) {
            return new ResponseEntity (
                this.buildErrorResponse ( deleteDraftResult ) , HttpStatus.OK );
          }
        }

        deleteDraftResult = this.apptService.cancelUnitAppointment ( apptNbr.longValue ( ) );
        if ( deleteDraftResult != null && deleteDraftResult.isEmpty ( ) ) {
          if ( acceptCharges ) {
            String unitCategory = ( String ) unitApptMap.get ( "unitCategory" );
            String unitId       = ( String ) unitApptMap.get ( "unitId" );
            String unitLineOpId = ( String ) unitApptMap.get ( "unitLineOpId" );
            this.apptService.recordRecoorToUnit ( unitGkey.longValue ( ) , unitId , unitCategory ,
                                                  unitLineOpId , this.getUserLogin ( )
                                                );
          }

          List < String > data = new ArrayList ( );
          data.add ( "apptNbr:" + apptNbr );
          data.add ( "acceptCharges:" + acceptCharges );
          this.auditService.saveAuditTransaction ( "cancelUnitAppointment" , data.toString ( ) ,
                                                   this.getUserContext ( ).getUsername ( )
                                                 );
          return new ResponseEntity (
              this.buildSuccessResponse ( "Se eliminó el appointment con éxito" ) , HttpStatus.OK );
        }
        else {
          return new ResponseEntity (
              this.buildErrorResponse ( deleteDraftResult ) , HttpStatus.OK );
        }
      }
    }
    else {
      return new ResponseEntity (
          this.buildErrorResponse ( "No tiene permisos para realizar esta acción" ) ,
          HttpStatus.OK
      );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/viewUnitApptSeals" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > viewUnitApptSeals ( @RequestBody Object unitApptDtoObject )
  throws SQLException, IOException {
    String  unitCategory = ( String ) ( ( Map ) unitApptDtoObject ).get ( "unitCategory" );
    Integer unitGkey     = ( Integer ) ( ( Map ) unitApptDtoObject ).get ( "unitGkey" );
    boolean addEvent =
        ( ( Map ) unitApptDtoObject ).get ( "addEvent" ) != null
        ? ( Boolean ) ( ( Map ) unitApptDtoObject ).get (
            "addEvent" ) : false;
    if ( "IMPRT".equals ( unitCategory ) && ! this.apptService.unitHasSealChargeEvent (
        unitGkey.longValue ( ) ) ) {
      if ( ! addEvent ) {
        return new ResponseEntity (
            this.buildQuestionResponse ( "Esta acción genera cargos adicionales" ) ,
            HttpStatus.OK
        );
      }

      String unitId       = ( String ) ( ( Map ) unitApptDtoObject ).get ( "unitId" );
      String unitLineOpId = ( String ) ( ( Map ) unitApptDtoObject ).get ( "unitLineOpId" );
      String resultStr = this.apptService.recordSeccharge2ToUnit ( unitGkey.longValue ( ) , unitId ,
                                                                   unitCategory , unitLineOpId ,
                                                                   this.getUserLogin ( )
                                                                 );
      if ( resultStr != null && ! resultStr.isEmpty ( ) ) {
        return new ResponseEntity (
            this.buildErrorResponse (
                "Error al generar los cargos. Por favor intente nuevamente" ) ,
            HttpStatus.OK
        );
      }
    }

    Map result = this.apptService.getUnitApptSeals ( unitGkey.longValue ( ) );
    if ( result != null ) {
      List < String > data = new ArrayList ( );
      data.add ( "getUnitApptSeals: invoked" );
      data.add ( "unitGkey:" + unitGkey );
      this.auditService.saveAuditTransaction ( "viewUnitApptSeals" , data.toString ( ) ,
                                               this.getUserContext ( ).getUsername ( )
                                             );
      return new ResponseEntity ( result , HttpStatus.OK );
    }
    else {
      return new ResponseEntity (
          this.buildErrorResponse (
              "No se pudo obtener la información de precintos. Por favor intente nuevamente" ) ,
          HttpStatus.OK
      );
    }
  }

  private
  ResponseEntity < Object > getOpenings (
      String apptSubType , String fromDateStr ,
      String toDateStr , boolean totalsOnly , Map unitApptMap
                                        )
  throws SQLException, ServiceException, IOException {
    unitApptMap.put ( "userId" , this.getUserLogin ( ) );
    Object result = this.apptService.getAppointmentOpenings ( apptSubType , fromDateStr ,
                                                              toDateStr ,
                                                              totalsOnly , unitApptMap , true
                                                            );
    if ( result instanceof String ) {
      return new ResponseEntity ( this.buildErrorResponse ( ( String ) result ) , HttpStatus.OK );
    }
    else {
      if ( unitApptMap != null && totalsOnly ) {
        List resultList = ( List ) result;
        this.addUnitDateToCalendar ( resultList , unitApptMap , fromDateStr , "unitStartDateName" ,
                                     "" ,
                                     ""
                                   );
        String unitCategory     = ( String ) unitApptMap.get ( "unitCategory" );
        String forcedDateStr    = ( String ) unitApptMap.get ( "forcedDateStr" );
        String unitLimitDateStr = ( String ) unitApptMap.get ( "unitLimitDateStr" );
        if ( "IMPRT".equals ( unitCategory ) && forcedDateStr != null
             && ! forcedDateStr.isEmpty ( ) ) {
          this.addUnitDateToCalendar ( resultList , unitApptMap , forcedDateStr , "" , "Forzoso" ,
                                       "#9EE6E5"
                                     );
        }
        else if ( "EXPRT".equals ( unitCategory ) && unitLimitDateStr != null
                  && ! unitLimitDateStr.isEmpty ( ) ) {
          this.addUnitDateToCalendar ( resultList , unitApptMap , unitLimitDateStr ,
                                       "unitEndDateName" ,
                                       "" , "#9EE6E5"
                                     );
        }
        else {
          this.addUnitDateToCalendar (
              resultList , unitApptMap , toDateStr , "unitEndDateName" , "" , "" );
        }
      }

      return new ResponseEntity ( ( List ) result , HttpStatus.OK );
    }
  }

  private
  String getUserLogin ( ) {
    org.springframework.security.core.userdetails.User loggedUser =
        ( org.springframework.security.core.userdetails.User ) SecurityContextHolder
            .getContext ( )
            .getAuthentication ( ).getPrincipal ( );
    String login = loggedUser.getUsername ( );
    return login;
  }

  private
  String getDataTableHtml ( boolean canAddItem , boolean canDeleteItem ) {
    StringBuilder sb = new StringBuilder ( );
    sb.append ( "<thead>\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <th>Contenedor</th>\n" );
    sb.append ( "    <th>Categoría</th>\n" );
    sb.append ( "    <th>Tipo</th>\n" );
    sb.append ( "    <th>Línea</th>\n" );
    sb.append ( "    <th>Estado</th>\n" );
    sb.append ( "    <th>Documento</th>\n" );
    sb.append ( "    <th>POL</th>\n" );
    sb.append ( "    <th>POD</th>\n" );
    sb.append ( "    <th>Nave</th>\n" );
    sb.append ( "    <th>Viaje</th>\n" );
    sb.append ( "    <th>Fecha</th>\n" );
    sb.append ( "    <th>Turno</th>\n" );
    sb.append ( "    <th>Precintos</th>\n" );
    if ( canAddItem ) {
      sb.append ( "    <th>Coordinar</th>\n" );
    }

    if ( canDeleteItem ) {
      sb.append ( "    <th>Anular</th>\n" );
    }

    sb.append ( "</tr>\n" );
    sb.append ( "</thead>\n" );
    sb.append ( "<tbody data-bind=\"foreach: apptViewModel.unitApptTable\">\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitId\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitCategory\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitIsoTypeId\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitLineOpId\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: ufvTransitStateDesc\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitDocumentNbr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitPol\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitPod\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitVesselName\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitVoyage\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: unitLimitDateStr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: apptRequestedDateStr\"></label></td>\n" );
    sb.append (
        "    <td><button class=\"btn btn-icon btn-info btn-sm\" data-bind=\"click: $parent"
        + ".viewUnitSeals, enable: isImportInYardWithFFD\"> <i class=\"ti-view-list\"></i> "
        + "</button></td>\n" );
    if ( canAddItem ) {
      sb.append (
          "    <td><button class=\"btn btn-icon btn-primary btn-sm\" data-bind=\"click: $parent"
          + ".addAppt, enable: allowToCreateAppt\"> <i class=\"ti-calendar\"></i> "
          + "</button></td>\n" );
    }

    if ( canDeleteItem ) {
      sb.append (
          "    <td><button class=\"btn btn-icon btn-danger btn-sm\" data-bind=\"click: $parent"
          + ".cancelAppt, enable: allowToCancelAppt\"> <i class=\"ti-close\"></i> "
          + "</button></td>\n" );
    }

    sb.append ( "</tr>\n" );
    return sb.toString ( );
  }

  private
  void addUnitDateToCalendar (
      List eventList , Map unitApptMap , String dateStr ,
      String dateNameField , String title , String color
                             ) {
    String   defaultColor  = "#AEFFA9";
    String[] dateStrArray  = dateStr.split ( " " );
    String   dateNoTimeStr = dateStrArray.length > 0 ? dateStrArray[ 0 ] : "";
    String   dateName      = ( String ) unitApptMap.get ( dateNameField );
    boolean addEvent = dateName != null && ! dateName.isEmpty ( )
                       || title != null && ! title.isEmpty ( );
    if ( addEvent && dateNoTimeStr != null && ! dateNoTimeStr.isEmpty ( ) ) {
      color = color != null && ! color.isEmpty ( ) ? color : defaultColor;
      title = title != null && ! title.isEmpty ( ) ? title : dateName;
      AppointmentOpeningDto dto = new AppointmentOpeningDto ( title , dateNoTimeStr , "" , "" , 0 ,
                                                              ( Long ) null , color , title
      );
      eventList.add ( dto );
    }

  }

  private
  Map buildSuccessResponse ( String msg ) {
    return this.buildResponse ( "OK" , msg );
  }

  private
  Map buildErrorResponse ( String msg ) {
    return this.buildResponse ( "ERROR" , msg );
  }

  private
  Map buildQuestionResponse ( String msg ) {
    return this.buildResponse ( "QUESTION" , msg );
  }

  private
  Map buildResponse ( String status , String msg ) {
    Map map = new HashMap ( );
    map.put ( "status" , status );
    map.put ( "msg" , msg );
    return map;
  }
}
