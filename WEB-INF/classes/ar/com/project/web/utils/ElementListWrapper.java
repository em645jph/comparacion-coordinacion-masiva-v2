//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package ar.com.project.web.utils;

public
class ElementListWrapper {

  private Integer id;
  private String  code;
  private String  name;

  public
  ElementListWrapper ( Integer id , String name ) {
    this.id   = id;
    this.name = name;
  }

  public
  ElementListWrapper ( Integer id , String code , String name ) {
    this.id   = id;
    this.code = code;
    this.name = name;
  }

  public
  Integer getId ( ) {
    return this.id;
  }

  public
  void setId ( Integer id ) {
    this.id = id;
  }

  public
  String getName ( ) {
    return this.name;
  }

  public
  void setName ( String name ) {
    this.name = name;
  }

  public
  String getCode ( ) {
    return this.code;
  }

  public
  void setCode ( String code ) {
    this.code = code;
  }
}
