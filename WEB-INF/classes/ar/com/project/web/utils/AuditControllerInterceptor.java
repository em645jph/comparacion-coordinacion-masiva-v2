//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package ar.com.project.web.utils;

import ar.com.project.core.service.ActivityLogService;
import ar.com.project.core.service.UserService;
import java.util.Enumeration;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

@Component
public
class AuditControllerInterceptor extends HandlerInterceptorAdapter {

  private static final String USB_ID = "8087:07da";
  @Autowired
  ActivityLogService auditoriaService;
  @Autowired
  UserService        userService;

  public
  AuditControllerInterceptor ( ) {
  }

  public
  boolean preHandle ( HttpServletRequest request , HttpServletResponse response , Object handler )
  throws Exception {
    Logger logger = Logger.getLogger ( this.getClass ( ) );
    String method = request.getMethod ( );
    if ( method.equalsIgnoreCase ( "GET" ) ) {
      return true;
    }
    else {
      String                 uri        = request.getRequestURI ( );
      String                 address    = request.getRemoteAddr ( );
      String                 params     = "";
      Enumeration < String > paramNames = request.getParameterNames ( );

      while ( paramNames.hasMoreElements ( ) ) {
        String key   = ( String ) paramNames.nextElement ( );
        String value = request.getParameter ( key );
        params = params + key + ":'" + value + "'";
        if ( paramNames.hasMoreElements ( ) ) {
          params = params + ", ";
        }
      }

      logger.debug ( address + " " + method + " " + uri + " " + params );

      try {
        if ( SecurityContextHolder.getContext ( ).getAuthentication ( ) != null
             && ! SecurityContextHolder.getContext ( ).getAuthentication ( ).getPrincipal ( )
                                       .toString ( )
                                       .equals ( "anonymousUser" ) ) {
          User securityUser = ( User ) SecurityContextHolder.getContext ( ).getAuthentication ( )
                                                            .getPrincipal ( );
          ar.com.project.core.domain.User user = this.userService.getUser (
              securityUser.getUsername ( ) );
          this.auditoriaService.log ( method , address + " " + method + " " + uri , params ,
                                      user.getGkey ( )
                                    );
        }
        else {
          logger.debug ( "El usuario todavÃ\u00ada no estÃ¡ autenticado" );
        }

        return true;
      }
      catch ( Exception var12 ) {
        logger.error ( var12.getMessage ( ) , var12 );
        return false;
      }
    }
  }
}
