//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package ar.com.project.web.utils;

public
class ResponseWrapper {

  Integer        id;
  String         code;
  ResponseStatus status;
  String         message;
  String         detail;
  Object         data;

  public
  ResponseWrapper ( ) {
  }

  public
  ResponseWrapper ( ResponseStatus status , String message ) {
    this.status  = status;
    this.message = message;
  }

  public
  ResponseWrapper ( ResponseStatus status , String code , String message ) {
    this.code    = code;
    this.status  = status;
    this.message = message;
  }

  public
  ResponseWrapper ( ResponseStatus status , Integer id , String message ) {
    this.id      = id;
    this.status  = status;
    this.message = message;
  }

  public
  ResponseWrapper ( ResponseStatus status , Object data ) {
    this.data   = data;
    this.status = status;
  }

  public
  ResponseWrapper ( Integer id , String code , ResponseStatus status , String message ) {
    this.id      = id;
    this.code    = code;
    this.status  = status;
    this.message = message;
  }

  public
  ResponseWrapper (
      Integer id , String code , ResponseStatus status , String message ,
      String detail
                  ) {
    this.id      = id;
    this.code    = code;
    this.status  = status;
    this.message = message;
    this.detail  = detail;
  }

  public
  ResponseWrapper ( ResponseStatus status , String code , String message , String detail ) {
    this.code    = code;
    this.status  = status;
    this.message = message;
    this.detail  = detail;
  }

  public
  Integer getId ( ) {
    return this.id;
  }

  public
  void setId ( Integer id ) {
    this.id = id;
  }

  public
  String getCode ( ) {
    return this.code;
  }

  public
  void setCode ( String code ) {
    this.code = code;
  }

  public
  ResponseStatus getStatus ( ) {
    return this.status;
  }

  public
  void setStatus ( ResponseStatus status ) {
    this.status = status;
  }

  public
  String getMessage ( ) {
    return this.message;
  }

  public
  void setMessage ( String message ) {
    this.message = message;
  }

  public
  String getDetail ( ) {
    return this.detail;
  }

  public
  void setDetail ( String detail ) {
    this.detail = detail;
  }

  public
  Object getData ( ) {
    return this.data;
  }

  public
  void setData ( Object data ) {
    this.data = data;
  }

  public static
  enum ResponseStatus {
    ERROR,
    OK;

    private
    ResponseStatus ( ) {
    }
  }
}
