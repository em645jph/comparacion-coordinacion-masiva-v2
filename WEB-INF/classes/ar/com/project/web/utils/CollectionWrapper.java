//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package ar.com.project.web.utils;

import ar.com.project.web.utils.ResponseWrapper.ResponseStatus;
import java.util.Collection;

public
class CollectionWrapper < T > extends ResponseWrapper {

  Collection < T > resultados;

  public
  CollectionWrapper ( ) {
  }

  public
  CollectionWrapper ( Collection < T > resultados ) {
    super ( ResponseStatus.OK , "" );
    this.resultados = resultados;
  }

  public
  CollectionWrapper (
      Collection < T > resultados , ResponseWrapper.ResponseStatus status ,
      String message
                    ) {
    super ( status , message );
    this.resultados = resultados;
  }

  public
  Collection < T > getResultados ( ) {
    return this.resultados;
  }

  public
  void setResultados ( Collection < T > resultados ) {
    this.resultados = resultados;
  }
}
