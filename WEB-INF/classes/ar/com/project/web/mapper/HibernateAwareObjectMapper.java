//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package ar.com.project.web.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module.Feature;

public
class HibernateAwareObjectMapper extends ObjectMapper {

  private static final long serialVersionUID = - 7456296014781423600L;

  public
  HibernateAwareObjectMapper ( ) {
    Hibernate4Module module = new Hibernate4Module ( );
    module.disable ( Feature.USE_TRANSIENT_ANNOTATION );
    this.registerModule ( module );
  }
}
