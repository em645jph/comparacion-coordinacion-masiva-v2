//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package ar.com.project.web.handler;

import ar.com.project.web.controller.CommonController;
import com.curcico.jproject.core.exception.BaseException;
import com.curcico.jproject.core.exception.BusinessException;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public
class BaseExceptionHandler extends CommonController {

  private static final Logger logger = Logger.getLogger ( BaseExceptionHandler.class );

  public
  BaseExceptionHandler ( ) {
  }

  @ExceptionHandler ( { BaseException.class })
  @ResponseStatus ( HttpStatus.INTERNAL_SERVER_ERROR)
  public
  ResponseEntity < Object > handleInternalErrorException (
      HttpServletRequest request ,
      HttpServletResponse response , BaseException ex
                                                         ) {
    HttpHeaders headers = new HttpHeaders ( );
    headers.setContentType ( MediaType.APPLICATION_JSON );
    return new ResponseEntity ( this.getMessage ( request , response , ex ) , headers ,
                                HttpStatus.INTERNAL_SERVER_ERROR
    );
  }

  @ExceptionHandler ( { BusinessException.class })
  @ResponseStatus ( HttpStatus.INTERNAL_SERVER_ERROR)
  public
  ResponseEntity < Object > handleBusinessException (
      HttpServletRequest request ,
      HttpServletResponse response , BusinessException ex
                                                    ) {
    HttpHeaders headers = new HttpHeaders ( );
    headers.setContentType ( MediaType.APPLICATION_JSON );
    return new ResponseEntity ( this.getMessage ( request , response , ex ) , headers ,
                                HttpStatus.INTERNAL_SERVER_ERROR
    );
  }

  private
  String getMessage (
      HttpServletRequest request , HttpServletResponse response ,
      Exception ex
                    ) {
    XStream xstream = new XStream ( new JettisonMappedXmlDriver ( ) );
    xstream.autodetectAnnotations ( true );
    ErrorMessage error   = new ErrorMessage ( ex );
    String       message = getBundle ( ex.getMessage ( ) );
    error.setMessage (
        message != null && ! message.isEmpty ( ) ? message : getBundle ( "internal.error" ) );
    logger.error ( "[ " + error.errorId + " ]" + ex.getClass ( ).getName ( ) + " occured: URL="
                   + request.getRequestURL ( ) , ex );
    return xstream.toXML ( error );
  }

  @XStreamAlias ( "error")
  private static
  class ErrorMessage {

    String errorId = RandomStringUtils.random ( 8 , true , true ).toUpperCase ( );
    String message;
    String exception;

    public
    ErrorMessage ( Exception e ) {
      this.exception = e.getClass ( ).getCanonicalName ( );
    }

    public
    void setMessage ( String message ) {
      this.message = message;
    }
  }
}
