<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec"   uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c"     uri="http://java.sun.com/jsp/jstl/core" %>
<!-- Footer -->
   <footer class="footer" style="bottom: 0 !important;">
       <div class="container-fluid">
           <div class="row" >
               <div class="col-12" >
                   <div style="margin-top: 0.5% !important;">
                       <strong >APM Terminals Buenos Aires</strong> - Copyright � 2019
                   </div>
               </div>
           </div>
       </div>
   </footer>
<!-- End Footer -->
</div>