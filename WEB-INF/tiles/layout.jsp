<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta content="IE=edge" http-equiv="X-UA-Compatible">
		<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
		<meta content="Puerto Digital" name="description">

		<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
		<meta http-equiv="Pragma" content="no-cache" />
		<meta http-equiv="Expires" content="0" />
		<title><tiles:insertAttribute name="title" ignore="true" /></title>
		
		<script type="text/javascript">
			var url_logout 		= '${pageContext.servletContext.contextPath}/j_spring_security_logout';
			var url_application = '${pageContext.servletContext.contextPath}';
		</script>
	
		<!-- Plugins  -->
		<script src="<c:url value="/resources/assets/js/modernizr.min.js" />"></script>
		<script src="<c:url value="/resources/assets/js/jquery.min.js" />"></script>
		<script src="<c:url value="/resources/assets/js/popper.min.js" />"></script>
		<script src="<c:url value="/resources/assets/js/bootstrap.min.js" />"></script>
		<script src="<c:url value="/resources/assets/js/jquery.slimscroll.js" />"></script>
		<script src="<c:url value="/resources/assets/js/jquery.core.js" />"></script>
		<script src="<c:url value="/resources/assets/js/jquery.app.js" />"></script>
		<script src="<c:url value="/resources/assets/js/jquery-ui.min.js" />"></script>
		<script src="<c:url value="/resources/assets/js/knockout-min.js" />"></script>
		<script src="<c:url value="/resources/assets/js/sweetalert2@8.js" />"></script>
		<script src="<c:url value="/resources/assets/js/modalsFunctions.js" />" charset="UTF-8"></script>
		<script src="<c:url value="/resources/assets/js/dataTableUtils.js" />" charset="UTF-8"></script>
		<script src="<c:url value="/resources/assets/js/jquery.table2excel.js" />" charset="UTF-8"></script>
				
		<!-- Styles -->
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/css/bootstrap.min.css" />" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/css/icons.css" />" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/css/style.css" />" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/plugins/datatables/dataTables.bootstrap4.min.css" />" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/plugins/datatables/buttons.bootstrap4.min.css" />" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/plugins/datatables/responsive.bootstrap4.min.css" />" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/css/jquery-ui.min.css" />" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/css/datatables.min.css" />" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/css/apmtba.css" />" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/plugins/timepicker/bootstrap-timepicker.min.css" />" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/plugins/fullcalendar/css/fullcalendar.min.css" />" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/plugins/switchery/switchery.min.css" />" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/plugins/spectrum/spectrum.css" />" />
 		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/plugins/bootstrap-multiselect/dist/css/bootstrap-multiselect.css" />" /> 
		<!-- Multi Item Selection examples -->
        <link href="<c:url value="/resources/assets/plugins/datatables/select.bootstrap4.min.css" />" rel="stylesheet" type="text/css" />
        
		<!-- App favicon -->
        <link rel="shortcut icon" href="<c:url value="/resources/assets/images/apmticon.ico" />">
        
        <!-- Plugins extras  -->
        <script	src="<c:url value="/resources/assets/plugins/datatables/jquery.dataTables.min.js" />"></script>
        <script	src="<c:url value="/resources/assets/plugins/datatables/dataTables.bootstrap4.min.js" />"></script>
        <script	src="<c:url value="/resources/assets/plugins/datatables/dataTables.buttons.min.js" />"></script>
        <script	src="<c:url value="/resources/assets/plugins/datatables/buttons.bootstrap4.min.js" />"></script>
        <script	src="<c:url value="/resources/assets/plugins/datatables/jszip.min.js" />"></script>
        <script	src="<c:url value="/resources/assets/plugins/datatables/pdfmake.min.js" />"></script>
        <script	src="<c:url value="/resources/assets/plugins/datatables/vfs_fonts.js" />"></script>
        <script	src="<c:url value="/resources/assets/plugins/datatables/buttons.html5.min.js" />"></script>
        <script	src="<c:url value="/resources/assets/plugins/datatables/buttons.print.min.js" />"></script>
        <script src="<c:url value="/resources/assets/plugins/timepicker/bootstrap-timepicker.js" />"></script>
        <script src="<c:url value="/resources/assets/plugins/moment/moment.js" />"></script>
        <script src="<c:url value="/resources/assets/plugins/fullcalendar/js/fullcalendar.min.js" />"></script>
        <script src="<c:url value="/resources/assets/pages/jquery.fullcalendar.js" />"></script>
        <script src="<c:url value="/resources/assets/plugins/switchery/switchery.min.js" />"></script>
        <script src="<c:url value="/resources/assets/plugins/spectrum/spectrum.js" />"></script>
        <script src="<c:url value="/resources/assets/pages/notify.min.js" />"></script>
        <!-- Key Tables -->
        <script src="<c:url value="/resources/assets/plugins/datatables/dataTables.keyTable.min.js" />"></script>
        
        <!-- Responsive examples -->
        <script src="<c:url value="/resources/assets/plugins/datatables/dataTables.responsive.min.js" />"></script>
        <script src="<c:url value="/resources/assets/plugins/datatables/responsive.bootstrap4.min.js" />"></script>
        

        <!-- Selection table -->
        <script src="<c:url value="/resources/assets/plugins/datatables/dataTables.select.min.js" />"></script>
        
		<!-- Multiple select -->
		<script src="<c:url value="/resources/assets/plugins/bootstrap-multiselect/dist/js/bootstrap-multiselect.js" />" charset="UTF-8"></script>
	</head>
	<body id="body">
		<div>
				<tiles:insertAttribute name="menu" />
				<tiles:insertAttribute name="body" />
		</div>
		<tiles:insertAttribute name="footer" />		
	</body>
</html>