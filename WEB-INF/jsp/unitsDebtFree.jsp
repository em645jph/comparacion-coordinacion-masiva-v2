<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" type="text/css"	href="<c:url value="/resources/assets/css/icons.css" />" />
<script src="<c:url value="/resources/assets/js/unitsDebtFree.js" />" charset="UTF-8"></script>	
<div class="wrapper">
	<div class="container-fluid">

		<script>
		if("${invNbr}" != null){
			var draft = "${invNbr}";
		}
		</script>

		<div class="row">
			<div class="col-sm-12">
				<h2>Validación Libre Deuda</h2>
			</div>
		</div>
		<br>
		
		<div class="row">
			<div class="col-md-12">
				<div class="card-box" id="tableBox">
					<table class="table table-striped datatable" id="tbUnits">
	                    <thead>
		                    <tr>
		                        <th>Contenedor</th>
		                        <th>Estado Libre Deuda</th>
		                        <th>Fecha Libre Deuda</th>
		                        <th>Turno</th>
		                    </tr>
	                    </thead>
	                    <tbody data-bind="foreach: viewModel.units">
		                    <tr data-bind="attr:{ 'id': $index }">                                       
		                        <td data-bind="text: unitId"></td>
		                        <td data-bind="text: ldStatus == null ? 'No liberado' : 'Liberado'"></td>
		                        <td data-bind="text: ldDate == null ? '-' : formatDate(ldDate)"></td>
		                        <td data-bind="text: apptTime == null ? '-' : formatDate(apptTime)"></td>
		                    </tr>
	                    </tbody>
	                </table>
				</div>
			</div>
		</div>
		
	</div>
<!-- end container -->