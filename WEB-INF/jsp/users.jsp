<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="<c:url value="/resources/assets/js/users.js" />" charset="UTF-8"></script>
<style>
.swal-large{
    width:650px !important;
}
</style>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
				<div class="col-sm-12">
					<h2>Usuarios</h2>
				</div>
			</div>
			<br>
            
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box">
                        <div class="table table-striped datatable">
                            <table class="table table-hover datatable" id="tbUsers">
                                <thead>
                                 <tr>
                                     <th>Usuario</th>
                                     <th>Razon Social / Nombre</th>
                                     <th>Correo Administración</th>
                                     <th>Usuario Facturación</th>
                                     <th>Habilitado</th>
                                     <th>Último acceso</th>
                                     <th></th>
                                     <th></th>
                                 </tr>
                                </thead>
                                <tbody data-bind="foreach: viewModel.users">
                                 <tr data-bind="attr:{usergkey: gkey}">                                       
                                     <td data-bind="text: login"></td>
                                     <td data-bind="text: fullName == null ? '-' : fullName"></td>
                                     <td data-bind="text: managementEmailAddress == null ? '-' : managementEmailAddress"></td>
                                     <td>
			                        	<!-- ko {ifnot: billingUser} -->
			                        		<i class="ti-close" title="No se puede facturar a este usuario"></i>
			                        	<!-- /ko -->
			                        	<!-- ko {if: billingUser} -->
			                        		<i class="ti-check" title="Es posible facturar a este usuario"></i>
			                        	<!-- /ko -->
			                         </td>
			                         <td>
			                        	<!-- ko {ifnot: enabled && lifeCycleState == 'ACT'} -->
			                        		<i class="ti-close" title="Usuario deshabilitado para operar"></i>
			                        	<!-- /ko -->
			                        	<!-- ko {if: enabled && lifeCycleState == 'ACT'} -->
			                        		<i class="ti-check" title="Usuario OK para operar"></i>
			                        	<!-- /ko -->
			                         </td>
                                     <td data-bind="text: millisecondsToDate(lastLogin)"></td>
                                     <td>
                                     	<button type="button" class="btn btn-primary" onclick="getRoles(this)" data-bind="attr: {'usergkey' : gkey}" title="Ver roles"><i class="ti-search"></i></button>
                                     </td>
                                     <td>
                                     	<button type="button" class="btn btn-info" onclick="getMoreData(this)" data-bind="attr: {'position': $index}" title="Mas información"><i class="ti-bookmark-alt"></i></button>
                                     </td>
                             </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end container -->