<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="addEditFilterParamModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title" id="sfpModalTitle">Parámetros - Filtro</h4>
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<input type="hidden" class="form-control" id="txtSfpGkey" disabled>
					</div>
				</div>
				<div class="row">					
					<div class="col-sm-6">
						<label for="cbSfpType"> Tipo Filtro(*): </label>										
						<select width="40%" id="cbSfpType" class="form-control" name="cbSfpType"
								data-bind="options:scheduleViewModel.paramTypeCb, optionsText:'description', optionsValue:'key', optionsCaption: '--'">
						</select>
					</div>
					<div class="col-sm-6">
						<label for="txtSfpValue"> Valor(*): </label>										
						<input type="text" style="text-transform: uppercase;" class="form-control" id="txtSfpValue" maxlength="100">
					</div>
				</div>
				<div class="row"><br></div>
				<div class="row">
					<div class="col-sm-12" align="center">
						${filterParamButtons}
					</div>
				</div>
				<div class="row"><br></div>
				<div class="row">
						<div class="col-sm-12">
							<div class="table-responsive" >
                                <table class="table table-hover datatable" id="tbFilterParam" width="100%">
                                	${filterParamDataTable}                                
                            </table>
                       </div>							
					</div>
				</div>
            </div>
            <!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-dark" id="btnCloseSfp" data-dismiss="modal">Cerrar</button>
			</div>	
		</div>
	</div>
</div>	