<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<!-- Modal -->
<div class="modal fade" id="scannerLockModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Bloquear Contenedor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <!-- ko {if: permisionAction() != false} -->
       	<div  class="alert alert-danger ng-star-inserted" role="alert">
		    <strong _ngcontent-c10="">Cutoff documental vencido o inexistente!</strong> No es posible bloquear los contenedores.
		</div>
	 <!-- /ko -->
        <form>
		  <div class="form-row">
		    <div class="form-group col-md-6">
		      <label for="unit">Contenedor</label>
		      <input type="text" class="form-control" id="unit"  disabled data-bind="textInput: unitModal">
		    </div>
		    <div class="form-group col-md-6">
		      <label for="permission">Permiso</label>
		      <input type="text" class="form-control" id="permission" disabled data-bind="textInput: permissionModal">
		    </div> 
		  </div>
		  <div class="form-group">
		    <label for="status">Estado</label>
		    <input type="text" class="form-control" id="status" disabled data-bind="textInput: statusModal">
		  </div>
		    <div class="form-group">
		    <label for="typeLock">Motivo: (*)</label>
		    <select id="typeLock" class="form-control" name="typeLock" data-bind="options:viewModel.searchScTypesLock, optionsText:'description' , optionsValue:'key', optionsCaption: '--', value: selectedTypeLock"></select>		   
		  </div>						
		  <div class="form-group">
		    <label for="noteLock">Observacion:</label>
		    <textarea class="form-control rounded-0"  rows="3" id="noteLock" placeholder="Razon del bloqueo" required="required" maxlength="170" style="text-transform: uppercase;"></textarea>
		  </div>
		</form>
      </div>
      <div class="modal-footer">
			<button type="button" class="btn btn-primary" data-bind="click: actionLock, text: 'Bloquear', style: { display: selectedTypeLock() == 'SCANNER' && permisionAction() != true ? 'block' : 'none' }">Bloquear</button>
			<button type="button" class="btn btn-dark" id="btnCancelLockPermission" data-dismiss="modal">Cancelar</button>
	 </div>		
    </div>
  </div>
</div>
