<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="addUnitPreadviseModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Preavisar</h4>
				<button type="button" class="close" data-dismiss="modal">�</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-4">
						<label for="txtUnitId"> Contenedor:</label> 
						<input type="text" name="txtUnitId" id="txtUnitId" class="form-control" style="text-transform: uppercase;" maxlength="11"/>
					</div>
					<div class="col-sm-4">
						<label for="cbUnitIsoType"> Tama�o: </label>										
						<select width="40%" id="cbUnitIsoType" class="form-control" name="cbUnitIsoType"
								data-bind="options:preadviseViewModel.bkgItemList, optionsText:'size', optionsValue:'itemGkey', optionsCaption: '--'">
						</select>
					</div>
					<div class="col-sm-4">
						<label for="txtUnitTemperature"> Temperatura:</label> 
						<input type="text" name="txtUnitTemperature" id="txtUnitTemperature" class="form-control" data-bind="value: itemTemperature" disabled/>
					</div>
				</div>
				<div class="row">
					<br>
				</div>
				<div class="row">
					<div class="col-sm-4">
						<label for="txtUnitTare">Tara (kg):</label>
						<input type="number" name="txtUnitTare" id="txtUnitTare" class="form-control" data-bind="value: itemTare"/>
					</div>
					<div class="col-sm-4">
						<label for="txtUnitContentWt"> Peso Mercader�a (kg):</label> 
						<input type="number" name="txtUnitContentWt" id="txtUnitContentWt" class="form-control"/>
					</div>
					<div class="col-sm-4">
						<label for="vgmRadio"> Realiza VGM:</label> 
						<div>
							<div class="radio radio-info form-check-inline">
	                            <input type="radio" id="radioVgmInternal" name="vgmRadio" value="VGMT4" checked>
	                            <label for="customRadio1">Terminal</label>
	                        </div>
	                        <div class="radio radio-info form-check-inline">
	                        	<input type="radio" id="radioVgmExternal" name="vgmRadio" value="VGMEXT">
	                            <label for="customRadio2">Externo</label>
	                        </div>
						</div>
					</div>
				</div>
				<div class="row">
					<br>
				</div>
				<div class="row">
					<div class="col-sm-4">
						<label for="txtUnitSeal2">Precinto Aduana:</label> <a data-toggle="modal" href="#sealNbr2Modal"> <i class="mdi mdi-help-circle" style="color:red;"></i> </a>
						<input type="text" name="txtUnitSeal2" id="txtUnitSeal2" class="form-control" style="text-transform: uppercase;" maxlength="15" placeholder="Precinto de PUERTA del contenedor"/>
					</div>
					<div class="col-sm-4">
						<label for="txtUnitCustomsPerm">Permiso de embarque:</label>
						<input type="text" name="txtUnitCustomsPerm" id="txtUnitCustomsPerm" class="form-control" style="text-transform: uppercase;" minlength="16" maxlength="16"/>
					</div>
					<div class="col-sm-4">
						<label for="txtUnitEmail"> Email Notificaci�n:</label> 
						<input type="text" name="txtUnitEmail" id="txtUnitEmail" class="form-control" style="text-transform: uppercase;"/>
					</div>
					
				</div>
				<div class="row">
					<br>
				</div>
				<div class="row">
					<div class="col-sm-4">
						<label for="cbUnitIsOog"> Fuera de medida: </label>										
						<select width="40%" id="cbUnitIsOog" class="form-control" name="cbUnitIsOog"
								data-bind="options:preadviseViewModel.yesNoCb, optionsText:'description', optionsValue:'key', optionsCaption: '--'">
						</select>
					</div>
					<div class="col-sm-4 oog-div">
						<label for="txtUnitTop"> Arriba (cm):</label> 
						<input type="number" name="txtUnitTop" id="txtUnitTop" class="form-control" />
					</div>
					<div class="col-sm-4 oog-div">
						<label for="txtUnitFront"> Frente (cm):</label> 
						<input type="number" name="txtUnitFront" id="txtUnitFront" class="form-control"/>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4 oog-div">
						<label for="txtUnitBack"> Atr�s (cm):</label> 
						<input type="number" name="txtUnitBack" id="txtUnitBack" class="form-control"/>
					</div>
					<div class="col-sm-4 oog-div">
						<label for="txtUnitLeft"> Izquierda (cm):</label> 
						<input type="number" name="txtUnitLeft" id="txtUnitLeft" class="form-control" />
					</div>
					<div class="col-sm-4 oog-div">
						<label for="txtUnitRight"> Derecha (cm):</label> 
						<input type="number" name="txtUnitRight" id="txtUnitRight" class="form-control" />
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<br>
						<small><em>Ten� en cuenta que el presente formulario tiene car�cter de declaraci�n jurada.<br><br>
						Por favor actualice el peso de su mercader�a de acuerdo a lo declarado en Aduana, en caso de estar err�neo al momento del ingreso, se modificar� el mismo y se adicionar�n extra costos.<br>
						Podr� corregir su preaviso de forma 100% online mediante este mismo m�dulo hasta el momento del ingreso, recuerde que si ya ha abonado el ingreso la edici�n tendr� extra-costos administrativos.</em></small>
					</div>					
				</div>
			</div>
            <!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="btnSaveUnitPreadvise">Preavisar</button>
				<button type="button" class="btn btn-dark" id="btnCancelAddUnitPreadvise" data-dismiss="modal">Cancelar</button>
			</div>		
		</div>
	</div>
</div>	