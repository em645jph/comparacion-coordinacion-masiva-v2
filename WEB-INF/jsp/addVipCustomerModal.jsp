<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="addVipCustomerModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title">Agregar Cliente Vip</h4>
				<button type="button" class="close" data-dismiss="modal">�</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<form class="form-horizontal m-t-10" role="form" method="POST" enctype="multipart/form-data" id="addVipCustomerForm">
					<div class="form-group">
						<div class="col-sm-9">
							<input type="hidden" class="form-control" id="txtUserGkey" disabled>
						</div>
					</div>
					<div class="form-group">
						<label for="txtCustomer" class="col-sm-12 col-form-label">Cliente:</label>
						<div class="input-group col-sm-12">
							<input type="text" name="txtCustomer" id="txtCustomer" class="form-control" maxlength="50" placeholder="Nombre o CUIT" style="text-transform: uppercase;"/>
							<span class="input-group-prepend">
                                    <button type="button" id="btnSearchUser" class="btn btn-primary"><i class="fa fa-search"></i></button>
                            </span>
						</div>
					</div>
					<div class="form-group">
						<label for="txtCustomerName" class="col-sm-12 col-form-label">Nombre</label>
						<div class="col-sm-12">
							<input type="text" class="form-control" id="txtCustomerName" disabled>
						</div>
					</div>
					<div class="form-group">
						<label for="txtCustomerDocNbr" class="col-sm-12 col-form-label"># Documento:</label>
						<div class="col-sm-12">
							<input type="text" class="form-control" id="txtCustomerDocNbr" disabled>
						</div>
					</div>
				</form>
            </div>
            <!-- Modal footer -->
			<div class="modal-footer">
				${customerModalButtons}
				<button type="button" class="btn btn-dark" id="btnCancelSaveVipCustomer" data-dismiss="modal">Cancelar</button>
			</div>	
		</div>
	</div>
</div>	