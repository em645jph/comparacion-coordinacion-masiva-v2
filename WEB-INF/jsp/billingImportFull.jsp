<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/assets/css/icons.css" />" />
<script src="<c:url value="/resources/assets/js/billingImportFull.js" />" charset="UTF-8"></script>	

<style>
#tbUnitsByBl,#btnGenerate {
	display: none;
}
</style>
	
<div class="wrapper">
	<div class="container-fluid">

		<div class="row">
			<div class="col-sm-12">
				<h2>Facturaci�n Importaci�n Full</h2>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6 offset-md-3">
				<div class="card-box">
					<br>
						<table class="table table-dark">
							<tr>
								<td>RAZ�N SOCIAL: <strong>${customer.customerName} (${customer.customerId})</strong></td>
								<td>CUIT/CUIL: <strong>${customer.customerTaxId}</strong></td>
								<td>CONDICI�N PAGO: <strong>${customer.customerCreditStatus != 'OAC' ? 'Contado' : 'Cuenta Corriente'}</strong></td>
							</tr>
						</table>
					<hr>
					<h6 class="text-muted font-13 m-t-0 text-uppercase text-center">
					Ingres�	n�mero BL/Contenedor</h6>
					<div class="row">
						<div class="col-sm-6">
							<label for="bkgnbr">N�mero (*)</label> 
							<input type="text" class="form-control" id="valueNbr" name="valueNbr" required>
						</div>
						<div class="col-sm-6">
							<label for="selectLineOp">Tipo (*)</label> 
								<select class="form-control" id="selectType" name="selectType" required>
								  <option value="-1">Seleccionar una opci�n...</option>
								  <option value="BL">Bill of lading</option>
								  <option value="UNIT">Contenedor</option>
								</select>
						</div>
					</div>
					<div class="row optionalView">
						<div class="col-sm-12">
							<br />
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12" align="center">
							<button type="button" class="button button--primary"
								id="btnFindEntity">Buscar</button>
							&nbsp;&nbsp;
							<button type="button" class="button button--success"
								id="btnClean">Limpiar</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				
				<table class="table table-striped datatable" id="tbUnitsByBl">
                    <thead>
                    <tr>
                    	<th></th>
                        <th>Contenedor</th>
                        <th>Tama�o</th>
                        <th>Ubicaci�n</th>
                        <th>Bill Of Lading</th>
                        <th>Buque</th>
                        <th>Viaje</th>
                        <th>Coordinado</th>
                        <th>Fecha Turno</th>
                    </tr>
                    </thead>
                    <tbody data-bind="foreach: viewModel.UnitsByBl">
                    <!-- ko {if: apptNbr == 0} -->
						<tr class="ignoreme" data-toggle="tooltip" data-placement="top" title="No disponible para facturaci�n">
							<td></td>
							<td data-bind="text: unitId"></td>
	                        <td data-bind="text: size"></td>
	                        <td data-bind="text: location"></td>
	                        <td data-bind="text: blNbr"></td>
	                        <td data-bind="text: vessel.vesselName"></td>
	                        <td data-bind="text: vessel.vesselId"></td>
	                        <td><i class="ti-close"></i></td>
	                        <td data-bind="text: apptTimeStr"></td>
						</tr>
					<!-- /ko -->
					<!-- ko {ifnot: apptNbr == 0} -->
						<tr>
							<td></td>
							<td data-bind="text: unitId"></td>
	                        <td data-bind="text: size"></td>
	                        <td data-bind="text: location"></td>
	                        <td data-bind="text: blNbr"></td>
	                        <td data-bind="text: vessel.vesselName"></td>
	                        <td data-bind="text: vessel.vesselId"></td>
	                        <td><i class="ti-check"></i></td>
	                        <td data-bind="text: apptTimeStr"></td>
						</tr>
					<!-- /ko -->
                    </tbody>
                </table>
			
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				<button type="button" class="button button--primary" id="btnGenerate">Generar draft</button>
			</div>
		</div>
	</div>
<!-- end container -->