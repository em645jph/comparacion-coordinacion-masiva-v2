<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- insert this in head -->
<script src="https://cdn.fromdoppler.com/formgenerator/latest/vendor.js?36817862"></script>
<link rel="stylesheet" href="https://cdn.fromdoppler.com/formgenerator/latest/styles.css?36817862">
<link rel="stylesheet" href="https://www.apmterminals.com/bundles/styles?v=tcHNPyCo1IV5Ajf16bdCRrlIe77KvzeYcCALUKULVsE1">
<script src="<c:url value="/resources/assets/js/index.js" />" charset="UTF-8"></script>
<!-- insert this in body --><script id="formRender" type="text/javascript">$jqf().ready(function () { renderForm("AyNY%2FICX3AXx4tPhDV%2FjWw%3D%3D", "637130425747601925") });</script>


<div class="wrapper">
	<div class="container-fluid">
		<div class="row">
			<br>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<h4 class="header-title m-t-0 m-b-20" id="welcomeMessageRandom">${fullname}!</h4>
				<input type="hidden" id="email" value="${email}"/>
				<input type="hidden" id="fullname" value="${fullname}"/>
				<input type="hidden" id="phoneNumber" value="${PhoneNumber}"/>
			</div>
		</div>
	</div>

	<div class="h-scroll">
        <div class="wrapper">
            <header>
                <h2 class="module-heading text-center">Enlaces R�pidos</h2>
            </header>
            <div class="card-image-text-container h-scroll__container equal-height copy-secondary animate-on-scroll animate-on-scroll--active">
                        <div class="card card--image-text animate-on-scroll__item animate-on-scroll__item--active">
                            <a href="https://apps.apmterminals.com.ar/GestionClientes" aria-label="card link" rel="noopener noreferrer" class="card__link ds-tilted" target="_blank"> 

                            <div class="card__image card__image--inline">
                                <figure>
                                    <span class="card__icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 84 84"><g><path d="M51,29.48A12.5,12.5,0,0,0,42.35,33a12.78,12.78,0,0,0-18.92.85,1,1,0,0,0-.31.39v0A12.76,12.76,0,0,0,23.27,50l.08.09a12.77,12.77,0,0,0,19,1A12.52,12.52,0,1,0,51,29.48ZM22.51,43H27.9a24.6,24.6,0,0,0,.51,4.08,21.26,21.26,0,0,0-4,1.14A10.7,10.7,0,0,1,22.51,43Zm1.88-7.16A20.75,20.75,0,0,0,28.34,37a25.23,25.23,0,0,0-.46,4H22.51A10.71,10.71,0,0,1,24.39,35.84Zm8.89-4.66h.2a16.42,16.42,0,0,1,1.87,4.15c-.73.07-1.47.11-2.23.11a22.29,22.29,0,0,1-2.33-.12,16.09,16.09,0,0,1,1.87-4.13Zm2.68.34a10.7,10.7,0,0,1,4.75,2.63,19.84,19.84,0,0,1-3.37.91A19.16,19.16,0,0,0,36,31.52ZM29.87,41a23.78,23.78,0,0,1,.43-3.73,26.36,26.36,0,0,0,2.82.15,27.06,27.06,0,0,0,2.72-.14A23.65,23.65,0,0,1,36.27,41Zm6.38,2a21.82,21.82,0,0,1-.49,3.77c-.86-.08-1.74-.13-2.64-.13a24.88,24.88,0,0,0-2.74.15A23.23,23.23,0,0,1,29.89,43Zm-7.45-8a19.67,19.67,0,0,1-3-.82,10.78,10.78,0,0,1,4.35-2.57A20.06,20.06,0,0,0,28.8,35.05Zm.11,14A20.21,20.21,0,0,0,30.3,52.4a10.77,10.77,0,0,1-4.48-2.58A20.29,20.29,0,0,1,28.91,49Zm2-.28c.72-.07,1.46-.1,2.21-.1a21.2,21.2,0,0,1,2.12.1,16,16,0,0,1-2,4.09h-.38A15.46,15.46,0,0,1,30.91,48.73Zm6.32.26a20.54,20.54,0,0,1,3.43.91,10.75,10.75,0,0,1-4.89,2.63A18.38,18.38,0,0,0,37.23,49Zm4.83-.67a21.39,21.39,0,0,0-4.32-1.26,25.46,25.46,0,0,0,.5-4.06h5.81A10.78,10.78,0,0,1,42.06,48.32ZM38.26,41a25,25,0,0,0-.45-4,21.76,21.76,0,0,0,4.29-1.26A10.81,10.81,0,0,1,44.05,41ZM51,52.53a10.55,10.55,0,0,1-7.37-3,12.77,12.77,0,0,0,0-15A10.53,10.53,0,1,1,51,52.53Z" fill="#ff6319"></path><path d="M52.53,36.74V35.6a1,1,0,0,0-2,0v1.12a2.71,2.71,0,0,0-2.6,2.65v1.12a2.16,2.16,0,0,0,1.77,2.27l.07,0,2.84.46c.3.12.3.27.3.35v1.14a.72.72,0,0,1-.77.67H50.72a.74.74,0,0,1-.79-.67V44.2a1,1,0,1,0-2,0v.53a2.71,2.71,0,0,0,2.6,2.65v1.15a1,1,0,1,0,2,0V47.36a2.7,2.7,0,0,0,2.38-2.63V43.59a2.34,2.34,0,0,0-1.7-2.25L53,41.29l-2.89-.47a.5.5,0,0,1-.19-.08.59.59,0,0,1,0-.25V39.37a.73.73,0,0,1,.78-.67h1.43a.74.74,0,0,1,.79.67v.52a1,1,0,1,0,2,0v-.52A2.69,2.69,0,0,0,52.53,36.74Z" fill="#ff6319"></path></g><g><circle cx="42" cy="42" r="40" fill="none" stroke="#ff6319" stroke-miterlimit="10" stroke-width="4px"></circle></g></svg>
                                    </span>
                                </figure>
                            </div>
                            <article>
                                <header>
                                    <h4 class="card__title">Centro de Gesti�n de Clientes</h4>
                                </header>
                                <ul class="bullet-list">

                                        <li class="bullet-list-item">�Acced� a toda la informaci�n! Informaci�n de Buques, Pagos Online, Seguimiento de tu carga y m�s.</li>
                                                                            <li class="bullet-list-item">Container tracking</li>
                                                                            <li class="bullet-list-item">Online payments and more...</li>
                                </ul>
                            </article>
                            </a>
                        </div>
                        <div class="card card--image-text animate-on-scroll__item animate-on-scroll__item--active">
                            <a href="https://apps.apmterminals.com.ar/puertodigital" aria-label="card link" rel="noopener noreferrer" class="card__link ds-tilted" target="_blank"> 

                            <div class="card__image card__image--inline">
                                <figure>
                                    <span class="card__icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 84 84"><g><path d="M60,51.3h2.83a1,1,0,0,0,1-1V44.46a.88.88,0,0,0,.07-.33,1,1,0,0,0-.22-.61l-4.9-9.18a1,1,0,0,0-.88-.52H49.8V31.73a4.9,4.9,0,0,0-1.26-3.29,4.37,4.37,0,0,0-3.26-1.24H21.68a1,1,0,0,0-1,1v8a1,1,0,0,0,1,1H36.12a1,1,0,0,0,1-1v-.85l3.7,3.22-3.7,3.23v-.86a1,1,0,0,0-1-1H21.68a1,1,0,0,0-1,1V50.3a1,1,0,0,0,1,1H23.3a4,4,0,0,0,2.46,3.27H21.15a1,1,0,1,0,0,2h41.4a1,1,0,1,0,0-2h-5A4,4,0,0,0,60,51.3ZM49.8,35.81h7.46l3.91,7.33H54V40.25h1.64a1,1,0,1,0,0-2H53a1,1,0,0,0-1,1v4.88a1,1,0,0,0,1,1h8.77v4.18H59.65a4,4,0,0,0-7.29,0H49.8ZM22.67,41.94H35.12v2a1,1,0,0,0,1.65.75L43,39.32a1,1,0,0,0,.34-.75,1,1,0,0,0-.34-.75l-6.2-5.41a1,1,0,0,0-1.07-.15,1,1,0,0,0-.58.9v2.05H22.67v-6H45.28a2.46,2.46,0,0,1,1.83.63,2.91,2.91,0,0,1,.7,1.88V49.31H41.46a4,4,0,0,0-7.3,0H30.91a4,4,0,0,0-7.3,0h-.94ZM37.81,52.87a2,2,0,1,1,2-2A2,2,0,0,1,37.81,52.87Zm-10.55-4a2,2,0,1,1-2,2A2,2,0,0,1,27.26,48.9Zm3.95,2.4h2.64a4,4,0,0,0,2.46,3.27H28.76A4,4,0,0,0,31.21,51.3Zm8.09,3.27a4,4,0,0,0,2.46-3.27H52.05a4,4,0,0,0,2.46,3.27ZM56,52.87a2,2,0,1,1,2-2A2,2,0,0,1,56,52.87Z" fill="#ff6319"></path></g><g><circle cx="42" cy="42" r="40" fill="none" stroke="#ff6319" stroke-miterlimit="10" stroke-width="4px"></circle></g></svg>
                                    </span>
                                </figure>
                            </div>
                            <article>
                                <header>
                                    <h4 class="card__title">Puerto Digital</h4>
                                </header>
                                <ul class="bullet-list">

                                        <li class="bullet-list-item">Coordin�</li>
                                                                            <li class="bullet-list-item">Presupuest�</li>
                                                                            <li class="bullet-list-item">Pag� Online e imprim� tu documentaci�n</li>
                                </ul>
                            </article>
                            </a>
                        </div>
                        <div class="card card--image-text animate-on-scroll__item animate-on-scroll__item--active">
                            <a href="https://www.apmterminals.com/es/buenos-aires/practical-information/procedures" aria-label="card link" class="card__link ds-tilted"> 

                            <div class="card__image card__image--inline">
                                <figure>
                                    <span class="card__icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 84 84"><g><path d="M63,44.74l-6-9.58H48.76a5.73,5.73,0,0,0-1.48-3.34,5.31,5.31,0,0,0-4-1.5H39.63a10,10,0,1,0-16,11.9V54H27a4.8,4.8,0,0,0,9.58,0H49.37A4.8,4.8,0,0,0,59,54H63ZM31,27.4a8.05,8.05,0,1,1-8,8.05A8.06,8.06,0,0,1,31,27.4Zm.75,29.17a2.82,2.82,0,1,1,2.82-2.82A2.82,2.82,0,0,1,31.77,56.57Zm15-20.78V52H36.49a.85.85,0,0,0-.23,0,4.8,4.8,0,0,0-9,0H25.61V43.9a10,10,0,0,0,15.45-8.45,9.89,9.89,0,0,0-.52-3.14H43.3a3.37,3.37,0,0,1,2.54.89,4,4,0,0,1,1,2.56Zm7.35,20.78A2.82,2.82,0,1,1,57,53.75,2.81,2.81,0,0,1,54.16,56.57ZM58.63,52a4.79,4.79,0,0,0-8.94,0H48.8V37.15H56l4,6.44H52.82v-2.9h1.64a1,1,0,1,0,0-2H50.83v6.86H61V52Z" fill="#ff6319"></path><path d="M29.53,39.7l6.89-7.29A1,1,0,0,0,35,31l-5.16,5.47-1.53-2.3a1,1,0,0,0-1.66,1.1Z" fill="#ff6319"></path></g><g><circle cx="42" cy="42" r="40" fill="none" stroke="#ff6319" stroke-miterlimit="10" stroke-width="4px"></circle></g></svg>
                                    </span>
                                </figure>
                            </div>
                            <article>
                                <header>
                                    <h4 class="card__title">�C�mo operar con nosotros?</h4>
                                </header>
                                <ul class="bullet-list">

                                        <li class="bullet-list-item">Preguntas frecuentes sobre como operar en APM Terminals Buenos Aires.</li>
                                                                            <li class="bullet-list-item">�No te pierdas de ning�n detalle!</li>
                                                                    </ul>
                            </article>
                            </a>
                        </div>
                        <div class="card card--image-text animate-on-scroll__item animate-on-scroll__item--active">
                            <a href="https://apps.apmterminals.com.ar/wbt/" aria-label="card link" rel="noopener noreferrer" class="card__link ds-tilted" target="_blank"> 

                            <div class="card__image card__image--inline">
                                <figure>
                                    <span class="card__icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 84 84"><g><path d="M60.7,22.67H23.3A3.37,3.37,0,0,0,20,26.11V48.74a3.36,3.36,0,0,0,3.27,3.43h3.42v8.16a1,1,0,0,0,.57.9.92.92,0,0,0,.42.1,1,1,0,0,0,.64-.24l10.54-8.92H60.7A3.36,3.36,0,0,0,64,48.74V26.11A3.37,3.37,0,0,0,60.7,22.67ZM62,48.74a1.38,1.38,0,0,1-1.28,1.44H38.52a1,1,0,0,0-.64.24l-9.17,7.77v-6h1.5a1,1,0,0,0,0-2H23.3A1.37,1.37,0,0,1,22,48.74V26.11a1.37,1.37,0,0,1,1.28-1.45H60.7A1.38,1.38,0,0,1,62,26.11Z" fill="#ff6319"></path><path d="M59.06,26.67,42.85,40.5a1.47,1.47,0,0,1-1.8,0L24.73,26.58a1,1,0,0,0-1.29,1.51L33.65,36.8,23.38,47.22a1,1,0,0,0,.71,1.69,1,1,0,0,0,.71-.3L35.17,38.09,39.76,42a3.43,3.43,0,0,0,4.38,0l4.62-3.94L58.53,48A1,1,0,0,0,60,46.61l-9.67-9.83,10.08-8.6a1,1,0,1,0-1.3-1.51Z" fill="#ff6319"></path></g><g><circle cx="42" cy="42" r="40" fill="none" stroke="#ff6319" stroke-miterlimit="10" stroke-width="4px"></circle></g></svg>
                                    </span>
                                </figure>
                            </div>
                            <article>
                                <header>
                                    <h4 class="card__title">Solicitudes Online</h4>
                                </header>
                                <ul class="bullet-list">

                                        <li class="bullet-list-item">�Ten�s alg�n inconveniente? �Comunicate con nuestro Centro de Gesti�n de Clientes!</li>
                                                                                                        </ul>
                            </article>
                            </a>
                        </div>
                        <div class="card card--image-text animate-on-scroll__item animate-on-scroll__item--active">
                            <a href="https://www.apmterminals.com/es/buenos-aires/contact/contacts-and-directions" aria-label="card link" class="card__link ds-tilted"> 

                            <div class="card__image card__image--inline">
                                <figure>
                                    <span class="card__icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 84 84"><g><path d="M53.92,41.15a11.73,11.73,0,0,0-1.13-5.06l0-.07v0a11.93,11.93,0,1,0,1.17,5.15ZM42,31.22A9.93,9.93,0,0,1,50.47,36c-2,.62-4.42.25-5.57-1a1,1,0,0,0-1.12-.23,1,1,0,0,0-.6,1,6.19,6.19,0,0,0,.16,1A23.93,23.93,0,0,1,41,35.4a16.12,16.12,0,0,0-1.47-.88,1,1,0,0,0-.93,0,1,1,0,0,0-.5.79,5.58,5.58,0,0,1-.09.58c0,.18-.07.39-.1.6a7.37,7.37,0,0,1-3-2.29A9.92,9.92,0,0,1,42,31.22Zm-9.94,9.93a9.83,9.83,0,0,1,1.57-5.33,8.16,8.16,0,0,0,5.35,3,1,1,0,0,0,.89-1.58.37.37,0,0,1,0-.17l.06,0a17.12,17.12,0,0,0,4.75,2.36.78.78,0,0,0,.26,0,1,1,0,0,0,.67-.27,1,1,0,0,0,.3-.91c0-.16-.06-.3-.09-.44a8.71,8.71,0,0,0,5.53-.07,9.93,9.93,0,1,1-19.27,3.37Z" fill="#ff6319"></path><path d="M62.56,41.55a7.1,7.1,0,0,0-3.91-6.35,1,1,0,0,0-.06-.41,17.77,17.77,0,0,0-33.18,0,1,1,0,0,0-.06.41,7.12,7.12,0,0,0,3.21,13.47,1,1,0,0,0,1-1V35.41a1,1,0,0,0-1-1,6,6,0,0,0-.85.06,15.77,15.77,0,0,1,28.58,0,6.07,6.07,0,0,0-.85-.06,1,1,0,0,0-1,1V47.68a1,1,0,0,0,1,1l.44,0a15.79,15.79,0,0,1-9.74,7.73A2.68,2.68,0,0,0,44,55.25H40a2.68,2.68,0,0,0,0,5.36H44a2.68,2.68,0,0,0,2.64-2.3A17.77,17.77,0,0,0,58.38,48v0A7.11,7.11,0,0,0,62.56,41.55Zm-35,5a5.14,5.14,0,0,1,0-10.08ZM44,58.62H40a.69.69,0,0,1,0-1.38H44a.68.68,0,0,1,.62.4.5.5,0,0,0,0,.12,1.09,1.09,0,0,0,0,.17h0A.69.69,0,0,1,44,58.62Zm12.47-12V36.5a5.14,5.14,0,0,1,0,10.08Z" fill="#ff6319"></path><path d="M35.87,39.67a1.6,1.6,0,1,0,1.6,1.6A1.6,1.6,0,0,0,35.87,39.67Z" fill="#ff6319"></path><path d="M47.8,42.87a1.6,1.6,0,1,0-1.6-1.6A1.61,1.61,0,0,0,47.8,42.87Z" fill="#ff6319"></path><path d="M45.91,44.73a1,1,0,0,0-2,0,1.92,1.92,0,1,1-3.84,0,1,1,0,0,0-2,0,3.91,3.91,0,1,0,7.82,0Z" fill="#ff6319"></path></g><g><circle cx="42" cy="42" r="40" fill="none" stroke="#ff6319" stroke-miterlimit="10" stroke-width="4px"></circle></g></svg>
                                    </span>
                                </figure>
                            </div>
                            <article>
                                <header>
                                    <h4 class="card__title">Contacto</h4>
                                </header>
                                <ul class="bullet-list">

                                        <li class="bullet-list-item">Detalles de contacto &amp; direcciones.</li>
                                                                                                        </ul>
                            </article>
                            </a>
                        </div>
                        <div class="card card--image-text animate-on-scroll__item animate-on-scroll__item--active">
                            <a href="https://www.apmterminals.com/es/buenos-aires/practical-information/tariffs" aria-label="card link" class="card__link ds-tilted"> 

                            <div class="card__image card__image--inline">
                                <figure>
                                    <span class="card__icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 84 84"><g><path d="M64.55,43.83s0,0,0,0a1.66,1.66,0,0,0,0-.22.14.14,0,0,1,0-.06,2,2,0,0,0-.1-.21h0a1.46,1.46,0,0,0-.16-.19.1.1,0,0,1,0,0L64,43h0l-10.27-4.6a1,1,0,0,0-.81,1.82l2.93,1.31L34.55,51.91,31.9,50.72l10.54-5.18a1,1,0,0,0-.88-1.79l-12,5.91-2.32-1,9.24-4.54a1,1,0,0,0-.88-1.79L24.86,47.56l-2.07-.92,9-4.41a1,1,0,0,0,.45-1.33,1,1,0,0,0-1.33-.45L20,45.79l0,0a.66.66,0,0,0-.17.12l0,0a1,1,0,0,0-.16.19l0,0a.64.64,0,0,0-.08.19.14.14,0,0,0,0,.06,1,1,0,0,0,0,.24v6.63a1,1,0,0,0,.59.91l3,1.36a1,1,0,0,0,.52.07.82.82,0,0,0,.4-.1l2.93-1.44.25.11v2.55a1,1,0,0,0,.58.9l3.1,1.39a.92.92,0,0,0,.46.08h0a1,1,0,0,0,.41-.09l2.94-1.45,1.08.48v2.54a1,1,0,0,0,.59.91L39.58,63a1,1,0,0,0,.84,0l2.82-1.39a1,1,0,0,0,.56-.89V58.17l3.46-1.7,2.8,1.25.15.05a1,1,0,0,0,.81,0l3.1-1.52a1,1,0,0,0,.56-.9V52.78l2.71-1.34,2.89,1.29a.89.89,0,0,0,.31.08,1.05,1.05,0,0,0,.63-.07L64,51.38a1,1,0,0,0,.56-.9V43.85h0Zm-6.4-1.31,3.06,1.37L40,54.33,36.91,53ZM24.46,53.1V53l.1.05Zm7.78,3.49V56.5l.09,0Zm4.93-.06-5.52-2.47a1,1,0,0,0-1.4.91v1.65l-1.11-.5V53.58a1,1,0,0,0-.59-.91l-4.69-2.1a1,1,0,0,0-.94.07,1,1,0,0,0-.46.84v1.65l-1-.46V48.21L39,56.07v4.46L37.76,60V57.43A1,1,0,0,0,37.17,56.53Zm12.42-1.2h0Zm10.2-5-.08,0,.08,0Zm2-.08V48.66a1,1,0,0,0-.47-.85,1,1,0,0,0-1,0l-7.1,3.5a1,1,0,0,0-.55.89v2.55l-1.11.55V53.71a1,1,0,0,0-.47-.84,1,1,0,0,0-1,0l-7.77,3.84a1,1,0,0,0-.56.9v2.5l-.83.41V56.05l21.58-10.6v4.41Z" fill="#ff6319"></path><path d="M42,42.07a9.87,9.87,0,1,0-9.87-9.87A9.88,9.88,0,0,0,42,42.07Zm0-17.75a7.88,7.88,0,1,1-7.88,7.88A7.88,7.88,0,0,1,42,24.32Z" fill="#ff6319"></path><path d="M41.11,38.1v.11a1,1,0,1,0,2,0v-.13a2.66,2.66,0,0,0,2.24-2.63V34.31a2.64,2.64,0,0,0-1.44-2.17.91.91,0,0,0-.17-.09l-.14,0-.08,0L41.05,31c-.33-.22-.4-.39-.4-.42V29.46a.68.68,0,0,1,.68-.68h1.34a.69.69,0,0,1,.69.68V30a1,1,0,1,0,2,0v-.54a2.67,2.67,0,0,0-2.24-2.63v-.26a1,1,0,0,0-2,0v.24a2.67,2.67,0,0,0-2.45,2.65V30.6a2.52,2.52,0,0,0,1.41,2.15.61.61,0,0,0,.18.09l.3.12h0l2.34.9c.34.23.43.41.43.44v1.14a.68.68,0,0,1-.68.69H41.33a.68.68,0,0,1-.68-.69v-.53a1,1,0,1,0-2,0v.53A2.66,2.66,0,0,0,41.11,38.1Z" fill="#ff6319"></path></g><g><circle cx="42" cy="42" r="40" fill="none" stroke="#ff6319" stroke-miterlimit="10" stroke-width="4px"></circle></g></svg>
                                    </span>
                                </figure>
                            </div>
                            <article>
                                <header>
                                    <h4 class="card__title">Tarifario</h4>
                                </header>
                                <ul class="bullet-list">

                                        <li class="bullet-list-item">Consult� nuestro tarifario p�blico actualizado.</li>
                                                                                                        </ul>
                            </article>
                            </a>                         
                        </div>
            </div>
        </div>
    </div>
<!-- end container -->