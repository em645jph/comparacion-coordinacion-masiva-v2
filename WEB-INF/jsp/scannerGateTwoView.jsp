<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script charset="UTF-8" src="<c:url value="/resources/assets/js/gateScanner.js" />"></script>


        <div class="" Style="heigth: 400px;">
            <div class="container-fluid" style="margin-bottom: 20px;">
                <div class="row" style="margin-top:15px;">
                    <div class="col-sm-12">
                        <h4 class="header-title m-t-0 m-b-0">Desbloqueo Escaner</h4>
                    </div>
                </div>

                <div id="tbInvoice_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                
                <div class="row" id="divTable">
                    <div class="col-sm-12">
                        <div class="card-box">
							
                            <div class="table-responsive">
                                <table class="table-custom table-hover-custom datatable" id="tbUnit" width="100%">
                                		<thead>
											<tr>
											    <th>Permiso</th>
											    <th>Contenedor </th>
											    <th>Categor�a</th>
											    <th>T Carga</th>
											    <th>Reserva</th>
											    <th>Ubicaci�n </th>
											    <th>Buque </th>
											    <th>Procedencia</th>
											    <th>Precintos</th>
											    <th>Shipper</th>
											    <th>Estado</th>
											    <th>Fecha de Salida</th>
											    <th class="noExl" style="padding: 8px;">Desbloquear</th>
											</tr>
										</thead>
										<tbody data-bind="foreach: supportUnitViewModel.unitTable">
											<tr data-toggle="tooltip" tabindex="0" data-bind="attr: { class: 'bg-danger text-white', id: id}" data-placement="top">
												<td><label  data-bind="text: reference_id"></label> </td>
												<td><label  data-bind="text: id"></label></td>
												<td><label  data-bind="text: category"></label> </td>
												<td><label  data-bind="text: freight_kind"></label></td>
												<td><label  data-bind="text: booking_nbr"></label></td>
												<td><label  data-bind="text: last_poisition"></label></td>
												<td><label  data-bind="text: obVesselInfo"></label></td>
												<td><label  data-bind="text: ibVesselInfo"></label></td>
												<td><label  data-bind="text: unitSeals"></label></td>
												<td><label  data-bind="text: shipper_name"></label></td>
												<td><label  data-bind="text: unit_status"></label></td>
												<td><label  data-bind="text: atd =! null ? atd : '' "></label></td>
												<td class="noExl">
												 	<button type="button" class="btn btn-outline-light-custom btn-sm-custom" data-bind="click: $parent.viewUnlockByScannerModal"><i class="ti-unlock"></i></button> 
												</td>												
											</tr>
										</tbody>                                
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
  		<%@include file="scannerUnLockModal.jsp" %>
            </div> <!-- end container -->