<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script charset="UTF-8" src="<c:url value="/resources/assets/js/earlyLock.js?version = 1.1" />"></script>
        <div class="wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="header-title m-t-0 m-b-20">Bloqueo Anticipado</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
						<div class="card-box">
								<div class="row">
									<div class="col-sm-4">
										<label for="txtSearchReferenceId"> Permiso de embarque:</label>
										<input type="text" name="txtSearchReferenceId" id="txtSearchReferenceId" class="form-control" maxlength="100" style="text-transform: uppercase;"/>
									</div>
									<div class="col-sm-4">
										<label for="txtSearchFromDate"> Reportado desde:</label>
										<input type="text" name="txtSearchFromDate" id="txtSearchFromDate" class="form-control" placeholder="YYYY-MM-DD" autocomplete="off"/>
									</div>
									<div class="col-sm-4">
										<label for="txtSearchToDate">Reportado hasta:</label>
										<input type="text" name="txtSearchToDate" id="txtSearchToDate" class="form-control" placeholder="YYYY-MM-DD" autocomplete="off"/>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<br />
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12" align="center">
										<button type="button" class="button button--primary" id="btnSearch">Buscar</button>&nbsp;&nbsp;
										<button type="button" class="button button--success" id="btnClean">Limpiar</button>
									</div>
								</div>
							</div>
                    </div>
                </div>
                <!-- end row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div id="actionButtons">
								<button type="button" class="btn btn-primary" id="btnAddEarlyLock" data-toggle="modal" data-target="#addEarlyLockModal">Agregar</button>
							</div>
							<div class="col-sm-12">
								<br />
							</div>
                            <div class="table-responsive">
                                <table class="table table-hover datatable" id="tbEarlyLock" width="100%">
                                	<thead>
	                                	<tr>
	                                	    <th>Reportado</th>
										    <th>Permiso de embarque</th>
										    <th>Notas</th>
										    <th>Fecha creaci�n</th>
										    <th>Creado por</th>
										    <th></th>
										</tr>
									</thead>
									<tbody data-bind="foreach: earlyLockViewModel.earlyLockList">
										<tr>
											<td><label data-bind="text: reported_str"></label></td>
											<td><label data-bind="text: reference_id"></label></td>
											<td><label data-bind="text: notes"></label></td>											
											<td><label data-bind="text: created_str"></label></td>
											<td><label data-bind="text: creator"></label></td>
											<td><button class="btn btn-icon btn-danger btn-sm" data-bind="click: $parent.deleteEarlyLock"> <i class="ti-close"></i> </button></td>
										</tr>
									</tbody>                               
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <%@include file="addEarlyLockModal.jsp" %>
            </div> <!-- end container -->