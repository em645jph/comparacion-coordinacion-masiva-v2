<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="viewApptUnitSealModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Precintos</h4>
				<button type="button" class="close" data-dismiss="modal">�</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="form-group">
						<label for="txtApptSealNbr1" class="col-sm-3 col-form-label">Precinto 1:</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="txtApptSealNbr1" readonly>
						</div>
				</div>
				<div class="form-group">
						<label for="txtApptSealNbr2" class="col-sm-3 col-form-label">Precinto 2:</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="txtApptSealNbr2" readonly>
						</div>
				</div>
				<div class="form-group">
						<label for="txtApptSealNbr3" class="col-sm-3 col-form-label">Precinto 3:</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="txtApptSealNbr3" readonly>
						</div>
				</div>
				<div class="form-group">
						<label for="txtApptSealNbr4" class="col-sm-3 col-form-label">Precinto 4:</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="txtApptSealNbr4" readonly>
						</div>
				</div>				
			</div>
            <!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-dark" id="btnCancelViewApptSeal" data-dismiss="modal">Volver</button>
			</div>
		</div>
	</div>
</div>	