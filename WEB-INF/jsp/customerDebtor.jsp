<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="wrapper">
	<div class="container-fluid">

		<c:if test = "${msg != null}">
         <script>
         var msg = "${msg}";
         showModalHtml("", msg, "info");</script>
        </c:if>
		
	</div>
<!-- end container -->