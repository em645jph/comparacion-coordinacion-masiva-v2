<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="addEditPrivilegeModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Privilegio</h4>
				<button type="button" class="close" data-dismiss="modal">�</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body">
				<form class="form-horizontal m-t-10" role="form" method="POST" enctype="multipart/form-data" id="addEditPrivilegeForm">
					<div class="form-group">
						<div class="col-sm-9">
							<input type="hidden" class="form-control" id="txtPrivilegeGkey" disabled>
						</div>
					</div>
					<div class="form-group">
						<label for="txtPrivilegeLabel" class="col-sm-3 col-form-label">Nombre (*)</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="txtPrivilegeLabel" maxlength="50">
						</div>
					</div>
					<div class="form-group">
						<label for="txtPrivilegeDesc" class="col-sm-3 col-form-label">Descripci�n</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="txtPrivilegeDesc" maxlength="50">
						</div>
					</div>
					<div class="form-group">
						<label for="cbMenu" class="col-sm-3 col-form-label">Men� (*):</label>
						<div class="col-sm-9">
						<select id="cbMenu" class="form-control" name="cbMenu"
							data-bind="options:privilegeViewModel.modalMenu, optionsText:'label', optionsValue:'gkey', optionsCaption: '--'">
						</select>
						</div>
					</div>
					<div class="form-group">
						<label for="cbAccessType" class="col-sm-6 col-form-label">Tipo Acceso (*):</label>
						<div class="col-sm-9">
						<select id="cbAccessType" class="form-control" name="cbAccessType"
							data-bind="options:privilegeViewModel.modalAccessType, optionsText:'description', optionsValue:'key', optionsCaption: '--'">
						</select>
						</div>
					</div>
					<div class="form-group" id="divPrivilegeId">
						<label for="txtPrivilegeId" class="col-sm-3 col-form-label">Id (*):</label>
						<div class="col-sm-9">
						<input type="text" class="form-control" id="txtPrivilegeId" maxlength="50">
						</select>
						</div>
					</div>
					
				</form>
			</div>
			<!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="btnSavePrivilege">Guardar</button>
				<button type="button" class="btn btn-dark" id="btnCancelSavePrivilege" data-dismiss="modal">Cancelar</button>
			</div>
		</div>
	</div>	
</div>