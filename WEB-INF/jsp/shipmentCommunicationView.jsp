<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script charset="UTF-8" src="<c:url value="/resources/assets/js/shipmentCommunication.js?version = 1.3" />"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css">
        <div class="" Style="heigth: 400px;">
            <div class="container-fluid" style="margin-bottom: 20px;">
                <div class="row" style="margin-top:15px;">
                    <div class="col-sm-12">
                        <h4 class="header-title m-t-0 m-b-0">Comunicaci�n de Embarque</h4>
                    </div>
                </div>
                <form id="formSearchUnits" method="post">
                <div class="row form-group" >
                    <div class="col-sm-12">
						<div class="card-box" style="margin-bottom: 0px !important; padding: 9px !important;">
								<div class="row">
									<div class="col-sm-2">
										<label for="searchFilter">Selecci�n de Filtro:</label>										
										<select width="40%" id="searchFilter" class="form-control form-control-sm" name="searchFilter"  style="height: 28px !important;"
											data-bind="options:supportUnitViewModel.searchFilters, optionsText:'description' , optionsValue:'key', optionsCaption: '--', value: selectedFilter" oninput="onChangeInputFilterForm(this.value);">
										</select>
									</div>
									
									<div class="col-sm-3" data-bind="if: selectedFilter() == 'vessel' ? true : false, style: { display: selectedFilter() == 'vessel' ? 'block' : 'none' }">
										<label for="searchVessel">Buque / Viaje:</label>										
										<select width="80%" id="searchVessel" class="form-control basic-single-customers" name="searchVessel" style="height: 28px !important;"
											data-bind="options:supportUnitViewModel.searchVessels, optionsText: function(searchVessels) { return ko.unwrap(searchVessels.vessel_name) + ' ' + ko.unwrap(searchVessels.ib_vyg) + '/' + ko.unwrap(searchVessels.ob_vyg); }, optionsValue:'id', optionsCaption: '--'" oninput="callfindUnit();">
										</select>								
									</div>
									<div class="col-sm-3" data-bind="if: selectedFilter()  === 'unit' ? true : false, style: { display: selectedFilter()  === 'unit' ? 'block' : 'none'}">
										<label for="searchContainer">Contenedor:</label>
										<input type="text" name="searchContainer" id="searchContainer" class="form-control" maxlength="100" style="text-transform: uppercase; height: 28px !important;" oninput="onChangeInputForm();" />
									</div>		
									<div class="col-sm-3" data-bind="style: { display: selectedFilter() == 'vessel' || selectedFilter() == 'unit'  ? 'block' : 'none' }">
										<label for="searchBoardingPermission">Permiso de Embarque:</label>										
										<input type="text" name="searchBoardingPermission" id="searchBoardingPermission" class="form-control" maxlength="100" style="text-transform: uppercase; height: 28px !important;" oninput="onChangeInputForm();" />
									</div>
									<div class="col-sm-2" data-bind="style: { display: selectedFilter() == 'vessel' || selectedFilter() == 'unit'  ? 'block' : 'none' }">
										<label for="searchFilter">Tipo de carga:</label>
										<select width="30%" id="searchFreightKind" class="form-control basic-single-customers" name="searchFreightKind" style="height: 28px !important;"
											data-bind="options:supportUnitViewModel.searchFilters, optionsText: 'description', optionsValue:'key', optionsCaption: 'FCL'" disabled>
										</select>
									</div>
									<div class="col-sm-2" data-bind="style: { display: selectedFilter() == 'vessel' || selectedFilter() == 'unit'  ? 'block' : 'none' }">
									    <label for="searchStatus">Estado:</label>
									    <br>
										<select id="searchStatus" class="form-control" name="searchStatus" multiple="multiple"  data-bind="options:supportUnitViewModel.searchScStatus, optionsText:'description' , optionsValue:'description', optionsCaption: '--'" oninput="onChangeInputForm();" onchange="onChangeInputForm();" style="height: 28px !important;"></select> 				
									</div>
								</div>										
								<div class="row " style="margin-top:7px;">
									<div class="col-sm-12" align="center" data-bind="style: { display: viewBottomSearh() &&  (selectedFilter() == 'vessel' || selectedFilter() == 'unit') ? 'block' : 'none' }">
										<button type="submit" class="button button--primary" id="btnSearch" style="padding: 0px 45px !important;">Buscar</button>&nbsp;&nbsp;
										<button type="button" class="button button--success" id="btnClean" style="padding: 0px 45px !important;">Limpiar</button>						
									</div>
								</div>
							</div>
                    </div>
                </div>
                </form>	
                <div id="tbInvoice_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                
                <div class="row" id="divTable">
                    <div class="col-sm-12">
                        <div class="card-box">
                         	<div class="row">
                         	 <!-- ko if: unitTable().length > 0 -->
	                         	<div class="col-sm-4" id="actionButtons" >
									${actionButtons}
								</div>
								  <!-- /ko -->
	                         	<div class="col-sm-8" style="text-align:right;" id="columnButtons">
									Ver/ocultar: &nbsp;									
									<a class="toggle-vis" style="color:#17a2b8" data-column="2">Categor�a</a>&nbsp;-&nbsp;  
									<a class="toggle-vis" style="color:#17a2b8" data-column="3">T Carga</a>&nbsp;-&nbsp;  
									<a class="toggle-vis" style="color:#17a2b8" data-column="4">Reserva</a>&nbsp;-&nbsp;  
									<a class="toggle-vis" style="color:#17a2b8" data-column="5">Ubicaci�n</a>&nbsp;-&nbsp;    
									<a class="toggle-vis" style="color:#17a2b8" data-column="6">Buque</a>&nbsp;-&nbsp;  
									<a class="toggle-vis" style="color:#17a2b8" data-column="7">Procedencia</a>&nbsp;-&nbsp;  
									<a class="toggle-vis" style="color:#17a2b8" data-column="8">Precintos</a>&nbsp;-&nbsp;  
									<a class="toggle-vis" style="color:#17a2b8" data-column="9">Shipper</a> 
								</div>
									                         	
                         	</div>
                         	
							</br>
							
                            <div class="table-responsive">
                                <table class="table-custom table-hover-custom datatable" id="tbUnit" width="100%">
                                		<thead>
											<tr>
											    <th>Permiso</th>
											    <th>Contenedor </th>
											    <th>Categor�a</th>
											    <th>T Carga</th>
											    <th>Reserva</th>
											    <th>Ubicaci�n </th>
											    <th>Buque </th>
											    <th>Procedencia</th>
											    <th>Precintos</th>
											    <th>Shipper</th>
											    <th>Estado</th>
											    <th>Fecha de Salida</th>
											    <th class="noExl">Bloqueos Activos</th>
											    <th class="noExl">Bloquear</th>
											    <th class="noExl">Aprobar</th>
											    <th class="noExl" style="padding: 8px;">Historial</th>
											</tr>
										</thead>
										<tbody data-bind="foreach: supportUnitViewModel.unitTable">
											<tr data-toggle="tooltip" tabindex="0" data-bind="attr: {title: isApproved? 'Este contenedor se encuentra en estado Aprobado' : isBlocked? 'Este contenedor se encuentra en estado Bloqueado' : 'Este contenedor se encuentra en estado Pendiente', class: isApproved? 'bg-success text-white' :  isBlocked? 'bg-danger text-white': isRolledAfterApprove? 'bg-warning text-white' : 'bg-white' }" data-placement="top">
												<td><label  data-bind="text: client_perm_ref_id"></label> </td>
												<td><label  data-bind="text: id"></label></td>
												<td><label  data-bind="text: category"></label> </td>
												<td><label  data-bind="text: freight_kind"></label></td>
												<td><label  data-bind="text: booking_nbr"></label></td>
												<td><label  data-bind="text: last_poisition"></label></td>
												<td><label  data-bind="text: obVesselInfo"></label></td>
												<td><label  data-bind="text: ibVesselInfo"></label></td>
												<td><label  data-bind="text: unitSeals"></label></td>
												<td><label  data-bind="text: shipper_name"></label></td>
												<td><label  data-bind="text: unit_status"></label></td>
												<td><label  data-bind="text: atd =! null ? atd : '' "></label></td>
												<td class="noExl">
												 	 <!-- ko {if: client_perm_ref_id == null || total_block_count == null || total_block_count == 0} -->
														<label>0</label>
													 <!-- /ko -->
												 	 <!-- ko {if: (client_perm_ref_id != null && total_block_count > 0)} -->
														<button  class="btn btn-icon btn-light btn-sm-custom"  type="button"  data-bind="text: active_block_count, click:  $parent.actiongetListEventsLock"></button>
													 <!-- /ko -->												 	 
												</td>
												<td class="noExl">
												 	 <!-- ko {if: client_perm_ref_id == null} -->
														<label></label>
													 <!-- /ko -->
												 	 <!-- ko {if: client_perm_ref_id != null && isBlocked && userCanUnblock} -->
														<button data-toggle="tooltip" data-bind="click: $parent.viewLockModal, enable: gkey ,id, client_perm_gkey, client_perm_ref_id, 
																attr: {title: 'Click aqui para Desbloquear', 
																class: 'btn btn-outline-light-custom btn-sm-custom'}"> <i class="ti-lock"></i> </button>
													 <!-- /ko -->
													 <!-- ko {if: client_perm_ref_id != null && isBlocked && !userCanUnblock} -->
														<label></label>
													 <!-- /ko -->
													 <!-- ko {if: client_perm_ref_id != null && !isBlocked && userCanBlock} -->
														<button data-toggle="tooltip" data-bind="click: $parent.viewLockModal, enable: gkey ,id, client_perm_gkey, client_perm_ref_id, 
																attr: {title: 'Click aqui para Bloquear', 
																class: isApproved || isRolledAfterApprove? 'btn btn-outline-light-custom btn-sm-custom': 'btn btn-outline-dark-custom btn-sm-custom'}"> <i class="ti-lock"></i> </button>
													 <!-- /ko -->
													 <!-- ko {if: client_perm_ref_id != null && !isBlocked && !userCanBlock} -->
														<label></label>
													 <!-- /ko -->
												</td>
												<td class="noExl">
													 <!-- ko {if: client_perm_ref_id == null || !isPending || !userCanApprove} -->
														<label></label>
													 <!-- /ko -->
													 <!-- ko {if: client_perm_ref_id != null && isPending && userCanApprove} -->
														<button data-bind="click: $parent.viewApproveModal, enable: gkey ,id, client_perm_gkey, client_perm_ref_id,
																			 attr: { class: isBlocked? 'btn btn-outline-light-custom btn-sm-custom' : isApproved || isRolledAfterApprove? 'btn btn-outline-light-custom btn-sm-custom': 'btn btn-outline-dark-custom btn-sm-custom'}"> <i class="ti-check"></i></button>
												 	 <!-- /ko --> 
												 	 
												</td>
												<td class="noExl" style="padding-top: 3px;padding-bottom: 3px;">
													<!-- ko {if: client_perm_ref_id != null} -->
													<button id="btnHistory" data-bind="click: $parent.viewHistoryModal, 
																		  attr: { class: isBlocked? 'btn btn-outline-light-custom btn-sm-custom' : isApproved || isRolledAfterApprove? 'btn btn-outline-light-custom btn-sm-custom': 'btn btn-outline-dark-custom btn-sm-custom'}"> <i class="ti-view-list"></i> </button>
													<!-- /ko -->
													<!-- ko {if: client_perm_ref_id == null} -->
														<label></label>
													 <!-- /ko --> 
												</td>
											</tr>
										</tbody>                                
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
     		
            </div>
            	<%@include file="shipmentCommunicationApproveModal.jsp" %>
                <%@include file="shipmentCommunicationLockModal.jsp" %>
                <%@include file="shipmentCommunicationListEventLockModal.jsp" %>
                <%@include file="shipmentCommunicationUnLockModal.jsp" %>
                <%@include file="shipmentCommunicationHistoryModal.jsp" %>
                <%@include file="shipmentCommunicationActionByPermModal.jsp" %>
             <!-- end container -->