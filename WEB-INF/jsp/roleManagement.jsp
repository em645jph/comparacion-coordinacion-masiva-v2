<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script charset="UTF-8" src="<c:url value="/resources/assets/js/roleManagement.js" />"></script>
        <div class="wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="header-title m-t-0 m-b-20"></h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
						<div class="card-box">
								<div class="row">
									<div class="col-sm-4">
										<label for="txtSearchRoleId">Id</label>
										<input type="text" name="txtSearchRoleId" id="txtSearchRoleId" class="form-control" maxlength="50"/>
									</div>
									<div class="col-sm-4">
										<label for="txtSearchRoleDesc">Descripción</label>
										<input type="text" name="txtSearchRoleDesc" id="txtSearchRoleDesc" class="form-control" maxlength="50"/>
									</div>
									<div class="col-sm-4">
										<label for="cbSearchPrivilege">Privilegio</label>										
										<select id="cbSearchPrivilege" class="form-control" name="cbSearchPrivilege"  
										        data-bind="options:roleViewModel.privilege, optionsText:'label', optionsValue:'gkey', optionsCaption: '--'">
										</select>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<br />
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<label for="txtSearchFromDate">Fecha Inicio</label>
										<input type="text" name="txtSearchFromDate" id="txtSearchFromDate" class="form-control" placeholder="YY-MM-DD"/>
									</div>
									<div class="col-sm-4">
										<label for="txtSearchToDate">Fecha Fin</label>
										<input type="text" name="txtSearchToDate" id="txtSearchToDate" class="form-control" placeholder="YY-MM-DD"/>
									</div>
									<div class="col-sm-4">
										<label for="cbSearchRoleStatus"> Estado</label>										
										<select width="40%" id="cbSearchRoleStatus" class="form-control" name="cbSearchRoleStatus">
											<option value="" >--</option>
											<option value="ACT">Activo</option>
											<option value="OBS">Obsoleto</option>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<br />
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12" align="center">
										<button type="button" class="button button--primary" id="btnSearch">Buscar</button>&nbsp;&nbsp;
										<button type="button" class="button button--success" id="btnClean">Limpiar</button>
									</div>
								</div>
							</div>
                    </div>
                </div>
                <!-- end row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div id="actionButtons">
								${actionButtons}
							</div>
							<div class="col-sm-12">
								<br />
							</div>
                            <div class="table-responsive">
                                <table class="table table-hover datatable" id="tbCreatedRole" width="100%">
                                	${dataTable}                                
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <%@include file="addEditRoleModal.jsp" %>
            </div> <!-- end container -->