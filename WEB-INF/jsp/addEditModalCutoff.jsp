<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<style>
<!--
.ui-datepicker {
	top: 278px !important;
	}
.iconInput {
	font-size: 20px;
	position: absolute;
	right: 25px;
	padding-top: 10px;
	}
.inputPadding {
	padding-right: 40px;
	}
-->
</style>
<div class="modal fade" id="modalEditCutoff">
	<div class="modal-dialog">
			<div class="modal-content">
			<!-- Modal Header -->
				<div class="modal-header">
					<h4 class="modal-title">Editar Cutoff Documental</h4>
					<button type="button" class="close" data-dismiss="modal">�</button>
				</div>
			<!-- Modal body -->
					<form class="form-horizontal m-t-10" role="form" enctype="multipart/form-data">					
						<div class="modal-body">
							<div class="form-group">							
								<label class="col-sm-6 col-form-label">Buque/Viaje</label>
									<div class="col-sm-9">
										<input readonly data-bind="textInput: vesselName" name="setDateCutoff" type="text" class="form-control mb-3" id="">
									</div>
							</div>				

							<div class="form-group">	
								<label class="col-sm-6 col-form-label">Seleccione una fecha (*)</label>				
									<div class="col-sm-9">
										<i id="iconCalendar" class="mdi mdi-calendar iconInput"></i>
										<input name="setDateCutoff" type="text" placeholder="MM/DD/AA" class="inputPadding form-control mb-3" id="dateCutoff" autocomplete="off">
								</div>
							</div>
							
							<div class="form-group">			
								<label class="col-sm-6 col-form-label">Ingrese un horario (*)</label>
									<div class="col-sm-9">
										<input name="setDateCutoff" type="time" required class="form-control mb-3" id="timeCutoff">
									</div>	
							</div>
						</div>													
					</form>		
			<!-- Modal footer -->
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="btnConfirm">Guardar</button>
					<button type="button" class="btn btn-dark" id="btnCloseModal" data-dismiss="modal">Cancelar</button>
				</div>																								
			</div>
	</div>
</div>	