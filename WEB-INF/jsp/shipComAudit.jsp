<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script charset="UTF-8" src="<c:url value="/resources/assets/js/shipComAudit.js?version = 1.1" />"></script>
        <div class="wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="header-title m-t-0 m-b-20">Auditor�a Comunicaci�n de Embarque</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
						<div class="card-box">
								<div class="row">
									<div class="col-sm-3">
										<label for="txtSearchReferenceId"> Permiso de embarque:</label>
										<input type="text" name="txtSearchReferenceId" id="txtSearchReferenceId" class="form-control sc-audit" maxlength="20" placeholder="PERMISO1234567" style="text-transform: uppercase;"/>
									</div>
									<div class="col-sm-3">
										<label for="txtSearchUnitId"> Contenedor:</label>
										<input type="text" name="txtSearchUnitId" id="txtSearchUnitId" class="form-control sc-audit" maxlength="100" placeholder="CONTENEDOR1, CONTENEDOR2" style="text-transform: uppercase;"/>
									</div>
									<div class="col-sm-3">
										<label for="txtSearchVesselName">Buque:</label>
										<input type="text" name="txtSearchVesselName" id="txtSearchVesselName" class="form-control sc-audit" maxlength="100" placeholder="NOMBRE DE BUQUE" style="text-transform: uppercase;"/>
									</div>
									<div class="col-sm-3">
										<label for="txtSearchVoyage">Viaje:</label>
										<input type="text" name="txtSearchVoyage" id="txtSearchVoyage" class="form-control sc-audit" maxlength="10" placeholder="N�MERO DE VIAJE DE SALIDA" style="text-transform: uppercase;"/>
									</div>
									
								</div>
								<div class="row">
									<div class="col-sm-12">
										<br />
									</div>
								</div>
								<div class="row">
									<div class="col-sm-3">
										<label for="cbSearchEventType">Acci�n:</label>
										<select id="cbSearchEventType" class="form-control" name="cbSearchEventType" data-bind="options:shipComAuditViewModel.eventTypeList, optionsText: 'description', optionsValue:'key', optionsCaption: '--'">
										</select>
									</div>									
									<div class="col-sm-3">
										<label for="txtSearchUser">Usuario:</label>
										<input type="text" name="txtSearchUser" id="txtSearchUser" class="form-control sc-audit" maxlength="15" placeholder="CUIT O ID DE USUARIO" style="text-transform: uppercase;"/>
									</div>
									<div class="col-sm-3">
										<label for="txtSearchFromDate"> Desde:</label>
										<input type="text" name="txtSearchFromDate" id="txtSearchFromDate" class="form-control sc-audit" maxlength="10" placeholder="YYYY-MM-DD" autocomplete="off"/>
									</div>
									<div class="col-sm-3">
										<label for="txtSearchToDate">Hasta:</label>
										<input type="text" name="txtSearchToDate" id="txtSearchToDate" class="form-control sc-audit" maxlength="10" placeholder="YYYY-MM-DD" autocomplete="off"/>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<br />
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12" align="center">
										<button type="button" class="button button--primary" id="btnSearch">Buscar</button>&nbsp;&nbsp;
										<button type="button" class="button button--success" id="btnClean">Limpiar</button>
									</div>
								</div>
							</div>
                    </div>
                </div>
                <!-- end row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="table-responsive">
                                <table class="table table-hover datatable" id="tbShipComAudit" width="100%">
                                	<thead>
	                                	<tr>
	                                	    <th>Fecha</th>
										    <th>Usuario</th>
										    <th>Acci�n</th>
										    <th>Permiso</th>
										    <th>Contenedor</th>
										    <th>Buque</th>
										    <th>Notas</th>
										</tr>
									</thead>
									<tbody data-bind="foreach: shipComAuditViewModel.auditList">
										<tr>
											<td><label data-bind="text: placedTimeStr"></label></td>
											<td><label data-bind="text: placedBy"></label></td>
											<td><label data-bind="text: eventTypeLabel"></label></td>											
											<td><label data-bind="text: keywordValue1"></label></td>						
											<td><label data-bind="text: appliedToNaturalKey"></label></td>
											<td><label data-bind="text: keywordValue2"></label></td>
											<td><label data-bind="text: note"></label></td>
										</tr>
									</tbody>                               
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- end container -->