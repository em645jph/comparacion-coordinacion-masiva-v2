<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="<c:url value="/resources/assets/js/appointmentImport.js" />" charset="UTF-8"></script>	
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/css/icons.css" />" />

<style>
#tbUnitsByBl,#tbUnitsByBl2,#btnModalServices {
	display: none;
}
.mbsc-mobiscroll .mbsc-cal-txt {
    text-align: center;
    font-size: 15px;
}
</style>
<div class="wrapper">
	<div class="container-fluid">

		<div class="row">
			<div class="col-sm-12">
				<h2>Coordinaci�n Importaci�n</h2>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6 offset-md-3">
				<div class="card-box">
					<h6 class="text-muted font-13 m-t-0 text-uppercase">Ingres� n�mero de contenedor</h6>
					<span class="input-group-prepend"> 
						<input type="text" class="form-control" id="entityid" name="entityid" required>
					</span>
					<br>
					<button type="button" id="btnFindEntity" name="btnFindEntity" class="button button--primary">Buscar</button>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<table class="table table-striped datatable" id="tbUnitsByBl">
                    <thead>
                    <tr>
                        <th>Coordinacion</th>
                        <th>Datos Transportista</th>
                        <th>N�mero</th>
                        <th>Bill Of Lading</th>
						<th>Tama�o</th>
						<th>Ubicacion</th>
						<th>Buque</th>
						<th>Viaje</th>
                        <th>Forzoso</th>
                        <th>Precinto</th>
                    </tr>
                    </thead>
                    <tbody data-bind="foreach: viewModel.Units">
                    <tr>
                    	<td>
	                    	<!-- ko {if: apptState == null} -->
	                   			<button type="button" onclick="javascript:unitCoordination(this.id)" 
	                 					class="button button--success" data-bind="attr: { 'id': $index, 'unitid': unitId }">
	                 						Coordinar <i class="mdi mdi-calendar-check-outline"></i> 
	                 			</button> 
	                   		<!-- /ko -->
	                   		<!-- ko {if: apptState == 'CREATED'} -->
                   				<button data-bind="attr: {title: apptNbr }" type="button" onclick="javascript:deleteCoordination(this.apptNbr)" 
                   					class="button button--primary" data-bind="attr: { 'apptNbr': apptNbr }">
                   						Cancelar <i	class="ti-close"></i> 
                   				</button> 
	                   		<!-- /ko -->
	                   		<!-- ko {if: apptState == 'USED'} -->
                   				<label data-bind="text: apptState "></label> 
	                   		<!-- /ko -->
                   		</td>
                   		<td>
                   			<!-- ko {if: location == 'S20_INBOUND'} -->
                   				<button data-bind="attr: {'id': $index}" type="button" onclick="javascript:manageTruckData(this.id)" 
                   					class="button button--success" data-bind="attr: { 'apptNbr': apptNbr }">
                   						Transportista <i class="ti-pencil-alt"></i> 
                   				</button> 
	                   		<!-- /ko -->
                   		</td>
                    	<td data-bind="text: unitId"></td>
                    	<td data-bind="text: blNbr"></td>
				  		<td data-bind="text: size"></td>
				  		<td data-bind="text: location"></td>
				    	<td data-bind="text: vessel.vesselName"></td>
						<td data-bind="text: vessel.vesselId"></td>
                    	<td data-bind="text: vesselCutOff"></td>                                       
                        <td><a data-bind="attr: {'id': $index, 'ugkey': unitGkey, 'unitid': unitId}" onclick="javascript: showSealsByUnit(this.id)" style="color: red;">VER</a></td>
                    </tr>
                    </tbody>
                </table>
			
			</div>
		</div>
		<br>
		<hr>
		<div class="row">
		    <div class="col-md-2">
					<button class="button button--success" id="btnModalServices">Coordinar Servicios</button>
		    </div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				<table class="table table-striped datatable" id="tbUnitsByBl2">
                    <thead>
	                    <tr>
	                        <th>Cancelar</th>
	                        <th>Contenedor</th>
	                        <th>Fecha</th>
	                        <th>Tipo</th>
	                        <th>Subtipo</th>
	                    </tr>
                    </thead>
                    <tbody data-bind="foreach: viewModel.Services">
	                    <tr>
	                    	<td><i data-bind="attr: {'id':$index}" onclick="javascript:deletePreadviseUnit(1)" class="ti-close" style="background-color: red; color: white;"></i></td>
	                    	<td data-bind="text: unitId"></td>      
	                    	<td data-bind="text: datetime"></td>                                       
	                        <td data-bind="text: type"></td>
	                        <td data-bind="text: subType"></td>
	                    </tr>
                    </tbody>
                </table>
			
			</div>
		</div>
		
	</div>
<!-- end container -->

<!-- Modal -->
<div class="modal fade" id="modalAppt" tabindex="-1" role="dialog" 
	aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Solicitud de turno</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">�</span>
        </button>
      </div>
      <div class="modal-body">
      	<div id="divForUnit"></div>
        <!-- <input type="text" id="datetimepicker3" onchange="refreshLabelDateTimeAppt()"/> -->
        <div mbsc-form>
	        <div class="mbsc-grid">
	            <div class="mbsc-row">
	                <div class="mbsc-col-md-12 mbsc-col-md-4">
	                    <div class="mbsc-form-group">
	                        <div id="demo-labels"></div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="button button--primary" data-dismiss="modal">Cancelar</button>
        <button type="button" class="button button--success" id="btnCoordinate">Coordinar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalSeals" tabindex="-1" role="dialog" 
	aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="titlePermissionModal"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">�</span>
        </button>
      </div>
      <div class="modal-body">
      	<div class="row">
	  		<div class="col-md-12">
	  			<table class="table table-hover">
	  				<thead class="thead-dark">
				  		<tr>
						    <th>Precintos</th>
				  		</tr>
				  	</thead>
				  	<tbody>
						<tr>
							<td>19001EC0112345F</td>
						</tr>				  	
				  	</tbody>
				</table>
	      	</div>
		</div>
      </div>
    </div>
  </div>
</div>