<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script charset="UTF-8" src="<c:url value="/resources/assets/js/bookingTracking.js" />"></script>
        <div class="wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="header-title m-t-0 m-b-20"></h4>
                    </div>
		        </div> <!-- end row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">
							<div class="row">
							    <div class="col-sm-4">
									<label for="txtSearchBookingNbr">Booking</label> 
									<input type="text" name="txtSearchBookingNbr" id="txtSearchBookingNbr" class="form-control" maxlength="100" style="text-transform: uppercase;"/>
								</div>
								<div class="col-sm-4">
									<label for="txtSearchContainerNbr">Contenedor</label> 
									<input type="text" name="txtSearchContainerNbr" id="txtSearchContainerNbr" class="form-control" maxlength="15" style="text-transform: uppercase;"/>
								</div>
								<div class="col-sm-4">
									<label for="cbSearchLineOp">L�nea Operadora</label> 
									<select id="cbSearchLineOp" class="form-control" name="cbSearchLineOp"
										data-bind="options:bookingTrackViewModel.lineOpCb, optionsText:'description', optionsValue:'id', optionsCaption: '--'">
									</select>
								</div>								
							</div>
							<div class="row">
								<div class="col-sm-12">
									<br />
								</div>
							</div>
							<div class="row">
							    <div class="col-sm-4">
									<label for="txtSearchVesselName">Nave</label> <input type="text" name="txtSearchVesselName" id="txtSearchVesselName"
										class="form-control" placeholder="Nombre completo o parcial de la nave" style="text-transform: uppercase;"/>
								</div>
							    <div class="col-sm-4">
									<label for="txtSearchVoyage">Viaje</label> <input
										type="text" name="txtSearchVoyage" id="txtSearchVoyage"
										class="form-control" placeholder="N�mero de viaje de salida" style="text-transform: uppercase;"/>
								</div>
								 <div class="col-sm-4">
									<label for="txtSearchPod">Puerto de Descarga</label> <input type="text" name="txtSearchPod" id="txtSearchPod"
										class="form-control" placeholder="ARBUE" style="text-transform: uppercase;"/>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<br />
								</div>
							</div>
							<div class="row">
							   <div class="col-sm-4">
									<label for="cbSearchReefer">Reefer</label> 
									<select id="cbSearchReefer" class="form-control" name="cbSearchReefer"
										data-bind="options:bookingTrackViewModel.yesNoCb, optionsText:'description', optionsValue:'key', optionsCaption: '--'">
									</select>
								</div>
								<div class="col-sm-4">
									<label for="cbSearchHazardous">Hazardous</label> 
									<select id="cbSearchHazardous" class="form-control" name="cbSearchHazardous"
										data-bind="options:bookingTrackViewModel.yesNoCb, optionsText:'description', optionsValue:'key', optionsCaption: '--'">
									</select>
								</div>
								<div class="col-sm-4">
									<label for="cbSearchOverrideCutoff">Override Cutoff</label> 
									<select id="cbSearchOverrideCutoff" class="form-control" name="cbSearchOverrideCutoff"
										data-bind="options:bookingTrackViewModel.yesNoCb, optionsText:'description', optionsValue:'key', optionsCaption: '--'">
									</select>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<br />
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12" align="center">
									<button type="button" class="button button--primary"
										id="btnSearchBooking">Buscar</button>
									&nbsp;&nbsp;
									<button type="button" class="button button--success"
										id="btnCleanSearchBooking">Limpiar</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">
							<div class="table-responsive">
								<table class="table table-hover datatable" id="tbBookingTracking" width="100%">
								    ${bookingTrackDataTable}
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- end row -->
				<%@include file="viewBookingItemModal.jsp" %>
				<%@include file="viewBookingUnitModal.jsp" %>
				<%@include file="viewBookingHazardModal.jsp" %>
            </div> <!-- end container -->