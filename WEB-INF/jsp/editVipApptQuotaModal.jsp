<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="editVipApptQuotaModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title">Editar Reservas - Turno</h4>
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			<!-- Modal body -->
			
			<div class="modal-body">
					<div class="form-group">
						<div class="col-sm-12">
							<div class="table-responsive" >
                                <table class="table table-hover datatable" id="tbApptQuota" width="100%">
	                                <thead>
										<tr>
										    <th>Id</th>
										    <th>Descripción</th>
										    <th>Hora Inicio</th>
										    <th>Hora Fin</th>
										    <th>N4 DLV FULL</th>
										    <th>N4 DLV MTY</th>
										    <th>N4 DLV CARGO</th>
										    <th>N4 RCV FULL</th>
										    <th>N4 RCV MTY</th>
										    <th>N4 RCV CARGO</th>
										    <th>PD DLV FULL</th>
										    <th>PD DLV MTY</th>
										    <th>PD DLV CARGO</th>
										    <th>PD RCV FULL</th>
										    <th>PD RCV MTY</th>
										    <th>PD RCV CARGO</th>
										</tr>
									</thead>
									<tbody data-bind="foreach: vipViewModel.apptQuotaList">
										<tr>
										    <td><label data-bind="text: id"></label></td>
										    <td><label data-bind="text: description"></label></td>
										    <td><label data-bind="text: start_time_str"></label></td>
										    <td><label data-bind="text: end_time_str"></label></td>
										    <td><label data-bind="text: dlv_full_quota"></label></td>
										    <td><label data-bind="text: dlv_mty_quota"></label></td>
										    <td><label data-bind="text: dlv_cargo_quota"></label></td>
										    <td><label data-bind="text: rcv_full_quota"></label></td>
										    <td><label data-bind="text: rcv_mty_quota"></label></td>
										    <td><label data-bind="text: rcv_cargo_quota"></label></td>
										    <td><input type="number" min="0" style="width: 3em;" class="inputable" size="2" data-bind="attr: {gkey: gkey, vipApptGkey: vip_appt_gkey, pdGkey: pd_quota_gkey, columnName:'dlv_full_quota'}, value: pd_dlv_full_quota, enable:dlv_full_quota > 0"/></td>
										    <td><input type="number" min="0" style="width: 3em;" class="inputable" size="2" data-bind="attr: {gkey: gkey, vipApptGkey: vip_appt_gkey, pdGkey: pd_quota_gkey, columnName:'dlv_mty_quota'}, value: pd_dlv_mty_quota, enable:dlv_mty_quota > 0"/></td>
										    <td><input type="number" min="0" style="width: 3em;" class="inputable" size="2" data-bind="attr: {gkey: gkey, vipApptGkey: vip_appt_gkey, pdGkey: pd_quota_gkey, columnName:'dlv_cargo_quota'}, value: pd_dlv_cargo_quota, enable:dlv_cargo_quota > 0"/></td>
										    <td><input type="number" min="0" style="width: 3em;" class="inputable" size="2" data-bind="attr: {gkey: gkey, vipApptGkey: vip_appt_gkey, pdGkey: pd_quota_gkey, columnName:'rcv_full_quota'}, value: pd_rcv_full_quota, enable:rcv_full_quota > 0"/></td>
										    <td><input type="number" min="0" style="width: 3em;" class="inputable" size="2" data-bind="attr: {gkey: gkey, vipApptGkey: vip_appt_gkey, pdGkey: pd_quota_gkey, columnName:'rcv_mty_quota'}, value: pd_rcv_mty_quota, enable:rcv_mty_quota > 0"/></td>
										    <td><input type="number" min="0" style="width: 3em;" class="inputable" size="2" data-bind="attr: {gkey: gkey, vipApptGkey: vip_appt_gkey, pdGkey: pd_quota_gkey, columnName:'rcv_cargo_quota'}, value: pd_rcv_cargo_quota, enable:rcv_cargo_quota > 0"/></td>
										</tr>
									</tbody>		                                
                              </table>
                            </div>							
						</div>
					</div>
            </div>
            <!-- Modal footer -->
			<div class="modal-footer">
				${quotaModalButtons}
				<button type="button" class="btn btn-dark" id="btnCancelSaveVipApptQuota" data-dismiss="modal">Cancelar</button>
			</div>	
		
		</div>
	</div>
</div>	