<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="selectCoordinationTimeCusModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Seleccionar hora</h4>
				<button type="button" class="close" data-dismiss="modal">�</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<label for="txtSelectedDate"> Fecha:</label> 
						<input type="text" name="txtSelectedDate" id="txtSelectedDate" class="form-control" disabled />
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<label for="cbDateOpening">Horarios disponibles:</label> 
						<select
							id="cbDateOpening" class="form-control" name="cbDateOpening"
							data-bind="options:supportUnitViewModel.dateOpeningCb, optionsText:'description' , optionsValue:'id', optionsCaption: '--'">
						</select>
					</div>
				</div>
				<div class="row">	
					<div class="col-sm-12"  id="appointmentFieldId1">
						<label for="cbChannel">Tipo de operativa:</label> 
						<select id="cbChannel" class="form-control" name="cbChannel"
							data-bind="options:supportUnitViewModel.channelCb, optionsText:'description' , optionsValue:'key', optionsCaption: '--'">
						</select>
					</div>
				</div>
				<div class="row">	
					<div class="col-sm-12"  id="serviceFieldId1">
						<label for="cbBunch">Ramo:</label> 
						<select id="cbBunch" class="form-control" name="cbBunch"
							data-bind="options:supportUnitViewModel.bunchCb, optionsText:'description' , optionsValue:'key', optionsCaption: '--'">
						</select>
					</div>
				</div>
				<div class="row">	
					<div class="col-sm-12"  id="serviceFieldId2">
						<label for="txtEmail">Email:</label>
						<input type="text" class="form-control" id="txtEmail" maxlength="50">
					</div>					
				</div>
			</div>
            <!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="btnSaveCoordination">Guardar</button>
				<button type="button" class="btn btn-dark" id="btnCancelSaveCoordination" data-dismiss="modal">Cancelar</button>
			</div>
		</div>
	</div>
</div>	