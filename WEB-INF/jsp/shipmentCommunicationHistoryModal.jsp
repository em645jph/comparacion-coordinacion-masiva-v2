<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<!-- Modal -->
<div class="modal fade" id="shipmentCommunicationHistoryModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle" data-bind="text: historyEventTitle"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      		<div class="row p-3 text-center">
		<div class="col-ms-12 col-md-12 text-center" data-bind="if: historyTable().length == 0 ">
						<div>
		  					 <span> No se encontraron resultados </span>
						</div>
					</div>
		</div>
        <div class="row">
				<div class="col-sm-12" data-bind="if: historyTable().length != 0 ">
					<div class="card-box">
						<div class="table-responsive">
							<table class="table datatable" width="100%" id="unitPermissionHistoryTable">
								<thead>
								 	<tr>				
									   <th>Fecha</th>							
									   <th>Usuario</th>				
									   <th>Acci�n</th>				
									   <th>Buque</th>
									   <th>Notas</th>				
						  			</tr>
						 		<thead>
							  	<tbody data-bind="foreach: supportUnitViewModel.historyTable">				  		
							  		<tr class="trVisits">
							  			<td data-bind="text: placedTimeStr"></td>
							  			<td data-bind="text: placedBy"></td>		  				
							  			<td data-bind="text: eventTypeLabel"></td>		  				
							  			<td data-bind="text: keywordValue2"></td>
							  			<td data-bind="text: note"></td>			  				
							  		</tr>						 		
							</table>	
						</div>
					</div>	  	
				</div>
		</div>	
      </div> 
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>