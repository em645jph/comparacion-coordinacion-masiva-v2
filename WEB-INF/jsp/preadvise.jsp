
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script charset="UTF-8" src="<c:url value="/resources/assets/js/preadvise.js" />"></script>
<script charset="UTF-8" src="<c:url value="/resources/assets/js/modalsFunctions.js" />"></script>
        <div class="wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="header-title m-t-0 m-b-20">Preaviso Exportaci�n</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
						<div class="card-box">
								<h6 class="text-muted font-13 m-t-0 text-uppercase text-center">Ingres� n�mero de Booking:</h6>
								<div class="row">									
									<div class="col-sm-3">
									</div>
									<div class="col-sm-3">
										<label for="txtSearchBooking">Booking (*)</label>
										<input type="text" name="txtSearchBooking" id="txtSearchBooking" class="form-control" maxlength="50" style="text-transform: uppercase;"/>
									</div>
									<div class="col-sm-3">
										<label for="cbSearchLineOp">L�nea Operadora:</label>
										<select id="cbSearchLineOp" class="form-control" name="cbSearchLineOp"  
										        data-bind="options:preadviseViewModel.lineOpCb, optionsText:'description' , optionsValue:'id', optionsCaption: '--'">
										</select>
									</div>
									<div class="col-sm-3">
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<br />
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12" align="center">
										<button type="button" class="button button--primary" id="btnSearch">Buscar</button>&nbsp;&nbsp;
										<button type="button" class="button button--success" id="btnClean">Limpiar</button>
										<button type="button" class="button button--info" id="btnHelp"><i class="ti-help"></i></button>
									</div>
								</div>
						</div>
                    </div>
                </div>
                <!-- end row -->
                <div class="row" id="divBookingDetails" style="display: none;">
                    <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-6">
                                	<div class="panel card panel-fill">
                                         <div class="card-header" id="divDetailHeader">
                                             <a href="#" id="btnBookingDetails" class="btn btn-sm btn-dark float-right">Ocultar</a>
                                             <h5 class="font-14 m-1"><strong id="txtBookingDetails">Detalle</strong></h5>
                                         </div>
                                         <div class="card-body" style="font-size:0.85em" id="divDetailBody">
                                             <div class="row">
	                                             <div class="col-sm-3">
	                                                <strong>Buque</strong>
	                                                <br>
	                                                <p class="text-muted" data-bind="text: bkgVesselInfo"></p>
	                                             </div>
	                                             <div class="col-sm-3">
	                                                 <strong>C�digo</strong>
	                                                 <br>
	                                                 <p class="text-muted" data-bind="text: bkgVisitId"></p>
	                                             </div>
	                                             <div class="col-sm-3">
	                                                 <strong>Cutoff</strong>
	                                                 <br>
	                                                 <p class="text-muted" data-bind="text: bkgCutoff"></p>
	                                             </div>
	                                             <div class="col-sm-3">
	                                                 <strong>Shipper</strong>
	                                                 <br>
	                                                 <p class="text-muted" data-bind="text: bkgShipper"></p>
	                                             </div>
                                             </div>
                                             <div class="row">
                                             	<br/>
                                             </div>
                                             <div class="row">
                                                 <div class="col-sm-3">
	                                                <strong>Cantidad</strong>
	                                                <br>
	                                                <p class="text-muted" data-bind="text: bkgQty"></p>
	                                             </div>
	                                             <div class="col-sm-3">
	                                                <strong>POD</strong>
	                                                <br>
	                                                <p class="text-muted" data-bind="text: bkgPOD"></p>
	                                             </div>
	                                             <div class="col-sm-3">
	                                                 <strong>Reefer</strong>
	                                                 <br>
	                                                 <!-- ko {if: bkgIsReefer} -->
	                                                 	<i class="ti-plug" style="color: blue;"></i>
	                                                 <!-- /ko -->
	                                                 <!-- ko {ifnot: bkgIsReefer} -->
	                                                 	<p class="text-muted">--</p>
	                                                 <!-- /ko -->	                                                 
	                                             </div>
	                                             <div class="col-sm-3">
	                                                 <strong>IMO</strong>
	                                                 <br>
	                                                 <!-- ko {if: bkgIsHazardous} -->
	                                                 	<i class="ti-alert" style="color: red;"></i>
	                                                 <!-- /ko -->
	                                                 <!-- ko {ifnot: bkgIsHazardous} -->
	                                                 	<p class="text-muted">--</p>
	                                                 <!-- /ko -->
	                                             </div>
                                             </div>
                                          </div>
                                    </div>										
								</div>
								<div class="col-sm-6">
									<div class="panel card panel-fill">
										<div class="card-header" id="divItemHeader">
                                             <a href="#" id="btnBookingItems" class="btn btn-sm btn-dark float-right">Ocultar</a>
                                             <h5 class="font-14 m-1"><strong id="txtBookingItems">Equipamento</strong></h5>
                                         </div>
                                         <div class="card-body" id="divItemBody">
                                         	<table class="table datatable" id="tbBkgItems">
												  	<tr>
								                        <th>Tama�o</th>
								                        <th>Cantidad</th>
								                        <th>Recibidos</th>
								                        <th>Mercader�a</th>
								                        <th>Temperatura</th>
								                        <th>Ventilaci�n</th>
								                        <th>Humedad</th>
								                        <th>CO2</th>
								                        <th>O2</th>
								                    </tr>
								               	<tbody id="tbodyEqs" data-bind="foreach: preadviseViewModel.bkgItemList">
												  	<tr>
												    	<td data-bind="text: size"></td>
														<td data-bind="text: quantity"></td>
														<td data-bind="text: tallyIn"></td>
														<td data-bind="text: commodity"></td>
														<td data-bind="text: temperature"></td>
														<td data-bind="text: ventilation"></td>
														<td data-bind="text: humidity"></td>
														<td data-bind="text: co2"></td>
														<td data-bind="text: o2"></td>
												  	</tr>
												</tbody>
											</table>
                                         </div>
									</div>
								</div>
                            </div>
                    </div>
                </div>
                <div class="row" id="divActionButtons" style="display: none;">
		   			<div class="col-md-4">
						<button type="button" class="btn btn-success" id="btnAddPreadviseUnit">Agregar contenedor</button>
				    </div>
				    <br>
				</div>
                <div class="row" id="divUnits" style="display: none;">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="table-responsive">
                                <table class="table table-hover datatable" id="tbUnit" width="100%">
				                    <thead>
				                    <tr>
				                        <th>Desvincular</th>
				                        <th>Contenedor</th>
				                        <th>Tama�o</th>
				                        <th>Ubicaci�n</th>
				                        <th>L�nea</th>
				                        <th>Peso Carga</th>
				                        <th>Precinto Linea</th>
				                        <th>Fuera de Medida</th>
				                        <th>VGM</th>
				                        <th>Peso VGM</th>
				                        <th>Permisos</th>
				                    </tr>
				                    </thead>
				                    <tbody data-bind="foreach: preadviseViewModel.bkgUnitList">
				                    	<tr>
				                    		<!-- ko {if: location.location == 'S20_INBOUND'} -->
				                    			<td><i class="ti-close btn-icon btn-danger" data-bind="click: $parent.cancelPreadvise"></i> </td>
				                    		<!-- /ko -->
				                    		<!-- ko {ifnot: location.location == 'S20_INBOUND'} -->
				                    			<td></td>
				                    		<!-- /ko -->
				                    		
				                    		<!-- ko {if: location.location == 'S20_INBOUND'} -->
				                    		<td><a href="#" style="color:#fd7e14;" data-bind="text: unitId, click: $parent.renumberUnit"></a></td>
				                    		<!-- /ko -->
				                    		<!-- ko {ifnot: location.location == 'S20_INBOUND'} -->
				                    		<td data-bind="text: unitId" ></td>
				                    		<!-- /ko -->
				                    		
				                    		<td data-bind="text: size"></td>
				                    		<td data-bind="text: location.description"></td>
				                    		<td data-bind="text: lineOp"></td>
				                    		
				                    		<!-- ko {if: location.location == 'S20_INBOUND'} -->
				                    		<td><a href="#" style="color:#fd7e14;" data-bind="text: weight, click: $parent.updateUnitWeight"></a></td>
				                    		<!-- /ko -->
				                    		<!-- ko {ifnot: location.location == 'S20_INBOUND'} -->
				                    		<td data-bind="text: weight" ></td>
				                    		<!-- /ko -->
				                    		
				                    		<!-- ko {if: location.location == 'S20_INBOUND'} -->
				                    		<td><a href="#" style="color:#fd7e14;" data-bind="text: seal2, click: $parent.updateUnitSeal"></a></td>
				                    		<!-- /ko -->
				                    		<!-- ko {ifnot: location.location == 'S20_INBOUND'} -->
				                    		<td data-bind="text: seal2" ></td>
				                    		<!-- /ko -->
				                    		
				                    		<!-- ko {if: location.location == 'S20_INBOUND'} -->
				                    		<td><a href="#" style="color:#fd7e14;" data-bind="text: oog ? 'SI' : 'NO', click: $parent.updateUnitOog"></a></td>
				                    		<!-- /ko -->
				                    		<!-- ko {ifnot: location.location == 'S20_INBOUND'} -->
				                    		<td data-bind="text: oog ? 'SI' : 'NO'" ></td>
				                    		<!-- /ko -->
				                    		
				                    		<!-- ko {if: location.location == 'S20_INBOUND'} -->
				                    		<td><a href="#" style="color:#fd7e14;" data-bind="text: vgmConditionEnum.description, click: $parent.updateUnitVgmCondition"></a></td>
				                    		<!-- /ko -->
				                    		<!-- ko {ifnot: location.location == 'S20_INBOUND'} -->
				                    		<td data-bind="text: vgmConditionEnum.description" ></td>
				                    		<!-- /ko -->
				                    		
				                    		<td data-bind="text: vgmWeight == null || vgmWeight == 0? '--' : vgmWeight "></td>
				                    		
				                    		<td><a data-bind="text: customsPrmQty, click: $parent.viewUnitPermission" style="color: red;"></a></td>
				                    	</tr>
				                    </tbody>
				                </table>                               
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
                <%@include file="addUnitPreadviseModal.jsp" %>
                <%@include file="viewUnitRenumberModal.jsp" %>
                <%@include file="viewUpdateUnitSealModal.jsp" %>
                <%@include file="viewUpdateUnitWeightModal.jsp" %>
                <%@include file="viewUpdateUnitOogModal.jsp" %>
                <%@include file="viewUpdateUnitVgmModal.jsp" %>
                <%@include file="viewUpdateUnitCustomsPermModal.jsp" %>
                <%@include file="sealNbr2Modal.jsp" %>
            </div> <!-- end container -->