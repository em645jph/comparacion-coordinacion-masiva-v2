<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/css/icons.css" />" />
<script src="<c:url value="/resources/assets/js/parameters.js" />" charset="UTF-8"></script>
<script src="<c:url value="/resources/assets/js/modalsFunctions.js" />" charset="UTF-8"></script>

<div class="wrapper">
	<div class="container-fluid">

		<div class="row">
			<div class="col-sm-12">
				<h2>Configuraciones del Sistema</h2>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-6 offset-md-3">
				<div class="card-box">
					<div class="row">
						<div class="col-sm-12 text-center">
							<h2>Facturación</h2>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3"> 
							<h3 id="lblCostCenterText"></h3>
						</div>
						<div class="col-sm-6">
							${costCenterSwitchery}
						</div>
					</div>
					<br>
					<small>EL TEXTO INDICA EL VALOR ACTIVO.</small>
					<br>
					<small>RECUERDE QUE CADA VEZ QUE HACE CLIC EN EL BOTÓN, ESTARÁ CAMBIANDO EL CENTRO DE COSTO DE LA FACTURACIÓN.</small>
				</div>
			</div>
		</div>
	</div>
