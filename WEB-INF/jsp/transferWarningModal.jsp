<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="transferWarningModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Informaci�n Cliente Billing</h4>
				<button type="button" class="close" data-dismiss="modal">�</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<p align="justify">
					El d�a <strong>7 de septiembre</strong> eliminaremos <strong>Transferencias</strong> como medio de pago en Puerto Digital para que puedas abonar por todos nuestros medios de pago autom�tico.<br>
					Es por eso que te recomendamos que leas los instructivos para que puedas tener todo listo para esa fecha.<br><br>
					Hac� click <a style="color:#fd7e14;" href="https://apps.apmterminals.com.ar/solicitudesonline" target="_blank">ac�</a> para acceder a los instructivos   
				</p>
			</div>
            <!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-dark" id="btnCancelModal" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>	