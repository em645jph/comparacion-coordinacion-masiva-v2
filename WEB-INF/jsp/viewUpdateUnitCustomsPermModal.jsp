<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="viewUpdateUnitCustomsPermModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title" id="modalTitle" id="txtUpdatePermTitle">Actualizar Permisos de Embarque</h4>
				<button type="button" class="close" data-dismiss="modal">�</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<label for="txtUnitCustomsPermUpdate">Permiso de embarque:</label>
						<input type="text" name="txtUnitCustomsPermUpdate" id="txtUnitCustomsPermUpdate" class="form-control" style="text-transform: uppercase;" minlength="16" maxlength="16"/>
					</div>
				</div>
				<div class="row"><br></div>
				<div class="row">
					<div class="col-sm-12" align="center">
						<button type="button" class="btn btn-primary" id="btnAddUnitCustomsPerm">Agregar</button>
						<button type="button" class="btn btn-dark" id="btnCleanUnitCustomsPerm">Limpiar</button>
					</div>
				</div>
				<div class="row"><br></div>
				<div class="row">
					<div class="col-sm-12">
                        <div class="table-responsive">
                        	<table class="table datatable" id="tbUnitPermission">
								<tr>
									<th>Permiso</th>
									<th>Creado</th>
									<th></th>
								</tr>
								<tbody data-bind="foreach: preadviseViewModel.customsPermList">
									<tr>
										<td data-bind="text: reference_id"></td>
										<td data-bind="text: flag_created_str"></td>
										<!-- ko {if: transit_state == 'S20_INBOUND'} -->
				                    		<td><i class="ti-close btn-icon btn-danger" data-bind="click: $parent.cancelUnitPermission"></i> </td>
				                    	<!-- /ko -->
				                    	<!-- ko {ifnot: transit_state == 'S20_INBOUND'} -->
				                    		<td></td>
				                    	<!-- /ko -->
									</tr>
								</tbody>
							</table>
						</div>	
					</div>					
				</div>
			</div>
            <!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-dark" id="btnCancelUpdateUnitCustomsPerm" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>	