<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script charset="UTF-8" src="<c:url value="/resources/assets/js/supportInvoice.js" />"></script>
        <div class="wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="header-title m-t-0 m-b-20"></h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
						<div class="card-box">
								<div class="row">
									<div class="col-sm-2">
										<label for="txtSearchDraftNbr"># Draft:</label>
										<input type="text" name="txtSearchDraftNbr" id="txtSearchDraftNbr" class="form-control" maxlength="100" placeholder="1234,3456,7890" style="text-transform: uppercase;"/>
									</div>
									<div class="col-sm-2">
										<label for="txtSearchStatus">Estado:</label>
										<input type="text" name="txtSearchStatus" id="txtSearchStatus" class="form-control" maxlength="10" placeholder="DRAFT o FINAL" style="text-transform: uppercase;"/>
									</div>
									<div class="col-sm-2">
										<label for="txtSearchFinalNbr"># Factura:</label>
										<input type="text" name="txtSearchFinalNbr" id="txtSearchFinalNbr" class="form-control" maxlength="50" placeholder="1234,3456,7890" style="text-transform: uppercase;"/>
									</div>
									<div class="col-sm-2">
										<label for="txtSearchCostCenter">Centro Costo:</label>
										<input type="text" name="txtSearchCostCenter" id="txtSearchCostCenter" class="form-control" maxlength="50" placeholder="5,6,12,15,16" style="text-transform: uppercase;"/>
									</div>
									<div class="col-sm-2">
										<label for="txtSearchInvoiceType">Tipo Factura:</label>
										<input type="text" name="txtSearchInvoiceType" id="txtSearchInvoiceType" class="form-control" maxlength="50" placeholder="MANIFIESTO" style="text-transform: uppercase;"/>
									</div>
									<div class="col-sm-2">
										<label for="txtSearchCustomer">Cliente:</label>
										<input type="text" name="txtSearchCustomer" id="txtSearchCustomer" class="form-control" maxlength="50" placeholder="ABC,30707920985,DEF" style="text-transform: uppercase;"/>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<br />
									</div>
								</div>
								<div class="row">
									<div class="col-sm-2">
										<label for="txtSearchEntityId">Entidad:</label>
										<input type="text" name="txtSearchEntityId" id="txtSearchEntityId" class="form-control" maxlength="50" placeholder="CONTENEDOR,MANIFIESTO,TURNO" style="text-transform: uppercase;"/>
									</div>
									<div class="col-sm-2">
										<label for="txtSearchEventId">Evento:</label>
										<input type="text" name="txtSearchEventId" id="txtSearchEventId" class="form-control" maxlength="100" placeholder="TASAS,STORAGE,REEFER" style="text-transform: uppercase;"/>
									</div>
									<div class="col-sm-2">
										<label for="txtSearchTariffId">Tarifa:</label>
										<input type="text" name="txtSearchTariffId" id="txtSearchTariffId" class="form-control" maxlength="20" placeholder="TARIFA1,TARIFA2" style="text-transform: uppercase;"/>
									</div>
									<div class="col-sm-2">
										<label for="txtSearchCreator">Creado por:</label>
										<input type="text" name="txtSearchCreator" id="txtSearchCreator" class="form-control" maxlength="20" placeholder="CUIT USUARIO PUERTO DIGITAL" style="text-transform: uppercase;"/>
									</div>
									<div class="col-sm-2">
										<label for="txtSearchFromDate">Desde:</label>
										<input type="text" name="txtSearchFromDate" id="txtSearchFromDate" class="form-control" autocomplete="nope" />
									</div>
									<div class="col-sm-2">
										<label for="txtSearchToDate">Hasta:</label>
										<input type="text" name="txtSearchToDate" id="txtSearchToDate" class="form-control" autocomplete="nope" />
									</div>
								</div>			
								<div class="row">
									<div class="col-sm-12">
										<br />
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12" align="center">
										<button type="button" class="button button--primary" id="btnSearch">Buscar</button>&nbsp;&nbsp;
										<button type="button" class="button button--success" id="btnClean">Limpiar</button>
									</div>
								</div>
						</div>
                    </div>
                </div>
                <!-- end row -->
                <div class="row" id="divTable">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="table-responsive">
                                <table class="table table-hover datatable" id="tbInvoice" width="100%">
                                	<thead>
										<tr>
										    <th># Draft</th>
										    <th>Estado</th>
										    <th>Centro Costo</th>
										    <th># Afip</th>
										    <th># Contingencia</th>		
										    <th>Fecha Finalización</th>
										    <th>Tipo</th>
										    <th>Id Cliente</th>
										    <th>Total</th>
										    <th>Pagado</th>
										    <th>Deuda</th>
										    <th>Fecha Creación</th>
										    <th>Creado por</th>
										    <th>Finalizado por</th>
										    <th>Descargar</th>
										    <th>Eliminar</th>
										</tr>
									</thead>
									<tbody data-bind="foreach: supportInvoiceViewModel.invoiceTable">
										<tr>
										    <td><label data-bind="text: draft_nbr"></label></td>
										    <td><label data-bind="text: status"></label></td>
										    <td><label data-bind="text: cost_center"></label></td>
										    <td><label data-bind="text: afip_nbr"></label></td>
										    <td><label data-bind="text: custom_final_nbr"></label></td>
										    <td><label data-bind="text: finalized_str"></label></td>
										    <td><label data-bind="text: invoice_type"></label></td>
										    <td><a href="#" style="color:#fd7e14;" data-bind="text: customer_id, click: $parent.viewInvoicePayee"></a></td>
										    <td><label data-bind="text: total_charges"></label></td>
										    <!-- ko {if: (total_paid != null && total_paid != 0) || epay_count > 0} -->
										    <td><a href="#" style="color:#fd7e14;" data-bind="text: total_paid == null? 0 : total_paid, click: $parent.viewInvoicePayment"></a></td>
											<!-- /ko -->
											<!-- ko {if: (total_paid == null || total_paid == 0) && epay_count == 0} -->
											<td><label data-bind="text: total_paid"></label></td>												
											<!-- /ko -->
										    <td><label data-bind="text: owed"></label></td>
										    <td><label data-bind="text: created_str"></label></td>
										    <td><label data-bind="text: creator"></label></td>
										    <td><label data-bind="text: finalization_user"></label></td>
										    <td><button class="btn btn-icon btn-info btn-sm" data-bind="click: $parent.downloadInvoice"> <i class="ti-download"></i> </button></td>
										    <td><button class="btn btn-icon btn-danger btn-sm" data-bind="click: $parent.deleteInvoice, visible: status == 'DRAFT', enable:status == 'DRAFT'"> <i class="ti-close"></i> </button></td>

										</tr>
									</tbody>	                                
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <%@include file="viewInvoicePayeeModal.jsp" %>
                <%@include file="viewInvoicePaymentModal.jsp" %>
            </div> <!-- end container -->