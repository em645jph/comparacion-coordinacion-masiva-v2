<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script charset="UTF-8" src="<c:url value="/resources/assets/js/agpAudit.js" />"></script>
        <div class="wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="header-title m-t-0 m-b-20"></h4>
                    </div>
		        </div> <!-- end row -->
							<div class="row">
								<div class="col-sm-12">
									<div class="card-box">
										<div class="row">
											<div class="col-sm-4">
												<label for="txtSearchUnitId">Contenedor</label> <input
													type="text" name="txtSearchUnitId" id="txtSearchUnitId"
													class="form-control" maxlength="50" />
											</div>
											<div class="col-sm-4">
												<label for="txtSearchVisitId">Id Visita</label> <input
													type="text" name="txtSearchVisitId" id="txtSearchVisitId"
													class="form-control" maxlength="50" placeholder="VISIT123E"/>
											</div>
											<div class="col-sm-4">
												<label for="cbSearchCategory">Categor�a</label> <select
													id="cbSearchCategory" class="form-control"
													name="cbSearchCategory"
													data-bind="options:agpAuditViewModel.categoryCb, optionsText:'description', optionsValue:'key', optionsCaption: '--'">
												</select>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12">
												<br />
											</div>
										</div>
										<div class="row">
										    <div class="col-sm-4">
												<label for="txtSearchCreator">Cliente</label> <input
													type="text" name="txtSearchCreator" id="txtSearchCreator"
													class="form-control" placeholder="CUIT o CUIL sin guiones" />
											</div>
											<div class="col-sm-4">
												<label for="txtSearchFromDate">Fecha Inicio</label> <input
													type="text" name="txtSearchFromDate" id="txtSearchFromDate"
													class="form-control" placeholder="YY-MM-DD" />
											</div>
											<div class="col-sm-4">
												<label for="txtSearchToDate">Fecha Fin</label> <input
													type="text" name="txtSearchToDate" id="txtSearchToDate"
													class="form-control" placeholder="YY-MM-DD" />
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12">
												<br />
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12" align="center">
												<button type="button" class="button button--primary"
													id="btnSearchAudit">Buscar</button>
												&nbsp;&nbsp;
												<button type="button" class="button button--success"
													id="btnCleanSearchAudit">Limpiar</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="card-box">
										<div class="table-responsive">
											<table class="table table-hover datatable" id="tbAgpAudit"
												width="100%">${agpAuditDataTable}
											</table>
										</div>
									</div>
								</div>
							</div>
				<!-- end row -->
				<%@include file="viewAgpUnitCalendarModal.jsp" %>
            </div> <!-- end container -->