
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>

	<body>
	
		<script type="text/javascript">
		
			function muestra_oculta() {
				var capa = document.getElementById( "detalleException" );
				capa.style.display = (capa.style.display == 'none') ? 'block' : 'none';
			}
		
		</script>

		<table width="100%" border="0" cellpadding="0" cellspacing="10">
			<tr>
				<td align="center">
					<table width="30%" border="0" cellpadding="0" cellspacing="20" bgcolor="grey">
						<tr>
							<td align="center">
								<span style="font-family: Verdana, Arial, sans-serif; font-size: 12px; color: red" >
									<spring:message code="general.error.header" />
								</span>
							</td>
						</tr>
						<tr>
							<td valign="middle">
								<span style="font-family: Verdana, Arial, sans-serif; font-size: 10px; color: black; align: justify;" >
									<spring:message code="general.error" />
								</span>
							</td>
						</tr>
						<tr>
							<td align="center" valign="middle">
								<input type="button" value="Ver Detalle" onclick="JavaScript: muestra_oculta();">
							</td>
						</tr>
					</table>
				</td>
			</tr>
				<c:if test="${not empty exceptionVO}">
					<c:if test="${not empty exceptionVO.throwabesVO}">
						<tr>
							<td align="center">
								<div id="detalleException" style="display: none;">
									<table width="80%" border="0" cellpadding="0" cellspacing="5">
										<c:forEach var="throwable" items="${exceptionVO.throwabesVO}">  
											<tr>
												<td align="left" bgcolor="grey">
													<table width="100%" border="0" cellpadding="0" cellspacing="1">
														<tr>
															<td align="left">
																<span style="font-family: Verdana, Arial, sans-serif; font-size: 10px; color: black; align: left;" >
																	<b>Nivel Exception:&nbsp;</b>
																</span>
																<br>
																<span style="font-family: Verdana, Arial, sans-serif; font-size: 10px; color: black; align: left;" >
																	<b>ClassName:</b>
																</span>
																<span style="font-family: Verdana, Arial, sans-serif; font-size: 10px; color: black; align: left;" >
																	<c:out value="${throwable.clazzName}" />
																</span>
																<br>
																<span style="font-family: Verdana, Arial, sans-serif; font-size: 10px; color: black; align: left;" >
																	<b>LocalizedMessage:</b>
																</span>
																<span style="font-family: Verdana, Arial, sans-serif; font-size: 10px; color: black; align: left;" >
																	<c:out value="${throwable.localizedMessage}" />
																</span>
																<br>
																<span style="font-family: Verdana, Arial, sans-serif; font-size: 10px; color: black; align: left;" >
																	<b>Message:</b>
																</span>
																<span style="font-family: Verdana, Arial, sans-serif; font-size: 10px; color: black; align: left;" >
																	<c:out value="${throwable.message}" />
																</span>
																<br>
															</td>
														</tr>
														<tr>
															<td align="left">
																<c:if test="${not empty throwable.stackTraceElementVO}">
																	<table width="100%" border="0" cellpadding="0" cellspacing="1">
																		<tr>
																			<td colspan="2">
																				<span style="font-family: Verdana, Arial, sans-serif; font-size: 10px; color: black; align: left;" >
																					<b>Stack:</b>
																				</span>
																			</td>
																		</tr>
																		<c:forEach var="stackVO" items="${throwable.stackTraceElementVO}">  
																			<tr>
																				<td width="10%" align="right">
																					<span style="font-family: Verdana, Arial, sans-serif; font-size: 10px; color: black; align: left;" >
																						&nbsp;&nbsp;&nbsp;&nbsp;<b>ClassName:</b>
																					</span>
																				</td>
																				<td align="left">
																					<span style="font-family: Verdana, Arial, sans-serif; font-size: 10px; color: black; align: left;" >
																						&nbsp;&nbsp;&nbsp;&nbsp;<c:out value="${stackVO.clazzName}" />
																					</span>
																				</td>
																			</tr>
																			<tr>
																				<td width="10%" align="right">
																					<span style="font-family: Verdana, Arial, sans-serif; font-size: 10px; color: black; align: left;" >
																						&nbsp;&nbsp;&nbsp;&nbsp;<b>MethodName:</b>
																					</span>
																				</td>
																				<td>
																					<span style="font-family: Verdana, Arial, sans-serif; font-size: 10px; color: black; align: left;" >
																						&nbsp;&nbsp;&nbsp;&nbsp;<c:out value="${stackVO.methodName}" />
																					</span>
																				</td>
																			</tr>
																			<tr>
																				<td width="10%" align="right">
																					<span style="font-family: Verdana, Arial, sans-serif; font-size: 10px; color: black; align: left;" >
																						&nbsp;&nbsp;&nbsp;&nbsp;<b>LineNumber:</b>
																					</span>
																				</td>
																				<td>
																					<span style="font-family: Verdana, Arial, sans-serif; font-size: 10px; color: black; align: left;" >
																						&nbsp;&nbsp;&nbsp;&nbsp;<c:out value="${stackVO.lineNumber}" />
																					</span>
																				</td>
																			</tr>
																			<tr>
																				<td colspan="2">
																					&nbsp;
																				</td>
																			</tr>
																		</c:forEach>
																	</table>
																</c:if>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</c:forEach>
									</table>
								</div>
							</td>
						</tr>
					</c:if>
				</c:if>
		</table>

	</body>

</html>
