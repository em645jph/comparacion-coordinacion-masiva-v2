<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="viewUpdateUnitOogModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title" id="modalTitle" id="txtUpdateOogTitle">Actualizar fuera de medida</h4>
				<button type="button" class="close" data-dismiss="modal">�</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-4">
						<label for="cbUnitIsOogUpdate"> Fuera de medida: </label>										
						<select width="40%" id="cbUnitIsOogUpdate" class="form-control" name="cbUnitIsOogUpdate"
								data-bind="options:preadviseViewModel.yesNoCb, optionsText:'description', optionsValue:'key', optionsCaption: '--'">
						</select>
					</div>
					<div class="col-sm-4 oog-div-update">
						<label for="txtUnitTopUpdate"> Arriba (cm):</label> 
						<input type="number" name="txtUnitTopUpdate" id="txtUnitTopUpdate" class="form-control" />
					</div>
					<div class="col-sm-4 oog-div-update">
						<label for="txtUnitFrontUpdate"> Frente (cm):</label> 
						<input type="number" name="txtUnitFrontUpdate" id="txtUnitFrontUpdate" class="form-control"/>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4 oog-div-update">
						<label for="txtUnitBackUpdate"> Atr�s (cm):</label> 
						<input type="number" name="txtUnitBackUpdate" id="txtUnitBackUpdate" class="form-control"/>
					</div>
					<div class="col-sm-4 oog-div-update">
						<label for="txtUnitLeftUpdate"> Izquierda (cm):</label> 
						<input type="number" name="txtUnitLeftUpdate" id="txtUnitLeftUpdate" class="form-control" />
					</div>
					<div class="col-sm-4 oog-div-update">
						<label for="txtUnitRightUpdate"> Derecha (cm):</label> 
						<input type="number" name="txtUnitRightUpdate" id="txtUnitRightUpdate" class="form-control" />
					</div>
				</div>
			</div>
            <!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="btnSaveUnitOog">Guardar</button>
				<button type="button" class="btn btn-dark" id="btnCancelUpdateUnitOog" data-dismiss="modal">Cancelar</button>
			</div>
		</div>
	</div>
</div>	