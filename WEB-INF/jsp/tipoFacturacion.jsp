<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="<c:url value="/resources/assets/js/tipoFacturacion.js" />"
	charset="UTF-8"></script>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/assets/css/icons.css" />" />
	
<style>
<!--


  
.btn-sq-lg {
  width: 100% !important;
  height: 150px !important;
}

.btn-sq {
  width: 100px !important;
  height: 100px !important;
  font-size: 10px;
}

.btn-sq-sm {
  width: 50px !important;
  height: 50px !important;
  font-size: 10px;
}

.btn-sq-xs {
  width: 25px !important;
  height: 25px !important;
  padding:2px;
}
.card-box > a.btn.btn-sq-lg.btn-primary {
    margin-left: 5%;
    padding: 20px;
}
-->
</style>
<div class="wrapper">
	<div class="container-fluid">

		<div class="row">
			<div class="col-sm-12">
				<h2>Facturaci�n</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-9 offset-md-2">
				<div class="card-box">
					<h4 style="text-align: center;">Seleccion� el tipo de operaci�n a facturar</h4>
					<br>
						<table class="table table-dark">
							<tr>
								<td>RAZ�N SOCIAL: <strong>${customer.customerName} (${customer.customerId})</strong></td>
								<td>CUIT/CUIL: <strong>${customer.customerTaxId}</strong></td>
								<td>CONDICI�N PAGO: <strong>${customer.customerCreditStatus != 'OAC' ? 'Contado' : 'Cuenta Corriente'}</strong></td>
							</tr>
						</table>
					<hr>
			<div class="container-fluid">
				<div class="row align-items-start" >
					<div class="col-sm-12 col-md-4 col-lg-4 text-center" style="margin-bottom:5px">			
	                        <div class="card ds-tilted">
	                            <a href="${pageContext.servletContext.contextPath}/Invoice/billingImport" class="py-4 "> 
								<i class="fa fa-truck fa-5x"></i>
	                            <article>
									Importaci�n 
	                            </article>
	                            </a>
	                        </div>
	                     </div>
                     <div class="col-sm-12 col-md-4 col-lg-4 text-center" style="margin-bottom:5px">
						 <div class="card card--image-text animate-on-scroll__item animate-on-scroll__item--active ds-tilted" class="py-4">
                            <a href="${pageContext.servletContext.contextPath}/Invoice/billingExport" class="py-4"> 
							<i class="fa fa-truck fa-flip-horizontal fa-5x"></i>
                            <article>
								 Exportaci�n
                            </article>
                            </a>
                        </div>
                     </div>
                     <div class="col-sm-12 col-md-4 col-lg-4 text-center" style="margin-bottom:5px"> 
						<div class="card card--image-text animate-on-scroll__item animate-on-scroll__item--active ds-tilted">
                            <a href="${pageContext.servletContext.contextPath}/Invoice/billingReceiveEmpty" class="py-4">
							<i class="ti-layout-media-left-alt fa-5x" ></i>
                            <article>
								Devoluci�n de vac�os
                            </article>
                            </a>
                        </div>
                     </div>
					</div>	
						<div class="row align-items-start">
				  		<div class="col-sm-12 col-md-6 col-lg-6 text-center" id="primary"> 
						<div class="card card--image-text animate-on-scroll__item animate-on-scroll__item--active ds-tilted">
                            <a href="${pageContext.servletContext.contextPath}/Invoice/billingManifest" class="py-4"> 
							<i class="fa fa-wpforms fa-5x"></i>
                            <article>
								 Manifiestos
                            </article>
                            </a>
                        </div>
                     </div>
                     <div class="col-sm-12 col-md-6 col-lg-6 text-center"> 
						<div class="card card--image-text animate-on-scroll__item animate-on-scroll__item--active ds-tilted">
                            <a href="${pageContext.servletContext.contextPath}/Invoice/billingDeliverEmpty" class="py-4"> 
							<i class="ti-layout-media-right-alt fa-5x"></i>
                            <article>
								Retiro Vac�os
                            </article>
                            </a>
                        </div>
                       </div>
					</div>	
                    </div>    
				</div>
			</div>
		</div>
		<%@include file="transferWarningModal.jsp" %>
	</div>
</div>
<!-- end container -->