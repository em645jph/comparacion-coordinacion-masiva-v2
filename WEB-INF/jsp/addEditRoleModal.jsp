<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="addEditRoleModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Role</h4>
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<form class="form-horizontal m-t-10" role="form" method="POST" enctype="multipart/form-data" id="addEditRoleForm">
					<div class="form-group">
						<div class="col-sm-9">
							<input type="hidden" class="form-control" id="txtRoleGkey" disabled>
						</div>
					</div>
					<div class="form-group">
						<label for="txtRoleId" class="col-sm-3 col-form-label">Id (*)</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="txtRoleId" maxlength="50">
						</div>
					</div>
					<div class="form-group">
						<label for="txtRoleDesc" class="col-sm-3 col-form-label">Descripción</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="txtRoleDesc" maxlength="50">
						</div>
					</div>
					<div class="form-group">
						<div>
							<label class="col-sm-3 col-form-label">Privilegios</label>
						<div>
						<div class="col-sm-12">
							<div class="table-responsive" >
                                <table class="table table-hover datatable" id="tbRolePrivilege" width="100%">
                                	${privilegeDataTable}                                
                                </table>
                            </div>							
						</div>
					</div>
				</form>
            </div>
            <!-- Modal footer -->
			<div class="modal-footer">
				${modalButtons}
			</div>		
		</div>
	</div>
</div>	