<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="addEditRuleModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title" id="ruleModalTitle">Regla Coordinaci�n</h4>
				<button type="button" class="close" data-dismiss="modal">�</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<input type="hidden" class="form-control" id="txtRuleGkey" disabled>
					</div>
				</div>
				<div class="row">					
					<div class="col-sm-6">
						<label for="txtSfdId"> Id (*) </label>										
						<input style="text-transform: uppercase;" type="text" class="form-control" id="txtRuleId" maxlength="50">
					</div>
					<div class="col-sm-6">
						<label for="txtSfdDesc"> Descripci�n: </label>										
						<input type="text" class="form-control" id="txtRuleDesc" maxlength="100">
					</div>
				</div>
				<div class="row"><br></div>
				<div class="row">					
					<div class="col-sm-6">
						<label for="cbRuleTranType"> Tipo Transacci�n (*): </label>										
						<select width="40%" id="cbRuleTranType" class="form-control" name="cbRuleTranType"
								data-bind="options:scheduleViewModel.searchTranTypeCb, optionsText:'description', optionsValue:'key', optionsCaption: '--'">
						</select>
					</div>
					<div class="col-sm-6">
						<label for="cbRuleAction"> Acci�n (*): </label>										
						<select width="40%" id="cbRuleAction" class="form-control" name="cbRuleAction"
								data-bind="options:scheduleViewModel.searchRuleActionCb, optionsText:'description', optionsValue:'key', optionsCaption: '--'">
						</select>
					</div>
				</div>
				<div class="row"><br></div>
				<div class="row">					
					<div class="col-sm-6">
						<label for="txtRuleDays"> D�as (*):</label>										
						<input type="text" class="form-control" id="txtRuleDays" maxlength="5">
					</div>
					<div class="col-sm-6">
						<label for="txtRuleTime"> Hora (*): </label>										
						<input type="text" class="form-control" id="txtRuleTime">
					</div>
				</div>
            </div>
            <!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="btnSaveRule">Guardar</button>
				<button type="button" class="btn btn-dark" id="btnCancelSaveRule" data-dismiss="modal">Cancelar</button>
			</div>	
		</div>
	</div>
</div>	