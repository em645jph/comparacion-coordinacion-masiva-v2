<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="<c:url value="/resources/assets/js/formManagerUser.js" />" charset="UTF-8"></script>
<script src="<c:url value="/resources/assets/js/fileValidator.js" />" charset="UTF-8"></script>

<style>
.wrapper-page {
    margin: 0% auto;
}
</style>

<div class="wrapper">
	<section>
	    <div class="container">
	        <div class="row">
	            <div class="col-sm-12">
	
	                <div class="wrapper-page">
	
	                    <div class="m-t-40 card-box">
	                        <div class="account-content">
	                        	<div class="text-center m-b-20">
	                                <h3 class="text-muted m-b-0 line-h-24">
	                                	Alta de Usuario  
	                                </h3>
	                            </div>
	                        	<form class="form-horizontal" id="formRegisterUser">
	                        		<div class="text-center">
	                        			<label class="text-center">CUIT: <strong>${userCuit}</strong></label>
	                        			<input type="hidden" id="documentNbr" name="documentNbr" value="${userCuit}">
	                        			<input type="hidden" id="beBillingUser" name="beBillingUser" value="${userRequestedBilling}">
	                        		</div>
	                        		${form}
	                        		<div class="form-group account-btn text-center m-t-10" id="divRegisterBtn">
										<div class="col-12">
											<button id="btnRegister" class="btn btn-lg btn-primary btn-block" type="button">Registrar</button>
										</div>
									</div>
	                        	</form>
	                        	
	                       	</div>
	                    </div>
	                    
	                </div>
	                
	                <script>
	                </script>
	                
	             </div>
	         </div>
	         
	    </div>
	    
	</section>