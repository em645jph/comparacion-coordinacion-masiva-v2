<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="viewInvoicePayeeModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Cliente Billing</h4>
				<button type="button" class="close" data-dismiss="modal">�</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="form-group">
						<label for="txtPayeeName" class="col-sm-12 col-form-label">Nombre:</label>
						<div class="col-sm-12">
							<input type="text" class="form-control" id="txtPayeeName" readonly>
						</div>
				</div>
				<div class="form-group">
						<label for="txtPayeeTaxId" class="col-sm-12 col-form-label">Tax Id:</label>
						<div class="col-sm-12">
							<input type="text" class="form-control" id="txtPayeeTaxId" readonly>
						</div>
				</div>
				<div class="form-group">
						<label for="txtPayeeCreditStatus" class="col-sm-12 col-form-label">Credit Status:</label>
						<div class="col-sm-12">
							<input type="text" class="form-control" id="txtPayeeCreditStatus" readonly>
						</div>
				</div>
				<div class="form-group">
						<label for="txtPayeeTaxGroup" class="col-sm-12 col-form-label">Tax Group:</label>
						<div class="col-sm-12">
							<input type="text" class="form-control" id="txtPayeeTaxGroup" readonly>
						</div>
				</div>
				<div class="form-group">
						<label for="txtPayeeEmail" class="col-sm-12 col-form-label">Email:</label>
						<div class="col-sm-12">
							<input type="text" class="form-control" id="txtPayeeEmail" readonly>
						</div>
				</div>
				<div class="form-group">
						<label for="txtPayeeBalance" class="col-sm-12 col-form-label">Balance:</label>
						<div class="col-sm-12">
							<input type="text" class="form-control" id="txtPayeeBalance" readonly>
						</div>
				</div>			
			</div>
            <!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-dark" id="btnCancelViewInvoicePayee" data-dismiss="modal">Volver</button>
			</div>
		</div>
	</div>
</div>	