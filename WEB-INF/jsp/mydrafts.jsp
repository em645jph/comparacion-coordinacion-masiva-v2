<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/css/icons.css" />" />
<script src="<c:url value="/resources/assets/js/mydrafts.js" />" charset="UTF-8"></script>	
<style>
#tbDrafts,#tableBox {
	display: none;
}
</style>
<div class="wrapper">
	<div class="container-fluid">

		<div class="row">
			<div class="col-sm-12">
				<h2>Mis facturas</h2>
			</div>
		</div>
		<br>
		
		<div class="row">
                    <div class="col-sm-12">
						<div class="card-box">
								<div class="row">
									<div class="col-sm-6">
										<label for="txtSearchFromDate">Fecha Inicio</label>
										<input type="text" name="txtSearchFromDate" placeholder="yyyy-mm-dd" autocomplete="off"
												id="txtSearchFromDate" class="form-control datepicker"/>
									</div>
									<div class="col-sm-6">
										<label for="txtSearchToDate">Fecha Fin</label>
										<input type="text" name="txtSearchToDate" id="txtSearchToDate" placeholder="yyyy-mm-dd" autocomplete="off" 
											class="form-control datepicker" />
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<br />
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12" align="center">
										<button type="button" class="button button--primary" id="btnSearch">Buscar</button>&nbsp;&nbsp;
										<button type="button" class="button button--success" id="btnClean">Limpiar</button>
										<br><br>
										<c:if test = "${msg != null}">
								         <script>
								         var msg = "${msg}";
								         showModalHtml("", msg, "info");</script>
								        </c:if>
									</div>
								</div>
							</div>
                    </div>
                </div>
                
                
			<div class="row">
				<div class="col-md-12">
					<div class="card-box" id="tableBox">
						<table class="table table-striped datatable" id="tbDrafts">
		                    <thead>
			                    <tr>
			                        <th>CUIT</th>
			                        <th>Cliente</th>
			                        <th>Draft #</th>
			                        <th>Fecha - Hora</th>
			                        <th>Estado</th>
			                        <th>Total</th>
			                        <th>Pagado</th>
			                        <th>Pendiente</th>
			                        <th>PDF</th>
			                        <th></th>
			                        <th></th>
			                    </tr>
		                    </thead>
		                    <tbody data-bind="foreach: viewModel.drafts">
			                    <tr data-bind="attr:{ 'id': $index }">                                       
			                        <td data-bind="text: customerCuit"></td>
			                        <td data-bind="text: customerName"></td>
			                        <td data-bind="text: invDraft, attr: {'invGkey': invGkey}"></td>
			                        <td data-bind="text: invCreated"></td>
			                        <td data-bind="text: invStatus"></td>
			                        <td data-bind="text: invAmount.toFixed(2)"></td>
			                        <td data-bind="text: invPayed.toFixed(2)"></td>
			                        <td data-bind="text: invOwed.toFixed(2)"></td>
			                        <td>
			                        	<button class="btn btn-icon btn-info btn-sm" data-bind="attr:{'invDraft': invDraft}" onclick="getPdfByDraftId($(this).attr('invDraft'))" id="btnPdf"> Descargar<!-- <i class="ti-file"></i> --> </button>
			                        </td>
									<td>
										<!-- ko {if: invStatus == 'DRAFT'} -->
											<button class="btn btn-icon btn-primary btn-sm" data-bind="attr:{'id': invGkey, 'nbr': invDraft}" onclick="payDraft(this)" id="btnPay"> PAGAR<!-- <i class="ti-money"></i> --> </button>
										<!-- /ko -->
									</td>
									<td>
										<!-- ko {if: invStatus == 'DRAFT'} -->
											<button class="btn btn-icon btn-danger btn-sm deleterow" data-bind="attr:{'invDraft': invDraft}" onclick="deleteDraft(this)" id="btnDelete"> <i class="ti-close"></i> </button>
										<!-- /ko -->
									</td>
			                    </tr>
		                    </tbody>
		                </table>
					</div>
				</div>
			</div>
		
	</div>
<!-- end container -->