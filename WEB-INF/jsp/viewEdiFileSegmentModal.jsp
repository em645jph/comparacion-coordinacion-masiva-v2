<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="viewEdiFileSegmentModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Archivo EDI</h4>
				<button type="button" class="close" data-dismiss="modal">�</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="form-group">
						<label for="txtEdiSegment" id="lblFileName" class="col-sm-12 col-form-label">Nombre archivo</label>
						<div class="col-sm-12">
							<textarea class="form-control" id="txtEdiSegment" rows="25"> </textarea>
						</div>
				</div>		
			</div>
            <!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="btnDownloadEdiFile">Descargar</button>
				<button type="button" class="btn btn-dark" id="btnCancelViewEdiFile" data-dismiss="modal">Volver</button>
			</div>
		</div>
	</div>
</div>	