<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/css/icons.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/css/jquery.datetimepicker.min.css" />" />
<script src="<c:url value="/resources/assets/js/documentalCutoff.js" />" charset="UTF-8"></script>
<script src="<c:url value="/resources/assets/js/modalsFunctions.js" />" charset="UTF-8"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<style>
	.tableEmpty{
		display: table-caption;
		caption-side: bottom;
		font-size: 20px;
		margin-top: 10px;
	}  	
</style>

<div class="wrapper">
	<div class="container-fluid">

		<div class="row" style="margin-top:15px;">
			<div class="col-sm-12">
				<h4 class="header-title m-t-0 m-b-20">Cutoff Documental</h4>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="card-box">
					<div class="table-responsive">
						<table class="table datatable table-hover" width="100%" id="dataTableVisits">
							<thead>
						 		<tr>
								   	<th>ID Visita</th>
									<th>Buque</th>
									<th>Ib Vyg</th>
									<th>Ob Vyg</th>
									<th>Linea Operadora</th>
									<th>Cutoff Documental</th>
									<th>Editar Cutoff</th>
									<th>Fecha de Modificaci&oacuten</th>
									<th>Historial</th>
							  	</tr>
						 	<thead> 	
						  	<tbody id="detailVisits" data-bind="foreach: viewModel.LoadVisit">				  		
						  		<tr class="trVisits">
						  			<td data-bind="text: visitId"></td>
						  			<td data-bind="text: vessel"></td>
						  			<td data-bind="text: ibVyg"></td>
						  			<td data-bind="text: obVyg"></td>
						  			<td data-bind="text: lineOp"></td>
						  			<td data-bind="text: cutoffDateDocStr"></td>					  			  							  							  							  							  			
		                    		<td>
		                    		<a data-bind="attr: { 'id': $index, 'cutoffDate': cutoffDateDoc, 'visitId': visitId, 'vessel': vessel, 'ibVyg': ibVyg, 'obVyg': obVyg }" 
		                    			class="btnEditCutoff ti-pencil-alt" style="color: blue;" onclick="editCutoff(this)"></a> 
		                    		</td>
		                    		<td data-bind="text: changedStr"></td>	
		                    		<td><button class="btn btn-icon btn-default btn-sm" data-bind="click: $parent.viewHistoryEvents"> <i class="ti-view-list"></i> </button></td>		  				
						  		</tr>		
						  	</tbody>
						</table>	
					</div>
				</div>	  	
			</div>
		</div>					
     		<%@include file="addEditModalCutoff.jsp" %>
     		<%@include file="showModalHistory.jsp" %>         
	</div>		
</div>