<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/css/icons.css" />" />
<script src="<c:url value="/resources/assets/js/myReportsView.js" />" charset="UTF-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.min.js"></script>
<script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
<div class="wrapper">
	<div class="container-fluid">
	
		<div class="row">
			<div class="col-sm-12">
				<h2>Mis Presentaciones</h2>
			</div>
		</div>
		<br>
	
			<div class="row">
				<div class="col-sm-4">
					<h6 class="text-muted font-13 mt-0 text-uppercase">Usuarios a gestionar</h6>
					<div class="input-group mb-3">
						<select id="selectCustomer" class="form-control" name="selectCustomer"
							data-bind="options:viewModel.customers, optionsText:'customerName' , optionsValue:'customerTaxId', optionsCaption: 'Seleccione opci�n...'">
						</select>
						<span class="input-group-append">
							<button class="btn btn-icon btn-primary" id="btnFindCustomerInformation">BUSCAR</button>
						</span>
					</div>
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-md-12">
					<div class="card-box" id="tableBox" style="display:none;">
						<table class="table table-striped datatable" id="tbCustomerReport">
		                    <thead>
			                    <tr>
			                    	<th>Reporte #</th>
			                        <th>Creado</th>
			                        <th>Creador</th>
			                        <th>Estado</th>
			                        <th>Notas</th>
			                        <th>Informaci�n</th>
			                        <th>Acci�n</th>
			                    </tr>
		                    </thead>
		                    <tbody data-bind="foreach: viewModel.customerReport">
			                    <tr data-bind="attr:{ 'id': $index }">                                       
			                        <td data-bind="text: reportNumberLeftPad(reportNumber)"></td>
			                        <td data-bind="text: getDateTime(created)"></td>
			                        <td data-bind="text: creator"></td>
			                        <td data-bind="text: status"></td>
			                        <td data-bind="text: notes"></td>
			                        <td data-bind="click: loadAdditionalDataFromRow"><i class="mdi mdi-information-outline" style="font-size: 20px;"></i></td>
			                        <!-- ko ifnot: status == 'RECIBIDA' -->
			                        	<td></td>
			                        <!-- /ko -->
			                        <!-- ko if: status == 'RECIBIDA' -->
			                        	<td><button type="button" id="deleteMyReportBtn" class="btn btn-danger" data-bind="reportId: reportNumber, click: deleteMyReport">ELIMINAR</button></td>
			                        <!-- /ko -->
			                    </tr>
		                    </tbody>
		                </table>
					</div>
				</div>
			</div>
		
	</div>
	
	<!-- Modal -->
	<div class="modal fade" id="modalInformation" tabindex="2" role="dialog" aria-hidden="true">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="titleInformationReport"># Reporte</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body" id="modalBodyReportData">
	      	<h5 class="modal-title" id="titleInformationReport2" style="display:none;"># Reporte</h5>
	      	<button type="button" id="htmlToPdfCreator" class="btn btn-primary" style="float:right;">DESCARGAR</button>
	      	<br>
	      	<h3>Documentos</h3>
	        <table class="table table-striped datatable" id="tbCustomerReportDocuments">
                <thead>
                 <tr>
                     <th>Fecha</th>
                 	 <th>Documento #</th>
                     <th>Monto</th>
                 </tr>
                </thead>
                <tbody data-bind="foreach: viewModel.customerReportDocuments">
                 <tr data-bind="attr:{ 'id': $index }">                                       
                     <td data-bind="text: documentDate"></td>
                     <td data-bind="text: documentNumber"></td>
                     <td data-bind="text: parseMoneyValue(documentAmount)"></td>
                 </tr>
                </tbody>
            </table>
            <hr>
            <h3>Orden de Pago</h3>
	        <table class="table table-striped datatable" id="tbCustomerReportItems">
                <thead>
                 <tr>
                 	 <th>Fecha</th>
                 	 <th>Item</th>
                 	 <th>Concepto</th>
                 	 <th># Referencia</th>
                     <th>Monto</th>
                 </tr>
                </thead>
                <tbody data-bind="foreach: viewModel.customerReportItems">
                 <tr data-bind="attr:{ 'id': $index }">                                       
                     <td data-bind="text: itemDate"></td>
                     <td data-bind="text: itemType"></td>
                     <td data-bind="text: itemString01"></td>
                     <td data-bind="text: itemReference"></td>
                     <td data-bind="text: parseMoneyValue(itemValue)"></td>
                 </tr>
                </tbody>
            </table>
            <hr>
            <h3>Documentos relacionados</h3>
	        <table class="table table-striped datatable" id="tbCustomerReportFiles">
                <thead>
                 <tr>
                 	 <th>#</th>
                     <th>Archivo</th>
                 </tr>
                </thead>
                <tbody data-bind="foreach: viewModel.customerReportFiles">
                 <tr data-bind="attr:{ 'id': $index }">                                       
                     <td data-bind="text: ($index() + 1)"></td>
                     <td data-bind="click: downloadFile, attr:{'url': filePath}"><i class="mdi mdi-file" style="font-size: 20px;"></i></td>
                 </tr>
                </tbody>
            </table>
	      </div>
	      <br>
	      <div class="modal-footer">
	        <br>
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
	      </div>
	    </div>
	  </div>
	</div>
	
<!-- end container -->
