<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="addVipEventModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title" id="sfpModalTitle">Agregar Evento</h4>
				<button type="button" class="close" data-dismiss="modal">�</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="row">					
					<div class="col-sm-12">
						<label for="txtVipEventId"> Evento: </label>										
						<input type="text" name="txtVipEventId" id="txtVipEventId" class="form-control" maxlength="50" placeholder="Evento de N4" style="text-transform: uppercase;"/>
					</div>
				</div>
            </div>
            <!-- Modal footer -->
			<div class="modal-footer">
				${eventModalButtons}
				<button type="button" class="btn btn-dark" id="btnCancelSaveVipEvent" data-dismiss="modal">Cancelar</button>
			</div>	
		</div>
	</div>
</div>	