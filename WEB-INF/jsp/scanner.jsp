<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="<c:url value="/resources/assets/js/scannerGate1.js" />" charset="UTF-8"></script>
<script src="<c:url value="/resources/assets/js/modalsFunctions.js" />" charset="UTF-8"></script>
<div class="wrapper">
	<div class="container-fluid">
		<div class="row" style="margin-top:15px;">
			<div class="col-sm-12">
				<h4 class="header-title m-t-0 m-b-20">Bloqueo por Scanner</h4>
			</div>
		</div>
		<form id="formSearchUnits" method="post">
			<div class="row form-group">
	            <div class="col-sm-12">
					<div class="card-box">
						<div class="row justify-content-center text-center">
							<div class="col-sm-12">
								<label for="formUnit">Ingrese un contenedor (*)</label>
							</div>						
						</div>
						<div class="row justify-content-center">
							<div class="col-sm-3">
								<input autofocus type="text" name="txtSearchUnit" id="txtSearchUnit" class="form-control" maxlength="11" style="text-transform: uppercase;"/>
							</div>
						</div> 
						<div class="row optionalView">
							<div class="col-sm-12">
								<br />
							</div>
						</div> 
						<div class="row">
							<div class="col-sm-12" align="center">												
								<button type="button" class="button button--primary" id="btnSearch">Buscar</button>&nbsp;&nbsp;
								<button type="button" class="button button--success" id="btnClean">Limpiar</button>&nbsp;&nbsp;
							</div>
						</div>							
					</div>
				</div>
			</div>
		</form>
				
		<div class="card-box">
			<div class="row">
              	<div class="col-sm-12">
			        <div class="table-responsive">
		   	        	<table class="table datatable table-hover" id="tbUnit" width="100%">
		                   	<thead>
								<tr>
								    <th>Permiso</th>
								    <th>Contenedor </th>
								    <th>Categoría</th>
								    <th>T-Carga</th>
								    <th>Reserva</th>
								    <th>Ubicación </th>
								    <th>Buque </th>
								    <th>Procedencia</th>
								    <th>Precintos</th>
								    <th>Shipper</th>
								    <th>Estado</th>
								    <th>Fecha de Salida</th>
								    <th class="noExl">Bloquear</th>
								</tr>
							</thead>
							<tbody data-bind="foreach: viewModel.unitBlockScanner"  class="text-center">
								<tr class="trVisits" data-toggle="tooltip" tabindex="0" data-placement="top">
									<td><label  data-bind="text: client_perm_ref_id"></label> </td>
									<td><label  data-bind="text: id"></label></td>
									<td><label  data-bind="text: category"></label> </td>
									<td><label  data-bind="text: freight_kind"></label></td>
									<td><label  data-bind="text: booking_nbr"></label></td>
									<td><label  data-bind="text: last_poisition"></label></td>
									<td><label  data-bind="text: obVesselInfo"></label></td>
									<td><label  data-bind="text: ibVesselInfo"></label></td>
									<td><label  data-bind="text: unitSeals"></label></td>
									<td><label  data-bind="text: ob_vessel_name"></label></td>
									<td><label  data-bind="text: unit_status"></label></td>
									<td><label  data-bind="text: atd =! null ? atd : '' "></label></td>
									<td class="noExl">
									 <!-- ko {if: client_perm_ref_id != null} -->
									<button data-bind="click: $parent.viewLockModal, enable: gkey ,id, client_perm_gkey, client_perm_ref_id"> <i class="ti-lock"></i> </button>
									 <!-- /ko -->
									 <!-- ko {if: client_perm_ref_id == null} -->
									<label></label>
									 <!-- /ko -->
									</td>
								</tr>
							</tbody>						
						</table>
					</div>
              	</div>
            </div>
        </div>											
	</div>
 	<%@include file="scannerLockModal.jsp" %>
</div>