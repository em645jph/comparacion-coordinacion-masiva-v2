<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="<c:url value="/resources/assets/js/audit.js" />" charset="UTF-8"></script>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
				<div class="col-sm-12">
					<h2>Auditor�a</h2>
				</div>
			</div>
			<br>
			
			<div class="row">
                    <div class="col-sm-12">
						<div class="card-box">
								<div class="row">
									<div class="col-sm-6">
										<label for="txtSearchFromDate">Fecha Inicio</label>
										<input type="text" name="txtSearchFromDate" placeholder="yyyy-mm-dd" autocomplete="off"
												id="txtSearchFromDate" class="form-control datepicker"/>
									</div>
									<div class="col-sm-6">
										<label for="txtSearchToDate">Fecha Fin</label>
										<input type="text" name="txtSearchToDate" id="txtSearchToDate" placeholder="yyyy-mm-dd" autocomplete="off" 
											class="form-control datepicker" />
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<br />
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12" align="center">
										<button type="button" class="button button--primary" id="btnSearch">Buscar</button>&nbsp;&nbsp;
										<button type="button" class="button button--success" id="btnClean">Limpiar</button>
									</div>
								</div>
							</div>
                    </div>
                </div>
            
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box">
                        <div class="table table-striped datatable">
                            <table class="table table-hover datatable" id="tbAudit">
                                <thead>
                                 <tr>
                                     <th>Usuario</th>
                                     <th>Acci�n</th>
                                     <th>Datos</th>
                                     <th>Fecha Hora</th>
                                 </tr>
                                </thead>
                                <tbody data-bind="foreach: viewModel.users">
                                 <tr data-bind="attr:{usergkey: gkey}">                                       
                                     <td data-bind="text: userId"></td>
                                     <td data-bind="text: action"></td>
                                     <td data-bind="text: data"></td>
                                     <td data-bind="text: millisecondsToDate(timestamp)"></td>
                             </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end container -->