<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="viewFilterDefModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Filtro - Info</h4>
				<button type="button" class="close" data-dismiss="modal">�</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<p id="filterDefInfo"></p>
			</div>
            <!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-dark" id="btnCancelViewFilterDef" data-dismiss="modal">Volver</button>
			</div>
		</div>
	</div>
</div>	