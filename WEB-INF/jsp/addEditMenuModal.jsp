<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="addEditMenuModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Men�</h4>
				<button type="button" class="close" data-dismiss="modal">�</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body">
				<form class="form-horizontal m-t-10" role="form" method="POST" enctype="multipart/form-data" id="addEditMenuForm">
					<div class="form-group">
						<div class="col-sm-9">
							<input type="hidden" class="form-control" id="txtMenuGkey" disabled>
						</div>
					</div>
					<div class="form-group">
						<label for="txtMenuId" class="col-sm-3 col-form-label">Id</label>
						<div class="col-sm-9">
							<input style="text-transform: uppercase;" type="text" class="form-control" id="txtMenuId" maxlength="50">
						</div>
					</div>
					<div class="form-group">
						<label for="txtMenuLabel" class="col-sm-3 col-form-label">Nombre</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="txtMenuLabel" maxlength="50">
						</div>
					</div>
					<div class="form-group">
						<label for="txtMenuDesc" class="col-sm-3 col-form-label">Descripci�n</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="txtMenuDesc" maxlength="50">
						</div>
					</div>
					<div class="form-group">
						<label for="cbMenuParent">Men� Padre</label>
						<div class="col-sm-9">
						<select id="cbMenuParent" class="form-control" name="cbMenuParent"
							data-bind="options:menuViewModel.modalParentMenu, optionsText:'label', optionsValue:'gkey', optionsCaption: '--'">
						</select>
						</div>
					</div>
					<div class="form-group">
						<label for="txtMenuIcon" class="col-sm-3 col-form-label">Icon</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="txtMenuIcon" maxlength="50">
						</div>
					</div>
					<div class="form-group">
						<label for="txtMenuController" class="col-sm-3 col-form-label">Controller</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="txtMenuController" maxlength="50">
						</div>
					</div>
					<div class="form-group">
						<label for="txtMenuAction" class="col-sm-3 col-form-label">Acci�n</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="txtMenuAction" maxlength="50">
						</div>
					</div>
				</form>
			</div>

			<!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="btnSaveMenu">Guardar</button>
				<button type="button" class="btn btn-dark" id="btnCancelSaveMenu" data-dismiss="modal">Cancelar</button>
			</div>
		</div>
	</div>	
</div>