<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="addEditFilterDefModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title" id="sfdModalTitle">Filtro Coordinaci�n</h4>
				<button type="button" class="close" data-dismiss="modal">�</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<input type="hidden" class="form-control" id="txtSfdGkey" disabled>
					</div>
				</div>
				<div class="row">					
					<div class="col-sm-6">
						<label for="txtSfdId"> Id (*) </label>										
						<input style="text-transform: uppercase;" type="text" class="form-control" id="txtSfdId" maxlength="50">
					</div>
					<div class="col-sm-6">
						<label for="txtSfdDesc"> Descripci�n: </label>										
						<input type="text" class="form-control" id="txtSfdDesc" maxlength="100">
					</div>
				</div>
				<div class="row"><br></div>
				<div class="row">					
					<div class="col-sm-6">
						<label for="cbSfdTranType"> Tipo Transacci�n (*): </label>										
						<select width="40%" id="cbSfdTranType" class="form-control" name="cbSfdTranType"
								data-bind="options:scheduleViewModel.tranTypeCb, optionsText:'description', optionsValue:'key', optionsCaption: '--'">
						</select>
					</div>
					<div class="col-sm-6">
						<label for="cbSdfAction"> Acci�n (*): </label>										
						<select width="40%" id="cbSdfAction" class="form-control" name="cbSdfAction"
								data-bind="options:scheduleViewModel.actionCb, optionsText:'description', optionsValue:'key', optionsCaption: '--'">
						</select>
					</div>
				</div>
				<div class="row"><br></div>
				<div class="row">					
					<div class="col-sm-6">
						<label for="txtSdfStartDate"> V�lido desde (*): </label>										
						<input type="text" name="txtSdfStartDate" id="txtSdfStartDate" class="form-control" placeholder="YY-MM-DD" />
					</div>
					<div class="col-sm-6">
						<label for="txtSdfEndDate"> V�lido hasta: </label>										
						<input type="text" name="txtSdfEndDate" id="txtSdfEndDate" class="form-control" placeholder="YY-MM-DD" />
					</div>
				</div>
				<div class="row"><br></div>
				<div class="row">					
					<div class="col-sm-6">
						<label for="cbSfdTimeUnit"> Unidad Tiempo (*): </label>										
						<select width="40%" id="cbSfdTimeUnit" class="form-control" name="cbSfdTimeUnit"
								data-bind="options:scheduleViewModel.timeUnitCb, optionsText:'description', optionsValue:'key', optionsCaption: '--'">
						</select>
					</div>
					<div class="col-sm-6">
						<label for="cbSfdDay"> D�a: </label>										
						<select width="40%" id="cbSfdDay" class="form-control" name="cbSfdDay"
								data-bind="options:scheduleViewModel.dayCb, optionsText:'description', optionsValue:'key', optionsCaption: '--'">
						</select>
					</div>
				</div>
				<div class="row"><br></div>
				<div class="row">					
					<div class="col-sm-6">
						<label for="txtSfdStartTime"> Hora Inicio (*): </label>										
						<input name="txtSfdStartTime" id="txtSfdStartTime" class="form-control"/>
					</div>
					<div class="col-sm-6">
						<label for="txtSfdEndTime"> Hora Fin (*): </label>										
						<input name="txtSfdEndTime" id="txtSfdEndTime" class="form-control"/>                         
					</div>
				</div>
				<div class="row"><br></div>
				<div class="row">
					<div class="col-sm-6">
						<label for="cbSfdRecurrent"> Recurrente:</label>										
							<select width="40%" id="cbSfdRecurrent" class="form-control" name="cbSfdRecurrent">
								<option value="" >--</option>
								<option value="Y">S�</option>
								<option value="N">No</option>
							</select>
					</div>
					<div class="col-sm-6">
						<label for="txtSfdColor"> Color: </label>						
						<br><input type="text" name="txtSfdColor" id="txtSfdColor" class="form-control" />
					</div>
				</div>
            </div>
            <!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="btnSaveSfd">Guardar</button>
				<button type="button" class="btn btn-dark" id="btnCancelSaveSfd" data-dismiss="modal">Cancelar</button>
			</div>	
		</div>
	</div>
</div>	