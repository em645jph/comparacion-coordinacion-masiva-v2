<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/css/icons.css" />" />
<script src="<c:url value="/resources/assets/js/paymentDebin.js" />" charset="UTF-8"></script>

<style>
#aliasCbuFrom {
	text-transform: uppercase;
}
</style>

<div class="wrapper">
	<div class="container-fluid">

		<div class="row">
			<div class="col-sm-12">
				<h2>Pago mediante Debin</h2>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-4 offset-md-4">
				<div class="card-box">
					<h4 class="text-uppercase text-center">Ingres� los datos para generar el DEBIN</h4>
					<br>
						<table class="table table-dark">
							<tr>
								<td>DRAFT: <strong>${invoice.invNbr}</strong></td>
								<td>MONTO: $<strong><fmt:formatNumber value = "${invoice.invOwed}" type = "number" maxFractionDigits = "2"/></strong></td>
							</tr>
						</table>
					<hr>
					<label for="docTo">CUIT/CUIL de la cuenta bancaria que abonar�</label> 
					<span class="input-group-prepend"> 
						<input type="text" class="form-control" id="cuitFrom" name="cuitFrom" required>
					</span>
					<label for="aliasTo">CBU de la cuenta bancaria que abonar�</label>
					<span class="input-group-prepend"> 
						<input type="text" class="form-control" id="aliasCbuFrom" name="aliasCbuFrom" required>
					</span>
					<br>
					<button type="button" class="btn btn-lg btn-block button button--primary" id="btnPay" name="btnPay">Pagar</button>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-1 offset-md-0">
			<br>
				<button type="button" class="btn btn-lg btn-block button button--success" id="btnBack" name="btnBack">Volver</button>
			</div>
		</div>
	</div>