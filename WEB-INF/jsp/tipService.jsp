<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script charset="UTF-8" src="<c:url value="/resources/assets/js/tipService.js" />"></script>
        <div class="wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <h2 id="tipServiceTitle"></h2>
                    </div>
                </div>
                <div class="row">
                	<br>
                </div>
                <div class="row">
                    <div class="col-sm-12">
						<div class="card-box">
								<div class="row">
									<div class="col-sm-4">
										<label for="txtContainer">Contenedor (*)</label>
										<input type="text" name="txtContainer" id="txtContainer" class="form-control" maxlength="11" style="text-transform: uppercase;" />
									</div>
									<div class="col-sm-4 optionalView">
										<label for="cbService">Servicio (*)</label>
										<select id="cbService" class="form-control" name="cbService"  
										        data-bind="options:tipViewModel.serviceCb, optionsText:'description' , optionsValue:'key', optionsCaption: '--'">
										</select>
									</div>
									<div class="col-sm-4">
										<label for="txtEmail">Email para recibir notificaciones(*)</label>
										<input type="text" name="txtEmail" id="txtEmail" class="form-control" maxlength="50" style="text-transform: uppercase;" value="<c:out value="${userEmail}"/>"/>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<br />
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12" align="center">
										<button type="button" class="button button--primary" id="btnSaveTipService">Guardar</button>&nbsp;&nbsp;
										<button type="button" class="button button--success" id="btnClean">Limpiar</button>
									</div>
								</div>
						</div>
                    </div>
                </div>
                <!-- end row -->
            </div> <!-- end container -->