<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="viewUpdateUnitWeightModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title" id="modalTitle" id="txtUpdateWeightTitle">Actualizar peso</h4>
				<button type="button" class="close" data-dismiss="modal">�</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-6">
						<label for="txtUnitTareUpdate">Tara (kg):</label>
						<input type="number" name="txtUnitTareUpdate" id="txtUnitTareUpdate" class="form-control"/>
					</div>
					<div class="col-sm-6">
						<label for="txtUnitContentWtUpdate"> Peso Mercader�a (kg):</label> 
						<input type="number" name="txtUnitContentWtUpdate" id="txtUnitContentWtUpdate" class="form-control"/>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<br>
						<small><em>Por favor actualice el peso de su mercader�a de acuerdo a lo declarado en Aduana.<br>
					 	En caso de estar err�neo al momento del ingreso, se modificar� el mismo y se adicionar�n extra costos.</em></small>
					</div>					
				</div>
			</div>
            <!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="btnSaveUnitWeight">Guardar</button>
				<button type="button" class="btn btn-dark" id="btnCancelUpdateUnitWeight" data-dismiss="modal">Cancelar</button>
			</div>
		</div>
	</div>
</div>	