/**
 * Author: Jimena Chavez
 *
 */

$(document).ready(function () {
  initHtmlElements();
  initKnockout();
});

function initKnockout() {
  roleViewModel = {
    privilege: ko.observableArray([]),
    createdRole: ko.observableArray([]),
    rolePrivilege: ko.observableArray([]),
    editRole: function (roleToEdit) {
      editRoleIntern(roleToEdit);
    },
    deleteRole: function (roleToDelete) {
      deleteRoleIntern(roleToDelete);
    },
    recoverRole: function (roleTorecover) {
      recoverRoleIntern(roleTorecover);
    },
  };
  ko.applyBindings(roleViewModel);
}

function initHtmlElements() {
  initInputs();
  initButtons();
  initDateTimePickers();
  initCombos();
  initModal();
  initCheckBoxs();
}

function initInputs() {
  $("#txtSearchRoleId").focus();
}

function initButtons() {
  $("#btnSearch").click(function () {
    $("#ckCheckAll").prop("checked", false);
    var errorMsg = validateSearchFields();
    if (errorMsg == "") {
      var searchParam = {
        prId: $("#txtSearchRoleId").val(),
        prDescription: $("#txtSearchRoleDesc").val(),
        prPrivilegeGkeyStr: $("#cbSearchPrivilege option:selected").val(),
        prFromDateStr: $("#txtSearchFromDate").val(),
        prToDateStr: $("#txtSearchToDate").val(),
        prLifeCycleState: $("#cbSearchRoleStatus option:selected").val(),
      };
      $.ajax({
        type: "GET",
        url: url_application + "/RoleManagement/findRoleByParam/",
        data: searchParam,
        success: function (data, statusCode) {
          showCreatedRole(data);
        },
        fail: function (data, statusCode) {
          showErrorMsg("Status code:" + statusCode);
        },
      });
    } else showErrorMsg(errorMsg);
  });

  $("#btnAddRole").click(function () {
    loadModalPrivilege();
    $("#modalTitle").text("Agregar Rol");
    $("#addEditRoleModal").modal();
  });

  $("#btnClean").click(function () {
    cleanSearchParams();
  });

  $("#btnSaveRole").click(function () {
    addOrUpdateRole();
  });

  $("#btnDeleteSelectedRole").click(function () {
    deleteSelectedRole();
  });

  $("#btnRecoverSelectedRole").click(function () {
    recoverSelectedRole();
  });
}

function initDateTimePickers() {
  $("#txtSearchFromDate").datepicker({ dateFormat: "yy-mm-dd" });
  $("#txtSearchToDate").datepicker({ dateFormat: "yy-mm-dd" });
}

function initCombos() {
  loadPrivilegeCb();
}

function initModal() {
  $("#addEditRoleModal").on("shown.bs.modal", function () {
    adjustDataTableColumns();
    $("#txtRoleId").focus();
  });

  $("#addEditRoleModal").on("hidden.bs.modal", function () {
    cleanAddEditFormParams();
    adjustDataTableColumns();
  });
}

function initCheckBoxs() {
  $("#ckCheckAll").change(function () {
    var check = $(this).is(":checked");
    var canBeSelected = $(".selectable:enabled");
    canBeSelected.each(function () {
      if (check) $(this).prop("checked", true);
      else $(this).prop("checked", false);
    });
  });

  $("#ckCheckAllPrivilege").change(function () {
    var check = $(this).is(":checked");
    var canBeSelected = $(".selectablePrivilege:enabled");
    canBeSelected.each(function () {
      if (check) $(this).prop("checked", true);
      else $(this).prop("checked", false);
    });
  });
}

function addOrUpdateRole() {
  var errorMsg = validateFormFields();
  if (errorMsg == "") {
    var updateParam = {
      prGkey: $("#txtRoleGkey").val(),
      prId: $("#txtRoleId").val(),
      prDescription: $("#txtRoleDesc").val(),
      prPrivilegeGkeyStr: getSelectedPrivilegeStr(),
    };
    console.log("updateParam:" + JSON.stringify(updateParam));
    $.ajax({
      type: "POST",
      url: url_application + "/RoleManagement/addOrUpdateRole/",
      data: updateParam,
      success: function (result, statusCode) {
        var msg = result["msg"];
        if (resultIsOk(result)) {
          showSuccessMsg("Listo!", msg);
          $("#btnCancelSaveRole").click();
          refreshSearchData();
        } else showErrorMsg(msg);
      },
      fail: function (result, statusCode) {
        showErrorMsg(
          statusCode +
            "Se produjo un error guardando el menú. Por favor intente nuevamente",
        );
      },
    });
  } else {
    showErrorMsg(errorMsg);
    $("#txtRoleId").focus();
  }
}

function editRoleIntern(roleToEdit) {
  var param = {
    prRoleGkey: roleToEdit.gkey,
  };
  $.ajax({
    type: "GET",
    contentType: "application/json",
    url: url_application + "/RoleManagement/getRole/",
    data: param,
    success: function (data, statusCode) {
      fillRoleInputOnModal(data);
    },
    fail: function (data, statusCode) {
      showErrorMsg(data);
      $("#btnCancelSaveRole").click();
    },
  });
}

function deleteRoleIntern(roleToDelete) {
  roleToDelete = deleteDtoAttributes(roleToDelete);
  console.log("roleToDelete:" + JSON.stringify(roleToDelete));
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/RoleManagement/deleteRole/",
    data: JSON.stringify(roleToDelete),
    success: function (result, statusCode) {
      var msg = result["msg"];
      if (resultIsOk(result)) {
        showSuccessMsg("Listo!", msg);
        refreshSearchData();
      } else showErrorMsg(msg);
    },
    fail: function (result, statusCode) {
      showErrorMsg(
        statusCode +
          " Se produjo un error eliminando el rol. Por favor intente nuevamente",
      );
    },
  });
}

function recoverRoleIntern(roleTorecover) {
  roleTorecover = deleteDtoAttributes(roleTorecover);
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/RoleManagement/recoverRole/",
    data: JSON.stringify(roleTorecover),
    success: function (result, statusCode) {
      var msg = result["msg"];
      if (resultIsOk(result)) {
        showSuccessMsg("Listo!", msg);
        refreshSearchData();
      } else showErrorMsg(msg);
    },
    fail: function (result, statusCode) {
      showErrorMsg(
        statusCode +
          " Se produjo un error activando el rol. Por favor intente nuevamente",
      );
    },
  });
}

function deleteSelectedRole() {
  var selectedRoleJson = getSelectedRoleJson();
  if (selectedRoleJson != null && selectedRoleJson != "") {
    $.ajax({
      type: "POST",
      contentType: "application/json",
      url: url_application + "/RoleManagement/deleteSelectedRole/",
      data: selectedRoleJson,
      success: function (result, statusCode) {
        var msg = result["msg"];
        if (resultIsOk(result)) {
          showSuccessMsg("Listo!", msg);
          refreshSearchData();
        } else showErrorMsg(msg);
      },
      fail: function (result, statusCode) {
        showErrorMsg(
          statusCode +
            " Se produjo un error eliminando los roles. Por favor intente nuevamente",
        );
      },
    });
  }
}

function loadPrivilegeCb() {
  $.ajax({
    type: "GET",
    url: url_application + "/RoleManagement/getPrivilegeList/",
    success: function (data, statusCode) {
      loadSearchPrivilegeCb(data);
    },
    fail: function (data, statusCode) {
      showModal("Error", "Status code:" + statusCode, "error");
    },
  });
}

function recoverSelectedRole() {
  var selectedRoleJson = getSelectedRoleJson();
  if (selectedRoleJson != null && selectedRoleJson != "") {
    $.ajax({
      type: "POST",
      contentType: "application/json",
      url: url_application + "/RoleManagement/recoverSelectedRole/",
      data: selectedRoleJson,
      success: function (result, statusCode) {
        var msg = result["msg"];
        if (resultIsOk(result)) {
          showSuccessMsg("Listo!", msg);
          refreshSearchData();
        } else showErrorMsg(msg);
      },
      fail: function (result, statusCode) {
        showErrorMsg(
          statusCode +
            " Se produjo un error activando los roles. Por favor intente nuevamente",
        );
      },
    });
  }
}

function loadModalPrivilege() {
  $.ajax({
    type: "GET",
    url: url_application + "/RoleManagement/getPrivilegeDtoList/",
    success: function (data, statusCode) {
      showRolePrivielege(data);
    },
    fail: function (data, statusCode) {
      showModal("Error", "Status code:" + statusCode, "error");
    },
  });
}

function showCreatedRole(data) {
  roleViewModel.createdRole.removeAll();
  deleteDataTables();
  roleViewModel.createdRole(data);
  $("#tbCreatedRole").DataTable({
    bDestroy: true,
    bFilter: false,
    sScrollX: true,
    sScrollY: "30vh",
    scrollCollapse: true,
    paging: false,
    bPaginate: false,
    bFilter: false,
    bSort: false,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
    },
  });
}

function showRolePrivielege(data) {
  roleViewModel.rolePrivilege.removeAll();
  roleViewModel.rolePrivilege(data);
  $("#tbRolePrivilege").DataTable({
    bDestroy: true,
    bFilter: false,
    sScrollX: false,
    sScrollY: "30vh",
    scrollCollapse: true,
    paging: false,
    bPaginate: false,
    bSort: true,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
      sSearch: "Buscar",
    },
  });
}

function refreshSearchData() {
  $("#btnSearch").click();
}

function loadSearchPrivilegeCb(data) {
  roleViewModel.privilege(data);
}

function getRoleDataFromModal() {
  var roleData = {};
  roleData["id"] = $("#txtRoleId").val();
  roleData["description"] = $("#txtRoleDesc").val();
  var roleGkey = $("#txtRoleGkey").val();
  if (roleGkey != null && roleGkey != "") roleData["gkey"] = roleGkey;
  return roleData;
}

function isEditingRole() {
  var roleGkey = $("#txtRoleGkey").val();
  return roleGkey != null && roleGkey != "";
}

function fillRoleInputOnModal(data) {
  $("#modalTitle").text("Editar Role");
  $("#txtRoleGkey").val(data["gkey"]);
  $("#txtRoleId").val(data["id"]);
  $("#txtRoleDesc").val(data["description"]);
  var privilegeList = data["privilegeList"];
  showRolePrivielege(privilegeList);
  $("#addEditRoleModal").modal();
}

function deleteDtoAttributes(roleDto) {
  delete roleDto["createdStr"];
  delete roleDto["changedStr"];
  delete roleDto["isActive"];
  delete roleDto["privilegeList"];
  return roleDto;
}

function validateSearchFields() {
  var id = $("#txtSearchRoleId").val();
  var description = $("#txtSearchRoleDesc").val();
  var errorMsg = "";
  if (!isValidStr(id) || !isValidStr(description))
    errorMsg = "Por favor elimine los caracteres especiales de la búsqueda";
  return errorMsg;
}

function validateFormFields() {
  var id = $("#txtRoleId").val();
  var description = $("#txtRoleDesc").val();
  var errorMsg = "";
  if (!isValidStr(id) || !isValidStr(description))
    errorMsg = "Los campos no pueden contener caracteres especiales";
  else if (id == "" || id.length < 3)
    errorMsg =
      "El campo Nombre es obligatorio y debe tener como mínimo 3 caracteres de longitud";
  return errorMsg;
}

function getSelectedRoleJson() {
  var selectedRoles = $(".selectable:checked");
  if (selectedRoles.length == 0) {
    showWarningMsg("Alerta", "Debe seleccionar al menos un registro");
    return "";
  } else return JSON.stringify(getRoleToDeleteOrRecover(selectedRoles));
}

function getRoleToDeleteOrRecover(selectedRoles) {
  var gkeyArray = [];
  if (selectedRoles != null) {
    selectedRoles.each(function () {
      var selectedRole = $(this);
      console.log("selectedRole:" + JSON.stringify(selectedRole));
      var gkey = selectedRole.attr("gkey");
      if (gkey != null && gkey != "") gkeyArray.push(gkey);
    });
  }
  return gkeyArray;
}

function getSelectedPrivilegeStr() {
  var selectedPrivileges = $(".selectablePrivilege:checked");
  return getRolePrivilegeToUpdate(selectedPrivileges);
}

function getRolePrivilegeToUpdate(selectedPrivileges) {
  var gkeyStr = "";
  if (selectedPrivileges != null) {
    selectedPrivileges.each(function () {
      var selectedPrivilege = $(this);
      var gkey = selectedPrivilege.attr("gkey");
      if (gkey != null && gkey != "") {
        gkeyStr += gkeyStr != "" ? "," : "";
        gkeyStr += gkey;
      }
    });
  }
  return gkeyStr;
}

function cleanSearchParams() {
  $("#txtSearchRoleId").val("");
  $("#txtSearchRoleDesc").val("");
  $("#cbSearchPrivilege").val("");
  $("#txtSearchFromDate").val("");
  $("#txtSearchToDate").val("");
  $("#cbSearchRoleStatus").val("");
  $("#txtSearchRoleId").focus();
  deleteDataTables();
}

function cleanAddEditFormParams() {
  $("#txtRoleId").val("");
  $("#txtRoleDesc").val("");
  deleteRolePrivilegeDataTable();
  $("#txtRoleGkey").val("");
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function deleteRolePrivilegeDataTable() {
  $("#tbRolePrivilege").DataTable().clear();
  $("#tbRolePrivilege").DataTable().destroy();
}

function adjustDataTableColumns() {
  $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
}

function resultIsOk(result) {
  var status = result["status"];
  return status == "OK";
}

function isValidStr(str) {
  return !/[~`!#$%\^&*+=\\[\]\\';,/{}|\\":<>\?]/g.test(str);
}
