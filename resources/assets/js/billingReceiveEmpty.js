/**
 * Author: Jimena Chavez
 *
 */

$(document).ready(function () {
  initHtmlElements();
  initKnockout();
  var unitDto = {};
  var storageUnitMap = {};
});

function initKnockout() {
  draftViewModel = {
    receiveEmptyTable: ko.observableArray([]),
  };
  ko.applyBindings(draftViewModel);
}

function initHtmlElements() {
  initInputs();
  initButtons();
  initCheckBoxs();
  //initCombos();
  //initApptModal();
}

function initInputs() {
  $("#txtSearchUnitId").focus();
}

function initButtons() {
  $("#btnSearch").click(function () {
    findUnit(true);
  });

  $("#btnGenerateDraft").click(function () {
    generateDraft();
  });

  $("#btnClean").click(function () {
    cleanSearchParams();
  });
}

function initCheckBoxs() {
  $("#ckCheckAll").change(function () {
    var check = $(this).is(":checked");
    var canBeSelected = $(".selectable:enabled");
    canBeSelected.each(function () {
      if (check) $(this).prop("checked", true);
      else $(this).prop("checked", false);
    });
  });
}

function findUnit(showProcessing) {
  $("#ckCheckAll").prop("checked", false);
  var errorMsg = validateSearchFields();
  if (errorMsg == "") {
    if (showProcessing) showProcessingModal();
    deleteReceiveEmptyDataTable();
    var searchParam = {
      prUnitId: $("#txtSearchUnitId").val(),
    };
    $.ajax({
      type: "GET",
      url: url_application + "/Billing/findReceiveEmptyUnit/",
      data: searchParam,
      success: function (data, statusCode) {
        var msg = data["msg"];
        if (typeof msg != "undefined" && !resultIsOk(data)) {
          showErrorMsg(msg);
        } else {
          showReceiveEmptyTable(data);
          if (showProcessing) closeProcessingModal();
        }
      },
      fail: function (data, statusCode) {
        s;
        showErrorMsg("Status code:" + statusCode);
      },
    });
  } else showErrorMsg(errorMsg);
}

function generateDraft() {
  var draftParam = buildDraftParam();
  if (draftParam != null && !jQuery.isEmptyObject(draftParam)) {
    showProcessingModal();
    $.ajax({
      type: "POST",
      contentType: "application/json",
      url: url_application + "/Invoice/generateReceiveEmptyDraft/",
      data: JSON.stringify(draftParam),
      success: function (data) {
        closeProcessingModal();
        if (data.status == "OK") {
          var url = url_application + "/Payment/paymentSelector/" + data.msg;
          showModalHtml(
            "",
            "<div id='divPdf'><div style='display: none;' id='divSpinner' class='spinner-border text-primary m-2' role='status'><span class='sr-only'>Loading...</span>" +
              "</div><button type='button' onclick='toggleSpinner(this);' id='btnDownload' invNbr='" +
              data.msg +
              "' class='btn btn-info'>DESCARGAR PDF - " +
              data.msg +
              "</button></div><hr>",
            "success",
            url,
          );
        } else {
          Swal.fire("", data, "error");
        }
      },
      fail: function (data) {
        closeProcessingModal();
        Swal.close();
        showModal("Request failed", jqXHR.responseText, "error");
      },
    });
  }
}

function showReceiveEmptyTable(data) {
  draftViewModel.receiveEmptyTable.removeAll();
  deleteReceiveEmptyDataTable();
  draftViewModel.receiveEmptyTable(data);
  var table = $("#tbReceiveEmptyUnit").DataTable({
    bDestroy: true,
    bFilter: true,
    sScrollX: true,
    sScrollY: "30vh",
    scrollCollapse: true,
    paging: true,
    bPaginate: true,
    bSort: true,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
    },
  });
}

function refreshSearchData() {
  findUnit(false);
}

function validateSearchFields() {
  var searchUnitId = $("#txtSearchUnitId").val();
  var errorMsg = "";
  if (searchUnitId == null || searchUnitId == "") {
    errorMsg = "Por favor ingrese el campo obligatorio (*) para la búsqueda";
  } else if (!isValidStr(searchUnitId))
    errorMsg =
      "La búsqueda sólo permite número de contenedor separado por comas. Por favor eliminá los caracteres especiales";
  return errorMsg;
}

function cleanSearchParams() {
  $("#txtSearchUnitId").val("");
  deleteDataTables();
  $("#txtSearchUnitId").focus();
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function deleteReceiveEmptyDataTable() {
  $("#tbReceiveEmptyUnit").DataTable().clear();
  $("#tbReceiveEmptyUnit").DataTable().destroy();
}

function resultIsOk(result) {
  var status = result["status"];
  return status == "OK";
}

function resultIsError(result) {
  var status = result["status"];
  return status == "ERROR";
}

function resultRequiresConfirm(result) {
  var status = result["status"];
  return status == "QUESTION";
}

function resultIsWarning(result) {
  var status = result["status"];
  return status == "WARNING";
}

function adjustDataTableColumns() {
  $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
}

function buildDraftParam() {
  var draftParam = {};
  var selectedUnitArray = getSelectedUnitArray();
  if (selectedUnitArray != null && selectedUnitArray.length > 0) {
    draftParam["unitId"] = selectedUnitArray;
  }
  return draftParam;
}

function getSelectedUnitArray() {
  var selectedUnits = $(".selectable:checked");
  if (selectedUnits.length == 0) {
    showWarningMsg("Debe seleccionar al menos un registro");
    return "";
  } else return getUnitIdArray(selectedUnits);
}

function getUnitIdArray(selectedUnits) {
  var idArray = [];
  if (selectedUnits != null) {
    selectedUnits.each(function () {
      var selectedUnit = $(this);
      var unitId = selectedUnit.attr("unitId");
      if (unitId != null && unitId != "") idArray.push(unitId);
    });
  }
  return idArray;
}

function isValidStr(str) {
  return !/[~`!#$%\^&*+=\\[\]\\';/{}|\\":<>\?]/g.test(str);
}

function toggleSpinner(input) {
  //Reemplazamos el boton de descarga por el spinner
  if ($("#btnDownload").is(":visible")) {
    $("#btnDownload").hide();
    $("#divSpinner").show();
    getPdfByDraftId(input.getAttribute("invNbr"));
  } else {
    $("#btnDownload").show();
    $("#divSpinner").hide();
  }
}

function getPdfByDraftId(draft) {
  //processingModal();

  if (draft == null || draft == "" || draft == undefined) {
    Swal.fire("", "Error al obtener documento. Reintente nuevamente.", "error");
    return;
  }

  var request = $.ajax({
    type: "POST",
    enctype: "multipart/form-data",
    url: url_application + "/Invoice/getPdfByDraftId/" + draft,
    timeout: 600000,
  });

  request.done(function (data) {
    $("#btnDownload").show();
    $("#divSpinner").hide();
    if (data == null || data == "") {
      Swal.fire("", "Sin resultados", "info");
      return;
    } else {
      data = data.replace(
        "http://10.54.1.93:11080",
        "https://apps.apmterminals.com.ar",
      );
      var win = window.open(data, "_blank");
      if (win) {
        //Browser has allowed it to be opened
        win.focus();
      } else {
        //Browser has blocked it
        showModal("", "No se pudo abrir el PDF", "error");
      }
    }
    //Swal.close();
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", jqXHR.responseText, "error");
  });
}

function showModalHtml(title, text, type, callbackUrl) {
  Swal.fire({
    allowOutsideClick: false,
    title: title,
    html: text,
    type: type,
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "S&iacute;, pagar!",
    cancelButtonText: "No, gracias.",
  }).then((result) => {
    if (result.dismiss != "cancel") {
      window.location.href = callbackUrl;
    } else {
      window.location.reload();
    }
  });
}
