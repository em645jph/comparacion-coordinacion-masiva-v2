/**
 * Author: Jimena Chavez
 *
 */

$(document).ready(function () {
  initHtmlElements();
  initKnockout();
  var calendarData = {};
});

function initKnockout() {
  agpAuditViewModel = {
    categoryCb: ko.observableArray([]),
    agpAuditTable: ko.observableArray([]),
    unitServiceTable: ko.observableArray([]),
    viewUnitCalendar: function (unitToShow) {
      viewUnitCalendarIntern(unitToShow);
    },
  };
  ko.applyBindings(agpAuditViewModel);
}

function initHtmlElements() {
  initInputs();
  initButtons();
  initCombos();
  initDateTimePickers();
  initModal();
}

function initInputs() {
  $("#txtSearchKeyParameter").focus();
}

function initButtons() {
  $("#btnSearchAudit").click(function () {
    findAgpAudit(true);
  });

  $("#btnCleanSearchAudit").click(function () {
    cleanSearchParams();
  });
}

function initCombos() {
  loadInitCombos();
}

function initDateTimePickers() {
  $("#txtSearchFromDate").datepicker({ dateFormat: "yy-mm-dd" });
  $("#txtSearchToDate").datepicker({ dateFormat: "yy-mm-dd" });
}

function initModal() {
  $("#viewAgpUnitCalendarModal").on("shown.bs.modal", function () {
    initCalendar();
    $("#calendar").fullCalendar("render");
  });

  $("#viewAgpUnitCalendarModal").on("hidden.bs.modal", function () {
    adjustDataTableColumns();
    cleanModalInputs();
  });
}

function loadInitCombos() {
  $.ajax({
    type: "GET",
    url: url_application + "/AgpAudit/loadInitCombos/",
    success: function (data, statusCode) {
      loadSearchCategoryCb(data);
    },
    fail: function (data, statusCode) {
      showModal("Error", "Status code:" + statusCode, "error");
    },
  });
}

function findAgpAudit(showProcessing) {
  var errorMsg = validateSearchFields();
  if (errorMsg == "") {
    if (showProcessing) showProcessingModal();
    deleteAgpAuditDataTable();
    var unitCategory = $("#cbSearchCategory option:selected").val();
    var searchParam = {
      prUnitId: $("#txtSearchUnitId").val(),
      prUnitCategory: unitCategory,
      prVisitId: $("#txtSearchVisitId").val(),
      prCreator: $("#txtSearchCreator").val(),
      prFromDateStr: $("#txtSearchFromDate").val(),
      prToDateStr: $("#txtSearchToDate").val(),
    };
    console.log("searchParam:" + JSON.stringify(searchParam));
    $.ajax({
      type: "GET",
      url: url_application + "/AgpAudit/findAgpAuditByParam/",
      data: searchParam,
      success: function (data, statusCode) {
        showAgpAuditTable(data, unitCategory);
        if (showProcessing) closeProcessingModal();
      },
      fail: function (data, statusCode) {
        s;
        showErrorMsg("Status code:" + statusCode);
      },
    });
  } else showErrorMsg(errorMsg);
}

function viewUnitCalendarIntern(unitToShow) {
  showProcessingModal();
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/CoordinationManagement/getUnitCalendar/",
    data: JSON.stringify(unitToShow),
    success: function (data) {
      var msg = data["msg"];
      if (resultIsError(data)) {
        showErrorMsg(msg);
      } else {
        console.log("data is not error");
        fillInputModal(unitToShow);
        calendarData = data;
        closeProcessingModal();
        $("#viewAgpUnitCalendarModal").modal();
      }
    },
    fail: function (data, statusCode) {
      closeProcessingModal();
      showErrorMsg(
        statusCode +
          " Se produjo un error obteniendo información del contenedor. Por favor intente nuevamente",
      );
    },
  });
}

function showAgpAuditTable(data, unitCategory) {
  agpAuditViewModel.agpAuditTable.removeAll();
  deleteAgpAuditDataTable();
  agpAuditViewModel.agpAuditTable(data);
  var table = $("#tbAgpAudit").DataTable({
    bDestroy: true,
    bFilter: true,
    sScrollX: true,
    sScrollY: "30vh",
    scrollCollapse: true,
    paging: false,
    bPaginate: false,
    bSort: true,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
    },
  });
  var limitDateColumnName = getLimitDateColumnName(unitCategory);
  $(table.column(4).header()).text(limitDateColumnName);
  adjustDataTableColumns();
}

function initCalendar() {
  var minDate = calendarData["minDate"];
  var maxDate = calendarData["maxDate"];
  $("#calendar").fullCalendar({
    header: {
      left: "prev",
      center: "title",
      right: "next",
      ignoreTimezone: true,
    },
    defaultDate: minDate,
    defaultView: "month",
    monthNames: [
      "Enero",
      "Febrero",
      "Marzo",
      "Abril",
      "Mayo",
      "Junio",
      "Julio",
      "Agosto",
      "Septiembre",
      "Octubre",
      "Noviembre",
      "Diciembre",
    ],
    dayNamesShort: ["D", "L", "M", "X", "J", "V", "S"],
    viewRender: function (view) {
      if (view.end < minDate) {
        $("#calendar").fullCalendar("gotoDate", minDate);
      } else if (view.start > maxDate) {
        $("#calendar").fullCalendar("gotoDate", maxDate);
      }
    },
    events: function (start, end, timezone, callback) {
      var events = [];
      calendarData["calendarEventList"].forEach(function (opening) {
        events.push({
          id: opening["id"],
          title: opening["title"],
          start: opening["dateStr"],
          color: opening["color"],
        });
      });
      callback(events);
    },
    eventClick: function (event, jsEvent, view) {
      // when some one click on any event
    },
  });
}

function refreshSearchData() {
  findAgpAudit(false);
}

function loadSearchCategoryCb(data) {
  agpAuditViewModel.categoryCb(data["categoryList"]);
}

function fillInputModal(unitToShow) {
  console.log("filling input modal");
  $("#txtUnitId").val(unitToShow["unitId"]);
  $("#txtLimitDate").val(unitToShow["limitDateStr"]);
  var limitDateLabel = getLimitDateColumnName(unitToShow["unitCategory"]);
  $("label[for='txtLimitDate']").text(limitDateLabel);
  console.log("opening modal");
}

function validateSearchFields() {
  var searchUnitId = $("#txtSearchUnitId").val();
  var searchVisitId = $("#txtSearchVisitId").val();
  var searchCreator = $("#txtSearchCreator").val();
  var errorMsg = "";
  if (
    !isValidStr(searchUnitId) ||
    !isValidStr(searchVisitId) ||
    !isValidStr(searchCreator)
  )
    errorMsg = "Por favor elimine los caracteres especiales de la búsqueda";
  return errorMsg;
}

function cleanSearchParams() {
  $("#txtSearchUnitId").val("");
  $("#txtSearchVisitId").val("");
  $("#cbSearchCategory").val("");
  $("#txtSearchCreator").val("");
  $("#txtSearchFromDate").val("");
  $("#txtSearchToDate").val("");
  deleteDataTables();
  $("#txtSearchUnitId").focus();
}

function cleanModalInputs() {
  $("#txtUnitId").val("");
  $("#txtLimitDate").val("");
  $("#calendar").fullCalendar("destroy");
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function deleteAgpAuditDataTable() {
  $("#tbAgpAudit").DataTable().clear();
  $("#tbAgpAudit").DataTable().destroy();
}

function getLimitDateColumnName(unitCategory) {
  return unitCategory == "EXPRT"
    ? "Cutoff"
    : unitCategory == "IMPRT"
      ? "Forzoso"
      : unitCategory == "STRGE"
        ? "Fecha Devolución"
        : "Forzoso";
}

function resultIsOk(result) {
  var status = result["status"];
  return status == "OK";
}

function resultIsError(result) {
  var status = result["status"];
  return status == "ERROR";
}

function resultRequiresConfirm(result) {
  var status = result["status"];
  return status == "QUESTION";
}

function resultIsWarning(result) {
  var status = result["status"];
  return status == "WARNING";
}

function adjustDataTableColumns() {
  $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
}

function isValidStr(str) {
  return !/[~`!#$%\^&*+=\\[\]\\';,/{}|\\":<>\?]/g.test(str);
}
