/**
 * Author: Lucas Juarez
 * APM Terminals Buenos Aires
 */

function checkextension(id) {
  var file = document.querySelector("#" + id);
  var fileSize = file.files[0].size; // / 1024 / 1024

  if (file.files.length == 0) {
    showModal("", "Sin archivos cargados.", "warning");
    return true;
  }
  if (/\.(pdf)$/i.test(file.files[0].name) === false) {
    showModal(
      "",
      "Archivo adjuntado para " + file.getAttribute("head") + " debe ser PDF.",
      "warning",
    );
    $("#" + file.name).val("");
    $("#" + file.name).attr("style", "background-color:orangered;color:white");
    return true;
  }
  if (fileSize > 5000000) {
    showModal(
      "",
      "Archivo " + file.getAttribute("head") + " supera 5MB.",
      "warning",
    );
    return true;
  } else {
    return false;
  }
}

function checkextensionbyform(formId) {
  var inputs = $("#" + formId + " input[type='file']");
  var i = 0;
  var error = false;
  while (i < inputs.length && !error) {
    error = checkextension(inputs[i].getAttribute("id"));
    i++;
  }

  /*$("#" + formId + " input[type='file']").each(function(){
		var resp = checkextension( $(this).attr('id') );
		if(error)
			return true;
	});*/

  return error;
}
