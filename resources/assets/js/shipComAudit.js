/**
 * Author: Jimena Chavez
 *
 */

$(document).ready(function () {
  initKnockout();
  initHtmlElements();
});

function initKnockout() {
  shipComAuditViewModel = {
    auditList: ko.observableArray([]),
    eventTypeList: ko.observableArray([]),
  };
  ko.applyBindings(shipComAuditViewModel);
}

function initHtmlElements() {
  initCombos();
  initInputs();
  initButtons();
  initDateTimePickers();
}

function initInputs() {
  $(".sc-audit").keypress(function (event) {
    var keycode = event.keyCode ? event.keyCode : event.which;
    if (keycode == "13") {
      $("#btnSearch").click();
    }
    event.stopPropagation();
  });

  $("#txtSearchReferenceId").focus();
}

function initButtons() {
  $("#btnSearch").click(function () {
    searchShipComAudit();
  });

  $("#btnClean").click(function () {
    cleanSearchShipComAudit();
  });
}

function initDateTimePickers() {
  $("#txtSearchFromDate").datepicker({ dateFormat: "yy-mm-dd", maxDate: 1 });
  $("#txtSearchToDate").datepicker({ dateFormat: "yy-mm-dd", maxDate: 1 });
}

function initCombos() {
  loadInitCombos();
}

function loadInitCombos() {
  showProcessingModal();
  $.ajax({
    type: "GET",
    url: url_application + "/ShipComAudit/loadInitCombos/",
    success: function (data, statusCode, xhr) {
      loadEventTypeCb(data);
      closeProcessingModal();
      initInputs();
    },
    fail: function (data, statusCode) {
      showModal("Error", "Status code:" + statusCode, "error");
    },
  });
}

function searchShipComAudit() {
  var errorMsg = validateSearchFields();
  if (errorMsg == "") {
    showProcessingModal();
    var searchParam = {
      prReferenceId: $("#txtSearchReferenceId").val().toUpperCase(),
      prUnitId: $("#txtSearchUnitId").val().toUpperCase(),
      prVesselName: $("#txtSearchVesselName").val().toUpperCase(),
      prVoyage: $("#txtSearchVoyage").val().toUpperCase(),
      prEventTypeId: $("#cbSearchEventType option:selected")
        .val()
        .toUpperCase(),
      prUserId: $("#txtSearchUser").val().toUpperCase(),
      prFromDateStr: $("#txtSearchFromDate").val(),
      prToDateStr: $("#txtSearchToDate").val(),
    };
    console.log("searchParam:" + JSON.stringify(searchParam));
    $.ajax({
      type: "GET",
      data: searchParam,
      url: url_application + "/ShipComAudit/findHistoryEvents/",
      success: function (result, statusCode) {
        closeProcessingModal();
        var msg = result["msg"];
        if (typeof msg != "undefined" && !resultIsOk(result)) {
          showErrorHtmlMsg(msg);
        } else {
          showShipComAuditTable(result);
        }
      },
      fail: function (data, statusCode) {
        closeProcessingModal();
        showErrorMsg("Status code:" + statusCode);
      },
    });
  } else showErrorMsg(errorMsg);
}

function showShipComAuditTable(data) {
  shipComAuditViewModel.auditList.removeAll();
  deleteShipComAuditDataTable();
  shipComAuditViewModel.auditList(data);
  var table = $("#tbShipComAudit").DataTable({
    bDestroy: true,
    bFilter: true,
    sScrollX: true,
    sScrollY: "60vh",
    scrollCollapse: true,
    paging: true,
    bPaginate: true,
    bSort: true,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
      sSearch: "Buscar",
      sLengthMenu: "Mostrar _MENU_ registros",
    },
    pageLength: 50,
  });
}

function loadEventTypeCb(data) {
  shipComAuditViewModel.eventTypeList(data["eventTypeEnumList"]);
}

function validateSearchFields() {
  var errorMsg = validateField(
    "#txtSearchReferenceId",
    "Permiso de embarque",
    false,
  );
  if (errorMsg != "") return errorMsg;
  errorMsg = validateField("#txtSearchUnitId", "Contenedor", true);
  if (errorMsg != "") return errorMsg;
  errorMsg = validateField("#txtSearchVesselName", "Buque", false);
  if (errorMsg != "") return errorMsg;
  errorMsg = validateField("#txtSearchVoyage", "Viaje", false);
  if (errorMsg != "") return errorMsg;
  return "";
}

function validateField(htmlId, fieldName, allowComma) {
  var fieldValue = $(htmlId).val();
  var errorMsg = "";
  if (
    (allowComma && !isValidSearchStr(fieldValue)) ||
    (!allowComma && !isValidStr(fieldValue))
  )
    errorMsg = "El " + fieldName + " no puede contener caracteres especiales";
  console.log("errorMsg:" + errorMsg);
  return errorMsg;
}

function cleanSearchShipComAudit() {
  $("#txtSearchReferenceId").val("");
  $("#txtSearchUnitId").val("");
  $("#txtSearchVesselName").val("");
  $("#txtSearchVoyage").val("");
  $("#cbSearchEventType").val("");
  $("#txtSearchUser").val("");
  $("#txtSearchFromDate").val("");
  $("#txtSearchToDate").val("");
  $("#txtSearchReferenceId").focus();
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function deleteShipComAuditDataTable() {
  $("#tbShipComAudit").DataTable().clear();
  $("#tbShipComAudit").DataTable().destroy();
}

function adjustDataTableColumns() {
  $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
}

function resultIsOk(result) {
  var status = result["status"];
  return status == "OK";
}

function isValidSearchStr(str) {
  return !/[~`!#$%\^&*+=\\[\]\\';/{}|\\":<>\?]/g.test(str);
}

function isValidStr(str) {
  return !/[~`!#$%\^&*+=\\[\]\\';,/{}|\\":<>\?]/g.test(str);
}
