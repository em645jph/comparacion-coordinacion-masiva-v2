/**
 * Created by Dionel Delgado
 */

const UNBLOCK_CUSTOMS_LOAD = "UNBLOCK_CUSTOMS_LOAD";
var response = [];
$(document).ready(function () {
  initKnockout();
  unitsLocks(true);
  initApptModal();
});

function initApptModal() {
  $("#scannerUnLockModel").on("hidden.bs.modal", function () {
    $("#unit").val("");
    $("#permission").val("");
    $("#radSaveMessage").val(false);
    $("#radUnlock").val(false);
    $("#noteUnlock").val("");
    supportUnitViewModel.gkeyModal(null);
    supportUnitViewModel.unitIdModal(null);
    supportUnitViewModel.holdGkeyModal(null);
    supportUnitViewModel.obVesselModal(null);
    supportUnitViewModel.holdReferenceIdModal(null);
    supportUnitViewModel.radioSelectedOptionModal(null);
  });
}

var unitsLocks = function loadContainerBlockedByScanner(showProcessing) {
  if (showProcessing) {
    showProcessingModal();
  }
  $.ajax({
    type: "GET",
    contentType: "application/json",
    url:
      url_application +
      "/scannerGateTwoManagement/getAllContainerBlockByScanner",
    success: function (data, statusCode, xhr) {
      var ct = xhr.getResponseHeader("content-type") || "";

      if (ct.indexOf("html") > -1) {
        showModalSessionClose(
          "Session Expirada",
          "Disculpe, por seguridad la session ha sido cerrada.",
          "info",
        );
      }

      showUnitTable(data);
      if (showProcessing) closeProcessingModal();
    },
    fail: function (data, statusCode) {
      showModal("Error", "Status code:" + statusCode, "error");
    },
  });
};

function initKnockout() {
  supportUnitViewModel = {
    unitTable: ko.observableArray(),
    gkeyModal: ko.observable(),
    unitIdModal: ko.observable(),
    holdGkeyModal: ko.observable(),
    holdReferenceIdModal: ko.observable(),
    radioSelectedOptionModal: ko.observable(),
    noteModal: ko.observable(),
    obVesselModal: ko.observable(),
    viewUnlockByScannerModal: function (data) {
      viewModelUnLock(
        data.gkey,
        data.id,
        data.flagGkey,
        data.reference_id,
        data.obVesselInfo,
      );
    },
    actionUnLock: function () {
      showProcessingModal();
      unLockContainerByScanner(true, UNBLOCK_CUSTOMS_LOAD);
    },
  };
  ko.applyBindings(supportUnitViewModel);
}

function showUnitTable(data) {
  supportUnitViewModel.unitTable.removeAll();
  deleteUnitDataTable();
  supportUnitViewModel.unitTable(data);
  var table = $("#tbUnit").DataTable({
    bDestroy: true,
    bFilter: true,
    deferRender: true,
    sScrollX: true,
    sScrollY: "60vh",
    scrollCollapse: true,
    paging: true,
    bPaginate: true,
    bSort: true,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
      sInfoFiltered: " - filtrado de _MAX_ registros",
      sLengthMenu: "Mostrar _MENU_ registros",
      oPaginate: {
        sPrevious: "Anterior",
        sNext: "Siguiente",
      },
    },
    pageLength: 100,
    aoColumnDefs: [
      {
        aTargets: [2],
        bVisible: false,
      },
      {
        aTargets: [3],
        bVisible: false,
      },
      {
        aTargets: [4],
        bVisible: false,
      },
    ],
  });
}

function deleteUnitDataTable() {
  $("#tbUnit").DataTable().clear();
  $("#tbUnit").DataTable().destroy();
}

function deleteRowTable(data) {
  var table = $("#tbUnit").DataTable();
  var rows = table
    .rows("#" + data.id)
    .remove()
    .draw();
}

function viewModelUnLock(
  unitGkey,
  unitId,
  holdGkey,
  holdReferenceId,
  ob_vessel_info,
) {
  supportUnitViewModel.gkeyModal(unitGkey);
  supportUnitViewModel.unitIdModal(unitId);
  supportUnitViewModel.holdGkeyModal(holdGkey);
  supportUnitViewModel.obVesselModal(ob_vessel_info);
  supportUnitViewModel.holdReferenceIdModal(holdReferenceId);
  $("#scannerUnLockModel").modal();
}

function unLockContainerByScanner(showProcessing, action) {
  let msgText = "";
  var searchParam = {
    gkey: supportUnitViewModel.gkeyModal(),
    id: supportUnitViewModel.unitIdModal(),
    referenceId: supportUnitViewModel
      .holdReferenceIdModal()
      .substr(0, supportUnitViewModel.holdReferenceIdModal().indexOf("_")),
    action: action,
    obVessel: supportUnitViewModel.obVesselModal(),
    holdGkey: supportUnitViewModel.holdGkeyModal(),
    holdReferenceId: supportUnitViewModel.holdReferenceIdModal(),
    notes:
      supportUnitViewModel.radioSelectedOptionModal() == "OK"
        ? "ESCANEO(Completo):ESCANEO COMPLETADO."
        : "ESCANEO(ERROR):" + $("#noteUnlock").val(),
  };

  console.log("REQUEST:" + JSON.stringify(searchParam, undefined, 2));
  if (supportUnitViewModel.radioSelectedOptionModal() == "OK") {
    unlockContainerByScanner(showProcessing, searchParam, action, msgText);
  } else {
    saveNovelty(showProcessing, searchParam, action, msgText);
  }
}

function refreshViewTable(data) {
  var table = $("#tbUnit").DataTable();
  table.ajax.url(data).load();
}

var unlockContainerByScanner = function unLockContainer(
  showProcessing,
  searchParam,
  action,
  msgText,
) {
  $.ajax({
    type: "POST",
    url: url_application + "/scannerGateTwoManagement/customsUnblockScanner",
    data: JSON.stringify(searchParam),
    contentType: "application/json",
    success: function (data, statusCode, xhr) {
      var ct = xhr.getResponseHeader("content-type") || "";

      if (ct.indexOf("html") > -1) {
        showModalSessionClose(
          "Session Expirada",
          "Disculpe, por seguridad la session ha sido cerrada.",
          "info",
        );
      }

      if (data != "SUCCESS") {
        showErrorMsg(data);
      } else {
        if (action == UNBLOCK_CUSTOMS_LOAD) {
          msgText =
            "Desbloqueo del contendor " +
            supportUnitViewModel.unitIdModal() +
            " Realizado exitosamente!";
          $("#scannerUnLockModel").modal("hide");
        }
        if (showProcessing) closeProcessingModal();
        unitsLocks(false);
        $.notify(msgText, "success");
      }
    },
    fail: function (data, statusCode) {
      s;
      showErrorMsg("Status code:" + statusCode);
    },
  });
};

var saveNovelty = function noveltyNotify(
  showProcessing,
  searchParam,
  action,
  msgText,
) {
  $.ajax({
    type: "POST",
    url: url_application + "/scannerGateTwoManagement/SaveNoveltyByScanner",
    data: JSON.stringify(searchParam),
    contentType: "application/json",
    success: function (data, statusCode, xhr) {
      var ct = xhr.getResponseHeader("content-type") || "";

      if (ct.indexOf("html") > -1) {
        showModalSessionClose(
          "Session Expirada",
          "Disculpe, por seguridad la session ha sido cerrada.",
          "info",
        );
      }

      if (data != "SUCCESS") {
      } else {
        if (action == UNBLOCK_CUSTOMS_LOAD) {
          msgText =
            "Se ha agregado una novedad al contenedor " +
            supportUnitViewModel.unitIdModal() +
            " exitosamente!";
          $("#scannerUnLockModel").modal("hide");
        }
        if (showProcessing) closeProcessingModal();
        $.notify(msgText, "success");
      }
    },
    fail: function (data, statusCode) {
      s;
      showErrorMsg("Status code:" + statusCode);
    },
  });
};
