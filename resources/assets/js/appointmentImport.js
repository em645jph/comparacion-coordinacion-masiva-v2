var table;
var table2;
var loaded = false;
$(document).ready(function () {
  $("#btnFindEntity").click(function () {
    findEntityById();
  });

  $("#tbUnitsByBl,#tbUnitsByBl2 tbody").on("click", "tr", function () {
    $(this).toggleClass("selected");
  });
});

function initKnockout() {
  viewModel = {
    UnitsByBl: ko.observableArray([]),
    Units: ko.observableArray([]),
    Services: ko.observableArray([]),
  };
  ko.applyBindings(viewModel);
}

function loadUnitsCb(data) {
  showUnitsGrid(data);
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function showUnitsGrid(data) {
  if (data.units != null) {
    viewModel.UnitsByBl.removeAll();
    deleteDataTables();
    viewModel.Units(data.units);
    for (var i = 0; i < data.units.length; i++) {
      for (var j = 0; j < data.units[i].services.length; j++) {
        viewModel.Services.push(data.units[i].services[j]);
      }
    }
  }

  $("#tbUnitsByBl").DataTable({
    //		select: true,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
      sSearch: "Buscar",
      sLengthMenu: "Mostrar _MENU_ resultados",
      /*"oPaginate": {
            	"sFirst": "Primera pagina", // This is the link to the first page
            	"sPrevious": "Pagina anterior", // This is the link to the previous page
            	"sNext": "Proxima pagina", // This is the link to the next page
            	"sLast": "Ultima pagina" // This is the link to the last page
        	}*/
    },
    select: {
      style: "single",
    },
    bDestroy: true,
    sDom: '<"toolbar">frtip',
    paging: true,
    bPaginate: true,
    bFilter: true,
    bSort: true,
  });
  $("#tbUnitsByBl2").DataTable({
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
      sSearch: "Buscar",
      sLengthMenu: "Mostrar _MENU_ resultados",
      /*"oPaginate": {
            	"sFirst": "Primera pagina", // This is the link to the first page
            	"sPrevious": "Pagina anterior", // This is the link to the previous page
            	"sNext": "Proxima pagina", // This is the link to the next page
            	"sLast": "Ultima pagina" // This is the link to the last page
        	}*/
    },
    select: {
      style: "single",
    },
    bDestroy: true,
    sDom: '<"toolbar">frtip',
    paging: true,
    bPaginate: true,
    bFilter: true,
    bSort: true,
  });
  $("#tbUnitsByBl_wrapper > div.toolbar").html("<b>Coordinación Retiro</b>");
  $("#tbUnitsByBl2_wrapper > div.toolbar").html("<b>Servicios Adicionales</b>");
}

function findEntityById() {
  processingModal();

  var unitId = $("#entityid").val();
  if (unitId == "") {
    showModal(
      "Request failed",
      "Por favor, complete los campos requeridos.",
      "error",
    );
    return;
  }

  var request = $.ajax({
    url: url_application + "/Appointment/findBillOfLading/" + unitId,
    type: "GET",
  });

  request.done(function (data) {
    if (data == null || data == "") {
      Swal.fire("Sin resultados");
      return;
    }
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    $("#tbUnitsByBl,#tbUnitsByBl2,#btnModalServices").show();
    loadUnitsCb(data);
    Swal.close();
  });

  request.fail(function (jqXHR, textStatus) {
    Swal.close();
    showModal("Request failed", jqXHR.responseText, "error");
  });
}

function processingModal() {
  Swal.fire({
    title: "Procesando...",
    text: "Aguarde un instante por favor",
    imageUrl: url_application + "/resources/assets/images/ajax_loading.gif",
    showConfirmButton: false,
    allowOutsideClick: false,
  });
}

function unitCoordination(id) {
  //console.log( d );
  cleanModalCoordination();
  $("#divForUnit").append(
    "Usted va a coordinar el Contenedor " + $("#" + id).attr("unitId"),
  );
  $("#modalAppt").modal("show");
}

function cleanModalCoordination() {
  //$('#datetimepicker3').datetimepicker({value:'2019/9/29 07:00'});
  $("#divForUnit").html("");
}

function preventApptUnknown() {
  Swal.fire({
    allowOutsideClick: false,
    type: "info",
    title: "Atención",
    text:
      "Esta seguro de coordinar éste contenedor para " +
      $("#datetimepicker3").val(),
    footer: "<a href>Por qu&eacute; aparece este mensaje?</a>",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "S&iacute;, estoy seguro!",
    cancelButtonText: "Cancelar",
  }).then((result) => {
    if (result.dismiss != "cancel") {
      //Cerramos el modal de preaviso
      $("#modalAppt").modal("hide");
      showModal("Contenedor coordinado para " + $("#datetimepicker3").val());
    }
  });
}

function refreshLabelDateTimeAppt() {
  var current = $("#datetimepicker3").val();
  $("#labelDatetimeAppt").text("La fecha y hora seleccionada es " + current);
}

function showSealsByUnit(id) {
  $("#titlePermissionModal").text(
    "Precintos para el contenedor " + $("#" + id).attr("unitid"),
  );
  $("#modalSeals").modal({
    backdrop: "static",
  });
}
