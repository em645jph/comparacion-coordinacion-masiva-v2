/**
 * Author: Lucas Juarez
 * APM Terminals Buenos Aires
 */

$(document).ready(function () {
  $("#btnChangePassword").click(function (e) {
    e.preventDefault();
    changePassword();
  });
});

function showPassword(idInput, idIcon) {
  var x = document.getElementById(idInput);
  var icon = document.getElementById(idIcon);
  if (x.type === "password") {
    x.type = "text";
    icon.className = "mdi mdi-eye";
  } else {
    x.type = "password";
    icon.className = "mdi mdi-eye-off";
  }
}

function validatePasswords(obj) {
  var err = false;
  if (obj.password == null || obj.password == "") {
    showModal(
      "Atención!",
      "Por favor, completar con una clave correcta.",
      "warning",
    );
    err = true;
  }
  if (obj.confirmpassword == null || obj.confirmpassword == "") {
    showModal(
      "Atención!",
      "Por favor, completar con una clave correcta.",
      "warning",
    );
    err = true;
  }
  if (obj.password != obj.confirmpassword) {
    showModal("Atención!", "Las claves no son iguales.", "error");
    err = true;
  }
  return err;
}

function changePassword() {
  processingModal();
  var obj = new Object();
  obj.password = $("#newPassword").val();
  obj.confirmpassword = $("#confirmPassword").val();
  if (validatePasswords(obj)) {
    return;
  }

  var request = $.ajax({
    url: url_application + "/User/changePassword",
    type: "POST",
    data: JSON.stringify(obj),
    contentType: "application/json",
  });

  request.done(function (message) {
    showModalCallback(
      "Atencion!",
      message,
      "success",
      url_application + "/index",
    );
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("Atencion!", jqXHR.responseText, "error");
  });
}

function processingModal() {
  Swal.fire({
    title: "Procesando...",
    text: "Aguarde un instante por favor",
    imageUrl: url_application + "/resources/assets/images/ajax_loading.gif",
    showConfirmButton: false,
    allowOutsideClick: false,
  });
}
