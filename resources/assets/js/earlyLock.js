/**
 * Author: Jimena Chavez
 *
 */

$(document).ready(function () {
  initKnockout();
  initHtmlElements();
  var tempUser = {};
});

function initKnockout() {
  earlyLockViewModel = {
    earlyLockList: ko.observableArray([]),
    deleteEarlyLock: function (earlyLockToDelete) {
      askToDeleteEarlyLock(earlyLockToDelete);
    },
  };
  ko.applyBindings(earlyLockViewModel);
}

function initHtmlElements() {
  initInputs();
  initButtons();
  initDateTimePickers();
  initModal();
  initLoad();
}

function initLoad() {
  initLoad();
}

function initInputs() {
  $("#txtSearchReferenceId").focus();
}

function initButtons() {
  $("#btnSearch").click(function () {
    searchEarlyLock(true);
  });

  $("#btnClean").click(function () {
    cleanSearchEarlyLock();
  });

  $("#btnSaveEarlyLock").click(function () {
    addEarlyLock();
  });
}

function initDateTimePickers() {
  $("#txtSearchFromDate").datepicker({ dateFormat: "yy-mm-dd" });
  $("#txtSearchToDate").datepicker({ dateFormat: "yy-mm-dd" });
  $("#txtReported").datepicker({ dateFormat: "yy-mm-dd", maxDate: 0 });
}

function initModal() {
  $("#addEarlyLockModal").on("shown.bs.modal", function () {
    $("#txtReferenceId").focus();
  });

  $("#addEarlyLockModal").on("hide.bs.modal", function () {
    cleanAddEarlyLockModal();
  });
}

function initLoad() {
  showProcessingModal();
  $.ajax({
    type: "GET",
    url: url_application + "/CustomsEarlyLock/getActiveEarlyLock/",
    success: function (result, statusCode) {
      showEarlyLockTable(result);
      closeProcessingModal();
    },
    fail: function (result, statusCode) {
      showErrorMsg("Error:" + statusCode + " - " + result);
    },
  });
}

function searchEarlyLock(showLoading) {
  var errorMsg = validateReferenceIdField();
  if (errorMsg == "") {
    if (showLoading) showProcessingModal();
    var searchParam = {
      prReferenceId: $("#txtSearchReferenceId").val().toUpperCase(),
      prReportedFrom: $("#txtSearchFromDate").val(),
      prReportedTo: $("#txtSearchToDate").val(),
    };
    console.log("searchParam:" + JSON.stringify(searchParam));
    $.ajax({
      type: "GET",
      data: searchParam,
      url: url_application + "/CustomsEarlyLock/findEarlyLock/",
      success: function (result, statusCode) {
        if (showLoading) closeProcessingModal();
        var msg = result["msg"];
        if (typeof msg != "undefined" && !resultIsOk(result)) {
          showErrorHtmlMsg(msg);
        } else {
          showEarlyLockTable(result);
        }
      },
      fail: function (data, statusCode) {
        closeProcessingModal();
        showErrorMsg("Status code:" + statusCode);
      },
    });
  } else showErrorMsg(errorMsg);
}

function addEarlyLock() {
  var validateMsg = validateReferenceId();
  if (validateMsg == "") {
    var earlyLockParam = {
      referenceId: $("#txtReferenceId").val().toUpperCase(),
      notes: $("#txtNote").val().toUpperCase(),
      reportedStr: $("#txtReported").val(),
    };
    showProcessingModal();
    $.ajax({
      type: "POST",
      contentType: "application/json",
      url: url_application + "/CustomsEarlyLock/addEarlyLock/",
      data: JSON.stringify(earlyLockParam),
      success: function (result, statusCode) {
        closeProcessingModal();
        var msg = result["msg"];
        if (resultIsOk(result)) {
          $("#btnCancelSaveEarlyLock").click();
          showSuccessHtmlMsg("Listo!", msg);
          searchEarlyLock(false);
        } else showErrorMsg(msg);
      },
      fail: function (result, statusCode) {
        showErrorMsg(
          statusCode +
            "Se produjo un error guardando bloqueo anticipado. Intentá nuevamente",
        );
      },
    });
  } else showErrorMsg(validateMsg);
}

function deleteEarlyLockIntern(earlyLockDelete) {
  showProcessingModal();
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/CustomsEarlyLock/deleteEarlyLock/",
    data: JSON.stringify(earlyLockDelete),
    success: function (result, statusCode) {
      closeProcessingModal();
      var msg = result["msg"];
      if (resultIsOk(result)) {
        showSuccessHtmlMsg("Listo!", msg);
        searchEarlyLock(false);
      } else showErrorMsg(msg);
    },
    fail: function (result, statusCode) {
      showErrorMsg(
        statusCode +
          " Se produjo un error eliminando el bloqueo anticipado. Intentá nuevamente",
      );
    },
  });
}

function showEarlyLockTable(data) {
  earlyLockViewModel.earlyLockList.removeAll();
  deleteEarlyLockDataTable();
  earlyLockViewModel.earlyLockList(data);
  var table = $("#tbEarlyLock").DataTable({
    bDestroy: true,
    bFilter: true,
    sScrollX: true,
    sScrollY: "80vh",
    scrollCollapse: true,
    paging: true,
    bPaginate: true,
    bSort: true,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
      sSearch: "Buscar",
      sLengthMenu: "Mostrar _MENU_ registros",
    },
    order: [[3, "desc"]],
    pageLength: 50,
  });
}

function validateReferenceIdField() {
  var referenceId = $("#txtSearchReferenceId").val();
  var errorMsg = "";
  if (!isValidSearchStr(referenceId))
    errorMsg = "El permiso de embarque no puede contener caracteres especiales";
  return errorMsg;
}

function validateReferenceId() {
  var referenceId = $("#txtReferenceId").val();
  var errorMsg = "";
  if (!isValidStr(referenceId))
    errorMsg = "El permiso de embarque no puede contener caracteres especiales";
  return errorMsg;
}

function cleanSearchEarlyLock() {
  $("#txtSearchReferenceId").val("");
  $("#txtSearchFrom").val("");
  $("#txtSearchTo").val("");
  $("#txtSearchReferenceId").focus();
}

function cleanAddEarlyLockModal() {
  $("#txtReferenceId").val("");
  $("#txtNote").val("");
  $("#txtReported").val("");
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function deleteEarlyLockDataTable() {
  $("#tbEarlyLock").DataTable().clear();
  $("#tbEarlyLock").DataTable().destroy();
}

function adjustDataTableColumns() {
  $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
}

function resultIsOk(result) {
  var status = result["status"];
  return status == "OK";
}

function isValidSearchStr(str) {
  return !/[~`!#$%\^&*+=\\[\]\\';/{}|\\":<>\?]/g.test(str);
}

function isValidStr(str) {
  return !/[~`!#$%\^&*+=\\[\]\\';,/{}|\\":<>\?]/g.test(str);
}

function askToDeleteEarlyLock(earlyLockToDelete) {
  Swal.fire({
    allowOutsideClick: false,
    title: "Desea continuar?",
    text: "No podrá recuperar el registro",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: "#4fa7f3",
    cancelButtonColor: "#d57171",
    confirmButtonText: "Sí, eliminar!",
    cancelButtonText: "Cancelar",
  }).then((result) => {
    if (result.value) {
      deleteEarlyLockIntern(earlyLockToDelete);
    } else if (result.dismiss === Swal.DismissReason.cancel) {
      adjustDataTableColumns();
    }
  });
}
