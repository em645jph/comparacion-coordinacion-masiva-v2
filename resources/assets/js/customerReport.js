var loaded = false;
var reportNbr = 0;
//var tablesLoaded = false;
$(document).ready(function () {
  processingModal();
  getCustomerReports();

  $("#btnReject").click(function () {
    //showConfirmModal("Atención!", "Esta seguro de rechazar la presentación informada por el cliente?", "info", "-1" );
    $("#reasonSmallModal").modal();
  });
  $("#btnReject2").click(function () {
    showConfirmModal(
      "Atención!",
      "Esta seguro de rechazar la presentación informada por el cliente?",
      "info",
      "-1",
    );
  });

  $("#htmlToPdfCreator").click(function () {
    printPDF();
  });
});

function getCustomerReports() {
  var request = $.ajax({
    type: "POST",
    url: url_application + "/balance/getCustomerReportsDefault",
    timeout: 600000,
  });

  request.done(function (data) {
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    loadCustomerReportCb(data);
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", jqXHR.responseText, "error");
  });
}

function loadCustomerReportById(docs, items, files) {
  //viewModel.customerReportDocuments.removeAll();
  //viewModel.customerReportItems.removeAll();
  viewModel.customerReportDocuments(docs);
  viewModel.customerReportItems(items);
  viewModel.customerReportFiles(files);
  /*if(!tablesLoaded){
		setTimeout(function()
				{
			var t1 = newDataTableWithTotals("tbCustomerReportDocuments", "15vh", true, true, false, false, null, 2);
			var t2 = newDataTableWithTotals("tbCustomerReportItems", "15vh", true, true, false, false, null, 4);
				}, 1000);
		tablesLoaded = true;
	}*/
}

function loadAdditionalDataFromRow(data, event) {
  $("#modalInformation").modal();
  reportNbr = data.reportNumber.toString();
  $("#titleInformationReport,#titleInformationReport2").text(
    "Presentación #" + reportNumberLeftPad(reportNbr),
  );
  loadCustomerReportById(data.docs, data.items, data.files);
  $("#btnUpdateStatus").remove();
  $("#btnsAction").append(returnNextStage(data.status));
}

function returnNextStage(stage) {
  var nextStageBtn = "";
  if (stage == "RECHAZADA") {
    $("#btnReject").hide();
  } else {
    $("#btnReject").show();
  }
  if (stage === "RECIBIDA") {
    nextStageBtn =
      '<button type="button" class="btn btn-success" id="btnUpdateStatus" onclick="javascript:prevalidateUpdate(this.id)" style="float: left;" nextStage="2">RECEPCIONAR</button>';
  } else if (stage === "EN PROCESO") {
    nextStageBtn =
      '<button type="button" class="btn btn-success" id="btnUpdateStatus" onclick="javascript:prevalidateUpdate(this.id)" style="float: left;" nextStage="3">CONTABILIZAR</button>';
  } else {
    return null;
  }
  return nextStageBtn;
}

function prevalidateUpdate(input) {
  var nextStage = $("#" + input).attr("nextStage");
  showConfirmModal(
    "Atención!",
    "Esta seguro de actualizar la presentación informada por el cliente?",
    "info",
    nextStage,
  );
}

function reportNumberLeftPad(value) {
  return value.toString().padStart(5, "0");
}

function parseMoneyValue(value) {
  return parseFloat(value, 10)
    .toFixed(2)
    .replace(/(\d)(?=(\d{3})+\.)/g, "$1,")
    .toString();
}

function inputMoneyValue(input) {
  $("#" + input.id).val(
    parseFloat(input.value, 10)
      .toFixed(2)
      .replace(/(\d)(?=(\d{3})+\.)/g, "$1,")
      .toString(),
  );
}

function getDateTime(value) {
  var d = new Date(value);
  return d.toLocaleDateString() + " " + d.toLocaleTimeString();
}

function initKnockout() {
  viewModel = {
    customerReport: ko.observableArray([]),
    customerReportDocuments: ko.observableArray([]),
    customerReportItems: ko.observableArray([]),
    customerReportFiles: ko.observableArray([]),
  };
  ko.applyBindings(viewModel);
}

function loadCustomerReportCb(data) {
  showCustomerReportGrid(data);
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function showCustomerReportGrid(data) {
  viewModel.customerReport.removeAll();
  deleteDataTables();
  for (var int = 0; int < data.length; int++) {
    var customerFullname = data[int].customerData.customerName;
    var customerCuit = data[int].customerData.customerCuit;
    data[int].reportData.customerKey =
      customerFullname + "(" + customerCuit + ")";
    viewModel.customerReport.push(data[int].reportData);
  }
  //showModalHtml('Informacion',ko.toJSON(data),'info');
  //$("#tableBox").show();
  newDataTable("tbCustomerReport", null, true, true, true);
  //$("#tbBalance").show();
  closeProcessingModal();
}

function downloadF(file) {
  var filename = file.filename;
  var data = file.data;
  var isFirefox = typeof InstallTrigger !== "undefined";
  if (isFirefox) {
    var win = window.open(
      "data:application/x-download;base64," + data,
      "_blank",
    );
  } else {
    const linkSource = "data:application/x-download;base64," + data;
    const downloadLink = document.createElement("a");
    const fileName = filename;

    downloadLink.href = linkSource;
    downloadLink.download = fileName;
    downloadLink.click();
  }
}

function downloadFile(data, event) {
  var b64;
  var request = $.ajax({
    type: "POST",
    url: url_application + "/balance/downloadFile/" + data.gkey,
    timeout: 600000,
    async: false,
  });

  request.done(function (data) {
    downloadF(data);
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", "No se pudo descargar el archivo", "error");
    return;
  });
}

function makeid(length) {
  var result = "";
  var characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

function showConfirmModal(title, text, type, action) {
  Swal.fire({
    allowOutsideClick: false,
    title: title,
    text: text,
    type: type,
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "S&iacute;, estoy seguro!",
    cancelButtonText: "Cancelar",
  }).then((result) => {
    if (result.dismiss != "cancel") {
      processingModal();
      $("#reasonSmallModal").hide();
      var reason = $("#reasonLbl").val();
      updateStatus(reportNbr, action, reason);
      getCustomerReports();
      $("#reasonSmallModal").modal("hide");
    }
  });
}

function updateStatus(reportNumber, stage, reason) {
  if (reason == null || reason == undefined || reason == "") reason = "OK";
  var request = $.ajax({
    type: "POST",
    url:
      url_application +
      "/balance/updateReportStatus/" +
      parseInt(reportNbr) +
      "/" +
      stage +
      "/" +
      reason,
    timeout: 600000,
    async: false,
  });

  request.done(function (data) {
    showModal("Atención!", data, "success");
    $("#modalInformation").modal("hide");
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", jqXHR.responseText, "error");
  });
}

function printPDF() {
  processingModal();
  $("#titleInformationReport2").show();
  $("#htmlToPdfCreator").hide();

  var HTML_Width = $("#modalBodyReportData").width();
  var HTML_Height = $("#modalBodyReportData").height();
  var top_left_margin = 15;
  var PDF_Width = HTML_Width + top_left_margin * 2;
  var PDF_Height = PDF_Width * 1.5 + top_left_margin * 2;
  var canvas_image_width = HTML_Width;
  var canvas_image_height = HTML_Height;

  var totalPDFPages = Math.ceil(HTML_Height / PDF_Height) - 1;

  html2canvas($("#modalBodyReportData")[0], { allowTaint: true }).then(
    function (canvas) {
      canvas.getContext("2d");

      console.log(canvas.height + "  " + canvas.width);

      var imgData = canvas.toDataURL("application/pdf", 1.0);
      var pdf = new jsPDF("p", "pt", [PDF_Width, PDF_Height]);
      pdf.addImage(
        imgData,
        "JPG",
        top_left_margin,
        top_left_margin,
        canvas_image_width,
        canvas_image_height,
      );

      for (var i = 1; i <= totalPDFPages; i++) {
        pdf.addPage(PDF_Width, PDF_Height);
        pdf.addImage(
          imgData,
          "JPG",
          top_left_margin,
          -(PDF_Height * i) + top_left_margin * 4,
          canvas_image_width,
          canvas_image_height,
        );
      }

      pdf.save("PresentacionPagoNro" + reportNbr + ".pdf");
    },
  );
  $("#titleInformationReport2").hide();
  $("#htmlToPdfCreator").show();
  closeProcessingModal();
}
