/**
 * Author: Jimena Chavez
 *
 */

$(document).ready(function () {
  initHtmlElements();
  initKnockout();
  var calendarData = {};
});

function initKnockout() {
  bookingTrackViewModel = {
    yesNoCb: ko.observableArray([]),
    lineOpCb: ko.observableArray([]),
    bookingTable: ko.observableArray([]),
    bookingItemTable: ko.observableArray([]),
    bookingUnitTable: ko.observableArray([]),
    bookingHazardTable: ko.observableArray([]),
    viewBookingItem: function (bookingToView) {
      viewBookingItemIntern(bookingToView);
    },
    viewBookingUnit: function (bookingToView) {
      viewBookingUnitIntern(bookingToView);
    },
    viewBookingHazard: function (bookingToView) {
      viewBookingHazardIntern(bookingToView);
    },
  };
  ko.applyBindings(bookingTrackViewModel);
}

function initHtmlElements() {
  initButtons();
  initCombos();
  initDateTimePickers();
  initModal();
}

function initButtons() {
  $("#btnSearchBooking").click(function () {
    findBooking(true);
  });

  $("#btnCleanSearchBooking").click(function () {
    cleanSearchParams();
  });
}

function initCombos() {
  loadInitCombos();

  $("#cbSearchMsgType").change(function () {
    $("#txtSearchKeyword").focus();
  });
}

function initDateTimePickers() {
  $("#txtSearchFromDate").datepicker({ dateFormat: "yy-mm-dd" });
  $("#txtSearchToDate").datepicker({ dateFormat: "yy-mm-dd" });
}

function initModal() {
  $("#viewBookingItemModal").on("shown.bs.modal", function () {
    adjustDataTableColumns();
  });

  $("#viewBookingItemModal").on("hidden.bs.modal", function () {
    deleteBookingItemDataTable();
    adjustDataTableColumns();
  });

  $("#viewBookingUnitModal").on("shown.bs.modal", function () {
    adjustDataTableColumns();
  });

  $("#viewBookingUnitModal").on("hidden.bs.modal", function () {
    deleteBookingUnitDataTable();
    adjustDataTableColumns();
  });

  $("#viewBookingHazardModal").on("shown.bs.modal", function () {
    adjustDataTableColumns();
  });

  $("#viewBookingHazardModal").on("hidden.bs.modal", function () {
    deleteBookingHazardDataTable();
    adjustDataTableColumns();
  });
}

function loadInitCombos() {
  $.ajax({
    type: "GET",
    url: url_application + "/BookingTracking/loadInitCombos/",
    success: function (data, statusCode) {
      loadSerchYesNoCb(data);
      loadSearchLineOpCb(data);
    },
    fail: function (data, statusCode) {
      showModal("Error", "Status code:" + statusCode, "error");
    },
  });
}

function findBooking(showProcessing) {
  var errorMsg = validateSearchFields();
  if (errorMsg == "") {
    if (showProcessing) showProcessingModal();
    deleteDataTables();
    var searchParam = {
      prBookingNbr: $("#txtSearchBookingNbr").val(),
      prContainerNbr: $("#txtSearchContainerNbr").val(),
      prLineOp: $("#cbSearchLineOp option:selected").val(),
      prVesselName: $("#txtSearchVesselName").val(),
      prObVoyage: $("#txtSearchVoyage").val(),
      prPod: $("#txtSearchPod").val(),
      prReefer: $("#cbSearchReefer option:selected").val(),
      prHazardous: $("#cbSearchHazardous option:selected").val(),
      prOverrideCutoff: $("#cbSearchOverrideCutoff option:selected").val(),
    };
    $.ajax({
      type: "GET",
      url: url_application + "/BookingTracking/findBookingByParam/",
      data: searchParam,
      success: function (data, statusCode) {
        if (showProcessing) closeProcessingModal();
        if (resultIsError(data)) {
          var msg = data["msg"];
          showErrorMsg(msg);
        } else showBookingTable(data);
      },
      fail: function (data, statusCode) {
        s;
        showErrorMsg("Status code:" + statusCode);
      },
    });
  } else showErrorMsg(errorMsg);
}

function viewBookingItemIntern(bookingToShow) {
  showProcessingModal();
  var searchParam = {
    prBookingGkeyStr: bookingToShow["gkey"],
  };
  $.ajax({
    type: "GET",
    url: url_application + "/BookingTracking/getBookingItem/",
    data: searchParam,
    success: function (result, statusCode) {
      closeProcessingModal();
      showBookingItemTable(result);
      $("#viewBookingItemModal").modal();
    },
    fail: function (result, statusCode) {
      showModal("Error", "Status code:" + statusCode, "error");
    },
  });
}

function viewBookingUnitIntern(bookingToShow) {
  showProcessingModal();
  var searchParam = {
    prBookingGkeyStr: bookingToShow["gkey"],
  };
  $.ajax({
    type: "GET",
    url: url_application + "/BookingTracking/getBookingUnit/",
    data: searchParam,
    success: function (result, statusCode) {
      closeProcessingModal();
      showBookingUnitTable(result);
      $("#viewBookingUnitModal").modal();
    },
    fail: function (result, statusCode) {
      showModal("Error", "Status code:" + statusCode, "error");
    },
  });
}

function viewBookingHazardIntern(bookingToShow) {
  showProcessingModal();
  var searchParam = {
    prBookingGkeyStr: bookingToShow["gkey"],
  };
  $.ajax({
    type: "GET",
    url: url_application + "/BookingTracking/getBookingHazard/",
    data: searchParam,
    success: function (result, statusCode) {
      closeProcessingModal();
      showBookingHazardTable(result);
      $("#viewBookingHazardModal").modal();
    },
    fail: function (result, statusCode) {
      showModal("Error", "Status code:" + statusCode, "error");
    },
  });
}

function showBookingTable(data) {
  bookingTrackViewModel.bookingTable.removeAll();
  bookingTrackViewModel.bookingTable(data);
  var table = $("#tbBookingTracking").DataTable({
    bDestroy: true,
    bFilter: true,
    sScrollX: true,
    sScrollY: "30vh",
    scrollCollapse: true,
    paging: true,
    bPaginate: true,
    bSort: true,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
    },
  });
}

function showBookingItemTable(data) {
  bookingTrackViewModel.bookingItemTable.removeAll();
  bookingTrackViewModel.bookingItemTable(data);
  var bkgItemTable = $("#tbBookingItem").DataTable({
    bDestroy: true,
    bFilter: false,
    sScrollX: false,
    sScrollY: "30vh",
    scrollCollapse: true,
    paging: false,
    bPaginate: false,
    bFilter: false,
    bSort: false,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
    },
  });
}

function showBookingUnitTable(data) {
  bookingTrackViewModel.bookingUnitTable.removeAll();
  bookingTrackViewModel.bookingUnitTable(data);
  var bkgUnitTable = $("#tbBookingUnit").DataTable({
    bDestroy: true,
    bFilter: true,
    sScrollX: false,
    sScrollY: "30vh",
    scrollCollapse: true,
    paging: true,
    bPaginate: true,
    bFilter: true,
    bSort: true,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
    },
  });
}

function showBookingHazardTable(data) {
  bookingTrackViewModel.bookingHazardTable.removeAll();
  bookingTrackViewModel.bookingHazardTable(data);
  var bkghazardTable = $("#tbBookingHazard").DataTable({
    bDestroy: true,
    bFilter: false,
    sScrollX: false,
    sScrollY: "30vh",
    scrollCollapse: true,
    paging: false,
    bPaginate: false,
    bFilter: false,
    bSort: false,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
    },
  });
}

function refreshSearchData() {
  findBooking(false);
}

function loadSerchYesNoCb(data) {
  bookingTrackViewModel.yesNoCb(data["yesNoList"]);
}

function loadSearchLineOpCb(data) {
  bookingTrackViewModel.lineOpCb(data["lineOpList"]);
}

function validateSearchFields() {
  var searchBookingNbr = $("#txtSearchBookingNbr").val();
  var searchContainerNbr = $("#txtSearchContainerNbr").val();
  var searchVessel = $("#txtSearchVesselName").val();
  var searchVoyage = $("#txtSearchVoyage").val();
  var searchPod = $("#txtSearchVoyage").val();
  var errorMsg = "";
  if (
    !isValidStr(searchBookingNbr) ||
    !isValidStr(searchContainerNbr) ||
    !isValidStr(searchVessel) ||
    !isValidStr(searchVoyage) ||
    !isValidStr(searchPod)
  )
    errorMsg = "Por favor elimine los caracteres especiales de la búsqueda";
  return errorMsg;
}

function cleanSearchParams() {
  $("#txtSearchBookingNbr").val("");
  $("#txtSearchContainerNbr").val("");
  $("#cbSearchLineOp").val("");
  $("#txtSearchVesselName").val("");
  $("#txtSearchVoyage").val("");
  $("#txtSearchPod").val("");
  $("#cbSearchReefer").val("");
  $("#cbSearchHazardous").val("");
  $("#cbSearchOverrideCutoff").val("");
  deleteDataTables();
  $("#txtSearchBookingNbr").focus();
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function deleteBookingItemDataTable() {
  $("#tbBookingItem").DataTable().clear();
  $("#tbBookingItem").DataTable().destroy();
}

function deleteBookingUnitDataTable() {
  $("#tbBookingUnit").DataTable().clear();
  $("#tbBookingUnit").DataTable().destroy();
}

function deleteBookingHazardDataTable() {
  $("#tbBookingHazard").DataTable().clear();
  $("#tbBookingHazard").DataTable().destroy();
}

function resultIsOk(result) {
  var status = result["status"];
  return status == "OK";
}

function resultIsError(result) {
  var status = result["status"];
  return status == "ERROR";
}

function resultRequiresConfirm(result) {
  var status = result["status"];
  return status == "QUESTION";
}

function resultIsWarning(result) {
  var status = result["status"];
  return status == "WARNING";
}

function adjustDataTableColumns() {
  $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
}

function isValidStr(str) {
  return !/[~`!#$%\^&*+=\\[\]\\';,/{}|\\":<>\?]/g.test(str);
}
