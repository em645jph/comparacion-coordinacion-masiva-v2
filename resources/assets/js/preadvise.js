/**
 * Author: Jimena Chavez
 *
 */

var unitDto = {};

$(document).ready(function () {
  initHtmlElements();
  initKnockout();
});

function initKnockout() {
  preadviseViewModel = {
    yesNoCb: ko.observableArray([]),
    lineOpCb: ko.observableArray([]),
    bkgItemList: ko.observableArray([]),
    bkgUnitList: ko.observableArray([]),
    customsPermList: ko.observableArray([]),
    bkgGkey: ko.observable(),
    bkgNbr: ko.observable(),
    bkgLineId: ko.observable(),
    bkgVessel: ko.observable(),
    bkgVesselInfo: ko.observable(),
    bkgVisitId: ko.observable(),
    bkgCutoff: ko.observable(),
    bkgShipper: ko.observable(),
    bkgQty: ko.observable(),
    bkgPOD: ko.observable(),
    bkgIsReefer: ko.observable(),
    bkgIsHazardous: ko.observable(),
    itemGkey: ko.observable(),
    itemIsoType: ko.observable(),
    itemTemperature: ko.observable(),
    itemTare: ko.observable(),
    unitGkey: ko.observable(),
    renumberUnit: function (unitToRenumber) {
      showUnitRenumberModal(unitToRenumber);
    },
    updateUnitSeal: function (unitToUpdate) {
      showUpdateUnitSealModal(unitToUpdate);
    },
    updateUnitWeight: function (unitToUpdate) {
      showUpdateUnitWeightModal(unitToUpdate);
    },
    updateUnitOog: function (unitToUpdate) {
      showUpdateUnitOogModal(unitToUpdate);
    },
    updateUnitVgmCondition: function (unitToUpdate) {
      showUpdateUnitVgmModal(unitToUpdate);
    },
    cancelPreadvise: function (unitToDelete) {
      showCancelPreadviseMsg(unitToDelete);
    },
    viewUnitPermission: function (unitToUpdate) {
      showUpdateUnitCustomsPermModal(unitToUpdate);
    },
    cancelUnitPermission: function (permissionToCancel) {
      cancelUnitPermissionIntern(permissionToCancel);
    },
  };
  ko.applyBindings(preadviseViewModel);
}

function initHtmlElements() {
  initInputs();
  initButtons();
  initCombos();
  initDivs();
  initModals();
}

function initInputs() {
  $(".oog-div").hide();
  $("#txtSearchBooking").focus();

  $("#txtSearchBooking").keypress(function (event) {
    var keycode = event.keyCode ? event.keyCode : event.which;
    if (keycode == "13") {
      $("#btnSearch").click();
    }
    event.stopPropagation();
  });

  $("#txtUnitRenumber").keypress(function (event) {
    var keycode = event.keyCode ? event.keyCode : event.which;
    if (keycode == "13") {
      $("#btnSaveUnitRenumber").click();
    }
    event.stopPropagation();
  });

  $("#txtUnitSeal2Update").keypress(function (event) {
    var keycode = event.keyCode ? event.keyCode : event.which;
    if (keycode == "13") {
      $("#btnSaveUnitSeal").click();
    }
    event.stopPropagation();
  });

  $("#txtUnitTareUpdate").keypress(function (event) {
    var keycode = event.keyCode ? event.keyCode : event.which;
    if (keycode == "13") {
      $("#btnSaveUnitWeight").click();
    }
    event.stopPropagation();
  });

  $("#txtUnitContentWtUpdate").keypress(function (event) {
    var keycode = event.keyCode ? event.keyCode : event.which;
    if (keycode == "13") {
      $("#btnSaveUnitWeight").click();
    }
    event.stopPropagation();
  });
}

function initButtons() {
  $("#btnSearch").click(function () {
    findBooking(true);
  });

  $("#btnClean").click(function () {
    cleanSearchParams();
  });

  $("#btnAddPreadviseUnit").click(function () {
    validateBkgQty();
  });

  $("#btnSaveUnitPreadvise").click(function () {
    preadviseUnit(true);
  });

  $("#btnSaveUnitSeal").click(function () {
    updateUnitSeal2();
  });

  $("#btnSaveUnitOog").click(function () {
    updateUnitOog();
  });

  $("#btnSaveUnitVgm").click(function () {
    updateUnitVgm();
  });

  $("#btnSaveUnitWeight").click(function () {
    updateUnitWeight();
  });

  $("#btnSaveUnitRenumber").click(function () {
    updateUnitId();
  });

  $("#btnAddUnitCustomsPerm").click(function () {
    addUnitCustomsPerm();
  });

  $("#btnCleanUnitCustomsPerm").click(function () {
    clearCustomsPermInfo();
  });

  $("#btnHelp").click(function () {
    showHelpMsg();
  });
}

function initCombos() {
  loadInitCombos();

  $("#cbUnitIsoType").change(function () {
    var selectedValue = $(this).val();
    var itemNotFound = true;
    if (selectedValue != null && selectedValue != "") {
      preadviseViewModel.bkgItemList().forEach(function (tempItem) {
        if (selectedValue == tempItem.itemGkey) {
          itemNotFound = false;
          preadviseViewModel.itemGkey(tempItem.itemGkey);
          preadviseViewModel.itemIsoType(tempItem.size);
          preadviseViewModel.itemTemperature(tempItem.temperature);
          preadviseViewModel.itemTare(tempItem.tare);
          $("#txtUnitTare").focus();
        }
      });
    }
    if (itemNotFound) {
      preadviseViewModel.itemGkey("");
      preadviseViewModel.itemIsoType("");
      preadviseViewModel.itemTemperature("");
      preadviseViewModel.itemTare("");
    }
  });

  $("#cbUnitIsOog").change(function () {
    var selectedValue = $(this).val();
    if (selectedValue == "1") showOogInfo();
    else hideOogInfo();
  });

  $("#cbUnitIsOogUpdate").change(function () {
    var selectedValue = $(this).val();
    if (selectedValue == "1") showOogUpdateInfo();
    else hideOogUpdateInfo();
  });

  $("#radioVgmExternal").click(function () {
    showVgmInfoMsg();
  });

  $("#radioVgmExternalUpdate").click(function () {
    showVgmInfoMsg();
  });
}

function initDivs() {
  $("#btnBookingDetails").click(function () {
    toggleBookingDetails();
  });

  $("#btnBookingItems").click(function () {
    toggleBookingDetails();
  });
}

function initModals() {
  $("#addUnitPreadviseModal").on("shown.bs.modal", function () {
    $("#txtUnitId").focus();
  });

  $("#addUnitPreadviseModal").on("hidden.bs.modal", function () {
    adjustDataTableColumns();
    cleanAddUnitPreadviseInputs();
  });

  $("#viewUnitRenumberModal").on("hidden.bs.modal", function () {
    unitDto = {};
    $("#txtUnitRenumber").val("");
  });

  $("#viewUpdateUnitSealModal").on("hidden.bs.modal", function () {
    unitDto = {};
    $("#txtUnitSeal2Update").val("");
  });

  $("#viewUpdateUnitWeightModal").on("hidden.bs.modal", function () {
    unitDto = {};
    $("#txtUnitTareUpdate").val("");
    $("#txtUnitContentWtUpdate").val("");
  });

  $("#viewUpdateUnitVgmModal").on("hidden.bs.modal", function () {
    unitDto = {};
    cleanUpdateVgmInputs();
  });

  $("#viewUpdateUnitCustomsPermModal").on("shown.bs.modal", function () {
    $("#txtUnitCustomsPermUpdate").focus();
  });

  $("#viewUpdateUnitCustomsPermModal").on("hidden.bs.modal", function () {
    unitDto = {};
    clearCustomsPermInfo();
  });
}

function findBooking(showProcessing) {
  var errorMsg = validateSearchFields();
  if (errorMsg == "") {
    if (showProcessing) showProcessingModal();
    //deleteUnitDataTable();
    var searchParam = {
      prBookingNbr: $("#txtSearchBooking").val(),
      prLineOpId: $("#cbSearchLineOp option:selected").val(),
    };
    $.ajax({
      type: "GET",
      url: url_application + "/Preadvise/findBooking/",
      data: searchParam,
      success: function (data, statusCode) {
        var msg = data["msg"];
        if (typeof msg != "undefined" && resultIsError(data)) {
          showErrorHtmlMsg(msg);
        } else {
          fillBookingInfo(data);
          if (showProcessing) closeProcessingModal();
        }
      },
      fail: function (data, statusCode) {
        s;
        showErrorMsg("Status code:" + statusCode);
      },
    });
  } else showErrorMsg(errorMsg);
}

function getBookingPreadvisedUnits(showProcessing) {
  if (showProcessing) showProcessingModal();
  deleteUnitDataTable();
  var searchParam = {
    prBookingNbr: preadviseViewModel.bkgNbr(),
    prLineOpId: preadviseViewModel.bkgLineId(),
  };
  $.ajax({
    type: "GET",
    url: url_application + "/Preadvise/getBookingPreadvisedUnits/",
    data: searchParam,
    success: function (data, statusCode) {
      var msg = data["msg"];
      if (typeof msg != "undefined" && !resultIsOk(data)) {
        showErrorHtmlMsg(msg);
      } else {
        showUnitTable(data);
        if (showProcessing) closeProcessingModal();
      }
    },
    fail: function (data, statusCode) {
      s;
      showErrorMsg("Status code:" + statusCode);
    },
  });
}

function preadviseUnit(showProcessing) {
  console.log("preadviseUnit.....");
  if (showProcessing) showProcessingModal();
  var unitObject = {
    unitId: $("#txtUnitId").val().toUpperCase(),
    unitTareWt: $("#txtUnitTare").val(),
    unitContentWt: $("#txtUnitContentWt").val(),
    unitSealNbr2: $("#txtUnitSeal2").val().toUpperCase(),
    unitVgmCondition: getVgmCondition().toUpperCase(),
    unitEmail: $("#txtUnitEmail").val().toUpperCase(),
    unitIsOog: $("#cbUnitIsOog").val(),
    oogTop: $("#txtUnitTop").val(),
    oogBack: $("#txtUnitBack").val(),
    oogFront: $("#txtUnitFront").val(),
    oogLeft: $("#txtUnitLeft").val(),
    oogRight: $("#txtUnitRight").val(),
    bookingGkey: preadviseViewModel.bkgGkey(),
    bookingNbr: preadviseViewModel.bkgNbr(),
    lineOpId: preadviseViewModel.bkgLineId(),
    bookingItemGkey: preadviseViewModel.itemGkey(),
    isoTypeId: preadviseViewModel.itemIsoType(),
    bookingGkey: preadviseViewModel.bkgGkey(),
    unitReferenceId: $("#txtUnitCustomsPerm").val().toUpperCase(),
  };
  $.ajax({
    type: "POST",
    url: url_application + "/Preadvise/preadviseUnit/",
    contentType: "application/json",
    data: JSON.stringify(unitObject),
    success: function (data, statusCode) {
      var msg = data["msg"];
      if (typeof msg != "undefined" && resultIsError(data)) {
        showErrorHtmlMsg(msg);
      } else {
        console.log("preaviso correcto");
        if (showProcessing) closeProcessingModal();
        $("#btnCancelAddUnitPreadvise").click();
        showSuccessHtmlMsg(msg);
        getBookingPreadvisedUnits(false);
      }
    },
    fail: function (data, statusCode) {
      s;
      showErrorMsg("Status code:" + statusCode);
    },
  });
}

function updateUnitSeal2() {
  showProcessingModal();
  unitDto.updateUnitSealNbr2 = $("#txtUnitSeal2Update").val().toUpperCase();
  $.ajax({
    type: "POST",
    url: url_application + "/Preadvise/updateUnitSeal2/",
    contentType: "application/json",
    data: JSON.stringify(unitDto),
    success: function (data, statusCode) {
      closeProcessingModal();
      var msg = data["msg"];
      if (typeof msg != "undefined" && resultIsError(data)) {
        showErrorHtmlMsg(msg);
      } else if (resultRequiresConfirm(data)) {
        var questionType = data["questionType"];
        showUpdateChargesMsg(msg, questionType, "btnSaveUnitSeal");
      } else {
        console.log("actualización preaviso seal correcto");
        $("#btnCancelUpdateUnitSeal").click();
        showSuccessHtmlMsg(msg);
        getBookingPreadvisedUnits(false);
      }
    },
    fail: function (data, statusCode) {
      s;
      closeProcessingModal();
      showErrorMsg("Status code:" + statusCode);
    },
  });
}

function updateUnitOog() {
  showProcessingModal();
  unitDto.updateUnitIsOog = $("#cbUnitIsOogUpdate").val();
  unitDto.updateOogTop = $("#txtUnitTopUpdate").val();
  unitDto.updateOogFront = $("#txtUnitFrontUpdate").val();
  unitDto.updateOogBack = $("#txtUnitBackUpdate").val();
  unitDto.updateOogLeft = $("#txtUnitLeftUpdate").val();
  unitDto.updateOogRight = $("#txtUnitRightUpdate").val();
  $.ajax({
    type: "POST",
    url: url_application + "/Preadvise/updateUnitOog/",
    contentType: "application/json",
    data: JSON.stringify(unitDto),
    success: function (data, statusCode) {
      closeProcessingModal();
      var msg = data["msg"];
      if (typeof msg != "undefined" && resultIsError(data)) {
        showErrorHtmlMsg(msg);
      } else if (resultRequiresConfirm(data)) {
        var questionType = data["questionType"];
        showUpdateChargesMsg(msg, questionType, "btnSaveUnitOog");
      } else {
        console.log("actualización preaviso oog correcto");
        $("#btnCancelUpdateUnitOog").click();
        showSuccessHtmlMsg(msg);
        getBookingPreadvisedUnits(false);
      }
    },
    fail: function (data, statusCode) {
      s;
      closeProcessingModal();
      showErrorMsg("Status code:" + statusCode);
    },
  });
}

function updateUnitVgm() {
  showProcessingModal();
  unitDto.updateVgmCondition = getVgmConditionUpdate();
  unitDto.updateVgmEmail = $("#txtUnitEmailUpdate").val();
  $.ajax({
    type: "POST",
    url: url_application + "/Preadvise/updateUnitVgm/",
    contentType: "application/json",
    data: JSON.stringify(unitDto),
    success: function (data, statusCode) {
      closeProcessingModal();
      var msg = data["msg"];
      if (typeof msg != "undefined" && resultIsError(data)) {
        showErrorHtmlMsg(msg);
      } else if (resultRequiresConfirm(data)) {
        var questionType = data["questionType"];
        showUpdateChargesMsg(msg, questionType, "btnSaveUnitVgm");
      } else {
        console.log("actualización preaviso vgm correcto");
        $("#btnCancelUpdateUnitVgm").click();
        showSuccessHtmlMsg(msg);
        getBookingPreadvisedUnits(false);
      }
    },
    fail: function (data, statusCode) {
      s;
      closeProcessingModal();
      showErrorMsg("Status code:" + statusCode);
    },
  });
}

function updateUnitWeight() {
  showProcessingModal();
  unitDto.updateUnitTareWt = $("#txtUnitTareUpdate").val();
  unitDto.updateUnitContentWt = $("#txtUnitContentWtUpdate").val();
  $.ajax({
    type: "POST",
    url: url_application + "/Preadvise/updateUnitWeight/",
    contentType: "application/json",
    data: JSON.stringify(unitDto),
    success: function (data, statusCode) {
      closeProcessingModal();
      var msg = data["msg"];
      if (typeof msg != "undefined" && resultIsError(data)) {
        showErrorHtmlMsg(msg);
      } else if (resultRequiresConfirm(data)) {
        var questionType = data["questionType"];
        showUpdateChargesMsg(msg, questionType, "btnSaveUnitWeight");
      } else {
        console.log("actualización preaviso weight correcto");
        $("#btnCancelUpdateUnitWeight").click();
        showSuccessHtmlMsg(msg);
        getBookingPreadvisedUnits(false);
      }
    },
    fail: function (data, statusCode) {
      s;
      closeProcessingModal();
      showErrorMsg("Status code:" + statusCode);
    },
  });
}

function updateUnitId() {
  showProcessingModal();
  unitDto.updateUnitId = $("#txtUnitRenumber").val().toUpperCase();
  $.ajax({
    type: "POST",
    url: url_application + "/Preadvise/updateUnitId/",
    contentType: "application/json",
    data: JSON.stringify(unitDto),
    success: function (data, statusCode) {
      closeProcessingModal();
      var msg = data["msg"];
      if (typeof msg != "undefined" && resultIsError(data)) {
        showErrorHtmlMsg(msg);
      } else if (resultRequiresConfirm(data)) {
        var questionType = data["questionType"];
        showUpdateChargesMsg(msg, questionType, "btnSaveUnitRenumber");
      } else {
        console.log("actualización preaviso id correcto");
        $("#btnCancelUnitRenumber").click();
        showSuccessHtmlMsg(msg);
        getBookingPreadvisedUnits(false);
      }
    },
    fail: function (data, statusCode) {
      s;
      closeProcessingModal();
      showErrorMsg("Status code:" + statusCode);
    },
  });
}

function cancelPreadvisedUnit(unitToDelete) {
  showProcessingModal();
  $.ajax({
    type: "POST",
    url: url_application + "/Preadvise/cancelPreadvisedUnit/",
    contentType: "application/json",
    data: JSON.stringify(unitToDelete),
    success: function (data, statusCode) {
      closeProcessingModal();
      var msg = data["msg"];
      if (typeof msg != "undefined" && resultIsError(data)) {
        showErrorHtmlMsg(msg);
      } else {
        console.log("preaviso cancelado " + unitToDelete.unitId);
        showSuccessHtmlMsg(msg);
        getBookingPreadvisedUnits(false);
      }
    },
    fail: function (data, statusCode) {
      s;
      closeProcessingModal();
      showErrorMsg("Status code:" + statusCode + " - " + JSON.stringify(data));
    },
  });
}

function getUnitCustomsPerms(showProcessing) {
  if (showProcessing) showProcessingModal();
  var searchParam = {
    prUnitGkey: unitDto.gkey,
  };
  $.ajax({
    type: "GET",
    url: url_application + "/Preadvise/getUnitCustomsPerms/",
    data: searchParam,
    success: function (data, statusCode) {
      var msg = data["msg"];
      if (typeof msg != "undefined" && resultIsError(data)) {
        showErrorHtmlMsg(msg);
      } else {
        showUnitCustomsPermTable(data);
        if (showProcessing) closeProcessingModal();
      }
    },
    fail: function (data, statusCode) {
      s;
      showErrorMsg("Status code:" + statusCode + JSON.stringify(data));
    },
  });
}

function addUnitCustomsPerm() {
  showProcessingModal();
  unitDto.referenceId = $("#txtUnitCustomsPermUpdate").val().toUpperCase();
  $.ajax({
    type: "POST",
    url: url_application + "/Preadvise/addUnitCustomsPerm/",
    contentType: "application/json",
    data: JSON.stringify(unitDto),
    success: function (data, statusCode) {
      closeProcessingModal();
      var msg = data["msg"];
      if (typeof msg != "undefined" && resultIsError(data)) {
        showErrorHtmlMsg(msg);
      } else {
        showSuccessHtmlMsg(msg);
        clearCustomsPermInfo();
        getUnitCustomsPerms(true);
      }
    },
    fail: function (data, statusCode) {
      s;
      closeProcessingModal();
      showErrorMsg("Status code:" + statusCode);
    },
  });
}

function cancelUnitPermissionIntern(permissionToCancel) {
  showProcessingModal();
  permissionToCancel.bookingNbr = preadviseViewModel.bkgNbr();
  $.ajax({
    type: "POST",
    url: url_application + "/Preadvise/cancelUnitCustomsPerm/",
    contentType: "application/json",
    data: JSON.stringify(permissionToCancel),
    success: function (data, statusCode) {
      closeProcessingModal();
      var msg = data["msg"];
      if (typeof msg != "undefined" && resultIsError(data)) {
        showErrorHtmlMsg(msg);
      } else {
        showSuccessHtmlMsg(msg);
        getUnitCustomsPerms(true);
      }
    },
    fail: function (data, statusCode) {
      closeProcessingModal();
      showErrorMsg("Unhandled error:" + statusCode + data);
    },
  });
}

function showUnitRenumberModal(unitToRenumber) {
  unitDto = unitToRenumber;
  $("#txtUnitRenumber").val(unitToRenumber.unitId);
  $("#viewUnitRenumberModal").modal();
  $("#txtUnitRenumber").focus();
}

function showUpdateUnitSealModal(unitToUpdate) {
  unitDto = unitToUpdate;
  $("#txtUnitSeal2Update").val(unitToUpdate.seal2);
  $("#viewUpdateUnitSealModal").modal();
  $("#txtUnitSeal2Update").focus();
}

function showUpdateUnitWeightModal(unitToUpdate) {
  unitDto = unitToUpdate;
  $("#txtUnitTareUpdate").val(unitToUpdate.tareKg);
  $("#txtUnitContentWtUpdate").val(unitToUpdate.contentKg);
  $("#viewUpdateUnitWeightModal").modal();
  $("#txtUnitTareUpdate").focus();
}

function showUpdateUnitOogModal(unitToUpdate) {
  unitDto = unitToUpdate;
  var isOog = unitToUpdate.oog;
  if (isOog) {
    $("#cbUnitIsOogUpdate").val("1");
    $("#txtUnitTopUpdate").val(unitToUpdate.oogTop);
    $("#txtUnitFrontUpdate").val(unitToUpdate.oogFront);
    $("#txtUnitBackUpdate").val(unitToUpdate.oogBack);
    $("#txtUnitLeftUpdate").val(unitToUpdate.oogLeft);
    $("#txtUnitRightUpdate").val(unitToUpdate.oogRight);
    showOogUpdateInfo();
  }
  $("#viewUpdateUnitOogModal").modal();
}

function showUpdateUnitVgmModal(unitToUpdate) {
  unitDto = unitToUpdate;
  var vgmConditionKey = unitToUpdate.vgmConditionEnum.key;
  var vgmEmail = unitToUpdate.vgmEmail;
  if (vgmConditionKey == "VGMT4")
    $("#radioVgmInternalUpdate").prop("checked", true);
  else if (vgmConditionKey == "VGMEXT")
    $("#radioVgmExternalUpdate").prop("checked", true);
  $("#txtUnitEmailUpdate").val(vgmEmail);
  $("#viewUpdateUnitVgmModal").modal();
}

function showUpdateUnitCustomsPermModal(unitToUpdate) {
  unitDto = unitToUpdate;
  getUnitCustomsPerms(true);
  $("#viewUpdateUnitCustomsPermModal").modal();
}

function getVgmCondition() {
  if ($("#radioVgmInternal").prop("checked"))
    return $("#radioVgmInternal").val();
  else return $("#radioVgmExternal").val();
}

function getVgmConditionUpdate() {
  if ($("#radioVgmInternalUpdate").prop("checked"))
    return $("#radioVgmInternalUpdate").val();
  else return $("#radioVgmExternalUpdate").val();
}

function fillBookingInfo(data) {
  preadviseViewModel.bkgGkey(data.bookingGkey);
  preadviseViewModel.bkgNbr(data.bookingId);
  preadviseViewModel.bkgLineId(data.lineOp);
  preadviseViewModel.bkgVessel(data.vessel.vesselName);
  preadviseViewModel.bkgVesselInfo(
    data.vessel.vesselName + " " + data.vessel.vesselOutVoyage,
  );
  preadviseViewModel.bkgVisitId(data.vessel.vesselId);
  preadviseViewModel.bkgCutoff(data.cutofffStr);
  preadviseViewModel.bkgShipper(data.shipper);
  preadviseViewModel.bkgQty(data.quantity);
  preadviseViewModel.bkgPOD(data.pod);
  preadviseViewModel.bkgIsReefer(data.reefer);
  preadviseViewModel.bkgIsHazardous(data.hazardous);
  preadviseViewModel.bkgItemList(data.items);
  $("#txtBookingDetails").text("Detalle booking " + data.bookingId);
  $("#txtBookingItems").text("Equipamento booking " + data.bookingId);
  showBookingInfo();
  showUnitTable(data.units);
}

function clearBookingInfo(data) {
  preadviseViewModel.bkgGkey("");
  preadviseViewModel.bkgNbr("");
  preadviseViewModel.bkgLineId("");
  preadviseViewModel.bkgVessel("");
  preadviseViewModel.bkgVesselInfo("");
  preadviseViewModel.bkgVisitId("");
  preadviseViewModel.bkgCutoff("");
  preadviseViewModel.bkgShipper("");
  preadviseViewModel.bkgQty("");
  preadviseViewModel.bkgPOD("");
  preadviseViewModel.bkgIsReefer("");
  preadviseViewModel.bkgIsHazardous("");
  preadviseViewModel.bkgUnitList.removeAll();
  preadviseViewModel.bkgItemList.removeAll();
  deleteUnitDataTable();
  hideBookingInfo();
}

function clearCustomsPermInfo() {
  $("#txtUnitCustomsPermUpdate").val("");
  $("#txtUnitCustomsPermUpdate").focus();
}

function loadInitCombos() {
  showProcessingModal();
  $.ajax({
    type: "GET",
    url: url_application + "/Preadvise/loadInitCombos/",
    success: function (data, statusCode) {
      loadSearchLineOpCb(data);
      loadYesNoCb(data);
      closeProcessingModal();
    },
    fail: function (data, statusCode) {
      showErrorMsg("Status code:" + statusCode);
    },
  });
}

function loadYesNoCb(data) {
  preadviseViewModel.yesNoCb(data["yesNoList"]);
}

function loadSearchLineOpCb(data) {
  preadviseViewModel.lineOpCb(data["lineOpList"]);
}

function showUnitTable(tableData) {
  deleteUnitDataTable();
  preadviseViewModel.bkgUnitList.removeAll();
  preadviseViewModel.bkgUnitList(tableData);
  var table = $("#tbUnit").DataTable({
    bDestroy: true,
    bFilter: true,
    sScrollX: true,
    sScrollY: "60vh",
    scrollCollapse: true,
    paging: true,
    bPaginate: true,
    bSort: true,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
      sSearch: "Buscar",
    },
  });
  console.log("showing table..");
  adjustDataTableColumns();
}

function showUnitCustomsPermTable(tableData) {
  preadviseViewModel.customsPermList.removeAll();
  preadviseViewModel.customsPermList(tableData);
}

function deleteUnitDataTable() {
  $("#tbUnit").DataTable().clear();
  $("#tbUnit").DataTable().destroy();
}

function deleteUnitCustomsPermDataTable() {
  $("#tbUnitPermission").DataTable().clear();
  $("#tbUnitPermission").DataTable().destroy();
}

function cleanSearchParams() {
  $("#txtSearchBooking").val("");
  $("#cbSearchLineOp").val("");
  $("#txtSearchBooking").focus();
  clearBookingInfo();
}

function cleanAddUnitPreadviseInputs() {
  $("#txtUnitId").val("");
  $("#cbUnitIsoType").val("");
  $("#txtUnitContentWt").val("");
  $("#txtUnitSeal2").val("");
  $("#txtUnitEmail").val("");
  $("#cbUnitIsOog").val("");
  $("#radioVgmExternal").prop("checked", false);
  $("#radioVgmInternal").prop("checked", true);
  preadviseViewModel.itemGkey("");
  preadviseViewModel.itemIsoType("");
  preadviseViewModel.itemTemperature("");
  preadviseViewModel.itemTare("");
  hideOogInfo();
}

function cleanOogInputs() {
  $("#txtUnitTop").val("");
  $("#txtUnitFront").val("");
  $("#txtUnitBack").val("");
  $("#txtUnitLeft").val("");
  $("#txtUnitRight").val("");
}

function cleanUpdateOogInputs() {
  $("#cbUnitIsOogUpdate").val("");
  $("#txtUnitTopUpdate").val("");
  $("#txtUnitFrontUpdate").val("");
  $("#txtUnitBackUpdate").val("");
  $("#txtUnitLeftUpdate").val("");
  $("#txtUnitRightUpdate").val("");
}

function cleanUpdateVgmInputs() {
  $("#radioVgmInternalUpdate").prop("checked", false);
  $("#radioVgmExternalUpdate").prop("checked", false);
}

function showBookingInfo() {
  $("#divBookingDetails").css("display", "block");
  $("#divActionButtons").css("display", "block");
  $("#divUnits").css("display", "block");
}

function hideBookingInfo() {
  $("#divBookingDetails").css("display", "none");
  $("#divActionButtons").css("display", "none");
  $("#divUnits").css("display", "none");
}

function toggleBookingDetails() {
  $("#divDetailBody").toggle();
  $("#divItemBody").toggle();
  var btnText = $("#btnBookingItems").text();
  var btnNewText = "";
  if ("Ocultar" == btnText) btnNewText = "Ver";
  else btnNewText = "Ocultar";
  $("#btnBookingDetails").prop("text", btnNewText);
  $("#btnBookingItems").prop("text", btnNewText);
}

function showOogInfo() {
  $(".oog-div").show();
  $("#txtUnitTop").focus();
}

function hideOogInfo() {
  $(".oog-div").hide();
  cleanOogInputs();
}

function showOogUpdateInfo() {
  $(".oog-div-update").show();
  $("#txtUnitTopUpdate").focus();
}

function hideOogUpdateInfo() {
  $(".oog-div-update").hide();
  cleanUpdateOogInputs();
}

function showVgmInfoMsg() {
  showInfoMsg(
    "Estimado cliente, usted eligió presentar VGM externo. Recuerde que es obligatoria dicha presentación previo al ingreso por el Gate.",
  );
}

function showHelpMsg() {
  showInfoHtmlMsg(
    "Recordá que podés modificar los datos del contenedor hasta antes del ingreso a la terminal. <br><br>" +
      "Si necesitás más información podés ingresar a nuestra web de instructivos haciendo click " +
      '<a href="https://www.apmterminals.com/es/buenos-aires/customer-management-center/e-services" target="_blank" style="color: #F39A3C;">acá</a>',
  );
}

function validateSearchFields() {
  var searchBooking = $("#txtSearchBooking").val();
  var errorMsg = "";
  if (searchBooking == null || searchBooking == "") {
    errorMsg = "Ingresá el campo obligatorio (*) para la búsqueda";
  } else if (!isValidStr(searchBooking))
    errorMsg = "Eliminá los caracteres especiales de la búsqueda";
  return errorMsg;
}

function validateBkgQty() {
  showProcessingModal();
  var searchParam = {
    prBookingNbr: preadviseViewModel.bkgNbr(),
    prLineOpId: preadviseViewModel.bkgLineId(),
  };
  $.ajax({
    type: "GET",
    url: url_application + "/Preadvise/validateBkgQty/",
    data: searchParam,
    success: function (data, statusCode) {
      var msg = data["msg"];
      if (typeof msg != "undefined" && resultIsError(data)) {
        showErrorHtmlMsg(msg);
      } else {
        closeProcessingModal();
        $("#addUnitPreadviseModal").modal();
      }
    },
    fail: function (data, statusCode) {
      s;
      showErrorMsg("Status code:" + statusCode);
    },
  });
}

function adjustDataTableColumns() {
  $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
}

function resultIsOk(result) {
  var status = result["status"];
  return status == "OK";
}

function resultIsError(result) {
  var status = result["status"];
  return status == "ERROR";
}

function resultRequiresConfirm(result) {
  var status = result["status"];
  return status == "QUESTION";
}

function resultIsWarning(result) {
  var status = result["status"];
  return status == "WARNING";
}

function isValidStr(str) {
  return !/[~`!#$%\^&*+=\\[\]\\';,/{}|\\":<>\?]/g.test(str);
}

function showUpdateChargesMsg(msg, questionType, buttonName) {
  Swal.fire({
    allowOutsideClick: false,
    title: "Desea continuar?",
    html: msg,
    type: "question",
    showCancelButton: true,
    confirmButtonColor: "#4fa7f3",
    cancelButtonColor: "#d57171",
    confirmButtonText: "Sí, actualizar preaviso!",
  }).then((result) => {
    if (result.value) {
      unitDto[questionType] = true;
      $("#" + buttonName).click();
      delete unitDto[questionType];
    } else if (result.dismiss === Swal.DismissReason.cancel) {
      delete unitDto[questionType];
    }
  });
}

function showCancelPreadviseMsg(unitToDelete) {
  Swal.fire({
    allowOutsideClick: false,
    title: "Desea continuar?",
    html:
      "Está seguro de despreavisar el contenedor " + unitToDelete.unitId + "?",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: "#4fa7f3",
    cancelButtonColor: "#d57171",
    confirmButtonText: "Sí, cancelar preaviso!",
  }).then((result) => {
    if (result.value) {
      cancelPreadvisedUnit(unitToDelete);
    }
  });
}
