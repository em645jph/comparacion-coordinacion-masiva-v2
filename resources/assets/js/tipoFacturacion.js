/**

*/
var regex =
  /^([20]{2}|[23]{2}|[24]{2}|[27]{2}|[30]{2}|[33]{2}|[34]{2})[0-9]{8}([0-9]{1})/gm;
var cuilLengthRegex = /^([0-9]{11})$/g;

$(document).ready(function () {
  $("#transferWarningModal").modal();
});
