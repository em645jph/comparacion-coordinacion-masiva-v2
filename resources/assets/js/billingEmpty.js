var loaded = false;
var table;
$(document).ready(function () {
  $("#btnFindEntity").click(function () {
    findEntityById();
  });

  $("#tbUnitsByBl tbody").on("click", "tr", function () {
    if ($("#tbUnitsByBl").DataTable().rows(".selected").count() > 0) {
      $("#btnGenerate").show();
    } else {
      $("#btnGenerate").hide();
    }
  });

  $("#btnGenerate").click(function () {
    generateDraftByUnitsIds();
  });
});

function initKnockout() {
  viewModel = {
    UnitsByBl: ko.observableArray([]),
  };
  ko.applyBindings(viewModel);
}

function loadUnitsCb(data) {
  showUnitsGrid(data);
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function showUnitsGrid(data) {
  viewModel.UnitsByBl.removeAll();
  deleteDataTables();
  viewModel.UnitsByBl(data);
  $("#tbUnitsByBl")
    .DataTable({
      //		select: true,
      oLanguage: {
        emptyTable: "No hay registros a mostrar",
        sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
        sSearch: "Buscar",
        sLengthMenu: "Mostrar _MENU_ resultados",
        /*"oPaginate": {
            	"sFirst": "Primera pagina", // This is the link to the first page
            	"sPrevious": "Pagina anterior", // This is the link to the previous page
            	"sNext": "Proxima pagina", // This is the link to the next page
            	"sLast": "Ultima pagina" // This is the link to the last page
        	}*/
      },
      sDom: "lfrtip",
      bDestroy: true,
      bFilter: true,
      bSort: true,
      sScrollY: "30vh",
    })
    .on("select", function (e, dt, type, indexes) {
      if (type === "row") {
        var rows = table.rows(indexes).nodes().to$();
        //var ignore = node.hasClass( 'ignoreme' );
        $.each(rows, function () {
          if ($(this).hasClass("ignoreme")) table.row($(this)).deselect();
        });
      }
      if ($("#tbUnitsByBl").DataTable().rows(".selected").count() > 0) {
        $("#btnGenerate").show();
      } else {
        $("#btnGenerate").hide();
      }
    })
    .on("deselect", function (e, dt, type, indexes) {
      if ($("#tbUnitsByBl").DataTable().rows(".selected").count() > 0) {
        $("#btnGenerate").show();
      } else {
        $("#btnGenerate").hide();
      }
    });
}

function findEntityById() {
  processingModal();

  var value = $("#valueNbr").val();
  if (value == "") {
    showModal("", "Por favor, complete los campos requeridos.", "error");
    return;
  }

  var request = $.ajax({
    url: url_application + "/Billing/findEmptyUnit/",
    type: "GET",
  });

  request.done(function (data) {
    if (data == null || data == "") {
      Swal.fire("Sin resultados");
      return;
    }
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    $("#tbUnitsByBl").show();
    loadUnitsCb(data);
    Swal.close();
  });

  request.fail(function (jqXHR, textStatus) {
    Swal.close();
    showModal("Request failed", jqXHR.responseText, "error");
  });
}

function processingModal() {
  Swal.fire({
    title: "Procesando...",
    text: "Aguarde un instante por favor",
    imageUrl: url_application + "/resources/assets/images/ajax_loading.gif",
    showConfirmButton: false,
    allowOutsideClick: false,
  });
}

function generateDraftByUnitsIds() {
  console.log("Armamos el string para enviar al backend..");
  processingModal();

  var dataArr = [];
  var rows = $("tr.selected");
  var rowData = $("#tbUnitsByBl").DataTable().rows(rows).data();
  $.each($(rowData), function (key, value) {
    dataArr.push(value["0"]); //"name" being the value of your first column.
  });
  console.log(dataArr);

  if ($("#tbUnitsByBl").DataTable().rows(rows).count() < 1) {
    showModal(
      "",
      "Por favor, seleccione al menos un contenedor para facturar.",
      "error",
    );
    return;
  }

  var obj = new Object();
  obj.formUnits = dataArr;
  obj.formCategory = "IMPRT";
  obj.formDocNbr = "30-62469861-0";
  obj.formCustomerId = "JCMA";
  obj.formPaidThruDay = "2019-10-11 13:32:00";

  var request = $.ajax({
    type: "POST",
    enctype: "multipart/form-data",
    url: url_application + "/Invoice/createInvoiceDraftByUnits",
    data: JSON.stringify(obj),
    processData: false,
    contentType: false,
    cache: false,
    timeout: 600000,
  });

  request.done(function (data) {
    if (data == null || data == "") {
      Swal.fire("Sin resultados");
      return;
    }
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    Swal.close();
    Swal.fire("", "Draft creado con éxito.", "success");
  });

  request.fail(function (jqXHR, textStatus) {
    Swal.close();
    showModal("Request failed", jqXHR.responseText, "error");
  });
}
