var loaded = false;
$(document).ready(function () {
  //findDrafts();

  $("#tbUnitsByBl tbody").on("click", "tr", function () {
    $(this).toggleClass("selected");
  });

  $("#btnSearch").click(function () {
    findDrafts();
  });

  $("#btnClean").click(function () {
    $("#txtSearchFromDate,#txtSearchToDate").val("");
  });

  $("#txtSearchToDate").datepicker({ dateFormat: "yy-mm-dd" });
  $("#txtSearchFromDate")
    .datepicker({ dateFormat: "yy-mm-dd" })
    .bind("change", function () {
      var minValue = $(this).val();
      minValue = $.datepicker.parseDate("yy-mm-dd", minValue);
      minValue.setDate(minValue.getDate() + 1);
      $("#txtSearchToDate").datepicker("option", "minDate", minValue);
    });
});

function getPdfByDraftId(draft) {
  processingModal();

  if (draft == null || draft == "" || draft == undefined) {
    Swal.fire("", "Error al obtener documento. Reintente nuevamente.", "error");
    return;
  }

  var request = $.ajax({
    type: "POST",
    enctype: "multipart/form-data",
    url: url_application + "/Invoice/getPdfByDraftId/" + draft,
    timeout: 600000,
  });

  request.done(function (data) {
    if (data == null || data == "") {
      Swal.fire("", "Sin resultados", "info");
      return;
    } else {
      data = data.replace(
        "http://10.54.1.93:11080",
        "https://apps.apmterminals.com.ar",
      );
      var win = window.open(data, "_blank");
      if (win) {
        //Browser has allowed it to be opened
        win.focus();
      } else {
        //Browser has blocked it
        showModal("", "No se pudo abrir el PDF", "error");
      }
    }
    Swal.close();
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", jqXHR.responseText, "error");
  });
}

function initKnockout() {
  viewModel = {
    drafts: ko.observableArray([]),
  };
  ko.applyBindings(viewModel);
}

function loadDraftsCb(data) {
  showDraftsGrid(data);
}

function getDateTime(value) {
  return (
    new Date(value).toLocaleDateString() +
    " " +
    new Date(value).toLocaleTimeString()
  );
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function showDraftsGrid(data) {
  viewModel.drafts.removeAll();
  deleteDataTables();
  viewModel.drafts(data);
  $("#tableBox").show();
  newDataTable("tbDrafts", null, true, true, false);
  $("#tbDrafts").show();
}

function findDrafts() {
  processingModal();

  var obj = new Object();
  obj.formCuits = "30-62469861-0;30-64499725-8;30-51119023-8;30-59267342-4";
  obj.formStartDate = $("#txtSearchFromDate").val();
  obj.formEndDate = $("#txtSearchToDate").val();

  var request = $.ajax({
    type: "POST",
    enctype: "multipart/form-data",
    url: url_application + "/Invoice/findDraftsByUsers",
    data: JSON.stringify(obj),
    processData: false,
    contentType: false,
    cache: false,
    timeout: 600000,
  });

  request.done(function (data) {
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    loadDraftsCb(data);
    Swal.close();
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", jqXHR.responseText, "error");
  });
}

function payDraft(input) {
  var url =
    url_application + "/Payment/paymentSelector/" + input.getAttribute("nbr");
  var message =
    "Usted va a abonar el draft Nº " +
    input.getAttribute("nbr") +
    " seleccionado, esta seguro?";
  Swal.fire({
    allowOutsideClick: false,
    title: "Atención!",
    text: message,
    type: "info",
    footer: "<a href>Por qu&eacute; aparece este mensaje?</a>",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "S&iacute;, estoy seguro!",
    cancelButtonText: "Cancelar",
  }).then((result) => {
    if (result.dismiss != "cancel") {
      window.location.href = url;
    }
  });
}

function processingModal() {
  Swal.fire({
    title: "Procesando...",
    text: "Aguarde un instante por favor",
    imageUrl: url_application + "/resources/assets/images/ajax_loading.gif",
    showConfirmButton: false,
    allowOutsideClick: false,
  });
}

function deleteDraft(input) {
  processingModal();
  var draftnbr = input.getAttribute("invdraft");

  var request = $.ajax({
    type: "POST",
    url: url_application + "/Invoice/deleteInvoiceByNbr/" + draftnbr,
  });

  request.done(function (data) {
    /*if(data == null || data == "") {
			Swal.fire("Sin resultados");
			return;
		}*/
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    if (data == "SUCCESS") {
      showModal("", "Draft eliminado");
      reloadGrid();
    } else {
      Swal.fire("", data, "error");
    }
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", jqXHR.responseText, "error");
  });
}

function reloadGrid() {
  findDrafts();
}

function showTimerModal(title, text, timerMillis, callback) {
  Swal.fire({
    title: title,
    html: text,
    timer: timerMillis,
    onBeforeOpen: () => {
      Swal.showLoading();
    },
    onClose: () => {
      callback();
    },
  });
}

function showTimerModal(title, text, timerMillis) {
  Swal.fire({
    title: title,
    html: text,
    timer: timerMillis,
    onBeforeOpen: () => {
      Swal.showLoading();
    },
  });
}
