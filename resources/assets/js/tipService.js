/**
 * Author: Jimena Chavez
 *
 */

$(document).ready(function () {
  initKnockout();
  initHtmlElements();
});

function initKnockout() {
  tipViewModel = {
    serviceCb: ko.observableArray([]),
  };

  ko.applyBindings(tipViewModel);
}

function initHtmlElements() {
  initLoad(true);
  initInputs();
  initButtons();
}

function initInputs() {
  $("#txtContainer").focus();
}

function initButtons() {
  $("#btnSaveTipService").click(function () {
    saveTipService();
  });

  $("#btnClean").click(function () {
    cleanTipService();
  });
}

function initLoad(showLoading) {
  console.log("initLoad");
  if (showLoading) showProcessingModal();
  $.ajax({
    type: "GET",
    url: url_application + "/TipService/initLoad/",
    success: function (data, statusCode) {
      loadServiceCb(data["tipServiceList"]);
      var isAnonimous = data["isAnonimous"];
      var userEmailAddress = data["emailAddress"];
      if (isAnonimous) {
        removeMenu();
        setTitle();
      } else {
        removeTitle();
        $("#txtEmail").val(userEmailAddress);
      }
      if (showLoading) closeProcessingModal();
    },
    fail: function (data, statusCode) {
      showErrorMsg("Status code:" + statusCode);
    },
  });
}

function saveTipService() {
  var errorMsg = validateFields();
  if (errorMsg == "") {
    var tipServiceParam = {
      prContainerNbr: $("#txtContainer").val(),
      prServiceType: $("#cbService option:selected").val(),
      prEmail: $("#txtEmail").val(),
    };
    console.log("tipServiceParam:" + JSON.stringify(tipServiceParam));
    showProcessingModal();
    $.ajax({
      type: "POST",
      url: url_application + "/TipService/saveTipService/",
      data: tipServiceParam,
      success: function (result, statusCode) {
        var msg = result["msg"];
        if (resultIsOk(result)) {
          cleanTipService();
          initLoad(false);
          showSuccessMsg("Listo!", msg);
        } else showErrorHtmlMsg(msg);
      },
      fail: function (result, statusCode) {
        showErrorMsg(
          statusCode +
            "Se produjo un error guardando el turno vip. Intentá nuevamente",
        );
      },
    });
  } else {
    showErrorMsg(errorMsg);
  }
}

function loadServiceCb(data) {
  tipViewModel.serviceCb.removeAll();
  tipViewModel.serviceCb(data);
}

function validateFields() {
  var container = $("#txtContainer").val();
  var email = $("#txtEmail").val();
  var errorMsg = "";
  if (!isValidStr(container))
    errorMsg = "El campo no puede contener caracteres especiales";
  else if (!isEmail(email)) errorMsg = "Ingresá un email válido";
  return errorMsg;
}

function removeMenu() {
  $("#navigation").remove();
}

function setTitle() {
  $("#tipServiceTitle").text("Servicios en TIP");
}

function removeTitle() {
  $("#tipServiceTitle").text("");
}

function cleanTipService() {
  $("#txtContainer").val("");
  $("#cbService").val("");
  $("#txtEmail").val("");
  $("#txtContainer").focus();
}

function resultIsOk(result) {
  var status = result["status"];
  return status == "OK";
}

function isValidStr(str) {
  return !/[~`!#$%\^&*+=\\[\]\\';,/{}|\\":<>\?]/g.test(str);
}

function isEmail(email) {
  var regex =
    /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}
