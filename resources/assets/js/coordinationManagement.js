/**
 * Author: Jimena Chavez
 *
 */

$(document).ready(function () {
  initHtmlElements();
  initKnockout();
  var unitDto = {};
  var storageUnitMap = {};
  var documentGkey;
});

function initKnockout() {
  coordinationViewModel = {
    categoryCb: ko.observableArray([]),
    lineOpCb: ko.observableArray([]),
    unitTable: ko.observableArray([]),
    unitServiceTable: ko.observableArray([]),
    coordinationTypeCb: ko.observableArray([]),
    channelCb: ko.observableArray([]),
    bunchCb: ko.observableArray([]),
    dateOpeningCb: ko.observableArray([]),
    archTypeCb: ko.observableArray([]),
    apptOrderItemCb: ko.observableArray([]),
    viewUnitSeals: function (unitToShow) {
      viewUnitSealsIntern(unitToShow);
    },
    viewUnitServices: function (unitToShowSrvOrder) {
      viewUnitServicesIntern(unitToShowSrvOrder);
    },
    coordinateUnit: function (unitToCoordinate) {
      viewUnitCalendar(unitToCoordinate);
    },
    cancelAppt: function (apptToCancel) {
      cancelUnitAppointment(apptToCancel);
    },
    cancelSo: function (soToCancel) {
      cancelUnitService(soToCancel);
    },
  };
  ko.applyBindings(coordinationViewModel);
}

function initHtmlElements() {
  initInputs();
  initButtons();
  initCombos();
  initApptModal();
}

function initInputs() {
  $("#appointmentFieldId1").hide();
  $("#serviceFieldId1").hide();
  $("#serviceFieldId2").hide();
  $("#actionButtons").hide();
  $("#txtSearchKeyParameter").focus();
}

function initButtons() {
  $("#btnSearch").click(function () {
    findUnit(true);
  });

  $("#btnSaveCoordination").click(function () {
    saveCoordination();
  });

  $("#btnClean").click(function () {
    cleanSearchParams();
  });

  $("#btnUpdateStorageContainer").click(function () {
    updateStorageUnit();
  });

  $("#btnAddPumAppt").click(function () {
    validateDMOrder();
  });

  $("#btnSelectApptOrderItem").click(function () {
    validateDMOrder(true, $("#cbApptOrderItem option:selected").val());
  });
}

function initCombos() {
  loadInitCombos();
  $("#cbSearchCategory").change(function () {
    var selectedValue = $(this).val();
    var hideOptionalView = false;
    if (selectedValue == "EXPRT" || selectedValue == "APPT") {
      $("#txtSearchKeyParameter").prop("maxLength", 50);
      var label = selectedValue == "EXPRT" ? "Booking" : "Booking/EDO";
      $("label[for='txtSearchKeyParameter']").text(label + " (*)");
    } else {
      $("#txtSearchKeyParameter").prop("maxLength", 11);
      $("label[for='txtSearchKeyParameter']").text("Contenedor (*)");
      if (selectedValue == "STRGE") {
        $("#cbSearchLineOp").val("");
        hideOptionalView = true;
      }
    }
    if (hideOptionalView) $(".optionalView").hide();
    else $(".optionalView").show();
    if (selectedValue != "APPT") {
      $("#actionButtons").hide();
    }
    $("#txtSearchKeyParameter").focus();
  });

  $("#cbCoordinationType").change(function () {
    loadUnitCalendar(false);
  });
}

function initApptModal() {
  $("#viewUnitCalendarModal").on("shown.bs.modal", function () {
    loadUnitCalendar(true);
    $("#calendar").fullCalendar("render");
  });

  $("#viewUnitCalendarModal").on("hidden.bs.modal", function () {
    adjustDataTableColumns();
    cleanModalInputs();
  });

  $("#viewApptUnitSealModal").on("hidden.bs.modal", function () {
    cleanSealModalInputs();
  });

  $("#selectCoordinationTimeModal").on("hidden.bs.modal", function () {
    cleanDateModalInputs();
  });

  $("#viewUnitSrvOrderModal").on("shown.bs.modal", function () {
    adjustDataTableColumns();
  });

  $("#viewUnitSrvOrderModal").on("hidden.bs.modal", function () {
    deleteUnitServiceOrderDataTable();
    adjustDataTableColumns();
  });

  $("#updateStorageContainerModal").on("hidden.bs.modal", function () {
    cleanStorageModalInputs();
    adjustDataTableColumns();
  });

  $("#selectApptOrderItemModal").on("hidden.bs.modal", function () {
    $("#cbApptOrderItem").val("");
  });
}

function initCalendar(data) {
  var minDate = data["minDate"];
  var maxDate = data["maxDate"];
  $("#calendar").fullCalendar({
    header: {
      left: "prev",
      center: "title",
      right: "next",
      ignoreTimezone: true,
    },
    defaultDate: minDate,
    defaultView: "month",
    monthNames: [
      "Enero",
      "Febrero",
      "Marzo",
      "Abril",
      "Mayo",
      "Junio",
      "Julio",
      "Agosto",
      "Septiembre",
      "Octubre",
      "Noviembre",
      "Diciembre",
    ],
    dayNamesShort: ["D", "L", "M", "X", "J", "V", "S"],
    viewRender: function (view) {
      if (view.end < minDate) {
        $("#calendar").fullCalendar("gotoDate", minDate);
      } else if (view.start > maxDate) {
        $("#calendar").fullCalendar("gotoDate", maxDate);
      }
    },
    events: function (start, end, timezone, callback) {
      var events = [];
      data["calendarEventList"].forEach(function (opening) {
        events.push({
          id: opening["id"],
          title: opening["title"],
          start: opening["dateStr"],
          color: opening["color"],
        });
      });
      callback(events);
    },
    eventClick: function (event, jsEvent, view) {
      // when some one click on any event
      resolveCalendarEvent(event);
    },
  });
}

function findUnit(showProcessing) {
  cleanDocumentGkey();
  var errorMsg = validateSearchFields();
  if (errorMsg == "") {
    if (showProcessing) showProcessingModal();
    deleteUnitDataTable();
    var unitCategory = $("#cbSearchCategory option:selected").val();
    var searchParam = {
      prUnitCategory: unitCategory,
      prKeyParameter: $("#txtSearchKeyParameter").val(),
      prLineOpId: $("#cbSearchLineOp option:selected").val(),
    };
    $.ajax({
      type: "GET",
      url: url_application + "/CoordinationManagement/findUnit/",
      data: searchParam,
      success: function (data, statusCode) {
        var msg = data["msg"];
        if (typeof msg != "undefined" && !resultIsOk(data)) {
          showErrorHtmlMsg(msg);
        } else {
          var containerNbr = data["unitId"];
          documentGkey = data["documentGkey"];
          console.log("documentGkey:" + documentGkey);
          if (typeof containerNbr != "undefined") {
            fillStorageInputModal(data);
          } else if (typeof documentGkey != "undefined") {
            $("#actionButtons").show();
            showUnitTable(data["unitList"], unitCategory);
          } else {
            showUnitTable(data, unitCategory);
          }
          if (showProcessing) closeProcessingModal();
        }
      },
      fail: function (data, statusCode) {
        s;
        showErrorMsg("Status code:" + statusCode);
      },
    });
  } else showErrorMsg(errorMsg);
}

function resolveCalendarEvent(event) {
  var paramObject = {};
  paramObject["unitDto"] = unitDto;
  paramObject["event"] = event;
  paramObject["coordinationType"] = $(
    "#cbCoordinationType option:selected",
  ).val();
  showProcessingModal();
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/CoordinationManagement/resolveCalendarEvent/",
    data: JSON.stringify(paramObject),
    success: function (data) {
      closeProcessingModal();
      if (data != null && data != "") {
        fillDateInputModal(event.id, data);
      }
    },
  });
}

function viewUnitCalendar(unitToCoordinate) {
  unitDto = unitToCoordinate;
  fillInputModal(unitToCoordinate);
}

function updateStorageUnit() {
  var isoTypeId = $("#cbContainerIsoTypeId").val();
  var lineOpId = $("#cbContainerLineOp").val();
  if (isoTypeId == null || isoTypeId == "")
    showErrorMsg("Por favor seleccione el tipo del contenedor");
  else if (lineOpId == null || lineOpId == "")
    showErrorMsg("Por favor seleccione la línea operadora del contenedor");
  else {
    unitDto["storageIsoTypeId"] = isoTypeId;
    unitDto["storageLineOpId"] = lineOpId;
    unitDto["unitLineOpId"] = lineOpId;
    $("#btnCancelUpdateStorageContainer").click();
    viewUnitCalendar(unitDto);
  }
}

function validateDMOrder(selecteItem, selectedItemGkey) {
  var documentParam = {
    prDocumentGkeyStr: typeof documentGkey != "undefined" ? documentGkey : "",
    prDocumentItemGkeyStr:
      typeof selectedItemGkey != "undefined" ? selectedItemGkey : "",
    prSelectItem: typeof selecteItem != "undefined" ? selecteItem : "",
  };
  showProcessingModal();
  $.ajax({
    type: "GET",
    url: url_application + "/CoordinationManagement/validateDMOrder/",
    data: documentParam,
    success: function (result, statusCode) {
      closeProcessingModal();
      var msg = result["msg"];
      if (typeof msg != "undefined" && !resultIsOk(result)) {
        showErrorHtmlMsg(msg);
      } else {
        var documentNbr = result["documentNbr"];
        if (typeof documentNbr != "undefined") {
          $("#btnCancelSelectApptOrderItem").click();
          viewUnitCalendar(result);
        } else {
          loadApptOrderItemCb(result);
          $("#selectApptOrderItemModal").modal();
        }
      }
    },
    fail: function (data, statusCode) {
      closeProcessingModal();
      showModal("Error", "Status code:" + statusCode, "error");
    },
  });
}

function loadInitCombos() {
  $.ajax({
    type: "GET",
    url: url_application + "/CoordinationManagement/loadInitCombos/",
    success: function (data, statusCode) {
      loadSearchCategoryCb(data);
      loadSearchLineOpCb(data);
    },
    fail: function (data, statusCode) {
      showModal("Error", "Status code:" + statusCode, "error");
    },
  });
}

function loadCoordinationTypeCombo(unitToCoordinate) {
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/CoordinationManagement/getCoordinationTypes/",
    data: JSON.stringify(unitToCoordinate),
    success: function (data) {
      loadCoordinationTypeCb(data);
    },
  });
}

function showUnitTable(data, unitCategory) {
  coordinationViewModel.unitTable.removeAll();
  deleteUnitDataTable();
  coordinationViewModel.unitTable(data);
  var table = $("#tbUnit").DataTable({
    bDestroy: true,
    bFilter: true,
    sScrollX: true,
    sScrollY: "30vh",
    scrollCollapse: true,
    paging: false,
    bPaginate: false,
    bSort: true,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
    },
  });
  var limitDateColumnName = getLimitDateColumnName(unitCategory);
  var primaryKeyColumnName = getPrimaryKeyColumnName(unitCategory);
  $(table.column(0).header()).text(primaryKeyColumnName);
  $(table.column(10).header()).text(limitDateColumnName);
  if (unitCategory == "EXPRT") $(table.column(10).visible(false));
}

function showUnitServiceTable(data) {
  coordinationViewModel.unitServiceTable.removeAll();
  deleteUnitServiceOrderDataTable();
  coordinationViewModel.unitServiceTable(data);
  var unitSoTable = $("#tbUnitServiceOrder").DataTable({
    bDestroy: true,
    bFilter: false,
    sScrollX: false,
    sScrollY: "30vh",
    scrollCollapse: true,
    paging: false,
    bPaginate: false,
    bFilter: false,
    bSort: false,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
    },
  });
}

function refreshSearchData() {
  findUnit(false);
}

function loadSearchCategoryCb(data) {
  coordinationViewModel.categoryCb(data["categoryList"]);
}

function loadSearchLineOpCb(data) {
  coordinationViewModel.lineOpCb(data["lineOpList"]);
}

function loadCoordinationTypeCb(data) {
  coordinationViewModel.coordinationTypeCb(data);
}

function loadArchTypeCb(data) {
  coordinationViewModel.archTypeCb(data["archTypeList"]);
}

function loadApptOrderItemCb(data) {
  coordinationViewModel.apptOrderItemCb(data);
}

function fillInputModal(unitToCoordinate) {
  var category = unitToCoordinate["unitCategory"];
  if (category != "APPT") {
    $("#txtUnitId").val(unitToCoordinate["unitId"]);
    $("label[for='txtUnitId']").text("Contenedor:");
    if (category == "EXPRT") {
      $("#divLimitDate").hide();
    } else {
      $("#txtLimitDate").val(unitToCoordinate["unitLimitDateStr"]);
      var limitDateLabel = getLimitDateColumnName(category);
      $("label[for='txtLimitDate']").text(limitDateLabel);
    }
  } else {
    $("#txtUnitId").val(unitToCoordinate["documentNbr"]);
    $("label[for='txtUnitId']").text("Booking/EDO:");
    $("#txtLimitDate").val(unitToCoordinate["documentItemIsoType"]);
    $("label[for='txtLimitDate']").text("Tipo:");
  }
  loadCoordinationTypeCombo(unitToCoordinate);
  $("#viewUnitCalendarModal").modal();
}

function fillSealInputModal(result) {
  $("#txtApptSealNbr1").val(result["sealNbr1"]);
  $("#txtApptSealNbr2").val(result["sealNbr2"]);
  $("#txtApptSealNbr3").val(result["sealNbr3"]);
  $("#txtApptSealNbr4").val(result["sealNbr4"]);
  $("#viewApptUnitSealModal").modal();
}

function fillDateInputModal(dateStr, data) {
  coordinationViewModel.dateOpeningCb.removeAll();
  coordinationViewModel.bunchCb.removeAll();
  coordinationViewModel.dateOpeningCb(data["openingList"]);
  $("#txtSelectedDate").val(dateStr);
  var channelList = data["customsChannelList"];
  var bunchList = data["bunchList"];
  var fieldToShow = data["fieldToShow"];
  if (typeof channelList != "undefined") {
    coordinationViewModel.channelCb(channelList);
  }
  if (typeof bunchList != "undefined") {
    coordinationViewModel.bunchCb(bunchList);
  }
  if (typeof fieldToShow != "undefined") {
    $(fieldToShow).show();
  }
  $("#selectCoordinationTimeModal").modal();
}

function fillStorageInputModal(data) {
  loadArchTypeCb(data);
  $("#txtContainerNbr").val(data["unitId"]);
  var isoTypeId = data["unitIsoTypeId"];
  var lineOpId = data["unitLineOpId"];
  var requireStorageInfo = data["requireStorageInfo"];
  if (isoTypeId != null) {
    $("#cbContainerIsoTypeId").prop("disabled", true);
    $("#cbContainerIsoTypeId").val(isoTypeId).change();
  }
  if (lineOpId != null) {
    $("#cbContainerLineOp").prop("disabled", true);
    $("#cbContainerLineOp").val(lineOpId).change();
  }
  delete data["archTypeList"];
  unitDto = data;
  if (typeof requireStorageInfo != "undefined" && requireStorageInfo)
    $("#updateStorageContainerModal").modal();
  else {
    closeProcessingModal();
    cleanStorageModalInputs();
    viewUnitCalendar(unitDto);
  }
}

function validateSearchFields() {
  var searchCategory = $("#cbSearchCategory option:selected").val();
  var searchKeyParameter = $("#txtSearchKeyParameter").val();
  var searchLineOpId = $("#cbSearchLineOp option:selected").val();
  var errorMsg = "";
  if (searchCategory == null || searchCategory == "")
    errorMsg = "Por favor seleccione la categoría del contenedor";
  else if (searchKeyParameter == null || searchKeyParameter == "") {
    errorMsg = "Por favor ingrese el campo obligatorio (*) para la búsqueda";
  } else if (!isValidStr(searchKeyParameter))
    errorMsg = "Por favor elimine los caracteres especiales de la búsqueda";
  return errorMsg;
}

function cleanSearchParams() {
  $("#cbSearchCategory").val("");
  $("#txtSearchKeyParameter").val("");
  $("#cbSearchLineOp").val("");
  cleanDocumentGkey();
  deleteDataTables();
}

function cleanModalInputs() {
  unitDto = {};
  coordinationViewModel.coordinationTypeCb.removeAll();
  $("#txtUnitId").val("");
  $("#txtLimitDate").val("");
  $("#divLimitDate").show();
  $("#cbCoordinationType").val("");
  $("#calendar").fullCalendar("destroy");
}

function cleanSealModalInputs() {
  $("#txtApptSealNbr1").val("");
  $("#txtApptSealNbr2").val("");
  $("#txtApptSealNbr3").val("");
  $("#txtApptSealNbr4").val("");
}

function cleanDateModalInputs() {
  coordinationViewModel.dateOpeningCb.removeAll();
  coordinationViewModel.channelCb.removeAll();
  coordinationViewModel.bunchCb.removeAll();
  $("#txtEmail").val("");
  $("#cbBunch").val("");
  $("#cbChannel").val("");
  $("#appointmentFieldId1").hide();
  $("#serviceFieldId1").hide();
  $("#serviceFieldId2").hide();
}

function cleanStorageModalInputs() {
  coordinationViewModel.archTypeCb.removeAll();
  $("#txtContainerNbr").val("");
  $("#cbContainerIsoTypeId").val("");
  $("#cbContainerLineOp").val("");
  $("#cbContainerIsoTypeId").prop("disabled", false);
  $("#cbContainerLineOp").prop("disabled", false);
}

function cleanDocumentGkey() {
  if (typeof documentGkey != undefined) documentGkey = undefined;
}

function cleanDocumentItemGkey() {
  if (typeof documentItemGkey != undefined) documentItemGkey = undefined;
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function deleteUnitDataTable() {
  $("#tbUnit").DataTable().clear();
  $("#tbUnit").DataTable().destroy();
}

function deleteUnitServiceOrderDataTable() {
  $("#tbUnitServiceOrder").DataTable().clear();
  $("#tbUnitServiceOrder").DataTable().destroy();
}

function getLimitDateColumnName(unitCategory) {
  return unitCategory == "EXPRT"
    ? "Cutoff"
    : unitCategory == "IMPRT"
      ? "Forzoso"
      : unitCategory == "STRGE"
        ? "Límite Devolución"
        : "Fecha límite";
}

function getPrimaryKeyColumnName(unitCategory) {
  return unitCategory == "APPT" ? "Num. Turno" : "Contenedor";
}

function resultIsOk(result) {
  var status = result["status"];
  return status == "OK";
}

function resultIsError(result) {
  var status = result["status"];
  return status == "ERROR";
}

function resultRequiresConfirm(result) {
  var status = result["status"];
  return status == "QUESTION";
}

function resultIsWarning(result) {
  var status = result["status"];
  return status == "WARNING";
}

function adjustDataTableColumns() {
  $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
}

function isValidStr(str) {
  return !/[~`!#$%\^&*+=\\[\]\\';,/{}|\\":<>\?]/g.test(str);
}

function viewUnitSealsIntern(unitToShow) {
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/CoordinationManagement/viewUnitApptSeals/",
    data: JSON.stringify(unitToShow),
    success: function (result) {
      var msg = result["msg"];
      if (resultIsError(result)) {
        showErrorMsg(msg);
      } else if (resultRequiresConfirm(result)) {
        Swal.fire({
          allowOutsideClick: false,
          title: "Desea continuar?",
          text: msg,
          type: "question",
          showCancelButton: true,
          confirmButtonColor: "#4fa7f3",
          cancelButtonColor: "#d57171",
          confirmButtonText: "Sí, ver precintos!",
        }).then((result) => {
          if (result.value) {
            unitToShow["addEvent"] = true;
            viewUnitSealsIntern(unitToShow);
          } else if (result.dismiss === Swal.DismissReason.cancel) {
            delete unitToShow["addEvent"];
          }
        });
      } else {
        fillSealInputModal(result);
      }
    },
    fail: function (result, statusCode) {
      showErrorMsg(
        statusCode +
          " Se produjo un error obteniendo los precintos. Por favor intente nuevamente",
      );
    },
  });
}

function viewUnitServicesIntern(unitToShowSrvOrder) {
  showProcessingModal();
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/CoordinationManagement/viewUnitServices/",
    data: JSON.stringify(unitToShowSrvOrder),
    success: function (data) {
      closeProcessingModal();
      showUnitServiceTable(data);
      $("#viewUnitSrvOrderModal").modal();
    },
    fail: function (data, statusCode) {
      closeProcessingModal();
      showErrorMsg(
        statusCode +
          " Se produjo un error obteniendo los servicios del contenedor. Por favor intente nuevamente",
      );
    },
  });
}

function loadUnitCalendar(isOnOpenModal) {
  if (typeof unitDto != "undefined" && !_.isEmpty(unitDto)) {
    $("#calendar").fullCalendar("destroy");
    unitDto["coordinationType"] = $(
      "#cbCoordinationType option:selected",
    ).val();
    showProcessingModal();
    $.ajax({
      type: "POST",
      contentType: "application/json",
      url: url_application + "/CoordinationManagement/getUnitCalendar/",
      data: JSON.stringify(unitDto),
      success: function (data) {
        closeProcessingModal();
        var msg = data["msg"];
        if (resultIsWarning(data)) showWarningMsg(msg);
        else if (resultRequiresConfirm(data)) {
          Swal.fire({
            allowOutsideClick: false,
            title: "Desea registrar el servicio?",
            text: msg,
            type: "question",
            showCancelButton: true,
            confirmButtonColor: "#4fa7f3",
            cancelButtonColor: "#d57171",
            confirmButtonText: "Sí, registrar!",
          }).then((result) => {
            if (result.value) {
              saveCoordination();
            } else if (result.dismiss === Swal.DismissReason.cancel) {
              adjustDataTableColumns();
            }
          });
        } else {
          initCalendar(data);
          var selectedCoordinationType = $(
            "#cbCoordinationType option:selected",
          ).val();
          if (
            selectedCoordinationType == null ||
            selectedCoordinationType == ""
          ) {
            var comboLength = $("#cbCoordinationType option").length;
            if (isOnOpenModal && comboLength == 2) {
              var selectedIndex = $("#cbCoordinationType").prop(
                "selectedIndex",
              );
              $("#cbCoordinationType").prop("selectedIndex", 1).change();
            } else {
              showTemporaryMsg(
                "",
                "Seleccioná un servicio para ver los turnos disponibles",
                2000,
              );
            }
          }
        }
        //$("#calendar").fullCalendar("render");
      },
      fail: function (data, statusCode) {
        closeProcessingModal();
        showErrorMsg("Status code:" + statusCode);
      },
    });
  }
}

function saveCoordination() {
  var paramObject = {};
  paramObject["unitDto"] = unitDto;
  paramObject["coordinationType"] = $(
    "#cbCoordinationType option:selected",
  ).val();
  paramObject["selectedDateTime"] = $("#cbDateOpening option:selected").val();
  paramObject["customsChannel"] = $("#cbChannel option:selected").val();
  paramObject["bunch"] = $("#cbBunch option:selected").val();
  paramObject["email"] = $("#txtEmail").val();
  showProcessingModal();
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/CoordinationManagement/saveCoordination/",
    data: JSON.stringify(paramObject),
    success: function (data) {
      closeProcessingModal();
      var msg = data["msg"];
      var questionType = data["questionType"];
      if (resultIsError(data)) {
        showErrorMsg(msg);
      } else if (resultRequiresConfirm(data)) {
        Swal.fire({
          allowOutsideClick: false,
          title: "Desea continuar?",
          text: msg,
          type: "question",
          showCancelButton: true,
          confirmButtonColor: "#4fa7f3",
          cancelButtonColor: "#d57171",
          confirmButtonText: "Sí, coordinar!",
        }).then((result) => {
          if (result.value) {
            unitDto[questionType] = true;
            saveCoordination();
            delete unitDto[questionType];
          } else if (result.dismiss === Swal.DismissReason.cancel) {
            delete unitDto[questionType];
          }
        });
      } else if (resultIsOk(data)) {
        delete unitDto[questionType];
        $("#btnCancelSaveCoordination").click();
        $("#btnCancelViewUnitCalendar").click();
        refreshSearchData();
        showSuccessHtmlMsg("Listo!", msg);
      } else {
        closeProcessingModal();
        showErrorMsg(
          statusCode +
            "Se produjo un error guardando la coordinación. Por favor intente nuevamente",
        );
      }
    },
    fail: function (data, statusCode) {
      closeProcessingModal();
      showErrorMsg(
        statusCode +
          "Se produjo un error guardando el menú. Por favor intente nuevamente",
      );
    },
  });
}

function cancelUnitAppointment(apptToCancel) {
  showProcessingModal();
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/CoordinationManagement/cancelUnitAppointment/",
    data: JSON.stringify(apptToCancel),
    success: function (data) {
      closeProcessingModal();
      var msg = data["msg"];
      var questionType = data["questionType"];
      if (resultIsError(data)) {
        showErrorMsg(msg);
      } else if (resultRequiresConfirm(data)) {
        Swal.fire({
          allowOutsideClick: false,
          title: "Desea continuar?",
          text: msg,
          type: "question",
          showCancelButton: true,
          confirmButtonColor: "#4fa7f3",
          cancelButtonColor: "#d57171",
          confirmButtonText: "Sí, eliminar!",
        }).then((result) => {
          if (result.value) {
            apptToCancel[questionType] = true;
            cancelUnitAppointment(apptToCancel);
          } else if (result.dismiss === Swal.DismissReason.cancel) {
            adjustDataTableColumns();
            delete apptToCancel[questionType];
          }
        });
      } else {
        refreshSearchData();
        showSuccessMsg("Listo!", msg);
      }
    },
    fail: function (result, statusCode) {
      showErrorMsg(
        statusCode +
          " Se produjo un eliminando el turno. Por favor intente nuevamente",
      );
    },
  });
}

function cancelUnitService(serviceToCancel) {
  Swal.fire({
    allowOutsideClick: false,
    title: "Desea cancelar el servicio?",
    text: "",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: "#4fa7f3",
    cancelButtonColor: "#d57171",
    confirmButtonText: "Sí, cancelar!",
  }).then((result) => {
    if (result.value) {
      showProcessingModal();
      $.ajax({
        type: "POST",
        contentType: "application/json",
        url: url_application + "/CoordinationManagement/cancelUnitService/",
        data: JSON.stringify(serviceToCancel),
        success: function (data) {
          closeProcessingModal();
          var msg = data["msg"];
          if (resultIsError(data)) {
            showErrorMsg(msg);
          } else {
            refreshSearchData();
            showUnitServiceTable(data["unitSoList"]);
            showSuccessMsg("Listo!", msg);
          }
        },
        fail: function (data, statusCode) {
          showErrorMsg(
            statusCode +
              " Se produjo un error cancelando el servicio. Por favor intente nuevamente",
          );
        },
      });
    } else if (result.dismiss === Swal.DismissReason.cancel) {
      adjustDataTableColumns();
    }
  });
}
