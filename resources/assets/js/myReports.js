var loaded = false;
var reportNbr = 0;
$(document).ready(function () {
  getCustomerReports();
});

function getCustomerReports() {
  var request = $.ajax({
    type: "POST",
    url: url_application + "/balance/getMyCustomerReports",
    timeout: 600000,
  });

  request.done(function (data) {
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    loadCustomerReportCb(data);
    $("#tableBox").show();
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", jqXHR.responseText, "error");
  });
}

function loadCustomerReportById(docs, items, files) {
  //viewModel.customerReportDocuments.removeAll();
  //viewModel.customerReportItems.removeAll();
  viewModel.customerReportDocuments(docs);
  viewModel.customerReportItems(items);
  viewModel.customerReportFiles(files);
}

function loadAdditionalDataFromRow(data, event) {
  reportNbr = data.reportNumber.toString();
  $("#titleInformationReport").text(
    "Reporte #" + reportNumberLeftPad(reportNbr),
  );
  loadCustomerReportById(data.docs, data.items, data.files);
  $("#modalInformation").modal();
}

function reportNumberLeftPad(value) {
  return value.toString().padStart(5, "0");
}

function parseMoneyValue(value) {
  return parseFloat(value, 10)
    .toFixed(2)
    .replace(/(\d)(?=(\d{3})+\.)/g, "$1,")
    .toString();
}

function inputMoneyValue(input) {
  $("#" + input.id).val(
    parseFloat(input.value, 10)
      .toFixed(2)
      .replace(/(\d)(?=(\d{3})+\.)/g, "$1,")
      .toString(),
  );
}

function getDateTime(value) {
  return new Date(value).toLocaleDateString();
}

function initKnockout() {
  viewModel = {
    customerReport: ko.observableArray([]),
    customerReportDocuments: ko.observableArray([]),
    customerReportItems: ko.observableArray([]),
    customerReportFiles: ko.observableArray([]),
  };
  ko.applyBindings(viewModel);
}

function loadCustomerReportCb(data) {
  showCustomerReportGrid(data);
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function showCustomerReportGrid(data) {
  viewModel.customerReport.removeAll();
  deleteDataTables();

  viewModel.customerReport(data.reportData);
  //showModalHtml('Informacion',ko.toJSON(data),'info');
  //$("#tableBox").show();
  newDataTable("tbCustomerReport", null, true, true, true);
  //$("#tbBalance").show();
}

function downloadFile(data, event) {
  const linkSource = "data:application/pdf;base64," + data.filePath;
  const downloadLink = document.createElement("a");
  const fileName =
    "ReportNro" + data.reportNumber + "_" + makeid(10).toUpperCase() + ".pdf";

  downloadLink.href = linkSource;
  downloadLink.download = fileName;
  downloadLink.click();
}

function makeid(length) {
  var result = "";
  var characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}
