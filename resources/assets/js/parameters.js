/**
 * Author: Lucas Juarez
 * APM Terminals Buenos Aires
 */

$(document).ready(function () {
  $('[data-plugin="switchery"]').each(function (idx, obj) {
    new Switchery($(this)[0], $(this).data());
  });

  if ($("#costCenterSwitch").prop("checked")) {
    $("#lblCostCenterText").text("AFIP");
  } else {
    $("#lblCostCenterText").text("Contingencia");
  }

  $("#costCenterSwitch").on("change.bootstrapSwitch", function (e) {
    if (e.target.checked) {
      updateCostCenter(1);
      $("#lblCostCenterText").text("AFIP");
    } else {
      updateCostCenter(2);
      $("#lblCostCenterText").text("Contingencia");
    }
  });
});

function updateCostCenter(id) {
  if (id == undefined || id == null || id == "") {
    Swal.fire(
      "",
      "Error al actualizar el Centro de Costo, por favor, refresque la página.",
      "error",
    );
    return;
  }

  processingModal();

  var request = $.ajax({
    type: "POST",
    enctype: "multipart/form-data",
    url: url_application + "/Parameter/changeCostCenter/" + id,
    processData: false,
    contentType: false,
    cache: false,
    timeout: 15000,
  });

  request.done(function (msg) {
    showTimerModal("", msg, 2000);
  });

  request.fail(function (jqXHR, textStatus) {
    Swal.fire("", jqXHR.responseText, "error");
  });
}

function showTimerModal(title, text, timerMillis) {
  Swal.fire({
    title: title,
    html: text,
    timer: timerMillis,
    onBeforeOpen: () => {
      Swal.showLoading();
    },
  });
}

function processingModal() {
  Swal.fire({
    title: "Procesando...",
    text: "Aguarde un instante por favor",
    imageUrl: url_application + "/resources/assets/images/ajax_loading.gif",
    showConfirmButton: false,
    allowOutsideClick: false,
  });
}
