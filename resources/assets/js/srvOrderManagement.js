/**
 * Author: Jimena Chavez
 *
 */

$(document).ready(function () {
  initHtmlElements();
  initKnockout();
});

function initKnockout() {
  soViewModel = {
    categoryCb: ko.observableArray([]),
    lineOpCb: ko.observableArray([]),
    serviceDocument: ko.observable(),
    unitServiceTable: ko.observableArray([]),
    activeUnitList: ko.observableArray([]),
    serviceTypeCb: ko.observableArray([]),
    dateOpeningCb: ko.observableArray([]),
    bunchCb: ko.observableArray([]),
    cancelSo: function (soToCancel) {
      cancelUnitService(soToCancel);
    },
  };
  ko.applyBindings(soViewModel);
}

function initHtmlElements() {
  initInputs();
  initButtons();
  initCombos();
  initServiceModal();
  initCheckBoxs();
}

function initInputs() {
  $("#txtSearchKeyParameter").focus();
  $("#serviceFieldId1").hide();
  $("#serviceFieldId2").hide();
}

function initButtons() {
  $("#btnSearch").click(function () {
    findUnitService(true);
  });

  $("#btnSaveSrvOrder").click(function () {
    saveUnitService();
  });

  $("#btnClean").click(function () {
    cleanSearchParams();
  });

  $("#btnAddSrvOrder").click(function () {
    loadModalCombos();
  });

  hideAddSrvOrderButton();
}

function initCombos() {
  loadSoInitPageCb();
  $("#cbSearchCategory").change(function () {
    var selectedValue = $(this).val();
    if (selectedValue == "EXPRT") {
      $("#txtSearchKeyParameter").prop("maxLength", 50);
      $("label[for='txtSearchKeyParameter']").text("Booking (*)");
    } else {
      $("#txtSearchKeyParameter").prop("maxLength", 11);
      $("label[for='txtSearchKeyParameter']").text("Contenedor (*)");
    }
    $("#txtSearchKeyParameter").focus();
  });

  $("#cbActiveUnit").change(function () {
    loadServiceCalendar();
  });

  $("#cbServiceType").change(function () {
    loadServiceCalendar();
  });

  $("#cbSearchCategory option[value=IMPRT]").attr("selected", "selected");
}

function initServiceModal() {
  $("#viewSrvOrderCalendarModal").on("hidden.bs.modal", function () {
    adjustDataTableColumns();
    cleanModalInputs();
  });

  $("#selectSrvOrderTimeModal").on("hidden.bs.modal", function () {
    cleanDateModalInputs();
  });
}

function initCheckBoxs() {
  $("#ckCheckAll").change(function () {
    var check = $(this).is(":checked");
    var canBeSelected = $(".selectable:enabled");
    canBeSelected.each(function () {
      if (check) $(this).prop("checked", true);
      else $(this).prop("checked", false);
    });
  });

  $("#ckCheckAllPrivilege").change(function () {
    var check = $(this).is(":checked");
    var canBeSelected = $(".selectablePrivilege:enabled");
    canBeSelected.each(function () {
      if (check) $(this).prop("checked", true);
      else $(this).prop("checked", false);
    });
  });
}

function findUnitService(showProcessing) {
  var errorMsg = validateSearchFields();
  if (errorMsg == "") {
    if (showProcessing) showProcessingModal();
    hideAddSrvOrderButton();
    deleteDataTables();
    var category = $("#cbSearchCategory option:selected").val();
    var searchParam = {
      prCategory: category,
      prKeyParameter: $("#txtSearchKeyParameter").val(),
      prLineOpId: $("#cbSearchLineOp option:selected").val(),
    };
    $.ajax({
      type: "GET",
      url: url_application + "/SrvOrderManagement/findUnitService/",
      data: searchParam,
      success: function (data, statusCode) {
        var msg = data["msg"];
        if (typeof msg != "undefined" && !resultIsOk(data)) {
          showErrorMsg(msg);
        } else {
          if (showProcessing) closeProcessingModal();
          showUnitServiceTable(data, category);
          loadServiceDocument(data);
          showAddSrvOrderButton();
          var question = data["question"];
          if (typeof question != "undefined" && question != "") {
            Swal.fire({
              allowOutsideClick: false,
              title: question,
              text: "No hay servicios registrados",
              type: "question",
              showCancelButton: true,
              confirmButtonColor: "#4fa7f3",
              cancelButtonColor: "#d57171",
              confirmButtonText: "Sí, coordinar!",
            }).then((result) => {
              if (result.value) {
                $("#btnAddSrvOrder").click();
              }
            });
          }
        }
      },
      fail: function (data, statusCode) {
        s;
        showErrorMsg("Status code:" + statusCode);
      },
    });
  } else showErrorMsg(errorMsg);
}

function loadModalCombos() {
  showProcessingModal();
  $.ajax({
    type: "POST",
    contentType: "application/json; charset=utf-8",
    url: url_application + "/SrvOrderManagement/loadModalCombos/",
    data: ko.toJSON(soViewModel.serviceDocument),
    success: function (data, statusCode) {
      var msg = data["msg"];
      if (typeof msg != "undefined" && !resultIsOk(data)) {
        showErrorMsg(msg);
      } else {
        loadActiveUnitCb(data);
        loadServiceTypeCb(data);
        closeProcessingModal();
        $("#viewSrvOrderCalendarModal").modal();
      }
    },
    fail: function (data, statusCode) {
      closeProcessingModal();
      showErrorMsg("Status code:" + statusCode);
    },
  });
}

function loadServiceCalendar() {
  var selectedUnit = $("#cbActiveUnit option:selected").val();
  var selectedService = $("#cbServiceType option:selected").val();
  $("#calendar").fullCalendar("destroy");
  if (
    selectedUnit != null &&
    selectedUnit != "" &&
    selectedService != null &&
    selectedService != ""
  ) {
    var searchParam = {
      prUnitGkeyStr: selectedUnit,
      prServiceTypeKey: selectedService,
    };
    showProcessingModal();
    $.ajax({
      type: "GET",
      url: url_application + "/SrvOrderManagement/getServiceDates/",
      data: searchParam,
      success: function (data, statusCode) {
        closeProcessingModal();
        console.log("data:" + JSON.stringify(data));
        var msg = data["msg"];
        if (resultIsWarning(data)) showWarningMsg(msg);
        else if (resultRequiresConfirm(data)) {
          //TODO show to save service
          console.log("question...");
          Swal.fire({
            allowOutsideClick: false,
            title: "Desea registrar el servicio?",
            text: msg,
            type: "question",
            showCancelButton: true,
            confirmButtonColor: "#4fa7f3",
            cancelButtonColor: "#d57171",
            confirmButtonText: "Sí, registrar!",
          }).then((result) => {
            if (result.value) {
              saveUnitService();
            } else if (result.dismiss === Swal.DismissReason.cancel) {
              adjustDataTableColumns();
            }
          });
        } else initCalendar(data, selectedUnit, selectedService);
        //$("#calendar").fullCalendar("render");
      },
      fail: function (data, statusCode) {
        closeProcessingModal();
        showErrorMsg("Status code:" + statusCode);
      },
    });
  }
}

function initCalendar(data, unitGkey, serviceTypeKey) {
  var minDate = data["serviceMinDate"];
  var maxDate = data["serviceMaxDate"];
  console.log("data:" + JSON.stringify(data));
  console.log("minDate:" + minDate + ", maxDate:" + maxDate);
  $("#calendar").fullCalendar({
    header: {
      left: "prev",
      center: "title",
      right: "next",
      ignoreTimezone: true,
    },
    defaultDate: minDate,
    defaultView: "month",
    monthNames: [
      "Enero",
      "Febrero",
      "Marzo",
      "Abril",
      "Mayo",
      "Junio",
      "Julio",
      "Agosto",
      "Septiembre",
      "Octubre",
      "Noviembre",
      "Diciembre",
    ],
    dayNamesShort: ["D", "L", "M", "X", "J", "V", "S"],
    viewRender: function (view) {
      if (view.end < minDate) {
        $("#calendar").fullCalendar("gotoDate", minDate);
      } else if (view.start > maxDate) {
        $("#calendar").fullCalendar("gotoDate", maxDate);
      }
    },
    events: function (start, end, timezone, callback) {
      showProcessingModal();
      var param = {
        prUnitGkeyStr: unitGkey,
        prServiceTypeKey: serviceTypeKey,
      };
      $.ajax({
        type: "GET",
        contentType: "application/json",
        url: url_application + "/SrvOrderManagement/getServiceOpenings/",
        data: param,
        success: function (data) {
          var events = [];
          data.forEach(function (opening) {
            events.push({
              id: opening["id"],
              title: opening["title"],
              start: opening["dateStr"],
              color: opening["color"],
            });
          });
          callback(events);
          closeProcessingModal();
        },
      });
    },
    eventClick: function (event, jsEvent, view) {
      // when some one click on any event
      resolveCalendarEvent(event.id, event.title);
    },
  });
}

function resolveCalendarEvent(eventId, eventTitle) {
  var param = {
    prUnitGkeyStr: $("#cbActiveUnit option:selected").val(),
    prServiceTypeKey: $("#cbServiceType option:selected").val(),
    prEventId: eventId,
    prEventTitle: eventTitle,
  };
  $.ajax({
    type: "GET",
    contentType: "application/json",
    url: url_application + "/SrvOrderManagement/resolveCalendarEvent/",
    data: param,
    success: function (data) {
      console.log("resolveCalendarEvent data:" + JSON.stringify(data));
      if (data != null && data != "") {
        fillDateInputModal(eventId, data);
      }
    },
  });
}

function saveUnitService() {
  var email = $("#txtEmail").val();
  if (!isValidStr(email))
    showErrorMsg("Por favor elimine los caracteres especiales del email");
  else {
    var soParam = {
      prUnitGkeyStr: $("#cbActiveUnit option:selected").val(),
      prServiceTypeKey: $("#cbServiceType option:selected").val(),
      prSelectedDateTime: $("#cbDateOpening option:selected").val(),
      prEmail: $("#txtEmail").val(),
      prBunch: $("#cbBunch option:selected").val(),
    };
    showProcessingModal();
    $.ajax({
      type: "POST",
      url: url_application + "/SrvOrderManagement/createUnitService/",
      data: soParam,
      success: function (result) {
        closeProcessingModal();
        var msg = result["msg"];
        if (resultIsOk(result)) {
          showSuccessMsg("Listo!", msg);
          $("#btnCancelSaveSrvOrder").click();
          $("#btnCancelViewSrvOrderCalendar").click();
          refreshSearchData();
        } else showErrorMsg(msg);
      },
      fail: function (result, statusCode) {
        closeProcessingModal();
        showErrorMsg(
          statusCode +
            "Se produjo un error guardando el servicio. Por favor intente nuevamente",
        );
      },
    });
  }
}

function cancelUnitService(unitServiceToCancel) {
  Swal.fire({
    allowOutsideClick: false,
    title: "Desea cancelar el servicio?",
    text: "",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: "#4fa7f3",
    cancelButtonColor: "#d57171",
    confirmButtonText: "Sí, cancelar!",
  }).then((result) => {
    if (result.value) {
      showProcessingModal();
      $.ajax({
        type: "POST",
        contentType: "application/json",
        url: url_application + "/SrvOrderManagement/cancelUnitService/",
        data: JSON.stringify(unitServiceToCancel),
        success: function (result) {
          closeProcessingModal();
          var msg = result["msg"];
          if (resultIsError(result)) {
            showErrorMsg(msg);
          } else {
            showSuccessMsg("Listo!", msg);
            refreshSearchData();
          }
        },
        fail: function (result, statusCode) {
          showErrorMsg(
            statusCode +
              " Se produjo un cancelando el servicio. Por favor intente nuevamente",
          );
        },
      });
    } else if (result.dismiss === Swal.DismissReason.cancel) {
      adjustDataTableColumns();
    }
  });
}

function loadSoInitPageCb() {
  $.ajax({
    type: "GET",
    url: url_application + "/SrvOrderManagement/loadCombos/",
    success: function (data) {
      loadSearchCategoryCb(data);
      loadSearchLineOpCb(data);
    },
    fail: function (data, statusCode) {
      showModal("Error", "Status code:" + statusCode, "error");
    },
  });
}

function showUnitServiceTable(data, category) {
  soViewModel.unitServiceTable.removeAll();
  deleteDataTables();
  soViewModel.unitServiceTable(data["unitServiceList"]);
  var table = $("#tbUnitService").DataTable({
    bDestroy: true,
    bFilter: true,
    sScrollX: true,
    sScrollY: "30vh",
    scrollCollapse: true,
    paging: false,
    bPaginate: false,
    bSort: true,
    oLanguage: {
      sEmptyTable: "No hay servicios coordinados a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
    },
  });
  var dateColumnName = getDateColumnName(category);
  $(table.column(8).header()).text(dateColumnName);
}

function refreshSearchData() {
  findUnitService();
}

function loadSearchCategoryCb(data) {
  soViewModel.categoryCb(data["soCategoryList"]);
  $("#cbSearchCategory").val("IMPRT");
}

function loadSearchLineOpCb(data) {
  soViewModel.lineOpCb(data["lineOpList"]);
}

function loadServiceDocument(data) {
  soViewModel.serviceDocument(data["serviceDocument"]);
}

function loadActiveUnitCb(data) {
  soViewModel.activeUnitList.removeAll();
  soViewModel.activeUnitList(data["activeUnitList"]);
}

function loadServiceTypeCb(data) {
  soViewModel.serviceTypeCb.removeAll();
  soViewModel.serviceTypeCb(data["serviceTypeList"]);
}

function fillDateInputModal(dateStr, data) {
  console.log("time data:" + JSON.stringify(data));
  soViewModel.dateOpeningCb.removeAll();
  soViewModel.bunchCb.removeAll();
  soViewModel.dateOpeningCb(data["openingList"]);
  $("#txtSelectedDate").val(dateStr);
  var bunchList = data["bunchList"];
  var serviceField = data["serviceField"];
  if (typeof bunchList != "undefined") {
    soViewModel.bunchCb(bunchList);
  }
  if (typeof serviceField != "undefined") {
    $(serviceField).show();
  }
  $("#selectSrvOrderTimeModal").modal();
}

function validateSearchFields() {
  var searchCategory = $("#cbSearchCategory option:selected").val();
  var searchKeyParameter = $("#txtSearchKeyParameter").val();
  var searchLineOpId = $("#cbSearchLineOp option:selected").val();
  var errorMsg = "";
  if (searchCategory == null || searchCategory == "")
    errorMsg = "Por favor seleccione el tipo de operación";
  else if (searchKeyParameter == null || searchKeyParameter == "") {
    errorMsg = "Por favor ingrese el campo obligatorio (*) para la búsqueda";
  } else if (!isValidStr(searchKeyParameter))
    errorMsg = "Por favor elimine los caracteres especiales de la búsqueda";
  return errorMsg;
}

function cleanSearchParams() {
  $("#cbSearchApptSubType").val("");
  $("#txtSearchKeyParameter").val("");
  $("#cbSearchLineOp").val("");
  deleteDataTables();
  hideAddSrvOrderButton();
  $("#txtSearchKeyParameter").focus();
}

function cleanModalInputs() {
  soViewModel.activeUnitList.removeAll();
  soViewModel.serviceTypeCb.removeAll();
  $("#calendar").fullCalendar("destroy");
}

function cleanDateModalInputs() {
  soViewModel.dateOpeningCb.removeAll();
  soViewModel.bunchCb.removeAll();
  $("#txtEmail").val("");
  $("#cbBunch").val("");
  $("#serviceFieldId1").hide();
  $("#serviceFieldId2").hide();
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function getDateColumnName(category) {
  return category == "EXPRT"
    ? "Cutoff"
    : category == "IMPRT"
      ? "Forzoso"
      : "Fecha";
}

function hideAddSrvOrderButton() {
  $("#btnAddSrvOrder").hide();
  $("#btnAddSrvOrder").attr("disabled", true);
}

function showAddSrvOrderButton() {
  $("#btnAddSrvOrder").attr("disabled", false);
  $("#btnAddSrvOrder").show();
}

function resultIsOk(result) {
  var status = result["status"];
  return status == "OK";
}

function resultIsError(result) {
  var status = result["status"];
  return status == "ERROR";
}

function resultIsWarning(result) {
  var status = result["status"];
  return status == "WARNING";
}

function resultRequiresConfirm(result) {
  var status = result["status"];
  return status == "QUESTION";
}

function adjustDataTableColumns() {
  $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
}

function isValidStr(str) {
  return !/[~`!#$%\^&*+=\\[\]\\';,/{}|\\":<>\?]/g.test(str);
}
