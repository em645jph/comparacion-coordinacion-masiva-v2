/**
 * Author: Lucas Juarez
 * APM Terminals Buenos Aires
 */
var loaded = false;
$(document).ready(function () {
  getUsers();
});

function getUsers() {
  processingModal();

  var request = $.ajax({
    type: "GET",
    url: url_application + "/User/getUsers",
  });

  request.done(function (data) {
    if (data == null || data == "") {
      showModal("Sin resultados");
      return;
    }
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    loadUsersCb(data);
    Swal.close();
    $(document).tooltip();
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", "No se pudo obtener la lista de Usuarios", "error");
  });
}

function showUsersGrid(data) {
  viewModel.users.removeAll();
  deleteDataTables();
  viewModel.users(data);
  $("#tbUsers").DataTable({
    responsive: true,
    oLanguage: {
      sLoadingRecords: "Cargando datos - por favor espere...",
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
      sSearch: "Buscar",
      sLengthMenu: "Mostrar _MENU_ resultados",
      sInfoFiltered: " - filtrando de _MAX_ resultados",
      sInfoEmpty: "Sin resultados",
      sProcessing: "Cargando datos...",
      oPaginate: {
        sFirst: "Primera",
        sLast: "Última",
        sNext: "Siguiente",
        sPrevious: "Anterior",
      },
      oAria: {
        sSortAscending: ": ordenamiento ascendente",
        sSortDescending: ": ordenamiento descendente",
      },
    },
    bDestroy: true,
    bFilter: true,
    bSort: true,
    sScrollY: "40vh",
  });
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function initKnockout() {
  viewModel = {
    users: ko.observableArray([]),
  };
  ko.applyBindings(viewModel);
}

function loadUsersCb(data) {
  showUsersGrid(data);
}

function processingModal() {
  Swal.fire({
    title: "Procesando...",
    text: "Aguarde un instante por favor",
    imageUrl: url_application + "/resources/assets/images/ajax_loading.gif",
    showConfirmButton: false,
    allowOutsideClick: false,
  });
}

function millisecondsToDate(lastLogin) {
  if (lastLogin == null) {
    return "-";
  }
  var d = new Date(lastLogin);
  return d.toLocaleDateString() + " " + d.toLocaleTimeString();
}

function getRoles(input) {
  processingModal();

  var data = new Object();
  data.userGkey = input.getAttribute("usergkey");

  var request = $.ajax({
    type: "POST",
    contentType: "application/json",
    data: JSON.stringify(data),
    url: url_application + "/User/getRolesByUserGkey",
  });

  request.done(function (data) {
    if (data == null || data == "") {
      showModal("", "El usuario no tiene roles asociados", "info");
      return;
    }
    showModalHtml("Roles asociados", data, "");
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", "No se pudo obtener la lista de Usuarios", "error");
  });
}

function getMoreData(input) {
  buildTable(viewModel.users()[input.getAttribute("position")]);
}

function buildTable(data) {
  var sb = "";
  sb += "<table id='tableMoreData' class=\"table\">";
  sb += "<thead>";
  sb += "<tr>";
  sb += "<th>CLAVE</th>";
  sb += "<th>VALOR</th>";
  sb += "</tr>";
  sb += "</thead>";
  sb += "<tbody>";
  for (var k in data) {
    if (data.hasOwnProperty(k) && !noShowKeys(k)) {
      sb += "<tr>";
      sb += "<td>" + k.toUpperCase() + "</td>";
      sb += "<td>" + valueIsNull(data[k]) + "</td>";
      sb += "</tr>";
    }
  }
  sb += "</tbody>";
  sb += "</table>";
  showModalHtmlLarge("Información adicional", sb, "");
  $("#tableMoreData").DataTable({
    responsive: true,
    bFilter: true,
    bInfo: false,
    bDestroy: true,
    bSort: false,
    sScrollY: "40vh",
    oLanguage: {
      sSearch: "Buscar",
      sLengthMenu: "Mostrar _MENU_ resultados",
      oPaginate: {
        sFirst: "Primera",
        sLast: "Última",
        sNext: "Siguiente",
        sPrevious: "Anterior",
      },
    },
  });
}

function valueIsNull(value) {
  return value == null ? "-" : value;
}

function noShowKeys(key) {
  var keys = [
    "lifeCycleState",
    "gkey",
    "lastLogin",
    "codeRequested",
    "name",
    "locked",
    "enabled",
    "change",
    "billingUser",
    "failedAttempts",
    "accountNonExpired",
    "accountNonLocked",
    "credentialsNonExpired",
  ];
  return keys.includes(key);
}
