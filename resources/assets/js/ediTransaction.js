/**
 * Author: Jimena Chavez
 *
 */

$(document).ready(function () {
  initHtmlElements();
  initKnockout();
  var calendarData = {};
});

function initKnockout() {
  ediTransactionViewModel = {
    filterByCb: ko.observableArray([]),
    msgTypeCb: ko.observableArray([]),
    lineOpCb: ko.observableArray([]),
    ediTransactionTable: ko.observableArray([]),
    viewEdiFile: function (ediTransaction) {
      viewEdiFileIntern(ediTransaction);
    },
    viewErrorDetail: function (ediTransaction) {
      viewErrorDetailIntern(ediTransaction);
    },
  };
  ko.applyBindings(ediTransactionViewModel);
}

function initHtmlElements() {
  initButtons();
  initCombos();
  initDateTimePickers();
  initModal();
}

function initButtons() {
  $("#btnSearchEdiTran").click(function () {
    findEdiTransaction(true);
  });

  $("#btnCleanSearchEdiTran").click(function () {
    cleanSearchParams();
  });

  $("#btnDownloadEdiFile").click(function () {
    downloadEdiFile();
  });
}

function initCombos() {
  loadInitCombos();

  $("#cbSearchFilterBy").change(function () {
    var selectedFilter = $(this).val();
    if (typeof selectedFilter != "undefined" && selectedFilter == "CONTAINER") {
      $("#txtSearchVesselName").val("");
      $("#txtSearchVoyage").val("");
      $("#txtSearchVesselName").prop("disabled", true);
      $("#txtSearchVoyage").prop("disabled", true);
    } else {
      $("#txtSearchVesselName").prop("disabled", false);
      $("#txtSearchVoyage").prop("disabled", false);
    }
    $("label[for='txtSearchKeyword']").text(getKeywordName());
    loadMsgTypeCombo(selectedFilter);
  });

  $("#cbSearchMsgType").change(function () {
    $("#txtSearchKeyword").focus();
  });
}

function initDateTimePickers() {
  $("#txtSearchFromDate").datepicker({ dateFormat: "yy-mm-dd" });
  $("#txtSearchToDate").datepicker({ dateFormat: "yy-mm-dd" });
}

function initModal() {
  $("#viewEdiFileSegmentModal").on("hidden.bs.modal", function () {});
}

function loadInitCombos() {
  $.ajax({
    type: "GET",
    url: url_application + "/EdiTransaction/loadInitCombos/",
    success: function (data, statusCode) {
      loadSearchFilterByCb(data);
      loadSearchLineOpCb(data);
    },
    fail: function (data, statusCode) {
      showModal("Error", "Status code:" + statusCode, "error");
    },
  });
}

function loadMsgTypeCombo(filterBy) {
  var searchParam = {
    prFilterBy: filterBy,
  };
  $.ajax({
    type: "GET",
    url: url_application + "/EdiTransaction/getEdiMsgTypeByFilter/",
    data: searchParam,
    success: function (data, statusCode) {
      loadSearchMsgTypeCb(data);
    },
    fail: function (data, statusCode) {
      showModal("Error", "Status code:" + statusCode, "error");
    },
  });
}

function findEdiTransaction(showProcessing) {
  var errorMsg = validateSearchFields();
  if (errorMsg == "") {
    if (showProcessing) showProcessingModal();
    deleteDataTables();
    var unitCategory = $("#cbSearchCategory option:selected").val();
    var searchParam = {
      prFilterBy: $("#cbSearchFilterBy  option:selected").val(),
      prMessageType: $("#cbSearchMsgType option:selected").val(),
      prPrimaryKeyword: $("#txtSearchKeyword").val().toUpperCase(),
      prLineOp: $("#cbSearchLineOp option:selected").val(),
      prVesselName: $("#txtSearchVesselName").val().toUpperCase(),
      prVoyage: $("#txtSearchVoyage").val().toUpperCase(),
      prFromDateStr: $("#txtSearchFromDate").val(),
      prToDateStr: $("#txtSearchToDate").val(),
    };
    $.ajax({
      type: "GET",
      url: url_application + "/EdiTransaction/findEdiTranByParam/",
      data: searchParam,
      success: function (data, statusCode) {
        if (showProcessing) closeProcessingModal();
        if (resultIsError(data)) {
          var msg = data["msg"];
          showErrorMsg(msg);
        } else showEdiTransactionTable(data);
      },
      fail: function (data, statusCode) {
        s;
        showErrorMsg("Status code:" + statusCode);
      },
    });
  } else showErrorMsg(errorMsg);
}

function viewEdiFileIntern(ediTransaction) {
  showProcessingModal();
  var searchParam = {
    prBatchNbrStr: ediTransaction["batchNbr"],
  };
  $.ajax({
    type: "GET",
    url: url_application + "/EdiTransaction/getEdiFileSegment/",
    data: searchParam,
    success: function (data, statusCode) {
      closeProcessingModal();
      $("#lblFileName").text(ediTransaction["fileName"]);
      $("#txtEdiSegment").val(data);
      $("#viewEdiFileSegmentModal").modal();
    },
    fail: function (data, statusCode) {
      showModal("Error", "Status code:" + statusCode, "error");
    },
  });
}

function viewErrorDetailIntern(ediTransaction) {
  var errorDetail = ediTransaction["errorReadableDetail"];
  if (
    typeof errorDetail != "undefined" &&
    errorDetail != null &&
    errorDetail != ""
  )
    showTemporaryMsg("", errorDetail, 5000);
}

function downloadEdiFile() {
  var ediFileName = $("#lblFileName").text();
  var ediSegment = $("#txtEdiSegment").val();
  downloadFile(ediFileName, ediSegment);
}

function showEdiTransactionTable(data) {
  ediTransactionViewModel.ediTransactionTable.removeAll();
  ediTransactionViewModel.ediTransactionTable(data);
  var table = $("#tbEdiTransaction").DataTable({
    bDestroy: true,
    bFilter: true,
    sScrollX: true,
    sScrollY: "30vh",
    scrollCollapse: true,
    paging: true,
    bPaginate: true,
    bSort: true,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
    },
  });
  $(table.column(1).header()).text(getKeywordName());
}

function refreshSearchData() {
  findEdiTransaction(false);
}

function loadSearchFilterByCb(data) {
  ediTransactionViewModel.filterByCb(data["filterByList"]);
}

function loadSearchLineOpCb(data) {
  ediTransactionViewModel.lineOpCb(data["lineOpList"]);
}

function loadSearchMsgTypeCb(data) {
  ediTransactionViewModel.msgTypeCb(data);
}

function validateSearchFields() {
  var searchKeyword = $("#txtSearchKeyword").val();
  var searchVessel = $("#txtSearchVesselName").val();
  var searchVoyage = $("#txtSearchVoyage").val();
  var errorMsg = "";
  if (
    !isValidStr(searchKeyword) ||
    !isValidStr(searchVessel) ||
    !isValidStr(searchVoyage)
  )
    errorMsg = "Por favor elimine los caracteres especiales de la búsqueda";
  return errorMsg;
}

function cleanSearchParams() {
  $("#cbSearchFilterBy").val("");
  $("#cbSearchMsgType").val("");
  $("#txtSearchKeyword").val("");
  $("#cbSearchLineOp").val("");
  $("#txtSearchVesselName").val("");
  $("#txtSearchVoyage").val("");
  $("#txtSearchFromDate").val("");
  $("#txtSearchToDate").val("");
  deleteDataTables();
}

function cleanModalInputs() {
  $("#lblFileName").val("Nombre archivo");
  $("#txtEdiSegment").val("");
}

function getKeywordName() {
  var selectedFilter = $("#cbSearchFilterBy option:selected").val();
  var keywordLabel = "Keyword";
  if (typeof selectedFilter != "undefined")
    keywordLabel =
      selectedFilter == "CONTAINER"
        ? "Contenedor"
        : selectedFilter == "BOOKING"
          ? "Booking"
          : keywordLabel;
  return keywordLabel;
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function resultIsOk(result) {
  var status = result["status"];
  return status == "OK";
}

function resultIsError(result) {
  var status = result["status"];
  return status == "ERROR";
}

function resultRequiresConfirm(result) {
  var status = result["status"];
  return status == "QUESTION";
}

function resultIsWarning(result) {
  var status = result["status"];
  return status == "WARNING";
}

function adjustDataTableColumns() {
  $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
}

function downloadFile(fileName, textToSave) {
  var textToSaveAsBlob = new Blob([textToSave], { type: "text/plain" });
  var textToSaveAsURL = window.URL.createObjectURL(textToSaveAsBlob);
  var downloadLink = document.createElement("a");
  downloadLink.download = fileName;
  downloadLink.href = textToSaveAsURL;
  downloadLink.onclick = destroyClickedElement;
  downloadLink.style.display = "none";
  document.body.appendChild(downloadLink);
  downloadLink.click();
}

function destroyClickedElement(event) {
  document.body.removeChild(event.target);
}

function isValidStr(str) {
  return !/[~`!#$%\^&*+=\\[\]\\';,/{}|\\":<>\?]/g.test(str);
}
