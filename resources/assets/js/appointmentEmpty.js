var table;
var loaded = false;
$(document).ready(function () {
  $("#btnFindEntity").click(function () {
    findEntityById();
  });

  $("#tbUnitsByBl tbody").on("click", "tr", function () {
    var row = this;
    if (row.getAttribute("class") === "odd") {
      $(this).toggleClass("selected");
      $("#btnStartCoordinate").show();
    } else {
      $(this).toggleClass("selected");
      $("#btnStartCoordinate").hide();
    }
  });

  $("#btnStartCoordinate").click(function () {
    unitCoordination(0);
  });

  $("#btnCoordinate").click(function () {
    if (
      $("#datetimepicker3").val() == "" ||
      $("#datetimepicker3").val() == undefined
    ) {
      showModal(
        "Atención",
        "Por favor, seleccione una fecha de coordinación para su contenedor",
        "error",
      );
      return;
    } else {
      preventApptUnknown();
    }
  });
});

function initKnockout() {
  viewModel = {
    UnitsByBl: ko.observableArray([]),
  };
  ko.applyBindings(viewModel);
}

function loadUnitsCb(data) {
  showUnitsGrid(data);
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function showUnitsGrid(data) {
  //viewModel.UnitsByBl.removeAll();
  //deleteDataTables();
  viewModel.UnitsByBl(data);
  table = $("#tbUnitsByBl").DataTable({
    select: "single",
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
      sSearch: "Buscar",
      sLengthMenu: "Mostrar _MENU_ resultados",
      /*"oPaginate": {
            	"sFirst": "Primera pagina", // This is the link to the first page
            	"sPrevious": "Pagina anterior", // This is the link to the previous page
            	"sNext": "Proxima pagina", // This is the link to the next page
            	"sLast": "Ultima pagina" // This is the link to the last page
        	}*/
    },
    paging: false,
    bPaginate: false,
    info: false,
    bDestroy: true,
    bFilter: true,
    bSort: true,
    sScrollY: "30vh",
  });
}

function findEntityById() {
  processingModal();

  var unitId = $("#entityid").val();
  if (unitId == "") {
    showModal(
      "Request failed",
      "Por favor, complete los campos requeridos.",
      "error",
    );
    return;
  }

  var request = $.ajax({
    url: url_application + "/Billing/findUnits/" + unitId,
    type: "GET",
  });

  request.done(function (data) {
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    $("#tbUnitsByBl").show();
    loadUnitsCb(data);
    Swal.close();
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("Request failed", jqXHR.responseText, "error");
  });
}

function processingModal() {
  Swal.fire({
    title: "Procesando...",
    text: "Aguarde un instante por favor",
    imageUrl: url_application + "/resources/assets/images/ajax_loading.gif",
    showConfirmButton: false,
    allowOutsideClick: false,
  });
}

function unitCoordination(id) {
  var d = table.rows(id).data();
  cleanModalCoordination();
  $("#divForUnit").append("Usted va a coordinar el Contenedor " + d[0][0]);
  $("#modalAppt").modal("show");
}

function cleanModalCoordination() {
  //$('#datetimepicker3').datetimepicker({value:'2019/9/29 07:00'});
  $("#divForUnit").html("");
}

function preventApptUnknown() {
  Swal.fire({
    allowOutsideClick: false,
    type: "info",
    title: "Atención",
    text:
      "Esta seguro de coordinar éste contenedor para " +
      $("#datetimepicker3").val(),
    footer: "<a href>Por qu&eacute; aparece este mensaje?</a>",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "S&iacute;, estoy seguro!",
    cancelButtonText: "Cancelar",
  }).then((result) => {
    if (result.dismiss != "cancel") {
      //Cerramos el modal de preaviso
      $("#modalAppt").modal("hide");
      showModal("Contenedor coordinado para " + $("#datetimepicker3").val());
    }
  });
}

function refreshLabelDateTimeAppt() {
  var current = $("#datetimepicker3").val();
  $("#labelDatetimeAppt").text("La fecha y hora seleccionada es " + current);
}
