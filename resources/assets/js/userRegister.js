/**
 * Author: Lucas Juarez
 * APM Terminals Buenos Aires
 */

var regex =
  /^([20]{2}|[23]{2}|[24]{2}|[27]{2}|[30]{2}|[33]{2}|[34]{2}|[55]{2})[0-9]{8}([0-9]{1})/gm;
var cuilLengthRegex = /^([0-9]{11})$/g;

$(document).ready(function (e) {
  $("#beBillingUser").click(function (e) {
    //e.preventDefault();
    $("#inputsBillingUser :input[type=text]").val("");
    $("#inputsBillingUser").toggle("fadeIn");
  });

  $("#userRegWhatThis").click(function (e) {
    e.preventDefault();
    showModal(
      "",
      "Sirve para poder realizar facturación con tu CUIT/CUIL",
      "question",
    );
  });

  $("#btnFindUser").click(function (e) {
    e.preventDefault();
    var userId = $("#documentNbr").val();
    userId.includes("-") == true ? (userId = userId.replace(/-/g, "")) : userId;
    if (validateDocumentNumber(userId)) {
      showModal(
        "",
        "El número de CUIL/CUIL sólo puede ser numérico y de 11 caracteres.",
        "warning",
      );
      return false;
    }
    findUserBeforeRegister(userId);
  });

  $("#btnRegister").click(function (e) {
    e.preventDefault();
    validateForm("formRegisterUser");
  });

  $('input[type="file"]').change(function () {
    checkextension(this.id);
  });

  $("input").change(function () {
    $(this).attr("style", "");
  });
});

function validateInputEmail(inputId) {
  var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,}$/i;
  return testEmail.test($("#" + inputId).val());
}

function validateNumeric(inputId) {
  var reg = /^\d+$/;
  return reg.test($("#" + inputId).val()) && $("#" + inputId).val().length > 7;
}

function validateForm(formId) {
  var error = false;

  if ($("#documentNbr").val() == "" || $("#documentNbr").val() == null) {
    error = true;
    $("#documentNbr").attr("style", "background-color:orangered;color:white");
  }
  if ($("#telephone").val() == "" || $("#telephone").val() == null) {
    error = true;
    $("#telephone").attr("style", "background-color:orangered;color:white");
  } else {
    error = !validateNumeric("telephone");
    error
      ? $("#telephone").attr("style", "background-color:orangered;color:white")
      : "";
  }
  if ($("#fullname").val() == "" || $("#fullname").val() == null) {
    error = true;
    $("#fullname").attr("style", "background-color:orangered;color:white");
  }
  if ($("#address").val() == "" || $("#address").val() == null) {
    error = true;
    $("#address").attr("style", "background-color:orangered;color:white");
  }
  if (
    $("#emailAddress").val() == "" ||
    $("#emailAddress").val() == null ||
    !validateInputEmail("emailAddress")
  ) {
    error = true;
    $("#emailAddress").attr("style", "background-color:orangered;color:white");
  }
  if ($("#beBillingUser").prop("checked")) {
    var elements = $("#inputsBillingUser :input");
    for (var i = 0; i < elements.length; i++) {
      if (
        elements[i].name == "emailComex" ||
        elements[i].name == "emailManagement"
      ) {
        error = !validateInputEmail(elements[i].name);
        error
          ? $("#" + elements[i].name).attr(
              "style",
              "background-color:orangered;color:white",
            )
          : "";
        continue;
      }
      if (
        $("#" + elements[i].name).val() == "" ||
        $("#" + elements[i].name).val() == null
      ) {
        error = true;
        $("#" + elements[i].name).attr(
          "style",
          "background-color:orangered;color:white",
        );
      }
    }
  }

  if (error) {
    showModal("", "Por favor, complete correctamente el formulario", "warning");
    return;
  } else {
    registerUser();
  }
}

function validateDocumentNumber(value) {
  var error = false;
  (value.length == 0 || value == "") == true ? (error = true) : error;
  value.match(regex) == null ? (error = true) : error;
  value.match(cuilLengthRegex) == null ? (error = true) : error;
  return error;
}

function findUserBeforeRegister(userId) {
  processingModal();

  var request = $.ajax({
    url: url_application + "/User/findUserBeforeRegister/" + userId,
    type: "GET",
  });

  request.done(function (data) {
    if (data == null || data == "") {
      Swal.close();
      $("#userAdditionalInformation,#divRegisterBtn").show();
    } else {
      showModal("", data, "warning");
    }
  });

  request.fail(function (jqXHR, textStatus) {
    showModal(
      "",
      "No es posible obtener información del CUIT solicitado.",
      "error",
    );
  });
}

function registerUser() {
  processingModal();
  var form = $("#formRegisterUser")[0];
  var data = new FormData(form);

  $.ajax({
    type: "POST",
    enctype: "multipart/form-data",
    url: url_application + "/User/registerUser",
    data: data,
    processData: false,
    contentType: false,
    cache: false,
    timeout: 600000,
    success: function (data) {
      showModalCallback(
        "",
        "Registrado correctamente.",
        "success",
        url_application,
      );
    },
    error: function (e) {
      showModal("", e.responseText, "error");
    },
  });
}
