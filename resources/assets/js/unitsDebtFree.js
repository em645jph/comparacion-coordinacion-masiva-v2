var loaded = false;

$(document).ready(function () {
  findUnitsByDraft(draft);
});

function loadUnitsCb(data) {
  showUnitsGrid(data);
}

function findUnitsByDraft(draft) {
  processingModal();

  var request = $.ajax({
    type: "GET",
    url: url_application + "/Unit/findUnitsByDraft/" + draft,
  });

  request.done(function (data) {
    if (data == null || data == "") {
      Swal.fire("", "Sin resultados", "info");
      return;
    }
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    loadUnitsCb(data);
    Swal.close();
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("Request failed", jqXHR.responseText, "error");
  });
}

function initKnockout() {
  viewModel = {
    units: ko.observableArray([]),
  };
  ko.applyBindings(viewModel);
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function showUnitsGrid(data) {
  viewModel.units.removeAll();
  deleteDataTables();
  viewModel.units(data);
  newDataTable("tbUnits", null, true, true, true);
}

function processingModal() {
  Swal.fire({
    title: "Procesando...",
    text: "Aguarde un instante por favor",
    imageUrl: url_application + "/resources/assets/images/ajax_loading.gif",
    showConfirmButton: false,
    allowOutsideClick: false,
  });
}

function formatDate(ldDate) {
  var date = new Date(ldDate);
  return date.toLocaleDateString() + " " + date.toLocaleTimeString();
}
