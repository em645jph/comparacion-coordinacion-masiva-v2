/**
 * Author: Lucas Juarez
 * APM Terminals Buenos Aires
 */
var cuilRegex = /[0-9]/g;
var cuilLengthRegex = /^([0-9]{11})$/g;

$(document).ready(function () {
  $("#btnReactivate").click(function (e) {
    e.preventDefault();
    var documentNbr = $("#documentNbr").val();
    documentNbr.includes("-") == true
      ? (documentNbr = documentNbr.replace(/-/g, ""))
      : documentNbr;
    if (validateDocumentNumber(documentNbr)) {
      showModal(
        "Atenci&oacute;n!",
        "El número de CUIL/CUIL sólo puede ser numérico y de 11 caracteres.",
        "warning",
      );
      return false;
    }
    sendCodeForUser();
  });
});

function sendCodeForUser() {
  var obj = new Object();
  obj.documentNbr = $("#documentNbr").val();
  obj.email = $("#emailaddress").val();
  if (obj.email == null || obj.email == "")
    showModal(
      "Atención!",
      "Por favor, completar con un email correcto.",
      "warning",
    );
  if (obj.documentNbr == null || obj.documentNbr == "")
    showModal(
      "Atención!",
      "Por favor, completar con un CUIT/CUIL correcto.",
      "warning",
    );

  processingModal();
  var request = $.ajax({
    url: url_application + "/User/sendCodeForUser",
    type: "POST",
    data: JSON.stringify(obj),
    contentType: "application/json",
  });

  request.done(function (message) {
    showModalCallback(
      "Atencion!",
      message,
      "success",
      url_application + "/confirmCode",
    );
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("Atencion!", jqXHR.responseText, "error");
  });
}

function validateDocumentNumber(value) {
  var error = false;
  (value.length == 0 || value == "") == true ? (error = true) : error;
  value.match(cuilRegex) == false ? (error = true) : error;
  value.match(cuilLengthRegex) == null ? (error = true) : error;
  return error;
}

function processingModal() {
  Swal.fire({
    title: "Procesando...",
    text: "Aguarde un instante por favor",
    imageUrl: url_application + "/resources/assets/images/ajax_loading.gif",
    showConfirmButton: false,
    allowOutsideClick: false,
  });
}
