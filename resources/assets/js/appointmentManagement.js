/**
 * Author: Jimena Chavez
 *
 */

$(document).ready(function () {
  initHtmlElements();
  initKnockout();
  var unitAppt = {};
});

function initKnockout() {
  apptViewModel = {
    apptSubTypeCb: ko.observableArray([]),
    lineOpCb: ko.observableArray([]),
    unitApptTable: ko.observableArray([]),
    dateOpeningCb: ko.observableArray([]),
    editTruckDriverDetails: function (apptUnitToEdit) {
      alert("COMMING SOON..");
    },
    viewUnitSeals: function (unitApptToShow) {
      viewUnitApptSeals(unitApptToShow);
    },
    addAppt: function (unitApptDto) {
      addAppointment(unitApptDto);
    },
    cancelAppt: function (apptToCancel) {
      cancelAppointment(apptToCancel);
    },
  };
  ko.applyBindings(apptViewModel);
}

function initHtmlElements() {
  initInputs();
  initButtons();
  initCombos();
  initApptModal();
  initCheckBoxs();
}

function initInputs() {
  $("#txtSearchKeyParameter").focus();
}

function initButtons() {
  $("#btnSearch").click(function () {
    findUnitAppt(true);
  });

  $("#btnSaveAppointment").click(function () {
    saveAppointment();
  });

  $("#btnClean").click(function () {
    cleanSearchParams();
  });
}

function initCombos() {
  loadApptSubTypeCb();
  loadLineOpCb();
  $("#cbSearchApptSubType").change(function () {
    var selectedValue = $(this).val();
    var hideOptionalView = false;
    if (selectedValue == "DOE") {
      $("#txtSearchKeyParameter").prop("maxLength", 50);
      $("label[for='txtSearchKeyParameter']").text("Booking (*)");
    } else {
      $("#txtSearchKeyParameter").prop("maxLength", 11);
      $("label[for='txtSearchKeyParameter']").text("Contenedor (*)");
      if (selectedValue == "DOM") {
        $("#cbSearchLineOp").val("");
        hideOptionalView = true;
      }
    }
    if (hideOptionalView) $(".optionalView").hide();
    else $(".optionalView").show();
    $("#txtSearchKeyParameter").focus();
  });
}

function initApptModal() {
  $("#viewApptCalendarModal").on("shown.bs.modal", function () {
    initCalendar();
    $("#calendar").fullCalendar("render");
  });

  $("#viewApptCalendarModal").on("hidden.bs.modal", function () {
    adjustDataTableColumns();
    cleanModalInputs();
  });

  $("#selectApptTimeModal").on("hidden.bs.modal", function () {
    apptViewModel.dateOpeningCb.removeAll();
  });

  $("#viewApptUnitSealModal").on("hidden.bs.modal", function () {
    adjustDataTableColumns();
    cleanSealModalInputs();
  });
}

function initCheckBoxs() {
  $("#ckCheckAll").change(function () {
    var check = $(this).is(":checked");
    var canBeSelected = $(".selectable:enabled");
    canBeSelected.each(function () {
      if (check) $(this).prop("checked", true);
      else $(this).prop("checked", false);
    });
  });

  $("#ckCheckAllPrivilege").change(function () {
    var check = $(this).is(":checked");
    var canBeSelected = $(".selectablePrivilege:enabled");
    canBeSelected.each(function () {
      if (check) $(this).prop("checked", true);
      else $(this).prop("checked", false);
    });
  });
}

function initCalendar() {
  var minDate = unitAppt["unitStartDate"];
  var maxDate = unitAppt["unitEndDate"];
  $("#calendar").fullCalendar({
    header: {
      left: "prev",
      center: "title",
      right: "next",
      ignoreTimezone: true,
    },
    defaultDate: minDate,
    defaultView: "month",
    monthNames: [
      "Enero",
      "Febrero",
      "Marzo",
      "Abril",
      "Mayo",
      "Junio",
      "Julio",
      "Agosto",
      "Septiembre",
      "Octubre",
      "Noviembre",
      "Diciembre",
    ],
    dayNamesShort: ["D", "L", "M", "X", "J", "V", "S"],
    viewRender: function (view) {
      if (view.end < minDate) {
        $("#calendar").fullCalendar("gotoDate", minDate);
      } else if (view.start > maxDate) {
        $("#calendar").fullCalendar("gotoDate", maxDate);
      }
    },
    events: function (start, end, timezone, callback) {
      showProcessingModal();
      $.ajax({
        type: "POST",
        contentType: "application/json",
        url: url_application + "/AppointmentManagement/getGateOpenings/",
        data: JSON.stringify(unitAppt),
        success: function (data) {
          var msg = data["msg"];
          if (typeof msg != "undefined" && !resultIsOk(data)) {
            showErrorMsg(msg);
          } else {
            var events = [];
            data.forEach(function (opening) {
              events.push({
                id: opening["id"],
                title: opening["title"],
                start: opening["dateStr"],
                color: opening["color"],
              });
            });
            callback(events);
            closeProcessingModal();
          }
        },
      });
    },
    eventClick: function (event, jsEvent, view) {
      // when some one click on any event
      getDateOpenings(event.id);
    },
  });
}

function findUnitAppt(showProcessing) {
  $("#ckCheckAll").prop("checked", false);
  var errorMsg = validateSearchFields();
  if (errorMsg == "") {
    if (showProcessing) showProcessingModal();
    deleteDataTables();
    var apptSubType = $("#cbSearchApptSubType option:selected").val();
    var searchParam = {
      prApptSubType: apptSubType,
      prKeyParameter: $("#txtSearchKeyParameter").val(),
      prLineOpId: $("#cbSearchLineOp option:selected").val(),
    };
    $.ajax({
      type: "GET",
      url: url_application + "/AppointmentManagement/findUnitAppointment/",
      data: searchParam,
      success: function (data, statusCode) {
        var msg = data["msg"];
        if (typeof msg != "undefined" && !resultIsOk(data)) {
          showErrorMsg(msg);
        } else {
          if (showProcessing) closeProcessingModal();
          showUnitApptTable(data, apptSubType);
          adjustDataTableColumns();
        }
      },
      fail: function (data, statusCode) {
        s;
        showErrorMsg("Status code:" + statusCode);
      },
    });
  } else showErrorMsg(errorMsg);
}

function getDateOpenings(dateStr) {
  unitAppt["requestedDateStr"] = dateStr;
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/AppointmentManagement/getDateOpenings/",
    data: JSON.stringify(unitAppt),
    success: function (data) {
      delete unitAppt["requestedDateStr"];
      var msg = data["msg"];
      if (typeof msg != "undefined" && !resultIsOk(data)) {
        showErrorMsg(msg);
      } else if (data != null && data != "") {
        apptViewModel.dateOpeningCb.removeAll();
        apptViewModel.dateOpeningCb(data);
        $("#txtSelectedDate").val(dateStr);
        $("#selectApptTimeModal").modal({ backdrop: "static" });
      }
    },
  });
}

function saveAppointment() {
  var apptParam = {
    prApptSubType: unitAppt["requestApptSubType"],
    prUnitId: unitAppt["unitId"],
    prUnitDocumentNbr: unitAppt["unitDocumentNbr"],
    prSelectedDateTime: $("#cbDateOpening option:selected").val(),
  };
  showProcessingModal();
  $.ajax({
    type: "POST",
    url: url_application + "/AppointmentManagement/createUnitAppointment/",
    data: apptParam,
    success: function (result) {
      closeProcessingModal();
      var msg = result["msg"];
      if (resultIsOk(result)) {
        showSuccessMsg("Listo!", msg);
        $("#btnCancelSaveAppointment").click();
        $("#btnCancelViewApptCalendar").click();
        refreshSearchData();
      } else showErrorMsg(msg);
    },
    fail: function (result, statusCode) {
      closeProcessingModal();
      showErrorMsg(
        statusCode +
          "Se produjo un error guardando el menú. Por favor intente nuevamente",
      );
    },
  });
}

function addAppointment(unitApptDto) {
  unitAppt = unitApptDto;
  fillInputModal();
}

function cancelAppointment(apptToCancel) {
  showProcessingModal();
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/AppointmentManagement/cancelUnitAppointment/",
    data: JSON.stringify(apptToCancel),
    success: function (result) {
      closeProcessingModal();
      var msg = result["msg"];
      var questionType = result["questionType"];
      if (resultIsError(result)) {
        showErrorMsg(msg);
      } else if (resultRequiresConfirm(result)) {
        Swal.fire({
          allowOutsideClick: false,
          title: "Desea continuar?",
          text: msg,
          type: "question",
          showCancelButton: true,
          confirmButtonColor: "#4fa7f3",
          cancelButtonColor: "#d57171",
          confirmButtonText: "Sí, eliminar!",
        }).then((result) => {
          if (result.value) {
            apptToCancel[questionType] = true;
            cancelAppointment(apptToCancel);
          } else if (result.dismiss === Swal.DismissReason.cancel) {
            delete apptToCancel[questionType];
          }
        });
      } else {
        showSuccessMsg("Listo!", msg);
        refreshSearchData();
      }
    },
    fail: function (result, statusCode) {
      showErrorMsg(
        statusCode +
          " Se produjo un eliminando el turno. Por favor intente nuevamente",
      );
    },
  });
}

function viewUnitApptSeals(unitApptToShow) {
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/AppointmentManagement/viewUnitApptSeals/",
    data: JSON.stringify(unitApptToShow),
    success: function (result) {
      var msg = result["msg"];
      if (resultIsError(result)) {
        showErrorMsg(msg);
      } else if (resultRequiresConfirm(result)) {
        Swal.fire({
          allowOutsideClick: false,
          title: "Desea continuar?",
          text: msg,
          type: "question",
          showCancelButton: true,
          confirmButtonColor: "#4fa7f3",
          cancelButtonColor: "#d57171",
          confirmButtonText: "Sí, ver precintos!",
        }).then((result) => {
          if (result.value) {
            unitApptToShow["addEvent"] = true;
            viewUnitApptSeals(unitApptToShow);
          } else if (result.dismiss === Swal.DismissReason.cancel) {
            delete unitApptToShow["addEvent"];
          }
        });
      } else {
        fillSealInputModal(result);
      }
    },
    fail: function (result, statusCode) {
      showErrorMsg(
        statusCode +
          " Se produjo un error obteniendo los precintos. Por favor intente nuevamente",
      );
    },
  });
}

function loadApptSubTypeCb() {
  $.ajax({
    type: "GET",
    url: url_application + "/AppointmentManagement/getApptSubTypeList/",
    success: function (data, statusCode) {
      loadSearchApptSubTypeCb(data);
    },
    fail: function (data, statusCode) {
      showModal("Error", "Status code:" + statusCode, "error");
    },
  });
}

function loadLineOpCb() {
  $.ajax({
    type: "GET",
    url: url_application + "/AppointmentManagement/getLineOpList/",
    success: function (data, statusCode) {
      loadSearchLineOpCb(data);
    },
    fail: function (data, statusCode) {
      showModal("Error", "Status code:" + statusCode, "error");
    },
  });
}

function showUnitApptTable(data, apptSubType) {
  apptViewModel.unitApptTable.removeAll();
  deleteDataTables();
  apptViewModel.unitApptTable(data);
  var table = $("#tbUnitAppointment").DataTable({
    bDestroy: true,
    bFilter: true,
    sScrollX: true,
    sScrollY: "30vh",
    scrollCollapse: true,
    paging: false,
    bPaginate: false,
    bSort: true,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
    },
  });
  var limitDateColumnName = getLimitDateColumnName(apptSubType);
  $(table.column(10).header()).text(limitDateColumnName);
}

function refreshSearchData() {
  findUnitAppt();
}

function loadSearchApptSubTypeCb(data) {
  apptViewModel.apptSubTypeCb(data);
}

function loadSearchLineOpCb(data) {
  apptViewModel.lineOpCb(data);
}

function fillRoleInputOnModal(data) {
  $("#modalTitle").text("Editar Role");
  $("#txtRoleGkey").val(data["gkey"]);
  $("#txtRoleId").val(data["id"]);
  $("#txtRoleDesc").val(data["description"]);
  var privilegeList = data["privilegeList"];
  showRolePrivielege(privilegeList);
  $("#addEditRoleModal").modal();
}

function fillInputModal() {
  $("#txtUnitId").val(unitAppt["unitId"]);
  $("#txtLimitDate").val(unitAppt["unitLimitDateStr"]);
  var limitDateLabel = getLimitDateColumnName(unitAppt["requestApptSubType"]);
  $("label[for='txtLimitDate']").text(limitDateLabel);
  $("#viewApptCalendarModal").modal();
}

function fillSealInputModal(result) {
  $("#txtApptSealNbr1").val(result["sealNbr1"]);
  $("#txtApptSealNbr2").val(result["sealNbr2"]);
  $("#txtApptSealNbr3").val(result["sealNbr3"]);
  $("#txtApptSealNbr4").val(result["sealNbr4"]);
  $("#viewApptUnitSealModal").modal();
}

function validateSearchFields() {
  var searchApptSubType = $("#cbSearchApptSubType option:selected").val();
  var searchKeyParameter = $("#txtSearchKeyParameter").val();
  var searchLineOpId = $("#cbSearchLineOp option:selected").val();
  var errorMsg = "";
  if (searchApptSubType == null || searchApptSubType == "")
    errorMsg = "Por favor seleccione el tipo de coordinación";
  else if (searchKeyParameter == null || searchKeyParameter == "") {
    errorMsg = "Por favor ingrese el campo obligatorio (*) para la búsqueda";
  } else if (!isValidStr(searchKeyParameter))
    errorMsg = "Por favor elimine los caracteres especiales de la búsqueda";
  return errorMsg;
}

function cleanSearchParams() {
  $("#cbSearchApptSubType").val("");
  $("#txtSearchKeyParameter").val("");
  $("#cbSearchLineOp").val("");
  deleteDataTables();
}

function cleanModalInputs() {
  $("#txtUnitId").val("");
  $("#txtLimitDate").val("");
  unitAppt = {};
  $("#calendar").fullCalendar("destroy");
}

function cleanSealModalInputs() {
  $("#txtApptSealNbr1").val("");
  $("#txtApptSealNbr2").val("");
  $("#txtApptSealNbr3").val("");
  $("#txtApptSealNbr4").val("");
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function getLimitDateColumnName(apptSubType) {
  return apptSubType == "DOE"
    ? "Cutoff"
    : apptSubType == "PIU"
      ? "Forzoso"
      : apptSubType == "DOM"
        ? "Fecha Devolución"
        : "Fecha límite";
}

function resultIsOk(result) {
  var status = result["status"];
  return status == "OK";
}

function resultIsError(result) {
  var status = result["status"];
  return status == "ERROR";
}

function resultRequiresConfirm(result) {
  var status = result["status"];
  return status == "QUESTION";
}

function adjustDataTableColumns() {
  $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
}

function isValidStr(str) {
  return !/[~`!#$%\^&*+=\\[\]\\';,/{}|\\":<>\?]/g.test(str);
}
