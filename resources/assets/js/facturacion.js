/**
 * Author: Lucas Juarez
 * APM Terminals Buenos Aires
 */
var regex =
  /^([20]{2}|[23]{2}|[24]{2}|[27]{2}|[30]{2}|[33]{2}|[34]{2})[0-9]{8}([0-9]{1})/gm;
var cuilLengthRegex = /^([0-9]{11})$/g;

$(document).ready(function () {
  //history.replaceState({}, null, url_application + "/Invoice/invoicing");
  $("#btnFindUser").click(function () {
    findUserToBilling();
    //$('#myModal').modal('show');
    //$('#divRegisterBtn').show();
  });

  $("#creditNoteWarningModal").modal();
});

function validateDocumentNumber(value) {
  var error = false;
  (value.length == 0 || value == "") == true ? (error = true) : error;
  value.match(regex) == null ? (error = true) : error;
  value.match(cuilLengthRegex) == null ? (error = true) : error;
  return error;
}

function findUserToBilling() {
  var cuit = $("#docTo").val();
  if (validateDocumentNumber(cuit) == null || cuit.length != 11) {
    showModal("Atencion!", "Por favor, ingrese un CUIT valido", "error");
    return;
  }

  window.location.href =
    url_application + "/Invoice/validateCuitToBilling/" + cuit;

  /*processingModal();
	var request = $.ajax({
	  url: url_application + "/Invoice/validateCuitToBilling/" + cuit,
	  type: "GET",
	  contentType : "application/json"
	});

	request.done(function(data) {
		showModal( "" ,data, "success");
	});

	request.fail(function(jqXHR, textStatus) {
		showModal( "" ,jqXHR.responseText, "error" );
	});*/
}

function beforeForm(formTypeId) {
  switch (formTypeId) {
    case 1:
      break;
    case 2:
      break;
    case 3:
      break;
    default:
      break;
  }
}
