/**
 * Author: Lucas Juarez
 * APM Terminals Buenos Aires
 */

function findUserInTOS(userId) {
  var request = $.ajax({
    url: url_application + "/User/findUserInTOS/" + userId,
    type: "GET",
    dataType: "json",
  });

  request.done(function (code) {
    //Si es -1 (Usuario no existe en base), 1 (Usuario en Billing), 2 (Usuario no existe en Billing)
    switch (code) {
      case -1:
        //Muestro todos los campos
        $("#userAdditionalInformation").show();
        $("#inputsBillingUser").show();
        break;
      case 1:
        //Precargo los datos sin poder elegir el checkbox
        $("#userAdditionalInformation").show();
        break;
      case 2:
        //Precargo los datos permitiendo elegir checkbox
        $("#inputsBillingUser").show();
        break;
      case 3:
        //Mensaje de Usuario ya existe + Usuario ya existe en facturacion
        showModal("Atenci&oacute;n!", "Usuario existe en Sistema", "error");
        break;
      default:
        break;
    }
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("Request failed", jqXHR.responseText, "error");
  });
}

function createUserTemporal() {
  var form = new Object();
  form.documentnbr = $("#documentNbr").val();
  form.fullname = $("#fullname").val();
  form.address = $("#address").val();
  form.email = $("#email").val();
  form.billing = $("#checkbox5").attr("checked");
  if (form.billing == true) {
    form.commercialEmailAddress = $("#commercialEmailAddress").val();
    form.contactemail = $("#emailContact").val();
    form.telephone = $("#telephone").val();
    form.city = $("#city").val();
    form.bussinestype = $("#businessType").val();
  }

  var request = $.ajax({
    url: url_application + "/User/createUserTemporal",
    type: "POST",
    data: JSON.stringify(form),
    dataType: "json",
  });
}

function showEntityExistInTos(entity, tagIdAfterInsert, message) {
  $("#alertWarning").remove();
  $("#" + tagIdAfterInsert).append(
    '<div id="alertWarning" class="alert alert-icon alert-warning alert-dismissible fade show" role="alert">' +
      "<strong>" +
      entity +
      "</strong> " +
      message +
      "." +
      "</div>",
  );
}

function showConfirmModal(
  title,
  text,
  type,
  showCancelButton,
  titleCallback,
  textCallback,
  callback,
) {
  Swal.fire({
    title: title,
    text: text,
    type: type,
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "S&iacute;, estoy seguro!",
    cancelButtonText: "Cancelar",
  }).then((result) => {
    if (result.dismiss != "cancel") {
      window.location.href = url_logout;
    }
  });
}

function showBasicModal(textMessage) {
  Swal.fire(textMessage);
}

function showModal(title, text, type) {
  Swal.fire({
    type: type,
    title: title,
    text: text,
    footer: "<a href>Por qu&eacute; aparece este mensaje?</a>",
  });
}

function showModalWithFunction(entityFinder) {
  var entityResp;
  Swal.fire({
    title: "Buscando " + entityFinder, // Buscando CUIT
    html: "Por favor aguarde un instante",
    onBeforeOpen: () => {
      Swal.showLoading();
      var result = 1;
      if (result == 1) {
        entityResp = $("#documentNbr").val();
        Swal.close();
      }
    },
  });
  return entityResp;
}

function showTimerModal(title, text, timerMillis, callback) {
  Swal.fire({
    title: title,
    text: text,
    timer: timerMillis,
  }).then(
    function () {},
    // handling the promise rejection
    function (dismiss) {
      if (dismiss === "timer") {
        callback;
      }
    },
  );
}
