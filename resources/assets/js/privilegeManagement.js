/**
 * Author: Jimena Chavez
 *
 */

$(document).ready(function () {
  initHtmlElements();
  initKnockout();
  getMenuList("INIT");
  getAccessTypeList();
});

function initKnockout() {
  privilegeViewModel = {
    menu: ko.observableArray([]),
    accessType: ko.observableArray([]),
    createdPrivilege: ko.observableArray([]),
    modalMenu: ko.observableArray([]),
    modalAccessType: ko.observableArray([]),
    editPrivilege: function (privilegeToEdit) {
      editPrivilegeIntern(privilegeToEdit);
    },
    deletePrivilege: function (privilegeToDelete) {
      deletePrivilegeIntern(privilegeToDelete);
    },
    recoverPrivilege: function (privilegeTorecover) {
      recoverPrivilegeIntern(privilegeTorecover);
    },
  };
  ko.applyBindings(privilegeViewModel);
}

function initHtmlElements() {
  $("#txtSearchPrivilegeLabel").focus();
  initInputs();
  initButtons();
  initDateTimePickers();
  initCombos();
  initModal();
  initCheckBoxs();
}

function initInputs() {
  $("#txtPrivilegeId").keypress(function () {
    var key = event.keyCode ? event.keyCode : event.which;
    if ((key < 65 || key > 90) && (key < 97 || key > 122) && key != 95)
      return false;
    return true;
  });

  $("#divPrivilegeId").hide();
}

function initButtons() {
  $("#btnSearch").click(function () {
    $("#ckCheckAll").prop("checked", false);
    var errorMsg = validateSearchFields();
    if (errorMsg == "") {
      var searchParam = {
        prLabel: $("#txtSearchPrivilegeLabel").val(),
        prMenuGkeyStr: $("#cbSearchMenu option:selected").val(),
        prAccessType: $("#cbSearchAccessType option:selected").val(),
        prFromDateStr: $("#txtSearchFromDate").val(),
        prToDateStr: $("#txtSearchToDate").val(),
        prLifeCycleState: $("#cbSearchPrivilegeStatus option:selected").val(),
      };
      $.ajax({
        type: "GET",
        url: url_application + "/PrivilegeManagement/findPrivilegeByParam/",
        data: searchParam,
        success: function (data, statusCode) {
          showCreatedPrivilege(data);
        },
        fail: function (data, statusCode) {
          showErrorMsg("Status code:" + statusCode);
        },
      });
    } else showErrorMsg(errorMsg);
  });

  $("#btnAddPrivilege").click(function () {
    $("#modalTitle").text("Agregar Privilegio");
    getMenuList("NEW");
    $("#addEditPrivilegeModal").modal();
  });

  $("#btnClean").click(function () {
    cleanSearchParams();
  });

  $("#btnSavePrivilege").click(function () {
    addOrUpdatePrivilege();
  });

  $("#btnDeleteSelectedPrivilege").click(function () {
    deleteSelectedPrivilege();
  });

  $("#btnRecoverSelectedPrivilege").click(function () {
    recoverSelectedPrivilege();
  });
}

function initDateTimePickers() {
  $("#txtSearchFromDate").datepicker({ dateFormat: "yy-mm-dd" });
  $("#txtSearchToDate").datepicker({ dateFormat: "yy-mm-dd" });
}

function initCombos() {}

function initModal() {
  $("#addEditPrivilegeModal").on("shown.bs.modal", function () {
    $("#txtPrivilegeLabel").focus();
  });

  $("#addEditPrivilegeModal").on("hidden.bs.modal", function () {
    cleanAddEditFormParams();
  });
}

function initCheckBoxs() {
  $("#ckCheckAll").change(function () {
    var check = $(this).is(":checked");
    var canBeSelected = $(".selectable:enabled");
    canBeSelected.each(function () {
      if (check) $(this).prop("checked", true);
      else $(this).prop("checked", false);
    });
  });

  $("#cbAccessType").change(function () {
    var selectedValue = $(this).val();
    console.log("selectedValue:" + selectedValue);
    if ("OTHER" == selectedValue) {
      console.log("showing div");
      $("#divPrivilegeId").show();
    } else {
      $("#divPrivilegeId").hide();
      $("#txtPrivilegeId").val("");
    }
  });
}

function addOrUpdatePrivilege() {
  var isEditingItem = isEditingPrivilege();
  if ((!isEditingItem && !canAddItem) || (isEditingItem && !canEditItem))
    showErrorMsg("No tiene permisos para realizar esta acción");
  else {
    var errorMsg = validateFormFields();
    if (errorMsg == "") {
      var privilegeData = getPrivilegeDataFromModal();
      $.ajax({
        type: "POST",
        contentType: "application/json",
        url: url_application + "/PrivilegeManagement/addOrUpdatePrivilege/",
        data: JSON.stringify(privilegeData),
        success: function (result, statusCode) {
          var msg = result["msg"];
          if (resultIsOk(result)) {
            showSuccessMsg("Listo!", msg);
            $("#btnCancelSavePrivilege").click();
            refreshSearchData();
          } else showErrorMsg(msg);
        },
        fail: function (result, statusCode) {
          showErrorMsg(
            statusCode +
              "Se produjo un error guardando el menú. Por favor intente nuevamente",
          );
        },
      });
    } else {
      showErrorMsg(errorMsg);
      $("#txtPrivilegeId").focus();
    }
  }
}

function editPrivilegeIntern(privilegeToEdit) {
  if (canEditItem) {
    getMenuList("UPDATE");
    var param = {
      prPrivilegeGkey: privilegeToEdit.gkey,
    };
    $.ajax({
      type: "GET",
      contentType: "application/json",
      url: url_application + "/PrivilegeManagement/getPrivilege/",
      data: param,
      success: function (data, statusCode) {
        fillPrivilegeInputOnModal(data);
      },
      fail: function (data, statusCode) {
        showErrorMsg(data);
        $("#btnCancelSavePrivilege").click();
      },
    });
  } else showErrorMsg("No tiene permisos para realizar esta acción");
}

function deletePrivilegeIntern(privilegeToDelete) {
  if (canDeleteItem) {
    privilegeToDelete = deleteDtoAttributes(privilegeToDelete);
    console.log("privilegeToDelete:" + JSON.stringify(privilegeToDelete));
    $.ajax({
      type: "POST",
      contentType: "application/json",
      url: url_application + "/PrivilegeManagement/deletePrivilege/",
      data: JSON.stringify(privilegeToDelete),
      success: function (result, statusCode) {
        showSuccessMsg("Listo!", result);
        refreshSearchData();
      },
      fail: function (result, statusCode) {
        showErrorMsg(
          statusCode +
            " Se produjo un error eliminando el privilegio. Por favor intente nuevamente",
        );
      },
    });
  } else showErrorMsg("No tiene permisos para realizar esta acción");
}

function recoverPrivilegeIntern(privilegeTorecover) {
  if (canRecoverItem) {
    privilegeTorecover = deleteDtoAttributes(privilegeTorecover);
    $.ajax({
      type: "POST",
      contentType: "application/json",
      url: url_application + "/PrivilegeManagement/recoverPrivilege/",
      data: JSON.stringify(privilegeTorecover),
      success: function (result, statusCode) {
        showSuccessMsg("Listo!", result);
        refreshSearchData();
      },
      fail: function (result, statusCode) {
        showErrorMsg(
          statusCode +
            " Se produjo un error activando el privilegio. Por favor intente nuevamente",
        );
      },
    });
  } else showErrorMsg("No tiene permisos para realizar esta acción");
}

function deleteSelectedPrivilege() {
  if (canDeleteItem) {
    var selectedPrivilegeJson = getSelectedPrivilegeJson();
    if (selectedPrivilegeJson != null && selectedPrivilegeJson != "") {
      $.ajax({
        type: "POST",
        contentType: "application/json",
        url: url_application + "/PrivilegeManagement/deleteSelectedPrivilege/",
        data: selectedPrivilegeJson,
        success: function (result, statusCode) {
          showSuccessMsg("Listo!", result);
          refreshSearchData();
        },
        fail: function (result, statusCode) {
          showErrorMsg(
            statusCode +
              " Se produjo un error eliminando los privilegios. Por favor intente nuevamente",
          );
        },
      });
    }
  } else showErrorMsg("No tiene permisos para realizar esta acción");
}

function recoverSelectedPrivilege() {
  if (canRecoverItem) {
    var selectedPrivilegeJson = getSelectedPrivilegeJson();
    if (selectedPrivilegeJson != null && selectedPrivilegeJson != "") {
      $.ajax({
        type: "POST",
        contentType: "application/json",
        url: url_application + "/PrivilegeManagement/recoverSelectedPrivilege/",
        data: selectedPrivilegeJson,
        success: function (result, statusCode) {
          showSuccessMsg("Listo!", result);
          refreshSearchData();
        },
        fail: function (result, statusCode) {
          showErrorMsg(
            statusCode +
              " Se produjo un error activando los privilegios. Por favor intente nuevamente",
          );
        },
      });
    }
  } else showErrorMsg("No tiene permisos para realizar esta acción");
}

function getMenuList(action) {
  $.ajax({
    type: "GET",
    url: url_application + "/PrivilegeManagement/getMenuList/",
    success: function (data, statusCode) {
      if (action == "NEW" || action == "UPDATE") loadModalMenuCb(data);
      else loadSearchMenuCb(data);
    },
    fail: function (data, statusCode) {
      showModal("Error", "Status code:" + statusCode, "error");
    },
  });
}

function getAccessTypeList() {
  $.ajax({
    type: "GET",
    url: url_application + "/PrivilegeManagement/getAccessTypeList/",
    success: function (data, statusCode) {
      loadAccessTypeCb(data);
    },
    fail: function (data, statusCode) {
      showModal("Error", "Status code:" + statusCode, "error");
    },
  });
}

function showCreatedPrivilege(data) {
  privilegeViewModel.createdPrivilege.removeAll();
  deleteDataTables();
  privilegeViewModel.createdPrivilege(data);
  $("#tbCreatedPrivilege").DataTable({
    bDestroy: true,
    bFilter: false,
    sScrollX: true,
    sScrollY: "30vh",
    scrollCollapse: true,
    paging: false,
    bPaginate: false,
    bFilter: false,
    bSort: false,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
    },
  });
}

function refreshSearchData() {
  $("#btnSearch").click();
}

function loadSearchMenuCb(data) {
  privilegeViewModel.menu(data);
}

function loadModalMenuCb(data) {
  privilegeViewModel.modalMenu(data);
}

function loadAccessTypeCb(data) {
  privilegeViewModel.accessType(data);
  privilegeViewModel.modalAccessType(data);
}

function getPrivilegeDataFromModal() {
  var privilegeData = {};
  privilegeData["label"] = $("#txtPrivilegeLabel").val();
  privilegeData["description"] = $("#txtPrivilegeDesc").val();
  privilegeData["menuGkey"] = $("#cbMenu option:selected").val();
  privilegeData["accessType"] = $("#cbAccessType option:selected").val();
  privilegeData["id"] = $("#txtPrivilegeId").val();
  var privilegeGkey = $("#txtPrivilegeGkey").val();
  if (privilegeGkey != null && privilegeGkey != "")
    privilegeData["gkey"] = privilegeGkey;
  return privilegeData;
}

function isEditingPrivilege() {
  var privilegeGkey = $("#txtPrivilegeGkey").val();
  return privilegeGkey != null && privilegeGkey != "";
}

function fillPrivilegeInputOnModal(data) {
  $("#modalTitle").text("Editar Privilegio");
  $("#txtPrivilegeGkey").val(data["gkey"]);
  $("#txtPrivilegeLabel").val(data["label"]);
  $("#txtPrivilegeDesc").val(data["description"]);
  var menuGkey = data["menuGkey"];
  if (menuGkey != null) $("#cbMenu").val(menuGkey).change();
  var accessType = data["accessType"];
  if (accessType != null) $("#cbAccessType").val(accessType).change();
  $("#addEditPrivilegeModal").modal();
}

function deleteDtoAttributes(privilegeDto) {
  delete privilegeDto["accessTypeDesc"];
  delete privilegeDto["menuLabel"];
  delete privilegeDto["creator"];
  delete privilegeDto["createdStr"];
  delete privilegeDto["changer"];
  delete privilegeDto["changedStr"];
  delete privilegeDto["isActive"];
  delete privilegeDto["isIncludedOnRole"];
  return privilegeDto;
}

function validateSearchFields() {
  var label = $("#txtSearchPrivilegeLabel").val();
  var errorMsg = "";
  if (!isValidStr(label))
    errorMsg = "Por favor elimine los caracteres especiales de la búsqueda";
  return errorMsg;
}

function validateFormFields() {
  var label = $("#txtPrivilegeLabel").val();
  var description = $("#txtPrivilegeDesc").val();
  var menuGkey = $("#cbMenu option:selected").val();
  var accessType = $("#cbAccessType option:selected").val();
  var errorMsg = "";
  if (!isValidStr(label) || !isValidStr(description))
    errorMsg = "Los campos no pueden contener caracteres especiales";
  else if (label == "" || label.length < 3)
    errorMsg =
      "El campo Nombre es obligatorio y debe tener como mínimo 3 caracteres de longitud";
  else if (menuGkey == "")
    errorMsg = "Por favor seleccione un menú para el privilegio";
  else if (accessType == "")
    errorMsg = "Por favor seleccione un tipo de acceso para el privilegio";
  return errorMsg;
}

function getSelectedPrivilegeJson() {
  var selectedPrivileges = $(".selectable:checked");
  if (selectedPrivileges.length == 0) {
    showWarningMsg("Alerta", "Debe seleccionar al menos un registro");
    return "";
  } else
    return JSON.stringify(getPrivilegeToDeleteOrRecover(selectedPrivileges));
}

function getPrivilegeToDeleteOrRecover(selectedPrivileges) {
  var gkeyArray = [];
  if (selectedPrivileges != null) {
    selectedPrivileges.each(function () {
      var selectedPrivilege = $(this);
      var gkey = selectedPrivilege.attr("gkey");
      if (gkey != null && gkey != "") gkeyArray.push(gkey);
    });
  }
  return gkeyArray;
}

function cleanSearchParams() {
  console.log("access type:" + $("#cbSearchAccessType option:selected").val());
  $("#txtSearchPrivilegeLabel").val("");
  $("#cbSearchMenu").val("");
  $("#cbSearchAccessType").val("");
  $("#txtSearchFromDate").val("");
  $("#txtSearchToDate").val("");
  $("#cbSearchPrivilegeStatus").val("");
  $("#txtSearchPrivilegeLabel").focus();
}

function cleanAddEditFormParams() {
  $("#txtPrivilegeLabel").val("");
  $("#txtPrivilegeDesc").val("");
  $("#cbMenu").val("");
  $("#cbAccessType").val("");
  $("#txtPrivilegeGkey").val("");
  $("#divPrivilegeId").hide();
  $("#txtPrivilegeId").val("");
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function resultIsOk(result) {
  var status = result["status"];
  return status == "OK";
}

function isValidStr(str) {
  return !/[~`!#$%\^&*+=\\[\]\\';,/{}|\\":<>\?]/g.test(str);
}
