/**
 * Author: Jimena Chavez
 *
 */

$(document).ready(function () {
  initHtmlElements();
  initKnockout();
  var unitDto = {};
  var storageUnitMap = {};
  var documentGkey;
});

function initKnockout() {
  supportUnitViewModel = {
    categoryCb: ko.observableArray([]),
    transitStateCb: ko.observableArray([]),
    lineOpCb: ko.observableArray([]),
    yesNoCb: ko.observableArray([]),
    unitTable: ko.observableArray([]),
    unitServiceTable: ko.observableArray([]),
    coordinationTypeCb: ko.observableArray([]),
    channelCb: ko.observableArray([]),
    bunchCb: ko.observableArray([]),
    dateOpeningCb: ko.observableArray([]),
    archTypeCb: ko.observableArray([]),
    apptOrderItemCb: ko.observableArray([]),
    coordinateUnit: function (unitToCoordinate) {
      viewUnitCalendar(unitToCoordinate);
    },
    cancelAppt: function (apptToCancel) {
      cancelUnitAppointment(apptToCancel);
    },
    viewUnitServices: function (unitToShowSrvOrder) {
      viewUnitServicesIntern(unitToShowSrvOrder);
    },
    cancelSo: function (soToCancel) {
      cancelUnitService(soToCancel);
    },
  };
  ko.applyBindings(supportUnitViewModel);
}

function initHtmlElements() {
  hideDivTable();
  initInputs();
  initButtons();
  initCombos();
  initApptModal();
  initToggleColumns();
}

function initInputs() {
  $("#appointmentFieldId1").hide();
  $("#serviceFieldId1").hide();
  $("#serviceFieldId2").hide();
  $("#actionButtons").hide();
  $("#txtSearchContainerNbr").focus();
}

function initButtons() {
  $("#btnSearch").click(function () {
    findUnit(true);
  });

  $("#btnSaveCoordination").click(function () {
    saveCoordination();
  });

  $("#btnClean").click(function () {
    cleanSearchParams();
  });
}

function initCombos() {
  loadInitCombos();

  $("#cbCoordinationType").change(function () {
    loadUnitCalendar(false);
  });
}

function initApptModal() {
  $("#viewUnitCalendarCusModal").on("shown.bs.modal", function () {
    loadUnitCalendar(true);
    $("#calendar").fullCalendar("render");
  });

  $("#viewUnitCalendarCusModal").on("hidden.bs.modal", function () {
    adjustDataTableColumns();
    cleanModalInputs();
  });

  $("#selectCoordinationTimeCusModal").on("hidden.bs.modal", function () {
    cleanDateModalInputs();
  });

  $("#viewUnitSrvOrderCusModal").on("shown.bs.modal", function () {
    adjustDataTableColumns();
  });

  $("#viewUnitSrvOrderCusModal").on("hidden.bs.modal", function () {
    deleteUnitServiceOrderDataTable();
    adjustDataTableColumns();
  });
}

function initToggleColumns() {
  $("a.toggle-vis").on("click", function (e) {
    e.preventDefault();
    var table = $("#tbUnit").DataTable();
    console.log("table:" + table);
    // Get the column API object
    var column = table.column($(this).attr("data-column"));
    console.log("column:" + column);
    // Toggle the visibility
    column.visible(!column.visible());
  });
}

function loadInitCombos() {
  showProcessingModal();
  $.ajax({
    type: "GET",
    url: url_application + "/SupportUnit/loadInitCombos/",
    success: function (data, statusCode) {
      loadSearchCategoryCb(data);
      loadSearchLineOpCb(data);
      loadSearchTransitStateCb(data);
      loadSearchYesNoCb(data);
      closeProcessingModal();
    },
    fail: function (data, statusCode) {
      showModal("Error", "Status code:" + statusCode, "error");
    },
  });
}

function findUnit(showProcessing) {
  console.log("findUnit");
  var errorMsg = validateSearchFields();
  if (errorMsg == "") {
    if (showProcessing) showProcessingModal();
    var searchParam = {
      prUnitId: $("#txtSearchContainerNbr").val(),
      prCategory: $("#cbSearchCategory option:selected").val(),
      prTransitState: $("#cbSearchTransitState option:selected").val(),
      prLineOp: $("#cbSearchLineOp option:selected").val(),
      prInboundVisit: $("#txtSearchIbVisit").val(),
      prOutboundVisit: $("#txtSearchObVisit").val(),
      prDocumentNbr: $("#txtSearchDocumentNbr").val(),
      prApptNbrStr: $("#txtSearchApptNbr").val(),
      prReeferStr: $("#cbSearchReefer option:selected").val(),
      prHazardousStr: $("#cbSearchHazardous option:selected").val(),
      prEventType: $("#txtSearchEventType").val(),
      prHoldId: $("#txtSearchHoldId").val(),
    };
    console.log("searchParam:" + JSON.stringify(searchParam));
    $.ajax({
      type: "GET",
      url: url_application + "/SupportUnit/findUnit/",
      data: searchParam,
      success: function (data, statusCode) {
        var msg = data["msg"];
        if (typeof msg != "undefined" && !resultIsOk(data)) {
          showErrorHtmlMsg(msg);
        } else {
          showDivTable();
          showUnitTable(data);
          if (showProcessing) closeProcessingModal();
        }
      },
      fail: function (data, statusCode) {
        s;
        showErrorMsg("Status code:" + statusCode);
      },
    });
  } else showErrorMsg(errorMsg);
}

function initCalendar(data) {
  var minDate = data["minDate"];
  var maxDate = data["maxDate"];
  $("#calendar").fullCalendar({
    header: {
      left: "prev",
      center: "title",
      right: "next",
      ignoreTimezone: true,
    },
    defaultDate: minDate,
    defaultView: "month",
    monthNames: [
      "Enero",
      "Febrero",
      "Marzo",
      "Abril",
      "Mayo",
      "Junio",
      "Julio",
      "Agosto",
      "Septiembre",
      "Octubre",
      "Noviembre",
      "Diciembre",
    ],
    dayNamesShort: ["D", "L", "M", "X", "J", "V", "S"],
    viewRender: function (view) {
      if (view.end < minDate) {
        $("#calendar").fullCalendar("gotoDate", minDate);
      } else if (view.start > maxDate) {
        $("#calendar").fullCalendar("gotoDate", maxDate);
      }
    },
    events: function (start, end, timezone, callback) {
      var events = [];
      data["calendarEventList"].forEach(function (opening) {
        events.push({
          id: opening["id"],
          title: opening["title"],
          start: opening["dateStr"],
          color: opening["color"],
        });
      });
      callback(events);
    },
    eventClick: function (event, jsEvent, view) {
      // when some one click on any event
      resolveCalendarEvent(event);
    },
  });
}

function resolveCalendarEvent(event) {
  var paramObject = {};
  paramObject["unitDto"] = unitDto;
  paramObject["event"] = event;
  paramObject["coordinationType"] = $(
    "#cbCoordinationType option:selected",
  ).val();
  showProcessingModal();
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/CoordinationManagement/resolveCalendarEventCus/",
    data: JSON.stringify(paramObject),
    success: function (data) {
      closeProcessingModal();
      if (data != null && data != "") {
        fillDateInputModal(event.id, data);
      }
    },
  });
}

function viewUnitCalendar(unitToCoordinate) {
  unitDto = unitToCoordinate;
  fillInputModal(unitToCoordinate);
}

function viewUnitServicesIntern(unitToShowSrvOrder) {
  showProcessingModal();
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/CoordinationManagement/viewUnitServicesCus/",
    data: JSON.stringify(unitToShowSrvOrder),
    success: function (data) {
      showUnitServiceTable(data);
      closeProcessingModal();
      $("#viewUnitSrvOrderCusModal").modal();
    },
    fail: function (data, statusCode) {
      showErrorMsg(
        statusCode +
          " Se produjo un error obteniendo los servicios del contenedor. Por favor intente nuevamente",
      );
    },
  });
}

function fillInputModal(unitToCoordinate) {
  var category = unitToCoordinate["unitCategory"];
  if (category != "APPT") {
    $("#txtUnitId").val(unitToCoordinate["unitId"]);
    $("label[for='txtUnitId']").text("Contenedor:");
    $("#txtLimitDate").val(unitToCoordinate["unitLimitDateStr"]);
    var limitDateLabel = getLimitDateColumnName(category);
    $("label[for='txtLimitDate']").text(limitDateLabel);
  } else {
    $("#txtUnitId").val(unitToCoordinate["documentNbr"]);
    $("label[for='txtUnitId']").text("Booking/EDO:");
    $("#txtLimitDate").val(unitToCoordinate["documentItemIsoType"]);
    $("label[for='txtLimitDate']").text("Tipo:");
  }
  loadCoordinationTypeCombo(unitToCoordinate);
  $("#viewUnitCalendarCusModal").modal();
}

function fillDateInputModal(dateStr, data) {
  supportUnitViewModel.dateOpeningCb.removeAll();
  supportUnitViewModel.bunchCb.removeAll();
  supportUnitViewModel.dateOpeningCb(data["openingList"]);
  $("#txtSelectedDate").val(dateStr);
  var channelList = data["customsChannelList"];
  var bunchList = data["bunchList"];
  var fieldToShow = data["fieldToShow"];
  if (typeof channelList != "undefined") {
    supportUnitViewModel.channelCb(channelList);
  }
  if (typeof bunchList != "undefined") {
    supportUnitViewModel.bunchCb(bunchList);
  }
  if (typeof fieldToShow != "undefined") {
    $(fieldToShow).show();
  }
  $("#selectCoordinationTimeCusModal").modal();
}

function loadCoordinationTypeCombo(unitToCoordinate) {
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/CoordinationManagement/getCoordinationTypes/",
    data: JSON.stringify(unitToCoordinate),
    success: function (data) {
      loadCoordinationTypeCb(data);
    },
  });
}

function showUnitTable(data) {
  supportUnitViewModel.unitTable.removeAll();
  deleteUnitDataTable();
  supportUnitViewModel.unitTable(data);
  var table = $("#tbUnit").DataTable({
    bDestroy: true,
    bFilter: true,
    sScrollX: true,
    sScrollY: "60vh",
    scrollCollapse: true,
    paging: false,
    bPaginate: false,
    bSort: true,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
      sSearch: "Buscar",
    },
    aoColumnDefs: [
      {
        aTargets: [5, 7, 8, 9, 10, 11, 16, 17, 18, 19, 20, 21, 24, 25],
        bVisible: false,
      },
    ],
  });
}

function showUnitServiceTable(data) {
  supportUnitViewModel.unitServiceTable.removeAll();
  deleteUnitServiceOrderDataTable();
  supportUnitViewModel.unitServiceTable(data);
  var unitSoTable = $("#tbUnitServiceOrder").DataTable({
    bDestroy: true,
    bFilter: false,
    sScrollX: false,
    sScrollY: "30vh",
    scrollCollapse: true,
    paging: false,
    bPaginate: false,
    bFilter: false,
    bSort: false,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
    },
  });
}

function refreshSearchData() {
  findUnit(false);
}

function loadSearchCategoryCb(data) {
  supportUnitViewModel.categoryCb(data["categoryList"]);
}

function loadSearchLineOpCb(data) {
  supportUnitViewModel.lineOpCb(data["lineOpList"]);
}

function loadSearchTransitStateCb(data) {
  supportUnitViewModel.transitStateCb(data["transitStateList"]);
}

function loadSearchYesNoCb(data) {
  supportUnitViewModel.yesNoCb(data["yesNoList"]);
}

function loadCoordinationTypeCb(data) {
  supportUnitViewModel.coordinationTypeCb(data);
}

function loadArchTypeCb(data) {
  supportUnitViewModel.archTypeCb(data["archTypeList"]);
}

function loadApptOrderItemCb(data) {
  supportUnitViewModel.apptOrderItemCb(data);
}

function validateSearchFields() {
  var searchContainer = $("#txtSearchContainerNbr").val();
  var searchIbVisit = $("#txtSearchIbVisit").val();
  var searchObVisit = $("#txtSearchObVisit").val();
  var searchDocumentNbr = $("#txtSearchDocumentNbr").val();
  var searchApptNbr = $("#txtSearchApptNbr").val();
  var searchEventType = $("#txtSearchEventType").val();
  var searchHoldId = $("#txtSearchHoldId").val();

  if (
    !isValidStr(searchContainer) ||
    !isValidStr(searchIbVisit) ||
    !isValidStr(searchObVisit) ||
    !isValidStr(searchDocumentNbr) ||
    !isValidStr(searchApptNbr) ||
    !isValidStr(searchEventType) ||
    !isValidStr(searchHoldId)
  )
    errorMsg = "Por favor elimine los caracteres especiales de la búsqueda";
  return "";
}

function hideDivTable() {
  $("#divTable").hide();
}

function showDivTable() {
  $("#divTable").show();
}
function cleanSearchParams() {
  $("#txtSearchContainerNbr").val("");
  $("#cbSearchCategory").val("");
  $("#cbSearchTransitState").val("");
  $("#cbSearchLineOp").val("");
  $("#txtSearchIbVisit").val("");
  $("#txtSearchObVisit").val("");
  $("#txtSearchDocumentNbr").val("");
  $("#txtSearchApptNbr").val("");
  $("#cbSearchReefer").val("");
  $("#cbSearchHazardous").val("");
  $("#txtSearchEventType").val("");
  $("#txtSearchHoldId").val("");
  deleteDataTables();
  hideDivTable();
  $("#txtSearchContainerNbr").focus();
}

function cleanModalInputs() {
  unitDto = {};
  supportUnitViewModel.coordinationTypeCb.removeAll();
  $("#txtUnitId").val("");
  $("#txtLimitDate").val("");
  $("#cbCoordinationType").val("");
  $("#calendar").fullCalendar("destroy");
}

function cleanDateModalInputs() {
  supportUnitViewModel.dateOpeningCb.removeAll();
  supportUnitViewModel.channelCb.removeAll();
  supportUnitViewModel.bunchCb.removeAll();
  $("#txtEmail").val("");
  $("#cbBunch").val("");
  $("#cbChannel").val("");
  $("#appointmentFieldId1").hide();
  $("#serviceFieldId1").hide();
  $("#serviceFieldId2").hide();
}

function getLimitDateColumnName(unitCategory) {
  return unitCategory == "EXPRT"
    ? "Cutoff"
    : unitCategory == "IMPRT"
      ? "Forzoso"
      : unitCategory == "STRGE"
        ? "Límite Devolución"
        : "Fecha límite";
}

function deleteUnitServiceOrderDataTable() {
  $("#tbUnitServiceOrder").DataTable().clear();
  $("#tbUnitServiceOrder").DataTable().destroy();
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function deleteUnitDataTable() {
  $("#tbUnit").DataTable().clear();
  $("#tbUnit").DataTable().destroy();
}

function resultIsOk(result) {
  var status = result["status"];
  return status == "OK";
}

function resultIsError(result) {
  var status = result["status"];
  return status == "ERROR";
}

function resultRequiresConfirm(result) {
  var status = result["status"];
  return status == "QUESTION";
}

function resultIsWarning(result) {
  var status = result["status"];
  return status == "WARNING";
}

function adjustDataTableColumns() {
  $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
}

function isValidStr(str) {
  return !/[~`!#$%\^&*+=\\[\]\\';,/{}|\\":<>\?]/g.test(str);
}

function loadUnitCalendar(isOnOpenModal) {
  if (typeof unitDto != "undefined" && !_.isEmpty(unitDto)) {
    $("#calendar").fullCalendar("destroy");
    unitDto["coordinationType"] = $(
      "#cbCoordinationType option:selected",
    ).val();
    showProcessingModal();
    $.ajax({
      type: "POST",
      contentType: "application/json",
      url: url_application + "/CoordinationManagement/getUnitCalendarCus/",
      data: JSON.stringify(unitDto),
      success: function (data) {
        closeProcessingModal();
        var msg = data["msg"];
        if (resultIsWarning(data)) showWarningMsg(msg);
        else if (resultRequiresConfirm(data)) {
          Swal.fire({
            allowOutsideClick: false,
            title: "Desea registrar el servicio?",
            text: msg,
            type: "question",
            showCancelButton: true,
            confirmButtonColor: "#4fa7f3",
            cancelButtonColor: "#d57171",
            confirmButtonText: "Sí, registrar!",
          }).then((result) => {
            if (result.value) {
              saveCoordination();
            } else if (result.dismiss === Swal.DismissReason.cancel) {
              adjustDataTableColumns();
            }
          });
        } else {
          initCalendar(data);
          var selectedCoordinationType = $(
            "#cbCoordinationType option:selected",
          ).val();
          if (
            selectedCoordinationType == null ||
            selectedCoordinationType == ""
          ) {
            var comboLength = $("#cbCoordinationType option").length;
            if (isOnOpenModal && comboLength == 2) {
              var selectedIndex = $("#cbCoordinationType").prop(
                "selectedIndex",
              );
              $("#cbCoordinationType").prop("selectedIndex", 1).change();
            } else {
              showTemporaryMsg(
                "",
                "Seleccioná un servicio para ver los turnos disponibles",
                2000,
              );
            }
          }
        }
        //$("#calendar").fullCalendar("render");
      },
      fail: function (data, statusCode) {
        closeProcessingModal();
        showErrorMsg("Status code:" + statusCode);
      },
    });
  }
}

function saveCoordination() {
  var paramObject = {};
  paramObject["unitDto"] = unitDto;
  paramObject["coordinationType"] = $(
    "#cbCoordinationType option:selected",
  ).val();
  paramObject["selectedDateTime"] = $("#cbDateOpening option:selected").val();
  paramObject["customsChannel"] = $("#cbChannel option:selected").val();
  paramObject["bunch"] = $("#cbBunch option:selected").val();
  paramObject["email"] = $("#txtEmail").val();
  showProcessingModal();
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/CoordinationManagement/saveCoordinationCus/",
    data: JSON.stringify(paramObject),
    success: function (data) {
      closeProcessingModal();
      var msg = data["msg"];
      var questionType = data["questionType"];
      if (resultIsError(data)) {
        showErrorMsg(msg);
      } else if (resultRequiresConfirm(data)) {
        Swal.fire({
          allowOutsideClick: false,
          title: "Desea continuar?",
          text: msg,
          type: "question",
          showCancelButton: true,
          confirmButtonColor: "#4fa7f3",
          cancelButtonColor: "#d57171",
          confirmButtonText: "Sí, coordinar!",
        }).then((result) => {
          if (result.value) {
            unitDto[questionType] = true;
            saveCoordination();
            delete unitDto[questionType];
          } else if (result.dismiss === Swal.DismissReason.cancel) {
            delete unitDto[questionType];
          }
        });
      } else if (resultIsOk(data)) {
        delete unitDto[questionType];
        $("#btnCancelSaveCoordination").click();
        $("#btnCancelViewUnitCalendar").click();
        refreshSearchData();
        showSuccessHtmlMsg("Listo!", msg);
      } else {
        closeProcessingModal();
        showErrorMsg(
          statusCode +
            "Se produjo un error guardando la coordinación. Por favor intente nuevamente",
        );
      }
    },
    fail: function (data, statusCode) {
      closeProcessingModal();
      showErrorMsg(
        statusCode +
          "Se produjo un error guardando el menú. Por favor intente nuevamente",
      );
    },
  });
}

function cancelUnitAppointment(apptToCancel) {
  showProcessingModal();
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/CoordinationManagement/cancelUnitAppointment/",
    data: JSON.stringify(apptToCancel),
    success: function (data) {
      closeProcessingModal();
      var msg = data["msg"];
      var questionType = data["questionType"];
      if (resultIsError(data)) {
        showErrorMsg(msg);
      } else if (resultRequiresConfirm(data)) {
        Swal.fire({
          allowOutsideClick: false,
          title: "Desea continuar?",
          text: msg,
          type: "question",
          showCancelButton: true,
          confirmButtonColor: "#4fa7f3",
          cancelButtonColor: "#d57171",
          confirmButtonText: "Sí, eliminar!",
        }).then((result) => {
          if (result.value) {
            apptToCancel[questionType] = true;
            cancelUnitAppointment(apptToCancel);
          } else if (result.dismiss === Swal.DismissReason.cancel) {
            adjustDataTableColumns();
            delete apptToCancel[questionType];
          }
        });
      } else {
        refreshSearchData();
        showSuccessMsg("Listo!", msg);
      }
    },
    fail: function (result, statusCode) {
      showErrorMsg(
        statusCode +
          " Se produjo un eliminando el turno. Por favor intente nuevamente",
      );
    },
  });
}

function cancelUnitService(serviceToCancel) {
  Swal.fire({
    allowOutsideClick: false,
    title: "Desea cancelar el servicio?",
    text: "",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: "#4fa7f3",
    cancelButtonColor: "#d57171",
    confirmButtonText: "Sí, cancelar!",
  }).then((result) => {
    if (result.value) {
      showProcessingModal();
      $.ajax({
        type: "POST",
        contentType: "application/json",
        url: url_application + "/CoordinationManagement/cancelUnitService/",
        data: JSON.stringify(serviceToCancel),
        success: function (data) {
          closeProcessingModal();
          var msg = data["msg"];
          if (resultIsError(data)) {
            showErrorMsg(msg);
          } else {
            refreshSearchData();
            showUnitServiceTable(data["unitSoList"]);
            showSuccessMsg("Listo!", msg);
          }
        },
        fail: function (data, statusCode) {
          showErrorMsg(
            statusCode +
              " Se produjo un error cancelando el servicio. Por favor intente nuevamente",
          );
        },
      });
    } else if (result.dismiss === Swal.DismissReason.cancel) {
      adjustDataTableColumns();
    }
  });
}
