/**
 * Author: Lucas Juarez
 * APM Terminals Buenos Aires
 */

$(document).ready(function () {
  $("#btnPay").click(function () {
    buildInterbankingData();
  });

  $("#btnBack").click(function () {
    confirmExit();
  });
});

function processingModal() {
  Swal.fire({
    title: "Procesando...",
    text: "Aguarde un instante por favor",
    imageUrl: url_application + "/resources/assets/images/ajax_loading.gif",
    showConfirmButton: false,
    allowOutsideClick: false,
  });
}

function buildInterbankingData() {
  var obj = new Object();
  obj.type = "INTERBANKING";
  obj.cuitFrom = $("#cuitFrom").val();
  if (obj.cuitFrom == null || obj.cuitFrom == "") {
    showModal(
      "Atención!",
      "Por favor, completar con un CUIT válido.",
      "warning",
    );
    return;
  }

  processingModal();
  var request = $.ajax({
    url: url_application + "/Payment/addPayment",
    type: "POST",
    data: JSON.stringify(obj),
    contentType: "application/json",
  });

  request.done(function (message) {
    showModalHtmlCallback(
      "",
      message,
      "info",
      url_application + "/Invoice/drafts",
    );
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", jqXHR.responseText, "error");
  });
}

function showTimerModal(title, text, timerMillis) {
  Swal.fire({
    title: title,
    html: text,
    timer: timerMillis,
    onBeforeOpen: () => {
      Swal.showLoading();
    },
    onClose: () => {
      window.location.href = url_application + "/Invoice/drafts";
    },
  });
}

function confirmExit() {
  Swal.fire({
    allowOutsideClick: false,
    title: "",
    text: "Esta seguro de salir del metodo de pago Interbanking?",
    type: "info",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "S&iacute;, estoy seguro!",
    cancelButtonText: "Cancelar",
  }).then((result) => {
    if (result.dismiss != "cancel") {
      history.back();
    }
  });
}
