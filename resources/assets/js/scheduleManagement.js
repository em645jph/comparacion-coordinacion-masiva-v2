/**
 * Author: Jimena Chavez
 *
 */

$(document).ready(function () {
  initHtmlElements();
  initKnockout();
});

function initKnockout() {
  scheduleViewModel = {
    searchTranTypeCb: ko.observableArray([]),
    searchActionCb: ko.observableArray([]),
    searchRuleActionCb: ko.observableArray([]),
    tranTypeCb: ko.observableArray([]),
    actionCb: ko.observableArray([]),
    timeUnitCb: ko.observableArray([]),
    dayCb: ko.observableArray([]),
    paramTypeCb: ko.observableArray([]),
    filterList: ko.observableArray([]),
    filterParamList: ko.observableArray([]),
    paramTypeCb: ko.observableArray([]),
    ruleList: ko.observableArray([]),
    editFilterDef: function (sfdToEdit) {
      editFilterDefIntern(sfdToEdit);
    },
    editFilterParam: function (sfdToEdit) {
      editFilterParamIntern(sfdToEdit);
    },
    editRule: function (ruleToEdit) {
      editRuleIntern(ruleToEdit);
    },
    deleteFilterDef: function (filterDefToDelete) {
      deleteFilterDefIntern(filterDefToDelete);
    },
    deleteFilterParam: function (filterParamToDelete) {
      deleteFilterParamIntern(filterParamToDelete);
    },
    deleteRule: function (ruleToDelete) {
      deleteRuleIntern(ruleToDelete);
    },
  };
  ko.applyBindings(scheduleViewModel);
}

function initHtmlElements() {
  initInputs();
  initButtons();
  initDateTimePickers();
  initColorPicker();
  initCombos();
  initModal();
  initCheckBoxs();
}

function initInputs() {
  $("#txtSearchSfdId").focus();

  $("#txtRuleDays").keypress(function () {
    var key = event.keyCode ? event.keyCode : event.which;
    if ((key < 48 || key > 57) && key != 95) return false;
    return true;
  });
}

function initButtons() {
  $("#btnSearchFilter").click(function () {
    //$("#ckCheckAll").prop("checked",false);
    var errorMsg = validateFilterSearchFields();
    if (errorMsg == "") {
      var searchParam = {
        prId: $("#txtSearchSfdId").val(),
        prDescription: $("#txtSearchSfdDesc").val(),
        prTranTypeKey: $("#cbSearchTranType option:selected").val(),
        prFromDateStr: $("#txtSearchSfdFromDate").val(),
        prToDateStr: $("#txtSearchSfdToDate").val(),
        prActionKey: $("#cbSearchAction option:selected").val(),
      };
      $.ajax({
        type: "GET",
        url: url_application + "/ScheduleManagement/findFilterDefByParam/",
        data: searchParam,
        success: function (data, statusCode) {
          showCreatedFilter(data);
        },
        fail: function (data, statusCode) {
          showErrorMsg("Status code:" + statusCode);
        },
      });
    } else showErrorMsg(errorMsg);
  });

  $("#btnSearchRule").click(function () {
    var errorMsg = validateRuleSearchFields();
    if (errorMsg == "") {
      var searchParam = {
        prId: $("#txtSearchRuleId").val(),
        prDescription: $("#txtSearchRuleDesc").val(),
        prTranTypeKey: $("#cbSearchRuleTranType option:selected").val(),
        prFromDateStr: $("#txtSearchRuleFromDate").val(),
        prToDateStr: $("#txtSearchRuleToDate").val(),
        prActionKey: $("#cbSearchRuleAction option:selected").val(),
      };
      $.ajax({
        type: "GET",
        url: url_application + "/ScheduleManagement/findRuleByParam/",
        data: searchParam,
        success: function (data, statusCode) {
          showCreatedRule(data);
        },
        fail: function (data, statusCode) {
          showErrorMsg("Status code:" + statusCode);
        },
      });
    } else showErrorMsg(errorMsg);
  });

  $("#btnAddFilterDef").click(function () {
    loadSfdModalCb();
    $("#sfdModalTitle").text("Agregar Filtro");
    $("#addEditFilterDefModal").modal();
  });

  $("#btnCleanSearchFilter").click(function () {
    cleanSearchFilterParams();
  });

  $("#btnSaveSfd").click(function () {
    addOrUpdateFilterDef();
  });

  $("#btnSaveSfp").click(function () {
    addOrUpdateFilterParam();
  });

  $("#btnCleanSfp").click(function () {
    $("#cbSfpType").val("");
    $("#txtSfpValue").val("");
  });

  $("#btnViewFilterCalendar").click(function () {
    $("#viewFilterCalendarModal").modal();
  });

  $("#btnAddRule").click(function () {
    $("#ruleModalTitle").text("Agregar Regla");
    $("#addEditRuleModal").modal();
  });

  $("#btnSaveRule").click(function () {
    addOrUpdateRule();
  });
}

function initDateTimePickers() {
  $("#txtSearchSfdFromDate").datepicker({ dateFormat: "yy-mm-dd" });
  $("#txtSearchSfdToDate").datepicker({ dateFormat: "yy-mm-dd" });
  $("#txtSearchRuleFromDate").datepicker({ dateFormat: "yy-mm-dd" });
  $("#txtSearchRuleToDate").datepicker({ dateFormat: "yy-mm-dd" });
  $("#txtSdfStartDate").datepicker({ dateFormat: "yy-mm-dd" });
  $("#txtSdfEndDate").datepicker({ dateFormat: "yy-mm-dd" });
  $("#txtSfdStartTime").timepicker({
    showMeridian: false,
    defaultTime: "07:00",
    minuteStep: 60,
    showSeconds: true,
    icons: {
      up: "mdi mdi-chevron-up",
      down: "mdi mdi-chevron-down",
    },
  });
  $("#txtSfdEndTime").timepicker({
    defaultTime: "23:00",
    minuteStep: 60,
    showMeridian: false,
    showSeconds: true,
    icons: {
      up: "mdi mdi-chevron-up",
      down: "mdi mdi-chevron-down",
    },
  });
  $("#txtRuleTime").timepicker({
    defaultTime: "15:00",
    minuteStep: 60,
    showMeridian: false,
    showSeconds: true,
    icons: {
      up: "mdi mdi-chevron-up",
      down: "mdi mdi-chevron-down",
    },
  });
}

function initColorPicker() {
  $("#txtSfdColor").spectrum({
    color: "grey",
    preferredFormat: "hex",
    chooseText: "Guardar",
    cancelText: "Cancelar",
    clickoutFiresChange: false,
  });
}

function initCombos() {
  loadSfdInitCb();
  loadRuleCb();
}

function initModal() {
  $("#addEditFilterDefModal").on("shown.bs.modal", function () {
    adjustDataTableColumns();
    $("#txtSfdId").focus();
  });

  $("#addEditFilterParamModal").on("shown.bs.modal", function () {
    adjustDataTableColumns();
    $("#txtSfpValue").focus();
  });

  $("#addEditFilterDefModal").on("hidden.bs.modal", function () {
    cleanAddEditSfdFormParams();
  });

  $("#viewFilterCalendarModal").on("shown.bs.modal", function () {
    getFilterCalendarDates();
  });

  $("#viewFilterCalendarModal").on("hidden.bs.modal", function () {
    $("#filterCalendar").fullCalendar("destroy");
    adjustDataTableColumns();
  });

  $("#addEditRuleModal").on("shown.bs.modal", function () {
    adjustDataTableColumns();
    $("#txtRuleId").focus();
  });

  $("#addEditRuleModal").on("hidden.bs.modal", function () {
    cleanAddEditRuleFormParams();
  });
}

function initCheckBoxs() {
  $("#ckCheckAll").change(function () {
    var check = $(this).is(":checked");
    var canBeSelected = $(".selectable:enabled");
    canBeSelected.each(function () {
      if (check) $(this).prop("checked", true);
      else $(this).prop("checked", false);
    });
  });

  $("#ckCheckAllPrivilege").change(function () {
    var check = $(this).is(":checked");
    var canBeSelected = $(".selectablePrivilege:enabled");
    canBeSelected.each(function () {
      if (check) $(this).prop("checked", true);
      else $(this).prop("checked", false);
    });
  });
}

function addOrUpdateFilterDef() {
  var errorMsg = validateFilterDefFormFields();
  if (errorMsg == "") {
    var sfdParam = {
      prGkeyStr: $("#txtSfdGkey").val(),
      prId: $("#txtSfdId").val(),
      prDescription: $("#txtSfdDesc").val(),
      prTranTypeKey: $("#cbSfdTranType option:selected").val(),
      prActionKey: $("#cbSdfAction option:selected").val(),
      prFromDateStr: $("#txtSdfStartDate").val(),
      prToDateStr: $("#txtSdfEndDate").val(),
      prTimeUnitKey: $("#cbSfdTimeUnit option:selected").val(),
      prDayKeyStr: $("#cbSfdDay option:selected").val(),
      prStartTime: $("#txtSfdStartTime").val(),
      prEndTime: $("#txtSfdEndTime").val(),
      prColor: $("#txtSfdColor").val(),
      prRecurrentStr: $("#cbSfdRecurrent option:selected").val(),
    };
    $.ajax({
      type: "POST",
      url: url_application + "/ScheduleManagement/addOrUpdateSchFilterDef/",
      data: sfdParam,
      success: function (result, statusCode) {
        var msg = result["msg"];
        if (resultIsOk(result)) {
          showSuccessMsg("Listo!", msg);
          refreshFilterSearchData();
          $("#btnCancelSaveSfd").click();
        } else showErrorMsg(msg);
      },
      fail: function (result, statusCode) {
        showErrorMsg(
          statusCode +
            "Se produjo un error guardando el filtro. Por favor intente nuevamente",
        );
      },
    });
  } else {
    showErrorMsg(errorMsg);
    $("#txtSfdId").focus();
  }
}

function addOrUpdateFilterParam() {
  var errorMsg = validateFilterParamFormFields();
  if (errorMsg == "") {
    var sfpParam = {
      prFilterDefGkeyStr: $("#txtSfpGkey").val(),
      prTypeKey: $("#cbSfpType option:selected").val(),
      prValue: $("#txtSfpValue").val(),
    };
    showProcessingModal();
    $.ajax({
      type: "POST",
      url: url_application + "/ScheduleManagement/addOrUpdateSchFilterParam/",
      data: sfpParam,
      success: function (result, statusCode) {
        closeProcessingModal();
        var msg = result["msg"];
        if (resultIsOk(result)) {
          showCreatedFilterParam(result);
        } else showErrorMsg(msg);
      },
      fail: function (result, statusCode) {
        closeProcessingModal();
        showErrorMsg(
          statusCode +
            "Se produjo un error guardando el parámetro. Por favor intente nuevamente",
        );
      },
    });
  } else {
    showErrorMsg(errorMsg);
    $("#txtSfdId").focus();
  }
}

function addOrUpdateRule() {
  var errorMsg = validateRuleFormFields();
  if (errorMsg == "") {
    var ruleParam = {
      prGkeyStr: $("#txtRuleGkey").val(),
      prId: $("#txtRuleId").val(),
      prDescription: $("#txtRuleDesc").val(),
      prTranTypeKey: $("#cbRuleTranType option:selected").val(),
      prActionKey: $("#cbRuleAction option:selected").val(),
      prDaysStr: $("#txtRuleDays").val(),
      prTimeStr: $("#txtRuleTime").val(),
    };
    $.ajax({
      type: "POST",
      url: url_application + "/ScheduleManagement/addOrUpdateScheduleRule/",
      data: ruleParam,
      success: function (result, statusCode) {
        var msg = result["msg"];
        if (resultIsOk(result)) {
          showSuccessMsg("Listo!", msg);
          refreshRuleSearchData();
          $("#btnCancelSaveRule").click();
        } else showErrorMsg(msg);
      },
      fail: function (result, statusCode) {
        showErrorMsg(
          statusCode +
            "Se produjo un error guardando el filtro. Por favor intente nuevamente",
        );
      },
    });
  } else {
    showErrorMsg(errorMsg);
    $("#txtSfdId").focus();
  }
}

function editFilterDefIntern(sfdToEdit) {
  var param = {
    prFilterDefGkey: sfdToEdit.gkey,
  };
  loadSfdModalCb();
  $.ajax({
    type: "GET",
    contentType: "application/json",
    url: url_application + "/ScheduleManagement/getFilterDef/",
    data: param,
    success: function (data, statusCode) {
      fillSfdInputOnModal(data);
    },
    fail: function (data, statusCode) {
      showErrorMsg(data);
      $("#btnCancelSaveSfd").click();
    },
  });
}

function editFilterParamIntern(sfdToEdit) {
  var param = {
    prFilterDefGkey: sfdToEdit.gkey,
  };
  showProcessingModal();
  $.ajax({
    type: "GET",
    contentType: "application/json",
    url: url_application + "/ScheduleManagement/getFilterParam/",
    data: param,
    success: function (data, statusCode) {
      closeProcessingModal();
      fillSfpInputOnModal(data);
    },
    fail: function (data, statusCode) {
      closeProcessingModal();
      showErrorMsg(data);
      $("#btnCancelSaveSfp").click();
    },
  });
}

function editRuleIntern(ruleToEdit) {
  var param = {
    prRuleGkey: ruleToEdit.gkey,
  };
  loadSfdModalCb();
  $.ajax({
    type: "GET",
    contentType: "application/json",
    url: url_application + "/ScheduleManagement/getRule/",
    data: param,
    success: function (data, statusCode) {
      fillRuleInputOnModal(data);
    },
    fail: function (data, statusCode) {
      showErrorMsg(data);
      $("#btnCancelSaveRule").click();
    },
  });
}

function deleteFilterDefIntern(filterDefToDelete) {
  filterDefToDelete = deleteFilterDefDtoAttributes(filterDefToDelete);
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/ScheduleManagement/deleteFilterDef/",
    data: JSON.stringify(filterDefToDelete),
    success: function (result, statusCode) {
      var msg = result["msg"];
      if (resultIsOk(result)) {
        showSuccessMsg("Listo!", msg);
        refreshFilterSearchData();
      } else showErrorMsg(msg);
    },
    fail: function (result, statusCode) {
      showErrorMsg(
        statusCode +
          " Se produjo un error eliminando el filtro. Por favor intente nuevamente",
      );
    },
  });
}

function deleteFilterParamIntern(filterParamToDelete) {
  filterParamToDelete = deleteFilterParamDtoAttributes(filterParamToDelete);
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/ScheduleManagement/deleteFilterParam/",
    data: JSON.stringify(filterParamToDelete),
    success: function (result, statusCode) {
      var msg = result["msg"];
      if (resultIsOk(result)) {
        showSuccessMsg("Listo!", msg);
        showCreatedFilterParam(result);
      } else showErrorMsg(msg);
    },
    fail: function (result, statusCode) {
      showErrorMsg(
        statusCode +
          " Se produjo un error eliminando el parámetro. Por favor intente nuevamente",
      );
    },
  });
}

function deleteRuleIntern(ruleToDelete) {
  ruleToDelete = deleteRuleDtoAttributes(ruleToDelete);
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/ScheduleManagement/deleteRule/",
    data: JSON.stringify(ruleToDelete),
    success: function (result, statusCode) {
      var msg = result["msg"];
      if (resultIsOk(result)) {
        showSuccessMsg("Listo!", msg);
        refreshRuleSearchData();
      } else showErrorMsg(msg);
    },
    fail: function (result, statusCode) {
      showErrorMsg(
        statusCode +
          " Se produjo un error eliminando la regla. Por favor intente nuevamente",
      );
    },
  });
}

function getFilterCalendarDates() {
  $.ajax({
    type: "GET",
    url: url_application + "/ScheduleManagement/getFilterCalendarDates/",
    success: function (data) {
      initFilterCalendar(data);
      $("#filterCalendar").fullCalendar("render");
    },
    fail: function (data, statusCode) {
      showModal("Error", "Status code:" + statusCode, "error");
    },
  });
  return null;
}

function loadSfdInitCb() {
  $.ajax({
    type: "GET",
    url: url_application + "/ScheduleManagement/loadSfdCombos/",
    success: function (data) {
      loadSearchTranTypeCb(data);
      loadSearchActionCb(data);
    },
    fail: function (data, statusCode) {
      showModal("Error", "Status code:" + statusCode, "error");
    },
  });
}

function loadSfdModalCb() {
  $.ajax({
    type: "GET",
    url: url_application + "/ScheduleManagement/loadSfdModalCombos/",
    success: function (data) {
      loadTranTypeCb(data);
      loadActionCb(data);
      loadTimeUnitCb(data);
      loadDayCb(data);
    },
    fail: function (data, statusCode) {
      showModal("Error", "Status code:" + statusCode, "error");
    },
  });
}

function loadRuleCb() {
  $.ajax({
    type: "GET",
    url: url_application + "/ScheduleManagement/loadRuleCombos/",
    success: function (data) {
      loadSearchTranTypeCb(data);
      loadSearchRuleActionCb(data);
    },
    fail: function (data, statusCode) {
      showModal("Error", "Status code:" + statusCode, "error");
    },
  });
}

function showCreatedFilter(data) {
  scheduleViewModel.filterList.removeAll();
  deleteDataTables();
  scheduleViewModel.filterList(data);
  $("#tbCreatedFilter").DataTable({
    bDestroy: true,
    bFilter: false,
    sScrollX: true,
    sScrollY: "30vh",
    scrollCollapse: true,
    paging: false,
    bPaginate: false,
    bFilter: false,
    bSort: false,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
    },
  });
}

function showCreatedFilterParam(data) {
  scheduleViewModel.filterParamList.removeAll();
  deleteFilterParamDataTable();
  scheduleViewModel.filterParamList(data["filterParamList"]);
  $("#tbFilterParam").DataTable({
    bDestroy: true,
    bFilter: false,
    sScrollX: false,
    sScrollY: "30vh",
    scrollCollapse: true,
    paging: false,
    bPaginate: false,
    bFilter: false,
    bSort: false,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
    },
  });
}

function showCreatedRule(data) {
  scheduleViewModel.ruleList.removeAll();
  deleteDataTables();
  scheduleViewModel.ruleList(data);
  $("#tbCreatedRule").DataTable({
    bDestroy: true,
    bFilter: false,
    sScrollX: true,
    sScrollY: "30vh",
    scrollCollapse: true,
    paging: false,
    bPaginate: false,
    bFilter: false,
    bSort: false,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
    },
  });
}

function initFilterCalendar(data) {
  var minDate = data["minDate"];
  var maxDate = data["maxDate"];
  $("#filterCalendar").fullCalendar({
    header: {
      left: "prev",
      center: "title",
      right: "next",
      ignoreTimezone: true,
    },
    defaultDate: minDate,
    defaultView: "agendaWeek",
    allDaySlot: false,
    axisFormat: "HH:mm",
    slotMinutes: 60,
    titleRangeSeparator: " - ",
    monthNames: [
      "Enero",
      "Febrero",
      "Marzo",
      "Abril",
      "Mayo",
      "Junio",
      "Julio",
      "Agosto",
      "Septiembre",
      "Octubre",
      "Noviembre",
      "Diciembre",
    ],
    dayNamesShort: ["D", "L", "M", "X", "J", "V", "S"],
    viewRender: function (view) {
      if (view.end < minDate) {
        $("#filterCalendar").fullCalendar("gotoDate", minDate);
      } else if (view.start > maxDate) {
        $("#filterCalendar").fullCalendar("gotoDate", maxDate);
      }
    },
    events: function (start, end, timezone, callback) {
      showProcessingModal();
      $.ajax({
        type: "POST",
        contentType: "application/json",
        url: url_application + "/ScheduleManagement/getFilterCalendarEvent/",
        data: ko.toJSON(scheduleViewModel.filterList),
        success: function (data) {
          var events = [];
          data.forEach(function (filterEvent) {
            events.push({
              id: filterEvent["id"],
              title: filterEvent["title"],
              start: filterEvent["startDateTimeStr"],
              end: filterEvent["endDateTimeStr"],
              color: filterEvent["color"],
              allDay: false,
            });
          });
          callback(events);
          closeProcessingModal();
        },
      });
    },
    eventClick: function (event, jsEvent, view) {
      // when some one click on any event
      $("#filterDefInfo").text(event.title);
      $("#viewFilterDefModal").modal();
    },
  });
}

function refreshFilterSearchData() {
  $("#btnSearchFilter").click();
}

function refreshRuleSearchData() {
  $("#btnSearchRule").click();
}

function loadSearchTranTypeCb(data) {
  scheduleViewModel.searchTranTypeCb.removeAll();
  scheduleViewModel.searchTranTypeCb(data["tranTypeList"]);
}

function loadSearchActionCb(data) {
  scheduleViewModel.searchActionCb.removeAll();
  scheduleViewModel.searchActionCb(data["actionList"]);
}

function loadTranTypeCb(data) {
  scheduleViewModel.tranTypeCb.removeAll();
  scheduleViewModel.tranTypeCb(data["tranTypeList"]);
}

function loadActionCb(data) {
  scheduleViewModel.actionCb.removeAll();
  scheduleViewModel.actionCb(data["actionList"]);
}

function loadTimeUnitCb(data) {
  scheduleViewModel.timeUnitCb.removeAll();
  scheduleViewModel.timeUnitCb(data["timeUnitList"]);
}

function loadDayCb(data) {
  scheduleViewModel.dayCb.removeAll();
  scheduleViewModel.dayCb(data["dayList"]);
}

function loadParamTypeCb(data) {
  scheduleViewModel.paramTypeCb.removeAll();
  scheduleViewModel.paramTypeCb(data["paramTypeList"]);
}

function loadSearchRuleActionCb(data) {
  scheduleViewModel.searchRuleActionCb.removeAll();
  scheduleViewModel.searchRuleActionCb(data["actionList"]);
}

function fillSfdInputOnModal(data) {
  $("#sfdModalTitle").text("Editar Filtro");
  $("#txtSfdGkey").val(data["gkey"]);
  $("#txtSfdId").val(data["id"]);
  $("#txtSfdDesc").val(data["description"]);
  $("#cbSfdTranType").val(data["tranType"]).change();
  $("#cbSdfAction").val(data["action"]).change();
  $("#txtSdfStartDate").val(data["startDateStr"]);
  $("#txtSdfEndDate").val(data["endDateStr"]);
  $("#cbSfdTimeUnit").val(data["timeUnit"]).change();
  $("#cbSfdDay").val(data["dayOfWeek"]).change();
  $("#cbSfdRecurrent").val(data["recurrent"]).change();
  setStartTime(data["startTime"]);
  setEndTime(data["endTime"]);
  setColor(data["color"]);
  $("#addEditFilterDefModal").modal();
}

function fillSfpInputOnModal(data) {
  $("#sfpModalTitle").text("Editar parámetros - Filtro");
  $("#txtSfpGkey").val(data["filterDefGkey"]);
  loadParamTypeCb(data);
  showCreatedFilterParam(data);
  $("#addEditFilterParamModal").modal();
}

function fillRuleInputOnModal(data) {
  $("#ruleModalTitle").text("Editar Regla");
  $("#txtRuleGkey").val(data["gkey"]);
  $("#txtRuleId").val(data["id"]);
  $("#txtRuleDesc").val(data["description"]);
  $("#cbRuleTranType").val(data["tranType"]).change();
  $("#cbRuleAction").val(data["action"]).change();
  $("#txtRuleDays").val(data["days"]).change();
  setRuleTime(data["time"]);
  $("#addEditRuleModal").modal();
}

function deleteFilterDefDtoAttributes(filterDefDto) {
  delete filterDefDto["startDateStr"];
  delete filterDefDto["endDateStr"];
  delete filterDefDto["createdStr"];
  delete filterDefDto["changedStr"];
  delete filterDefDto["actionEnum"];
  delete filterDefDto["tranTypeEnum"];
  delete filterDefDto["actionDesc"];
  delete filterDefDto["tranTypeDesc"];
  delete filterDefDto["recurrent"];
  return filterDefDto;
}

function deleteFilterParamDtoAttributes(filterParamDto) {
  delete filterParamDto["typeEnum"];
  return filterParamDto;
}

function deleteRuleDtoAttributes(ruleDto) {
  delete ruleDto["tranTypeEnum"];
  delete ruleDto["actionEnum"];
  delete ruleDto["actionDesc"];
  delete ruleDto["tranTypeDesc"];
  delete ruleDto["createdStr"];
  delete ruleDto["changedStr"];
  return ruleDto;
}

function validateFilterSearchFields() {
  var id = $("#txtSearchSfdId").val();
  var description = $("#txtSearchSfdDesc").val();
  var errorMsg = "";
  if (!isValidStr(id) || !isValidStr(description))
    errorMsg = "Por favor elimine los caracteres especiales de la búsqueda";
  return errorMsg;
}

function validateFilterDefFormFields() {
  var id = $("#txtSfdId").val();
  var description = $("#txtSfdDesc").val();
  var errorMsg = "";
  if (!isValidStr(id) || !isValidStr(description))
    errorMsg = "Los campos no pueden contener caracteres especiales";
  else if (id == "" || id.length < 3)
    errorMsg =
      "El campo Id es obligatorio y debe tener como mínimo 3 caracteres de longitud";
  return errorMsg;
}

function validateFilterParamFormFields() {
  var type = $("#cbSfpType option:selected").val();
  var txtSfpValue = $("#txtSfpValue").val();
  var errorMsg = "";
  if (!isValidStr(type) || !isValidStr(txtSfpValue))
    errorMsg = "Los campos no pueden contener caracteres especiales";
  return errorMsg;
}

function validateRuleSearchFields() {
  var id = $("#txtSearchRuleId").val();
  var description = $("#txtSearchRuleDesc").val();
  var errorMsg = "";
  if (!isValidStr(id) || !isValidStr(description))
    errorMsg = "Por favor elimine los caracteres especiales de la búsqueda";
  return errorMsg;
}

function validateRuleFormFields() {
  var id = $("#txtRuleId").val();
  var description = $("#txtRuleDesc").val();
  var errorMsg = "";
  if (!isValidStr(id) || !isValidStr(description))
    errorMsg = "Los campos no pueden contener caracteres especiales";
  else if (id == "" || id.length < 3)
    errorMsg =
      "El campo Id es obligatorio y debe tener como mínimo 3 caracteres de longitud";
  return errorMsg;
}

function cleanSearchFilterParams() {
  $("#txtSearchSfdId").val("");
  $("#txtSearchSfdDesc").val("");
  $("#cbSearchTranType").val("");
  $("#txtSearchFromDate").val("");
  $("#txtSearchToDate").val("");
  $("#cbSearchAction").val("");
  $("#txtSearchSfdId").focus();
  deleteDataTables();
}

function cleanAddEditSfdFormParams() {
  $("#txtSfdGkey").val("");
  $("#txtSfdId").val("");
  $("#txtSfdDesc").val("");
  $("#cbSfdTranType").val("");
  $("#cbSdfAction").val("");
  $("#txtSdfStartDate").val("");
  $("#txtSdfEndDate").val("");
  $("#cbSfdTimeUnit").val("");
  $("#cbSfdDay").val("");
  $("#cbSfdRecurrent").val("");
  setColor();
  setStartTime();
  setEndTime();
  adjustDataTableColumns();
}

function cleanAddEditRuleFormParams() {
  $("#txtRuleGkey").val("");
  $("#txtRuleId").val("");
  $("#txtRuleDesc").val("");
  $("#cbRuleTranType").val("");
  $("#cbRuleAction").val("");
  $("#txtRuleDays").val("");
  $("#txtRuleTime").val("");
  setRuleTime();
  adjustDataTableColumns();
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function deleteFilterParamDataTable() {
  $("#tbFilterParam").DataTable().clear();
  $("#tbFilterParam").DataTable().destroy();
}

function adjustDataTableColumns() {
  $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
}

function setColor(color) {
  if (typeof color != "undefined" && color != "")
    $("#txtSfdColor").spectrum("set", color);
  else $("#txtSfdColor").spectrum("set", "grey");
}

function setStartTime(startTime) {
  if (typeof startTime != "undefined" && startTime != "")
    $("#txtSfdStartTime").timepicker("setTime", startTime);
  else $("#txtSfdStartTime").timepicker("setTime", "07:00:00");
}

function setEndTime(endTime) {
  if (typeof endTime != "undefined" && endTime != "")
    $("#txtSfdEndTime").timepicker("setTime", endTime);
  else $("#txtSfdEndTime").timepicker("setTime", "23:00:00");
}

function setRuleTime(ruleTime) {
  if (typeof ruleTime != "undefined" && ruleTime != "")
    $("#txtRuleTime").timepicker("setTime", ruleTime);
  else $("#txtRuleTime").timepicker("setTime", "15:00:00");
}

function resultIsOk(result) {
  var status = result["status"];
  return status == "OK";
}

function isValidStr(str) {
  return !/[~`!#$%\^&*+=\\[\]\\';,/{}|\\":<>\?]/g.test(str);
}
