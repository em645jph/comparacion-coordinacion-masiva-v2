/**
 * Author: Jimena Chavez
 *
 */

$(document).ready(function () {
  initHtmlElements();
  initKnockout();
  getInvoiceToday();
});

function initKnockout() {
  supportInvoiceViewModel = {
    invoiceTable: ko.observableArray([]),
    paymentTable: ko.observableArray([]),
    epaymentTable: ko.observableArray([]),
    viewInvoicePayment: function (invoiceKo) {
      viewInvoicePaymentIntern(invoiceKo);
    },
    viewInvoicePayee: function (invoiceKo) {
      viewInvoicePayeeIntern(invoiceKo);
    },
    downloadInvoice: function (invoiceKo) {
      downloadInvoiceIntern(invoiceKo);
    },
    deleteInvoice: function (invoiceKo) {
      deleteInvoiceIntern(invoiceKo);
    },
  };
  ko.applyBindings(supportInvoiceViewModel);
}

function initHtmlElements() {
  initInputs();
  initButtons();
  initDateTimePickers();
}

function initInputs() {
  $("#txtSearchDraftNbr").focus();
}

function initButtons() {
  $("#btnSearch").click(function () {
    findInvoice(true);
  });

  $("#btnClean").click(function () {
    cleanSearchParams();
  });
}

function initDateTimePickers() {
  $("#txtSearchFromDate").datepicker({ dateFormat: "yy-mm-dd" });
  $("#txtSearchToDate").datepicker({ dateFormat: "yy-mm-dd" });
}

function initModals() {
  $("#viewInvoicePayeeModal").on("hidden.bs.modal", function () {
    clearPayeeModal();
  });

  $("#viewInvoicePaymentModal").on("shown.bs.modal", function () {
    showPaymentTable();
    showEpaymentTable();
    adjustDataTableColumns();
  });

  $("#viewInvoicePaymentModal").on("hidden.bs.modal", function () {
    deletePaymentDataTable();
    deleteEpaymentDataTable();
    adjustDataTableColumns();
  });
}

function findInvoice(showProcessing) {
  var errorMsg = validateSearchFields();
  if (errorMsg == "") {
    if (showProcessing) showProcessingModal();
    var searchParam = {
      prDraftNbrStr: $("#txtSearchDraftNbr").val(),
      prStatus: $("#txtSearchStatus").val(),
      prCostCenterStr: $("#txtSearchCostCenter").val(),
      prFinalNbrStr: $("#txtSearchFinalNbr").val(),
      prInvoiceType: $("#txtSearchInvoiceType").val(),
      prCustomerIdTax: $("#txtSearchCustomer").val(),
      prEntityId: $("#txtSearchEntityId").val(),
      prEventId: $("#txtSearchEventId").val(),
      prTariffId: $("#txtSearchTariffId").val(),
      prCreator: $("#txtSearchCreator").val(),
      prFromDateStr: $("#txtSearchFromDate").val(),
      prToDateStr: $("#txtSearchToDate").val(),
    };
    $.ajax({
      type: "GET",
      url: url_application + "/SupportInvoice/findInvoice/",
      data: searchParam,
      success: function (data, statusCode) {
        var msg = data["msg"];
        if (typeof msg != "undefined" && !resultIsOk(data)) {
          showErrorHtmlMsg(msg);
        } else {
          showInvoiceTable(data);
          if (showProcessing) closeProcessingModal();
        }
      },
      fail: function (data, statusCode) {
        s;
        showErrorMsg("Status code:" + statusCode);
      },
    });
  } else showErrorMsg(errorMsg);
}

function getInvoiceToday() {
  setDefaultDates();
  findInvoice(true);
}

function viewInvoicePaymentIntern(invoiceKo) {
  showProcessingModal();
  var searchParam = {
    prInvoiceGkeyStr: invoiceKo["gkey"],
    prDraftNbrStr: invoiceKo["draft_nbr"],
  };
  $.ajax({
    type: "GET",
    url: url_application + "/SupportInvoice/getInvoicePayments/",
    data: searchParam,
    success: function (data, statusCode) {
      var msg = data["msg"];
      if (typeof msg != "undefined" && !resultIsOk(data)) {
        showErrorHtmlMsg(msg);
      } else {
        closeProcessingModal();
        fillPaymentTable(data["paymentList"]);
        fillEpaymentTable(data["ePaymentList"]);
        $("#viewInvoicePaymentModal").modal();
      }
    },
    fail: function (data, statusCode) {
      s;
      showErrorMsg("Status code:" + statusCode);
    },
  });
}

function viewInvoicePayeeIntern(invoiceKo) {
  showProcessingModal();
  var searchParam = {
    prCustomerGkeyStr: invoiceKo["customer_gkey"],
  };
  $.ajax({
    type: "GET",
    url: url_application + "/SupportInvoice/getCustomerInfo/",
    data: searchParam,
    success: function (data, statusCode) {
      var msg = data["msg"];
      if (typeof msg != "undefined" && !resultIsOk(data)) {
        showErrorHtmlMsg(msg);
      } else {
        fillPayeeModal(data);
        $("#viewInvoicePayeeModal").modal();
        closeProcessingModal();
      }
    },
    fail: function (data, statusCode) {
      s;
      showErrorMsg("Status code:" + statusCode);
    },
  });
}

function downloadInvoiceIntern(invoiceKo) {
  showProcessingModal();
  var request = $.ajax({
    type: "POST",
    enctype: "multipart/form-data",
    url: url_application + "/Invoice/getPdfByDraftId/" + invoiceKo["draft_nbr"],
    timeout: 600000,
  });

  request.done(function (data) {
    if (data == null || data == "") {
      Swal.fire("", "Sin resultados", "info");
      return;
    } else {
      closeProcessingModal();
      data = data.replace(
        "http://10.54.1.93:11080",
        "https://apps.apmterminals.com.ar",
      );
      var win = window.open(data, "_blank");
      if (win) {
        //Browser has allowed it to be opened
        win.focus();
      } else {
        //Browser has blocked it
        showErrorMsg("No se pudo abrir el PDF", "error");
      }
    }
    Swal.close();
  });

  request.fail(function (jqXHR, textStatus) {
    showErrorMsg(jqXHR.responseText);
  });
}

function deleteInvoiceIntern(invoiceKo) {
  showProcessingModal();
  var request = $.ajax({
    type: "POST",
    url:
      url_application + "/Invoice/deleteInvoiceByNbr/" + invoiceKo["draft_nbr"],
  });

  request.done(function (data) {
    if (data == "SUCCESS") {
      showSuccessMsg("Listo!", "Draft eliminado");
      findInvoice(false);
    } else {
      showErrorMsg(data);
    }
  });

  request.fail(function (jqXHR, textStatus) {
    showErrorMsg(jqXHR.responseText);
  });
}

function fillPayeeModal(data) {
  $("#txtPayeeName").val(data["name"]);
  $("#txtPayeeTaxId").val(data["tax_id"]);
  $("#txtPayeeCreditStatus").val(data["credit_status"]);
  $("#txtPayeeTaxGroup").val(data["tax_group_id"]);
  $("#txtPayeeEmail").val(data["email_address"]);
  var balance = data["balance"];
  if (balance != null) {
    $("#txtPayeeBalance").val("$" + data["balance"]);
    if (balance < 0) $("#txtPayeeBalance").css("color", "#dc3545");
    else $("#txtPayeeBalance").css("color", "##4fc55b");
  } else $("#txtPayeeBalance").val("");
}

function clearPayeeModal(data) {
  $("#txtPayeeName").val("");
  $("#txtPayeeTaxId").val("");
  $("#txtPayeeCreditStatus").val("");
  $("#txtPayeeTaxGroup").val("");
  $("#txtPayeeEmail").val("");
  $("#txtPayeeBalance").val("");
}

function showInvoiceTable(data) {
  supportInvoiceViewModel.invoiceTable.removeAll();
  deleteInvoiceDataTable();
  supportInvoiceViewModel.invoiceTable(data);
  var table = $("#tbInvoice").DataTable({
    bDestroy: true,
    bFilter: true,
    sScrollX: true,
    sScrollY: "60vh",
    scrollCollapse: true,
    paging: true,
    bPaginate: true,
    bSort: true,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
      sSearch: "Buscar",
    },
  });
}

function fillPaymentTable(data) {
  supportInvoiceViewModel.paymentTable.removeAll();
  deletePaymentDataTable();
  supportInvoiceViewModel.paymentTable(data);
}

function showPaymentTable() {
  var table = $("#tbInvoicePayment").DataTable({
    bDestroy: true,
    bFilter: false,
    sScrollX: false,
    sScrollY: "10vh",
    scrollCollapse: true,
    paging: false,
    bPaginate: false,
    bSort: false,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
    },
  });
}

function fillEpaymentTable(data) {
  supportInvoiceViewModel.epaymentTable.removeAll();
  deleteEpaymentDataTable();
  supportInvoiceViewModel.epaymentTable(data);
}

function showEpaymentTable() {
  var table = $("#tbInvoiceEpayment").DataTable({
    bDestroy: true,
    bFilter: false,
    sScrollX: false,
    sScrollY: "10vh",
    scrollCollapse: true,
    paging: false,
    bPaginate: false,
    bSort: false,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
    },
  });
}

function refreshSearchData() {
  findInvoice(false);
}

function setDefaultDates() {
  $("#txtSearchFromDate").datepicker("setDate", "+0");
  $("#txtSearchToDate").datepicker("setDate", "+1");
}

function validateSearchFields() {
  var error = "";
  var draftNbr = $("#txtSearchDraftNbr").val();
  var status = $("#txtSearchStatus").val();
  var costCenter = $("#txtSearchCostCenter").val();
  var finalNbr = $("#txtSearchFinalNbr").val();
  var invoiceType = $("#txtSearchInvoiceType").val();
  var customerIdTax = $("#txtCustomerIdTax").val();
  var entityId = $("#txtSearchEntityId").val();
  var eventId = $("#txtSearchEventId").val();
  var creator = $("#txtSearchCreator").val();
  var tariffId = $("#txtSearchTariffId").val();
  if (
    !isValidStr(draftNbr) ||
    !isValidStr(status) ||
    !isValidStr(costCenter) ||
    !isValidStr(finalNbr) ||
    !isValidStr(invoiceType) ||
    !isValidStr(customerIdTax) ||
    !isValidStr(entityId) ||
    !isValidStr(eventId) ||
    !isValidStr(tariffId) ||
    !isValidStr(creator)
  )
    error = "Eliminá los caracteres espciales de la búsqueda";
  return error;
}

function cleanSearchParams() {
  $("#txtSearchDraftNbr").val("");
  $("#txtSearchStatus").val("");
  $("#txtSearchCostCenter").val("");
  $("#txtSearchFinalNbr").val("");
  $("#txtSearchInvoiceType").val("");
  $("#txtCustomerIdTax").val("");
  $("#txtSearchEntityId").val("");
  $("#txtSearchEventId").val("");
  $("#txtSearchTariffId").val("");
  $("#txtSearchCreator").val("");
  setDefaultDates();
  $("#txtSearchDraftNbr").focus();
  deleteDataTables();
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function deleteInvoiceDataTable() {
  $("#tbInvoice").DataTable().clear();
  $("#tbInvoice").DataTable().destroy();
}

function deletePaymentDataTable() {
  $("#tbInvoicePayment").DataTable().clear();
  $("#tbInvoicePayment").DataTable().destroy();
}

function deleteEpaymentDataTable() {
  $("#tbInvoiceEpayment").DataTable().clear();
  $("#tbInvoiceEpayment").DataTable().destroy();
}

function resultIsOk(result) {
  var status = result["status"];
  return status == "OK";
}

function resultIsError(result) {
  var status = result["status"];
  return status == "ERROR";
}

function resultRequiresConfirm(result) {
  var status = result["status"];
  return status == "QUESTION";
}

function resultIsWarning(result) {
  var status = result["status"];
  return status == "WARNING";
}

function adjustDataTableColumns() {
  $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
}

function isValidStr(str) {
  return !/[~`!#$%\^&*+=\\[\]\\';/{}|\\":<>\?]/g.test(str);
}
