/**
 * Author: Jimena Chavez
 *
 */

$(document).ready(function () {
  initHtmlElements();
  initKnockout();
});

function initKnockout() {
  draftViewModel = {
    lineOpCb: ko.observableArray([]),
    pumApptTable: ko.observableArray([]),
  };
  ko.applyBindings(draftViewModel);
}

function initHtmlElements() {
  initInputs();
  initButtons();
  initCheckBoxs();
  loadInitCombos();
}

function initInputs() {
  $("#txtSearchApptNbr").on("keypress keyup blur", function (event) {
    if (event.which != 44 && (event.which < 48 || event.which > 57)) {
      event.preventDefault();
    }
  });

  $("#txtSearchUnitId").focus();
}

function initButtons() {
  $("#btnSearch").click(function () {
    findPumAppointment(true);
  });

  $("#btnGenerateDraft").click(function () {
    generateDraft();
  });

  $("#btnClean").click(function () {
    cleanSearchParams();
  });
}

function initCheckBoxs() {
  $("#ckCheckAll").change(function () {
    var check = $(this).is(":checked");
    var canBeSelected = $(".selectable:enabled");
    canBeSelected.each(function () {
      if (check) $(this).prop("checked", true);
      else $(this).prop("checked", false);
    });
  });
}

function loadInitCombos() {
  showProcessingModal();
  $.ajax({
    type: "GET",
    url: url_application + "/Billing/loadInitCombos/",
    success: function (data, statusCode) {
      closeProcessingModal();
      loadSearchLineOpCb(data);
    },
    fail: function (data, statusCode) {
      closeProcessingModal();
      showModal("Error", "Status code:" + statusCode, "error");
    },
  });
}

function findPumAppointment(showProcessing) {
  $("#ckCheckAll").prop("checked", false);
  var errorMsg = validateSearchFields();
  if (errorMsg == "") {
    if (showProcessing) showProcessingModal();
    deletePumApptDataTable();
    var searchParam = {
      prOrderNbr: $("#txtSearchOrderNbr").val(),
      prLineOpId: $("#cbSearchLineOp").val(),
      prApptNbrStr: $("#txtSearchApptNbr").val(),
    };
    $.ajax({
      type: "GET",
      url: url_application + "/Billing/findAppointmentList/",
      data: searchParam,
      success: function (data, statusCode) {
        var msg = data["msg"];
        if (typeof msg != "undefined" && !resultIsOk(data)) {
          showErrorMsg(msg);
        } else {
          showpumApptTable(data);
          if (showProcessing) closeProcessingModal();
          if (typeof notFoundManifest != "undefined") {
            showWarningHtmlMsg(notFoundManifest);
          }
        }
        adjustDataTableColumns();
      },
      fail: function (data, statusCode) {
        s;
        showErrorMsg("Status code:" + statusCode);
      },
    });
  } else showErrorMsg(errorMsg);
}

function generateDraft() {
  var draftParam = buildDraftParam();
  console.log("draftParam:" + JSON.stringify(draftParam));
  if (draftParam != null && !jQuery.isEmptyObject(draftParam)) {
    showProcessingModal();
    $.ajax({
      type: "POST",
      contentType: "application/json",
      url: url_application + "/Invoice/generateDeliverEmptyDraft/",
      data: JSON.stringify(draftParam),
      success: function (data) {
        closeProcessingModal();
        var msg = data["msg"];
        if (typeof msg != "undefined" && !resultIsOk(data)) {
          showErrorMsg(msg);
        } else if (data.status == "OK") {
          var url = url_application + "/Payment/paymentSelector/" + data.msg;
          showModalHtml(
            "",
            "<div id='divPdf'><div style='display: none;' id='divSpinner' class='spinner-border text-primary m-2' role='status'><span class='sr-only'>Loading...</span>" +
              "</div><button type='button' onclick='toggleSpinner(this);' id='btnDownload' invNbr='" +
              data.msg +
              "' class='btn btn-info'>DESCARGAR PDF - " +
              data.msg +
              "</button></div><hr>",
            "success",
            url,
          );
        } else {
          Swal.fire("", data, "error");
        }
      },
      fail: function (data) {
        closeProcessingModal();
        Swal.close();
        showModal("Request failed", jqXHR.responseText, "error");
      },
    });
  }
}

function showpumApptTable(data) {
  draftViewModel.pumApptTable.removeAll();
  deletePumApptDataTable();
  draftViewModel.pumApptTable(data);
  var table = $("#tbPumAppointment").DataTable({
    bDestroy: true,
    bFilter: true,
    sScrollX: true,
    sScrollY: "30vh",
    scrollCollapse: true,
    paging: true,
    bPaginate: true,
    bSort: true,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
    },
  });
}

function refreshSearchData() {
  findPumAppointment(false);
}

function loadSearchLineOpCb(data) {
  draftViewModel.lineOpCb(data["lineOpList"]);
}

function validateSearchFields() {
  var searchOrderNbr = $("#txtSearchOrderNbr").val();
  var searchApptNbr = $("#txtSearchApptNbr").val();
  var errorMsg = "";
  if (
    (searchOrderNbr != null && !isValidStr(searchOrderNbr)) ||
    (searchApptNbr != null && !isValidStr(searchApptNbr))
  )
    errorMsg = "Por favor eliminá los caracteres especiales de la búsqueda";
  return errorMsg;
}

function cleanSearchParams() {
  $("#txtSearchOrderNbr").val("");
  $("#txtSearchApptNbr").val("");
  $("#cbSearchLineOp").val("");
  deleteDataTables();
  $("#txtSearchOrderNbr").focus();
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function deletePumApptDataTable() {
  $("#tbPumAppointment").DataTable().clear();
  $("#tbPumAppointment").DataTable().destroy();
}

function resultIsOk(result) {
  var status = result["status"];
  return status == "OK";
}

function resultIsError(result) {
  var status = result["status"];
  return status == "ERROR";
}

function resultRequiresConfirm(result) {
  var status = result["status"];
  return status == "QUESTION";
}

function resultIsWarning(result) {
  var status = result["status"];
  return status == "WARNING";
}

function adjustDataTableColumns() {
  $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
}

function buildDraftParam() {
  var draftParam = {};
  var selectedApptNbrArray = getSelectedApptNbrArray();
  if (selectedApptNbrArray != null && selectedApptNbrArray.length > 0) {
    draftParam["apptNbr"] = selectedApptNbrArray;
  }
  var orderNbr = getOrderNbr();
  if (orderNbr != null && orderNbr != "") {
    draftParam["orderNbr"] = orderNbr;
  }
  return draftParam;
}

function getSelectedApptNbrArray() {
  var selectedAppts = $(".selectable:checked");
  if (selectedAppts.length == 0) {
    showWarningMsg("Debe seleccionar al menos un registro");
    return "";
  } else return getApptNbrArray(selectedAppts);
}

function getApptNbrArray(selectedAppts) {
  var nbrArray = [];
  if (selectedAppts != null) {
    selectedAppts.each(function () {
      var selectedAppt = $(this);
      var apptNbr = selectedAppt.attr("nbr");
      if (apptNbr != null && apptNbr != "") nbrArray.push(apptNbr);
    });
  }
  return nbrArray;
}

function getOrderNbr() {
  var selectedAppts = $(".selectable:checked");
  var orderNbr = "";
  if (selectedAppts.length > 0) {
    selectedAppts.each(function () {
      var selectedAppt = $(this);
      orderNbr = selectedAppt.attr("orderNbr");
      if (orderNbr != null && orderNbr != "") return orderNbr;
    });
  }
  return orderNbr;
}

function adjustDataTableColumns() {
  $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
}

function isValidStr(str) {
  return !/[~`!#$%\^&*+=\\[\]\\';/{}|\\":<>\?]/g.test(str);
}

function toggleSpinner(input) {
  //Reemplazamos el boton de descarga por el spinner
  if ($("#btnDownload").is(":visible")) {
    $("#btnDownload").hide();
    $("#divSpinner").show();
    getPdfByDraftId(input.getAttribute("invNbr"));
  } else {
    $("#btnDownload").show();
    $("#divSpinner").hide();
  }
}

function getPdfByDraftId(draft) {
  //processingModal();

  if (draft == null || draft == "" || draft == undefined) {
    Swal.fire("", "Error al obtener documento. Reintente nuevamente.", "error");
    return;
  }

  var request = $.ajax({
    type: "POST",
    enctype: "multipart/form-data",
    url: url_application + "/Invoice/getPdfByDraftId/" + draft,
    timeout: 600000,
  });

  request.done(function (data) {
    $("#btnDownload").show();
    $("#divSpinner").hide();
    if (data == null || data == "") {
      Swal.fire("", "Sin resultados", "info");
      return;
    } else {
      data = data.replace(
        "http://10.54.1.93:11080",
        "https://apps.apmterminals.com.ar",
      );
      var win = window.open(data, "_blank");
      if (win) {
        //Browser has allowed it to be opened
        win.focus();
      } else {
        //Browser has blocked it
        showModal("", "No se pudo abrir el PDF", "error");
      }
    }
    //Swal.close();
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", jqXHR.responseText, "error");
  });
}

function showModalHtml(title, text, type, callbackUrl) {
  Swal.fire({
    allowOutsideClick: false,
    title: title,
    html: text,
    type: type,
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "S&iacute;, pagar!",
    cancelButtonText: "No, gracias.",
  }).then((result) => {
    if (result.dismiss != "cancel") {
      window.location.href = callbackUrl;
    } else {
      window.location.reload();
    }
  });
}
