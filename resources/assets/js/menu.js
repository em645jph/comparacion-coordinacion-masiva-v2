/**
 * Author: Lucas Juarez
 * APM Terminals Buenos Aires
 */
var oldElement = null;
$(document).ready(function () {
  $("#btnLogout").click(function () {
    //showConfirmModal(title, text, type, showCancelButton, titleCallback, textCallback )
    showConfirmModal(
      "¿Esta seguro de salir?",
      "Será desconectado del sistema",
      "warning",
      true,
      url_logout,
    );
  });
  loadMenuByRoles();
});

function loadMenuByRoles() {
  $.ajax({
    type: "GET",
    url: url_application + "/Menu/loadMenuByLogin/",
    success: function (htmlMenu) {
      $("#menu-home").after(htmlMenu);
    },
  });
}

function activeMenuResponsive() {
  var x = document.getElementById("navigation");
  var lines = document.getElementById("lines");
  if (
    x.style.display === "none" ||
    x.style.display == null ||
    x.style.display == undefined ||
    x.style.display == ""
  ) {
    x.style.display = "block";
    $("#lines").addClass("open");
    $("#lines").addClass("active");
  } else {
    x.style.display = "none";
    $("#lines").removeClass("open");
    $("#lines").removeClass("active");
  }
}

function findElementById(element) {
  $("#" + element + "> ul").toggleClass("open");
}
