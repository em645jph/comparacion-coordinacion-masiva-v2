/**
 * Author: Jimena Chavez
 *
 */

$(document).ready(function () {
  initHtmlElements();
  initKnockout();
});

function initKnockout() {
  userViewModel = {
    privilege: ko.observableArray([]),
    roleList: ko.observableArray([]),
    createdUser: ko.observableArray([]),
    rolePrivilege: ko.observableArray([]),
    userRoleList: ko.observableArray([]),
    editUser: function (userToEdit) {
      //editRoleIntern(userToEdit);
    },
    deleteUser: function (userToDelete) {
      //deleteRoleIntern(userToDelete);
    },
    recoverUser: function (userTorecover) {
      //recoverRoleIntern(userTorecover);
    },
  };
  ko.applyBindings(userViewModel);
}

function initHtmlElements() {
  initInputs();
  initButtons();
  initDateTimePickers();
  initCombos();
  initModal();
  initCheckBoxs();
}

function initInputs() {
  $("#txtSearchUserDocumentNbr").focus();
}

function initButtons() {
  $("#btnSearch").click(function () {
    searchUser(true);
  });

  $("#btnUpdateUserCredential").click(function () {
    $("#updateUserCredentialModal").modal();
  });

  $("#btnUserRole").click(function () {
    alert("COMING SOON...");
    /*loadModalPrivilege();
		$("#modalTitle").text("Agregar Rol");
		$("#addEditRoleModal").modal();
		*/
  });

  $("#btnClean").click(function () {
    cleanSearchParams();
  });

  $("#btnSaveRole").click(function () {
    addOrUpdateRole();
  });

  $("#btnSaveRole").click(function () {
    addOrUpdateRole();
  });

  $("#btnDeleteSelectedRole").click(function () {
    deleteSelectedRole();
  });

  $("#btnRecoverSelectedRole").click(function () {
    recoverSelectedRole();
  });

  $("#btnSaveUserCredential").click(function () {
    updateSelectedUserCredentials();
  });
}

function initDateTimePickers() {
  $("#txtSearchFromDate").datepicker({ dateFormat: "yy-mm-dd" });
  $("#txtSearchToDate").datepicker({ dateFormat: "yy-mm-dd" });
}

function initCombos() {
  loadPrivilegeCb();
}

function initModal() {
  /*$("#addEditRoleModal").on("shown.bs.modal", function () {
		adjustDataTableColumns();
		$("#txtRoleId").focus();
	});
	
	$("#addEditRoleModal").on("hidden.bs.modal", function () {
		cleanAddEditFormParams();
		adjustDataTableColumns();
	});
	*/
  $("#updateUserCredentialModal").on("hidden.bs.modal", function () {
    cleanUpdateCredentialParams();
    adjustDataTableColumns();
  });
}

function initCheckBoxs() {
  $("#ckCheckAll").change(function () {
    var check = $(this).is(":checked");
    var canBeSelected = $(".selectable:enabled");
    canBeSelected.each(function () {
      if (check) $(this).prop("checked", true);
      else $(this).prop("checked", false);
    });
  });

  $("#ckCheckAllUser").change(function () {
    var check = $(this).is(":checked");
    var canBeSelected = $(".selectableUser:enabled");
    canBeSelected.each(function () {
      if (check) $(this).prop("checked", true);
      else $(this).prop("checked", false);
    });
  });

  $("#ckUpdateEmail").change(function () {
    var check = $(this).is(":checked");
    if (check) {
      $("#txtCredentialEmail").prop("disabled", false);
      $("#txtCredentialEmail").focus();
    } else {
      $("#txtCredentialEmail").prop("disabled", true);
      $("#txtCredentialEmail").val("");
    }
  });

  $("#ckUpdateComexEmail").change(function () {
    var check = $(this).is(":checked");
    if (check) {
      $("#txtComexEmail").prop("disabled", false);
      $("#txtComexEmail").focus();
    } else {
      $("#txtComexEmail").prop("disabled", true);
      $("#txtComexEmail").val("");
    }
  });

  $("#ckUpdateManagementEmail").change(function () {
    var check = $(this).is(":checked");
    if (check) {
      $("#txtManagementEmail").prop("disabled", false);
      $("#txtManagementEmail").focus();
    } else {
      $("#txtManagementEmail").prop("disabled", true);
      $("#txtManagementEmail").val("");
    }
  });

  $("#ckUpdatePassword").change(function () {
    var check = $(this).is(":checked");
    if (check) {
      $("#txtCredentialPassword").prop("disabled", false);
      $("#txtCredentialPassword").focus();
    } else {
      $("#txtCredentialPassword").prop("disabled", true);
      $("#txtCredentialPassword").val("");
    }
  });
}

function searchUser(showProcessing) {
  $("#ckCheckAll").prop("checked", false);
  var errorMsg = validateSearchFields();
  if (errorMsg == "") {
    var searchParam = {
      prDocumentNbrStr: $("#txtSearchUserDocumentNbr").val(),
      prName: $("#txtSearchUserName").val(),
      prRoleGkeyStr: $("#cbSearchRole option:selected").val(),
      prFromDateStr: $("#txtSearchFromDate").val(),
      prToDateStr: $("#txtSearchToDate").val(),
      prLifeCycleStateKey: $("#cbSearchUserStatus option:selected").val(),
    };
    if (showProcessing) showProcessingModal();
    $.ajax({
      type: "GET",
      url: url_application + "/UserManagement/findUserByParam/",
      data: searchParam,
      success: function (data, statusCode) {
        var msg = data["msg"];
        if (showProcessing) closeProcessingModal();
        if (typeof msg != "undefined" && msg != "" && !resultIsOk(data)) {
          showErrorMsg(msg);
        } else {
          showCreatedUser(data);
        }
      },
      fail: function (data, statusCode) {
        if (showProcessing) closeProcessingModal();
        showErrorMsg("Status code:" + statusCode);
      },
    });
  } else {
    if (showProcessing) closeProcessingModal();
    showErrorMsg(errorMsg);
  }
}

function addOrUpdateRole() {
  var errorMsg = validateFormFields();
  if (errorMsg == "") {
    var updateParam = {
      prGkey: $("#txtRoleGkey").val(),
      prId: $("#txtRoleId").val(),
      prDescription: $("#txtRoleDesc").val(),
      prPrivilegeGkeyStr: getSelectedPrivilegeStr(),
    };
    $.ajax({
      type: "POST",
      url: url_application + "/RoleManagement/addOrUpdateRole/",
      data: updateParam,
      success: function (result, statusCode) {
        var msg = result["msg"];
        if (resultIsOk(result)) {
          showSuccessMsg("Listo!", msg);
          $("#btnCancelSaveRole").click();
          refreshSearchData();
        } else showErrorMsg(msg);
      },
      fail: function (result, statusCode) {
        showErrorMsg(
          statusCode +
            "Se produjo un error guardando el menú. Por favor intente nuevamente",
        );
      },
    });
  } else {
    showErrorMsg(errorMsg);
    $("#txtRoleId").focus();
  }
}

function editRoleIntern(roleToEdit) {
  var param = {
    prRoleGkey: roleToEdit.gkey,
  };
  $.ajax({
    type: "GET",
    contentType: "application/json",
    url: url_application + "/RoleManagement/getRole/",
    data: param,
    success: function (data, statusCode) {
      fillRoleInputOnModal(data);
    },
    fail: function (data, statusCode) {
      showErrorMsg(data);
      $("#btnCancelSaveRole").click();
    },
  });
}

function deleteRoleIntern(roleToDelete) {
  roleToDelete = deleteDtoAttributes(roleToDelete);
  console.log("roleToDelete:" + JSON.stringify(roleToDelete));
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/RoleManagement/deleteRole/",
    data: JSON.stringify(roleToDelete),
    success: function (result, statusCode) {
      var msg = result["msg"];
      if (resultIsOk(result)) {
        showSuccessMsg("Listo!", msg);
        refreshSearchData();
      } else showErrorMsg(msg);
    },
    fail: function (result, statusCode) {
      showErrorMsg(
        statusCode +
          " Se produjo un error eliminando el rol. Por favor intente nuevamente",
      );
    },
  });
}

function recoverRoleIntern(roleTorecover) {
  roleTorecover = deleteDtoAttributes(roleTorecover);
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/RoleManagement/recoverRole/",
    data: JSON.stringify(roleTorecover),
    success: function (result, statusCode) {
      var msg = result["msg"];
      if (resultIsOk(result)) {
        showSuccessMsg("Listo!", msg);
        refreshSearchData();
      } else showErrorMsg(msg);
    },
    fail: function (result, statusCode) {
      showErrorMsg(
        statusCode +
          " Se produjo un error activando el rol. Por favor intente nuevamente",
      );
    },
  });
}

function deleteSelectedRole() {
  var selectedRoleJson = getSelectedUserJson();
  if (selectedRoleJson != null && selectedRoleJson != "") {
    $.ajax({
      type: "POST",
      contentType: "application/json",
      url: url_application + "/RoleManagement/deleteSelectedRole/",
      data: selectedRoleJson,
      success: function (result, statusCode) {
        var msg = result["msg"];
        if (resultIsOk(result)) {
          showSuccessMsg("Listo!", msg);
          refreshSearchData();
        } else showErrorMsg(msg);
      },
      fail: function (result, statusCode) {
        showErrorMsg(
          statusCode +
            " Se produjo un error eliminando los roles. Por favor intente nuevamente",
        );
      },
    });
  }
}

function loadPrivilegeCb() {
  $.ajax({
    type: "GET",
    url: url_application + "/RoleManagement/getPrivilegeList/",
    success: function (data, statusCode) {
      loadSearchPrivilegeCb(data);
    },
    fail: function (data, statusCode) {
      showModal("Error", "Status code:" + statusCode, "error");
    },
  });
}

function recoverSelectedRole() {
  var selectedRoleJson = getSelectedUserJson();
  if (selectedRoleJson != null && selectedRoleJson != "") {
    $.ajax({
      type: "POST",
      contentType: "application/json",
      url: url_application + "/RoleManagement/recoverSelectedRole/",
      data: selectedRoleJson,
      success: function (result, statusCode) {
        var msg = result["msg"];
        if (resultIsOk(result)) {
          showSuccessMsg("Listo!", msg);
          refreshSearchData();
        } else showErrorMsg(msg);
      },
      fail: function (result, statusCode) {
        showErrorMsg(
          statusCode +
            " Se produjo un error activando los roles. Por favor intente nuevamente",
        );
      },
    });
  }
}

function updateSelectedUserCredentials() {
  var selectedUsers = $(".selectable:checked");
  var selectedUserArray = getUserToDeleteOrRecover(selectedUsers);
  if (selectedUserArray.length == 0) {
    showErrorMsg("Debe seleccionar al menos un registro");
  } else if (selectedUserArray != null && selectedUserArray != "") {
    var param = {};
    param["gkeyArray"] = selectedUserArray;
    param["updateCredentialEmail"] = $("#ckUpdateEmail").prop("checked");
    param["updateComexEmail"] = $("#ckUpdateComexEmail").prop("checked");
    param["updateManagementEmail"] = $("#ckUpdateManagementEmail").prop(
      "checked",
    );
    param["updateCredentialPassword"] = $("#ckUpdatePassword").prop("checked");
    param["requiresEmailChange"] = $("#ckChangePassword").prop("checked");
    param["emailAddress"] = $("#txtCredentialEmail").val();
    param["comexEmail"] = $("#txtComexEmail").val();
    param["managementEmail"] = $("#txtManagementEmail").val();
    param["password"] = $("#txtCredentialPassword").val();
    showProcessingModal();
    $.ajax({
      type: "POST",
      contentType: "application/json",
      url: url_application + "/UserManagement/updateSelectedUserCredential/",
      data: JSON.stringify(param),
      success: function (result, statusCode) {
        var msg = result["msg"];
        if (resultIsOk(result)) {
          showSuccessMsg("Listo!", msg);
          $("#btnCancelUserCredential").click();
          refreshSearchData();
        } else {
          showErrorMsg(msg);
        }
      },
      fail: function (result, statusCode) {
        closeProcessingModal();
        showErrorMsg(
          statusCode +
            " Se produjo un error actualizando las credenciales. Por favor intente nuevamente",
        );
      },
    });
  }
}

function loadModalPrivilege() {
  $.ajax({
    type: "GET",
    url: url_application + "/RoleManagement/getPrivilegeDtoList/",
    success: function (data, statusCode) {
      showRolePrivielege(data);
    },
    fail: function (data, statusCode) {
      showModal("Error", "Status code:" + statusCode, "error");
    },
  });
}

function showCreatedUser(data) {
  userViewModel.createdUser.removeAll();
  deleteDataTables();
  userViewModel.createdUser(data);
  $("#tbCreatedUser").DataTable({
    bDestroy: true,
    bFilter: true,
    bPaginate: true,
    sScrollX: true,
    sScrollY: "30vh",
    scrollCollapse: true,
    paging: true,
    bSort: true,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
    },
    oPaginate: {
      sFirst: "Primera",
      sLast: "Última",
      sNext: "Siguiente",
      sPrevious: "Anterior",
    },
  });
}

/*
function showRolePrivielege(data){
	userViewModel.rolePrivilege.removeAll();
	userViewModel.rolePrivilege(data);
	$("#tbRolePrivilege").DataTable({
		"bDestroy": true,
		"bFilter" : false,
		"sScrollX": false,	
		"sScrollY": "30vh",	
		"scrollCollapse": true,
		"paging" : false,
		"bPaginate": false,
		"bFilter": false,
		"bSort": false,
		"oLanguage": {
            "sEmptyTable": "No hay registros a mostrar",
            "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros"          
        }
	});
}
*/
function refreshSearchData() {
  $("#btnSearch").click();
}

function loadSearchPrivilegeCb(data) {
  userViewModel.privilege(data);
}

function getRoleDataFromModal() {
  var roleData = {};
  roleData["id"] = $("#txtRoleId").val();
  roleData["description"] = $("#txtRoleDesc").val();
  var roleGkey = $("#txtRoleGkey").val();
  if (roleGkey != null && roleGkey != "") roleData["gkey"] = roleGkey;
  return roleData;
}

function isEditingRole() {
  var roleGkey = $("#txtRoleGkey").val();
  return roleGkey != null && roleGkey != "";
}

function fillRoleInputOnModal(data) {
  $("#modalTitle").text("Editar Role");
  $("#txtRoleGkey").val(data["gkey"]);
  $("#txtRoleId").val(data["id"]);
  $("#txtRoleDesc").val(data["description"]);
  var privilegeList = data["privilegeList"];
  showRolePrivielege(privilegeList);
  $("#addEditRoleModal").modal();
}

function deleteDtoAttributes(roleDto) {
  delete roleDto["createdStr"];
  delete roleDto["changedStr"];
  delete roleDto["isActive"];
  delete roleDto["privilegeList"];
  return roleDto;
}

function validateSearchFields() {
  var id = $("#txtSearchRoleId").val();
  var description = $("#txtSearchRoleDesc").val();
  var errorMsg = "";
  if (!isValidStr(id) || !isValidStr(description))
    errorMsg = "Por favor elimine los caracteres especiales de la búsqueda";
  return errorMsg;
}

function validateFormFields() {
  var id = $("#txtRoleId").val();
  var description = $("#txtRoleDesc").val();
  var errorMsg = "";
  if (!isValidStr(id) || !isValidStr(description))
    errorMsg = "Los campos no pueden contener caracteres especiales";
  else if (id == "" || id.length < 3)
    errorMsg =
      "El campo Nombre es obligatorio y debe tener como mínimo 3 caracteres de longitud";
  return errorMsg;
}

function getSelectedUserJson() {
  var selectedUsers = $(".selectable:checked");
  if (selectedUsers.length == 0) {
    showWarningMsg("Alerta", "Debe seleccionar al menos un registro");
    return "";
  } else return JSON.stringify(getUserToDeleteOrRecover(selectedUsers));
}

function getUserToDeleteOrRecover(selectedUsers) {
  var gkeyArray = [];
  if (selectedUsers != null) {
    selectedUsers.each(function () {
      var selectedUser = $(this);
      var gkey = selectedUser.attr("gkey");
      if (gkey != null && gkey != "") gkeyArray.push(gkey);
    });
  }
  return gkeyArray;
}

function getSelectedPrivilegeStr() {
  var selectedPrivileges = $(".selectablePrivilege:checked");
  return getRolePrivilegeToUpdate(selectedPrivileges);
}

function getRolePrivilegeToUpdate(selectedPrivileges) {
  var gkeyStr = "";
  if (selectedPrivileges != null) {
    selectedPrivileges.each(function () {
      var selectedPrivilege = $(this);
      var gkey = selectedPrivilege.attr("gkey");
      if (gkey != null && gkey != "") {
        gkeyStr += gkeyStr != "" ? "," : "";
        gkeyStr += gkey;
      }
    });
  }
  return gkeyStr;
}

function cleanSearchParams() {
  $("#txtSearchUserDocumentNbr").val("");
  $("#txtSearchUserName").val("");
  $("#cbSearchRole").val("");
  $("#txtSearchFromDate").val("");
  $("#txtSearchToDate").val("");
  $("#cbSearchUserStatus").val("");
  $("#txtSearchUserDocumentNbr").focus();
  deleteDataTables();
}

function cleanAddEditFormParams() {
  $("#txtRoleId").val("");
  $("#txtRoleDesc").val("");
  deleteRolePrivilegeDataTable();
  $("#txtRoleGkey").val("");
}

function cleanUpdateCredentialParams() {
  $("#txtCredentialEmail").val("");
  $("#txtComexEmail").val("");
  $("#txtManagementEmail").val("");
  $("#txtCredentialPassword").val("");
  $("#ckUpdateEmail").prop("checked", true);
  $("#ckUpdateComexEmail").prop("checked", false);
  $("#ckUpdateManagementEmail").prop("checked", false);
  $("#ckUpdatePassword").prop("checked", true);
  $("#ckChangePassword").prop("checked", true);
  $("#txtComexEmail").prop("disabled", true);
  $("#txtManagementEmail").prop("disabled", true);
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function deleteRolePrivilegeDataTable() {
  $("#tbRolePrivilege").DataTable().clear();
  $("#tbRolePrivilege").DataTable().destroy();
}

function adjustDataTableColumns() {
  $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
}

function resultIsOk(result) {
  var status = result["status"];
  return status == "OK";
}

function isValidStr(str) {
  return !/[~`!#$%\^&*+=\\[\]\\';,/{}|\\":<>\?]/g.test(str);
}
